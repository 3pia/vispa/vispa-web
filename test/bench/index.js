const request = require("request-promise-native");
const { readFileSync } = require("fs");
const { dirname, basename } = require("path");
const EventEmitter = require("events");
const assert = require("assert");

const rando = base =>
  Math.random()
    .toString(base)
    .substr(2);
const rdx = (req, { qs = {}, ...opts } = {}) => {
  const ret = req.defaults({ qs, ...opts });
  ret.qs = qs;
  return ret;
};
const sleep = delay => new Promise(done => setTimeout(done, delay));
const b2h = (b, { t = 800, s = 1024, u = "B" } = {}) => {
  for (let i of " kMGTE")
    if (b < t) return b.toPrecision(3) + i + u;
    else b /= s;
};
const dirOp = op => (a, b) => {
  const c = {};
  for (let key in a) c[key] = op(a[key], b[key], key);
  return c;
};
const dirSum = dirOp((a, b) => (a || 0) + (b || 0));

const pageFiles = {};
for (const key of ["login", "index", "editor"])
  pageFiles[key] = readFileSync(`${key}.urls`)
    .toString()
    .split("\n")
    .map(x => x.trim())
    .filter(x => x);

class View extends EventEmitter {
  constructor(user) {
    super();
    this.user = user;
    this.vid = `${this.constructor.name}-${rando()}`;
    this.req = rdx(this.user.req, {
      qs: { _viewId: this.vid }
    });
    const topicPrefix = `extension.${this.vid}.socket.`;
    this.user.on("data", ({ topic, data }) => {
      if (topic.startsWith(topicPrefix))
        this.emit(topic.substr(topicPrefix.length), data);
    });
  }

  async ajax(api, qs, opts) {
    return this.req.get(`/ajax/${api}`, { qs, ...opts });
  }
}

class CodeEditor extends View {
  constructor(user, path) {
    super(user);
    this.path = path;
  }

  async open() {
    await this.user.static("editor");
    return await this.ajax("fs/getfile", {
      utf8: "true",
      path: this.path
    });
  }

  async list() {
    return this.ajax("fs/filelist", {
      filefilter: String.raw`\.(png|jpg|jpeg|bmp)$`,
      reverse: "true",
      path: dirname(this.path)
    });
  }

  async save(content) {
    return this.req.post("/ajax/fs/savefile", {
      form: { path: this.path, content, utf8: "true" }
    });
  }

  async execute(cmd = "python %1") {
    cmd = cmd.replace("%1", JSON.stringify(basename(this.path)));
    this.user.poll();
    await this.req.post("/extensions/codeeditor/execute", {
      form: { base: dirname(this.path), cmd }
    });
    await new Promise(done => this.once("done", done));
  }

  async run({ payload, iter = 0, output, delay, cmd }) {
    if (output) output = `${dirname(this.path)}/${output}`;
    await this.list();
    if (payload) await this.save(payload);
    await this.open();
    for (let i = iter; i--; ) {
      await this.execute(cmd);
      await this.list();
      if (output) {
        await this.user.thumb(output);
        await this.user.load(output);
      }
      await sleep(delay());
    }
  }
}

class Stats {
  fill(other) {
    for (let key in other) this.count(key, other[key] || 0);
  }
  count(key, num = 1) {
    this[key] = (this[key] || 0) + (num || 0);
  }
}

class User extends EventEmitter {
  constructor(username, { agentOptions, baseUrl, stats = new Stats() } = {}) {
    super();
    this.stats = stats;
    this.req = rdx(request, {
      baseUrl,
      jar: request.jar(),
      time: true,
      transform: (body, res) => {
        stats.fill({
          reqs: 1,
          recv: body ? body.length : 0
        });
        if (!res.request.uri.href.includes("poll"))
          stats.fill(res.timingPhases);
        let status = res.statusCode;
        if ((res.headers["content-type"] || "").includes("json")) {
          const json = JSON.parse(body);
          status = json.code;
          body = json.data;
        }
        this.srv =
          (res.headers["x-app-server"] || "").replace(/.+\//, "") || this.srv;
        const lg = msg => [
          "%s[%d@%s/%s] URL=%s time=%d req.body=%o len(res)=%d res=%o",
          msg,
          status,
          this.srv,
          this.hn,
          res.request.uri.href,
          res.timingPhases.total,
          res.request.body,
          res.body && res.body.length,
          res.headers["content-type"].includes("text") ? res.body : null
        ];
        if (status != 200) {
          console.error(...lg("ERR"));
          throw new Error(
            `failed[${status}@${this.srv}/${this.hn}] ${lg().slice(2)}`
          );
        } else if (res.timingPhases.total > 25e3) console.log(...lg("SLO"));
        // else console.log(...lg("RES"));
        return body;
      },
      agentOptions: {
        maxSockets: 6,
        keepAlive: true,
        maxFreeSockets: 1,
        ...agentOptions
      },
      pool: {}
    });
    this.username = username;
    this.bust = `${username}-${rando(16)}`;
    this.srv = "";
    this.hn = "";
    this.on("data", ({ topic, data: { workspaceId } = {} }) => {
      // console.log("data: %o", data);
      if (topic.startsWith("workspace.")) this.emit(`${topic}:${workspaceId}`);
    });
  }
  async static(key) {
    await Promise.all(
      pageFiles[key].map(url => this.req.get(`${url}?bust=${this.bust}`))
    );
  }

  async login(password, quick) {
    if (!quick) {
      await this.req.get("/login");
      await this.static("login");
    }
    await this.req.post("/ajax/login", {
      form: { username: this.username, password }
    });
    this.req.qs._windowId = rando(36);
  }

  poll() {
    let active = true;
    const prom = (async () => {
      while (active) {
        const res = await this.req.get("/bus/poll");
        this.stats.count("poll");
        if (res) for (const data of res) this.emit("data", JSON.parse(data));
      }
    })();
    this.pollStop = prom.stop = async () => {
      active = false;
      await prom;
      delete this.poll;
      delete this.pollStop;
    };
    this.poll = () => prom;
    return prom;
  }
  pollStop() {}

  async workspace(wid, password) {
    const wsInfo = (await this.req.get("/ajax/getworkspacedata"))[wid];
    if (!wsInfo) throw new Error(`no such workspace: ${wid}`);
    if (wsInfo.connection_status !== "connected") {
      await this.req.post("/ajax/connectworkspace", {
        form: { wid, password }
      });
      this.poll();
      await new Promise(done => this.once(`workspace.connected:${wid}`, done));
    }
    this.req.qs._workspaceId = wid;
    this.hn = (await this.load("/etc/hostname")).trim();
    process.stdout.write(
      `connected: ${this.username.padEnd(17)} ${this.srv.padEnd(
        8
      )} ${this.hn.padEnd(11)}`.padEnd(79) + "\n"
    );
  }

  async thumb(path, height = 100, width = height) {
    await this.req.get("/fs/thumbnail", {
      qs: { path, height, width, _mtime: Date.now() }
    });
  }

  async load(path) {
    return await this.req.get("/fs/getfile", {
      qs: { path, _mtime: Date.now() }
    });
  }

  editor(path) {
    return new CodeEditor(this, path);
  }

  async logout() {
    const wid = this.req.qs._workspaceId;
    if (wid !== undefined) {
      await this.req.post("/ajax/disconnectworkspace", {
        form: { wid }
      });
      delete this.req.qs._workspaceId;
    }
    await this.pollStop();
    await this.req.get("logout");
    delete this.req.qs._windowId;
  }

  async runEdit(password, { wsId, path, ...opts }) {
    await this.login(password);
    await this.req.get("");
    await this.static("index");
    // this.poll();
    await this.workspace(wsId, password);
    await this.editor(path).run(opts);
    await this.logout();
    return this;
  }

  async runStorm(password, { wsId, ...opts } = {}) {
    await this.login(password, true);
    await this.workspace(wsId, password);
    await this.pollStop();
    await this.storm(opts);
    await this.logout();
    return this;
  }

  async storm({ iter = 1e4, conc = 5, delay } = {}) {
    const edit = this.editor("$HOME/test.py");
    const q = new Set();
    for (let i = iter; i--; ) {
      while (q.size < conc) {
        const prom = edit.list();
        prom.finally(() => q.delete(prom));
        q.add(prom);
      }
      await Promise.race(Array.from(q));
      if (delay) await sleep(delay());
    }
    await Promise.all(Array.from(q));
  }
}

const loop = async ({
  conc = 10,
  delay = () => 10e3 * (1 + 2 * Math.random()),
  total = 10000,
  genUsername = () => "testuser",
  password,
  statusPassword,
  wsId,
  baseUrl,
  storm,
  edit
} = {}) => {
  if (!password) throw new Error(`"password" is missing`);
  if (!baseUrl) throw new Error(`"baseUrl" is missing`);
  if (!wsId) throw new Error(`"wsId" is missing`);
  const users = new Set();
  const stats = new Stats();
  let bad = 0;
  for (let stop, i = 0, d = 0; i < total || users.size; ) {
    const username = genUsername(i);
    if (
      i < total &&
      !stop &&
      users.size < conc &&
      d < Date.now() &&
      !Array.from(users).some(user => user.username === username)
    ) {
      d = Date.now() + delay();
      i++;
      const user = new User(username, {
        baseUrl,
        stats
      });
      const prom = user[storm ? "runStorm" : "runEdit"](password, {
        wsId,
        ...(storm || edit)
      })
        .catch(async err => {
          console.error("[%d] failed: %s", i, err);
          bad++;
          if (statusPassword) {
            const status = await user.req.post("status", {
              form: { password: statusPassword }
            });
            await new Promise((done, fail) =>
              writeFile(`bad/${i}.html`, status, err =>
                err ? fail(err) : done()
              )
            );
          }
        })
        .finally(() => users.delete(user));
      users.add(user);
    }

    process.stdout.write(
      `users=${users.size}/${conc}@${i}/${total} polls=${stats.poll} reqs=${
        stats.reqs
      } recv=${b2h(stats.recv)} firstByte=${(
        stats.firstByte / stats.reqs
      ).toFixed(0)}ms bad=${bad}`.padEnd(79) + "\r"
    );

    if (stop && !users.size) break;

    await sleep(100);

    for (let c; (c = process.stdin.read(1)); ) {
      switch (c + "") {
        case "n":
          d = 0;
          break;
        case "+":
          conc++;
          break;
        case "-":
          conc && conc--;
          break;
        case "0":
          conc = 0;
          break;
        case "q":
          stop = true;
          break;
      }
    }
  }
  process.stdout.write("\n");
  process.stdin.pause();
};

loop({
  delay: () => 15 * (1 + Math.random()),
  conc: 15,
  total: 500,
  // storm: { conc: 1, iter: 1 },
  // storm: { conc: 5 },
  edit: {
    path: "$HOME/test.py",
    payload: readFileSync("payload.py"),
    iter: 50,
    delay: () => 1e3 * (1 + 15 * Math.random()),
    output: "test.png"
  }
});
