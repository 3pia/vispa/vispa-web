print("starting payload")

import numpy as np
import matplotlib.pyplot as plt

xa = np.random.normal(0, 1, 50)
ya = np.random.normal(0, 3, 50)

plt.scatter(xa, ya, marker=".", color="red")
plt.scatter(ya, xa, marker=".", color="black")
plt.savefig("test.png")

print("finished payload")
