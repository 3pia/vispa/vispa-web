#!/usr/bin/env python
# -*- coding: utf-8 -*-


from setuptools import setup, find_packages, findall

# http://stackoverflow.com/a/24517154
metadata = {}
execfile("vispa/version.py", metadata)

setup(
    name="vispa",
    version=metadata['__version__'],
    description="VISPA - Integrated Development Environment for Physicists",
    author="VISPA Project",
    author_email="vispa@lists.rwth-aachen.de",
    url="http://vispa.physik.rwth-aachen.de/",
    license="GNU GPL v2",
    packages=find_packages(),
    package_data={
        "vispa": [
            path.split("/", 1)[-1]
            for path in findall("vispa")
            if "/static/" in path
            or "/templates/" in path
            or "/models/alembic/versions/" in path
        ]
    },
    scripts=findall("bin"),
    install_requires=[
        "sqlalchemy >= 0.9.0",
        "mako",
        "cherrypy >= 10.1",
        "paramiko",
        "rpyc >= 4",
        "alembic >= 0.7.3",  # for Operations.batch_alter_table
        "passlib",
        "ws4py",
        "ldap3"
    ],
    extras_require={"doc": ["sphinx", "sphinx-bootstrap-theme"]},
    classifiers=[
        "Development Status :: 5 - Production/Stable",
        "Environment :: Web Environment",
        "Framework :: CherryPy",
        "Intended Audience :: Developers",
        "Intended Audience :: Education",
        "Intended Audience :: Information Technology",
        "Intended Audience :: Science/Research",
        "Intended Audience :: System Administrators",
        "License :: OSI Approved :: GNU General Public License v2 (GPLv2)",
        "Natural Language :: English",
        "Operating System :: MacOS",
        "Operating System :: POSIX",
        "Programming Language :: JavaScript",
        "Programming Language :: Python",
        "Topic :: Internet",
        "Topic :: Scientific/Engineering",
        "Topic :: Software Development",
    ]
)
