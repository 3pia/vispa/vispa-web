# -*- coding: utf-8 -*-

"""
Custom JavaScript domain that adds features used by the VISPA client.
"""


from sphinx import addnodes
from sphinx.domains import Domain, ObjType
from sphinx.locale import l_, _
from sphinx.directives import ObjectDescription
from sphinx.roles import XRefRole
from sphinx.util.nodes import make_refnode
from sphinx.util.docfields import Field, GroupedField, TypedField
from docutils import nodes

# make this compatible with sphinx pre 1.4.5
# https://github.com/sphinx-doc/sphinx/commit/2c396601bf87d0a035a151a5e8fd25b9f702ebe5
def ref_context(env):
    try:
        return env.ref_context
    except:
        return env.temp_data

class NumberedField(TypedField):

    list_type = nodes.enumerated_list


class JSObject(ObjectDescription):

    display_prefix = None

    has_arguments = False

    def add_target_and_index(self, name_obj, sig, signode):
        objectname = self.options.get("object", ref_context(self.env).get("js:object"))
        fullname   = name_obj[0]
        if fullname not in self.state.document.ids:
            signode["names"].append(fullname)
            signode["ids"].append(fullname.replace("$", "_S_"))
            signode["first"] = not self.names
            self.state.document.note_explicit_target(signode)
            objects = self.env.domaindata["js"]["objects"]
            if fullname in objects:
                self.state_machine.reporter.warning(
                    "duplicate object description of %s, " % fullname + "other instance in " +
                    self.env.doc2path(objects[fullname][0]), line=self.lineno)
            objects[fullname] = self.env.docname, self.objtype

        indextext = self.get_index_text(objectname, name_obj)
        if indextext:
            self.indexnode["entries"].append(("single", indextext, fullname.replace("$", "_S_"), ""))

    def handle_signature(self, sig, signode):
        sig = sig.strip()
        if "(" in sig and sig[-1] == ")":
            prefix, arglist = sig.split("(", 1)
            prefix = prefix.strip()
            arglist = arglist[:-1].strip()
        else:
            prefix = sig
            arglist = None
        if "." in prefix:
            nameprefix, name = prefix.rsplit(".", 1)
        else:
            nameprefix = None
            name = prefix

        objectname = ref_context(self.env).get("js:object")
        if nameprefix:
            if objectname:
                nameprefix = objectname + "." + nameprefix
            fullname = nameprefix + "." + name
        elif objectname:
            fullname = objectname + "." + name
        else:
            # just a function or constructor
            objectname = ""
            fullname = name

        signode["object"] = objectname
        signode["fullname"] = fullname

        sigprefix = self.get_signature_prefix()
        if sigprefix:
            signode += addnodes.desc_annotation(sigprefix, sigprefix)

        if self.display_prefix:
            signode += addnodes.desc_annotation(self.display_prefix, self.display_prefix)

        if nameprefix:
            signode += addnodes.desc_addname(nameprefix + ".", nameprefix + ".")

        signode += addnodes.desc_name(name, name)

        if self.has_arguments:
            if not arglist:
                signode += addnodes.desc_parameterlist()
            else:
                self.parse_arglist(signode, arglist)
        return fullname, nameprefix

    def parse_arglist(self, signode, arglist):
        possig, _, optsig = arglist.replace(" ", "").partition("[")
        if optsig:
            optsig = "[" + optsig.replace(",", "")

        # create the params list
        paramlist = addnodes.desc_parameterlist()
        signode += paramlist

        # parse positionals
        for pos in possig.split(","):
            node = addnodes.desc_parameter(pos, pos)
            paramlist += node

        # parse optionals
        opt    = ""
        parent = paramlist
        def add(opt):
            node = addnodes.desc_optional()
            node.append(addnodes.desc_parameter(opt, opt))
            parent.append(node)
            return node
        for c in optsig:
            if c == "[":
                if opt:
                    parent = add(opt)
                    opt = ""
            elif c == "]":
                if opt:
                    add(opt)
                    opt = ""
                else:
                    parent = parent.parent
            else:
                opt += c

    def get_signature_prefix(self):
        return ""

    def get_index_text(self, objectname, name_obj):
        return ""


class JSAttribute(JSObject):

    doc_field_types = [
        Field("type",
            label   = l_("Type"),
            has_arg = False,
            names   = ("type",)
        )
    ]

    def get_signatures(self):
        return [self.arguments[0].partition(" ")[0]]

    def get_signature_prefix(self):
        return self.arguments[0].partition(" ")[2]


class JSCallable(JSObject):

    has_arguments = True

    doc_field_types = [
        TypedField("arguments",
            label        = l_("Arguments"),
            names        = ("argument", "arg", "parameter", "param"),
            typerolename = "func",
            typenames    = ("paramtype", "type")
        ),
        GroupedField("errors",
            label        = l_("Throws"),
            names        = ("throws",),
            rolename     = "err",
            can_collapse = True
        ),
        NumberedField("callback",
            label = l_("Callback args"),
            names = ("callback", "cb")
        )
    ]

    def get_signatures(self):
        return [self.arguments[0].partition(")")[0] + ")"]

    def get_signature_prefix(self):
        return self.arguments[0].partition(")")[2][1:]


class JSFunction(JSCallable):

    doc_field_types = JSCallable.doc_field_types[:] + [
        Field("returnvalue",
            label   = l_("Returns"),
            has_arg = False,
            names   = ("returns", "return")
        ),
        Field("returntype",
            label   = l_("Return type"),
            has_arg = False,
            names   = ("rtype",)
        )
    ]

    def get_index_text(self, objectname, name_obj):
        return _("%s()") % name_obj[0]


class JSClassConstructor(JSCallable):

    display_prefix = "class "

    doc_field_types = JSCallable.doc_field_types[:] + [
        GroupedField("extends",
            label        = l_("Extends"),
            names        = ("extends", "extend"),
            can_collapse = True
        ),
        GroupedField("mixins",
            label        = l_("Mixins"),
            names        = ("mixins", "mixin"),
            can_collapse = True
        )
    ]

    def get_index_text(self, objectname, name_obj):
        name, obj = name_obj
        return _("%s() (class)") % name


class JSMixinConstructor(JSCallable):

    display_prefix = "mixin "

    doc_field_types = JSCallable.doc_field_types[:] + [
        GroupedField("extends",
            label        = l_("Extends"),
            names        = ("extends", "extend"),
            can_collapse = True
        )
    ]

    def get_index_text(self, objectname, name_obj):
        name, obj = name_obj
        return _("%s() (mixin)") % name


class JSXRefRole(XRefRole):
    def process_link(self, env, refnode, has_explicit_title, title, target):
        # basically what sphinx.domains.python.PyXRefRole does
        refnode["js:object"] = ref_context(env).get("js:object")
        if not has_explicit_title:
            title = title.lstrip(".")
            target = target.lstrip("~")
            if title[0:1] == "~":
                title = title[1:]
                dot = title.rfind(".")
                if dot != -1:
                    title = title[dot+1:]
        if target[0:1] == ".":
            target = target[1:]
            refnode["refspecific"] = True
        return title, target


class JavaScriptDomain(Domain):

    name  = "js"
    label = "JavaScript"

    object_types = {
        "function" : ObjType(l_("function"),  "func"),
        "class"    : ObjType(l_("class"),     "class"),
        "mixin"    : ObjType(l_("mixin"), "mixin"),
        "attribute": ObjType(l_("attribute"), "attr")
    }

    directives = {
        "function" : JSFunction,
        "class"    : JSClassConstructor,
        "mixin"    : JSMixinConstructor,
        "attribute": JSAttribute
    }

    roles = {
        "func" : JSXRefRole(fix_parens=True),
        "class": JSXRefRole(fix_parens=True),
        "mixin": JSXRefRole(fix_parens=True),
        "attr" : JSXRefRole()
    }

    initial_data = {
        "objects": {} # fullname -> docname, objtype
    }

    def clear_doc(self, docname):
        for fullname, (fn, _l) in list(self.data["objects"].items()):
            if fn == docname:
                del self.data["objects"][fullname]

    def merge_domaindata(self, docnames, otherdata):
        for fullname, (fn, objtype) in otherdata["objects"].items():
            if fn in docnames:
                self.data["objects"][fullname] = (fn, objtype)

    def find_obj(self, env, obj, name, typ, searchorder=0):
        if name[-2:] == "()":
            name = name[:-2]
        objects = self.data["objects"]
        newname = None
        if searchorder == 1:
            if obj and obj + "." + name in objects:
                newname = obj + "." + name
            else:
                newname = name
        else:
            if name in objects:
                newname = name
            elif obj and obj + "." + name in objects:
                newname = obj + "." + name
        return newname, objects.get(newname)

    def resolve_xref(self, env, fromdocname, builder, typ, target, node, contnode):
        objectname = node.get("js:object")
        searchorder = node.hasattr("refspecific") and 1 or 0
        name, obj = self.find_obj(env, objectname, target, typ, searchorder)
        if not obj:
            return None
        return make_refnode(builder, fromdocname, obj[0], name.replace("$", "_S_"), contnode, name)

    def resolve_any_xref(self, env, fromdocname, builder, target, node, contnode):
        objectname = node.get("js:object")
        name, obj = self.find_obj(env, objectname, target, None, 1)
        if not obj:
            return []
        return [(
            "js:" + self.role_for_objtype(obj[1]),
            make_refnode(builder, fromdocname, obj[0], name.replace("$", "_S_"), contnode, name)
        )]

    def get_objects(self):
        for refname, (docname, type) in list(self.data["objects"].items()):
            yield refname, refname, type, docname, \
                refname.replace("$", "_S_"), 1


def setup(app):
    app.domains["js"] = JavaScriptDomain
    app._init_i18n()
    app._init_env(freshenv=True)
