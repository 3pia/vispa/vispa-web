****************
User Guide
****************

User Management
===============

VISPA's user management is designed for collaborative work on different scales. If provides content
management as well as a server-side permission system.

Groups
------
First of all, it is useful to cluster users together to treat them as one unit. This is done by
groups, which are just collections of users as well as other groups. Users can join and leave groups
following three different stages of privacy:

* public:    open for everyone
* protected: protected by password
* private:   new users must be confirmed

Groups are organized by selected users, called managers, who can add and remove members as well as
changing the groups name, privacy, password etc.

Projects
--------
Projects are the points, where users and groups are connected to content. Therefore, a project
consists of several users and groups as well as a collection of items. In order to control the
access to the content, a project assigns permissions via roles to each member (see below). Similar
to groups, projects are organized by selected users, called managers.

Workgroups
----------
Workgroups are a lightweight version of projects. They have also content in the form of items but
only users are members, no groups. Furthermore, there is no explicit handling of permissions because
every user has full access to the workgroups content. There are also managers, which organize the
membership of the users.

Roles and Permissions
---------------------
Permissions are descriptive rights, which allow certain actions of the user. They are collected into
roles, which users and groups can fulfill in context of a project. Permissions are abstract rights
like 'read items of project' and not concrete ones like 'read item X of project Y'. Thus,
permissions are usable in every project but get their full context not before being assigned to a
certain user or group inside one.

Example
-------
In order to bring light to the user managements concept, here is an example of how it can be used.
Let's assume there is a lecture 'theoretical physics 101', which wants to use VISPA. First of all,
we look at the people, who belong to the lecture. There we have Professor Einstein, several
graduates for the tutorials and many students (amongst others Peter, Paul and Mary). Thus, to make
it simpler, we have two groups:
* 'theoretical physics 101 - tutors'
* 'theoretical physics 101 - students'
which consist of the corresponding people. The tutors group should be private, because only a few 
dedicated persons belong to them while the students group could be public or protected by a
password, which is given to the students during the first lecture. Furthermore, we need a project:
'theoretical physics 101'. Of course, the lecture has some content e.g. slides or homework sheets
and solutions, which
can be added to the project as items. So far, we have:

* 'theoretical physics 101'

   * members:

      * user: Professor Einstein
      * group: 'theoretical physics 101 - tutors'

         * ...

      * group: 'theoretical physics 101 - students'

         * Peter
         * Paul
         * Mary
         * ...

   * content:

      * lecture slides
      * homework sheets
      * homework solutions

Now, we need to control, who can do what. Therefore, we use different roles. These could be:

* role 1

   * read slides
   * read sheets

* role 2

   * read slides
   * read sheets
   * read solutions

* role 3

   * read slides
   * read sheets
   * read solutions
   * write solution

Thus, we can assign the roles as follows:

* Professor Einstein:

   * role 3

* 'theoretical physics 101 - tutors':

   * role 2

* 'theoretical physics 101 - students':

   * role 1

Thus, the access to the different resources is controlled. Additionally, Peter, Paul and Mary want
to work on their homework together. Therefore, they can create their own workgroup, which consists
only of them and can be utilized e.g. to exchange some files.

Global permissions
------------------
In addition to a concept as in the example above, VISPA is capable of managing global permissions.
This is done with a global project, by default called 'VISPA'. A global Permission is e.g. the right
to use a certain extension.
