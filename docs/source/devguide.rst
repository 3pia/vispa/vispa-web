***************
Developer Guide
***************

.. toctree::
   :maxdepth: 2

   js/extensions


Extensions
==========

Extensions add functionality to the the VISPA Platform.
They provide new views in the browser and new functionality on the server.

Overview
--------

Extensions contain code to be executed on the client browser, HTML, CSS and Javascript, on the VISPA Server, Python, and on the workspaces, also Python.


Tutorial
--------

TODO


Server
------

The main entry point is the AbstractExtension class in an extension module.
Extension modules are either activated in the config file, or by its name: vispa_<name>.

Signals
-------

In VISPA important events are communicated by messages.
Events concerning the webserver are communicated using the CherryPy bus.
VISPA internal events are send through the VISPA bus.
For example, each time a user is logged in, an event with the topic "user.login" is sent.
The only parameter is the model.User object: 

    vispa.publish("user.login", user_object)
    
To receive the message, you need to subscribe to the topic:

    vispa.subscribe("user.login", login_callback)
    
Where login_callback is a function which is called with the parameters provided to the publish call.
In the case of "user.login", the user object:

    def login_callback(user):
        print "a new user logged in:", user.name
        
Currently the following topics are published by VISPA:

exit, stop, bus.session_added, bus.session_removed, bus.all_user_sessions_removed


Client
------

Folderstructure:

* myextension/

   * __init__.py
   * controller.py
   * extension.py
   * myextension.ini
   * workspace/

      * ...

   * static/

      * js/

         * myextension.js
         * myfile.js
         * ...

      * css/

         * myextension.css/less

      * templates/

         * ...

RequireJs usage:
The namespace is "vispa/myextension/myfile".
Vendor modules are simply identified by their name, e.g. "jquery" or "ace".
There is no need to register the extension yourself.


Workspace
---------

TODO


User Management
---------------

The concept of VISPA's user management is explained in the User Guide. VISPA has an AJAX controller,
which gives a simple and convenient access to the user management. The general syntax is:

.. code-block:: js

   ExtensionView.METHOD("/ajax/um/some_usermanagement_function", ...)

Thus, it is a simple AJAX request, e.g.:

.. code-block:: js

   ExtensionView.GET("/ajax/um/user_get_groups", {}, ...)

It is recommended to use the UM controller also on server side python code, because it automatically
checks dependencies and permissions. Such a call looks like:

.. code-block:: py

   Server.controller.ajax.um.some_usermanagement_function(...)

For a full documentation of the user management controller look into the class reference.

The user management delivers a cherrypy tool, which can be used to check for global permissions. For
further information look into the class reference.

The user management also delivers a simple interface for automatically setting up permissions and
roles the extension needs. Therefore, the myextension.ini can be used (look above for folder
structure). The myextension.ini is loaded an server ramp up and the extensions can define a set of
permission it needs as well as an assignment of the permissions to three default roles. The
myextension.ini itself is structured as follows:

.. code-block:: ini

   [usermanagement]
   # list of permissions that must exist
   permissions = ["myextension.permission1", "myextension.permission2", "myextension.permission3"]
   # assignment of permissions to default roles. permissions must exists (use line above)
   role_1_permissions = ["myextension.permission1"]
   role_2_permissions = ["myextension.permission1", "myextension.permission2"]
   role_3_permissions = ["myextension.permission1", "myextension.permission2", "myextension.permission3"]

The prefix "myextensions" for the permissions is a namespace which should be used by convention.


Create Database Migration Script
================================

alembic -c <path-to-vispa.ini> revision -m "commit message" --autogenerate
