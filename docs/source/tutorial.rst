The starting point for each extenion is a class extending AbstractExtension:

.. autoclass:: vispa.server.AbstractExtension
   :members:
   :undoc-members:
   
A minimal example looks like this:

.. code-block:: python

   from vispa.server import AbstractExtension
   
   class MyExtension(AbstractExtension):
      
      def name(self):
         return "myext"
         
      def dependencies(self):
        return []

      def setup(self):
        pass   

Directories from which extensions are loaded:
   - vispa/extensions
   - $(var_dir)/extensions
   - global packages starting with `vispa_`

In our example, the code above could be placed in a `__init__.py` file in a global package/directory named `vispa_myext-1.0`.   
By default VISPA loads all extensions it finds, but extensions listed in `vispa.ini`, section `extensions`, option `ignore`
will be ignored:

.. code-block:: ini
   
   [extensions]
   ignore = myext 
   
   
Controller
----------

AbstractController

.. autoclass:: vispa.controller.AbstractController
   :members:
   :undoc-members:
   
add_controller

every exposed function is visible

parameters need to be present or 404, or default

cherrypy.tools.ajax()

return values, sring, objects

available cherrypy.request variables


Workspace
---------

self.get_workspace_instance()

Client
------

TODO
