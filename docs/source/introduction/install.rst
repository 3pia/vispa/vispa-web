************
Installation
************

:ref:`prerequisites`

:ref:`stableversions`

:ref:`developmentversions`

.. _prerequisites:

Prerequisites
=============

.. _stableversions:

Download Stable Versions 
========================

Using `pip` or `easy_install`
-----------------------------

Using pip::

    $ pip install vispa

or with easy_install::

    $ easy_install vispa

It is recommended to use `pip` instead of `easy_install`.
If you want to download and install VISPA for yourself proceed to the 
next instructions depending on your platform. 

Unix/Mac
--------

You may download the most current version from `PyPI <https://pypi.python.org/pypi/vispa>`_  

For other releases, browse our
`download index <https://forge.physik.rwth-aachen.de/projects/vispa-web/files>`_.

* Unzip/untar the files
* Enter the directory created by the file extraction.
* Type "python setup.py install" to install the VISPA module


Windows
-------

TODO


Next Steps
==========

TODO

.. _developmentversions:

Development versions
====================

TODO
