# -*- coding: utf-8 -*-

"""
Sphinx documentation steering file.
"""

import sys
import os

# adjust the path to find vispa
sys.path.insert(0, os.path.abspath("../.."))
sys.path.insert(0, os.path.abspath("../extensions"))


#
# project setup
#

project   = "VISPA"
version   = "2.0"
release   = version + ".0"
author    = "The VISPA project, vispa@lists.rwth-aachen.de"
copyright = "2016, Prof. M. Erdmann, III. Physics Institute A, RWTH Aachen University"


#
# documentation setup
#

master_doc         = "index"
templates_path     = ["_templates"]
html_theme         = "sphinx_rtd_theme"
html_theme_options = {}
html_title         = project + " v" + release
html_short_title   = html_title
html_logo          = "_static/logo.png"
html_favicon       = "_static/favicon.ico"
html_static_path   = ["_static"]


#
# extensions
#

extensions = ["sphinx.ext.autodoc", "customjsdomain"]


#
# sphinx hooks
#

def setup(app):
    # monkey path the cherrypy toolbox
    app.connect("builder-inited", patch_cherrypy_toolbox)

    # preprocess javascript files
    app.connect("builder-inited", preprocess_js_files)


def patch_cherrypy_toolbox(app):
    import cherrypy
    def cptoolbox_getattr(self, attr):
      def member(*args, **kwargs):
        def wrapper(fn):
          return fn
        return wrapper
      return member
    cherrypy.tools.__class__.__getattr__ = cptoolbox_getattr


def preprocess_js_files(app):
    import subprocess

    thisfile = os.path.abspath(__file__)
    docsdir = os.path.normpath(os.path.join(thisfile, "../.."))

    cmd = "python jspreprocess -s ../vispa/static/js/ -t source/js && " \
          "sphinx-apidoc -o source/py ../vispa -s rst ../vispa/workspace_loader.py"
    subprocess.call(cmd, cwd=docsdir, shell=True)
