****************
Deployment Guide
****************


Optimized Static Resource
=========================

VISPA uses require.js to load JavaScript, CSS and Template files. In normal or development mode each file is loaded dynamically using the AMD loader. Since the VISPA frontend consists of many files, this i quite some overhead.
Require.js provides the means to merge and minify all the files using the r.js optimizer.
The basic idea is to provide the optimized files instead of the original files, so not path or other flags need to be changed.

.. note::

   The optimizer currently requires that all extensions are placed in the vispa/extensions folder. Symlinks are possible though.

The following steps are required to use optimized builds in VISPA:

#. Install the npm packages: 

.. code-block:: bash

   $ npm install

#. Add all extensions that shall be optimized in build.js. Add another module, like the filebrowser extension:

.. code-block:: js

    {
         name: 'extensions/file/static/js/extension',
         exclude: ['vispa/config', 'vispa/vispa']
    },

#. Run the optimizer. All static files are copied to the build folder.

.. code-block:: bash

   $ node_modules/.bin/r.js -o build.js

#. Set the build_path variable in the [web] section in the vispa.ini. Use a relative path to the vispa package folder or an absolute path. 

.. code-block:: ini

   [web]
   build_path = ../build


User Management
===============

The concept of VISPA's user management is explained in the User Guide. VISPA is able to
automatically setup the user management when the server is ramped up. It is highly recommended to
use the auto setup in order to use the full capability of the user management. The auto setup can be
configured via the vispa.ini. The vispa.ini.sample contains a full example of how such a setup
should look:

.. code-block:: ini

   [usermanagement]
   # do auto setup?
   autosetup = True
   # the role/permission setup of the extensions can be accepted or not
   accept_extensions = True
   # name of the global project
   global_project = VISPA
   # default user and guest group
   user_group = default_user
   guest_group = default_guest
   # three roles, e.g. with ascending rank
   role_1 = Student
   role_2 = Tutor
   role_3 = Manager
   # list of permissions that must exist
   permissions = ["project.read_items", "project.create_items", "project.edit_items", "project.delete_items"]
   # assignment of permissions to default roles. permissions must exists (use line above)
   role_1_permissions = ["project.read_items"]
   role_2_permissions = ["project.read_items", "project.create_items"]
   role_3_permissions = ["project.read_items", "project.create_items", "project.edit_items", "project.delete_items"]
   # assignment of roles to user and guest group
   user_group_roles = [1]
   guest_group_roles = []

The comments explain the different options. For a successful setup it is important, that all
options are implemented. Otherwise, the automatic setup fails and nothing is committed to the
database. It is remarkable, that the permissions "project.*_items" are hard coded into the user
management controller and needed for the access to the items of a project.


Upgrade the Database
====================

VISPA uses alembic to perform database migrations.
Per default VISPA upgrades automatically to the latest version of the database schema (head).
In a production setup this may not be desirable. To disable automatic database migration set
`use_alembic` option in the `alembic` section in the vispa.ini file to `False`:

.. code-block:: ini

   [alembic]
   use_alembic = False

NOTE: In case of an SQLite database, alembic is never used by VISPA, because SQLite does not support
all necessary database migration steps.

Manual Upgrade
--------------

To manually upgrade the database schema, you also need to set the `sqlalchemy_url` option in the
`database` section:

.. code-block:: ini

   [database]
   sqlalchemy.url = sqlite:///var/db/vispa.db

   [alembic]
   use_alembic = False
   script_location = vispa/models/alembic

Now you can use the alembic tool to migrate the database:
   
.. code-block:: bash

   $ alembic -c conf/vispa.ini upgrade head

NOTE: The manual upgrade can also be performed on SQLite database, but it can cause errors due to
the mentioned limitations of the SQLite dialect. In such a case, the database must deleted and
recreated without alembic, as done by VISPA.
