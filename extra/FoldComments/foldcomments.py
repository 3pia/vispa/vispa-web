import sublime, sublime_plugin

def find_fold_regions(view):
    fold_regions = []
    regions = view.find_by_selector("comment")
    for region in regions:
        lines = view.lines(region)
        if len(lines) < 3:
            continue
        startpos = view.find("\/\*\*", lines[0].a)
        if not startpos:
            continue
        endpos = view.find("\*\/", lines[-1].a)
        if not endpos:
            continue
        miss = False
        for line in lines[1:-1]:
            midpos = view.find("\*", line.a)
            if not midpos:
                miss = True
                break
        if miss:
            continue
        fold_regions.append(sublime.Region(startpos.a+3, endpos.b-2))
    return fold_regions

class FoldCommentsCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        for region in find_fold_regions(self.view):
            self.view.fold(region)

class UnfoldCommentsCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        for region in find_fold_regions(self.view):
            self.view.unfold(region)
