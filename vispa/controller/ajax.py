# -*- coding: utf-8 -*-

import cherrypy
from vispa.controller import AbstractController
from vispa.controller.filesystem import FSAjaxController
from vispa.controller.usermanagement import UMAjaxController
from vispa.models.user import User
from vispa.models.group import Group, Group_User_Assoc
from vispa.models.workspace import Workspace
from vispa.models.jsondata import JSONData
from vispa import AjaxException
import vispa.workspace
import logging
import json
import os
import paramiko
import json as JSON

logger = logging.getLogger(__name__)


class AjaxController(AbstractController):

    def __init__(self, root):
        super(AjaxController, self).__init__(mount_static=False)

        self._root = root
        self.fs    = FSAjaxController()
        self.um    = UMAjaxController()

    # authentication
    @cherrypy.expose
    @cherrypy.tools.user(on=False)
    @cherrypy.tools.method(accept="POST")
    def login(self, username, password):
        user = User.login(cherrypy.request.db, username, password)

        cherrypy.session["user_id"]   = user.id
        cherrypy.session["user_name"] = user.name

        self.autoconnectworkspace(user, password)

        return { "userId": user.id, "sessionId": cherrypy.session.id }

    @cherrypy.expose
    @cherrypy.tools.user(on=False)
    @cherrypy.tools.method(accept="POST")
    def register(self, username, email):
        user = User.register(cherrypy.request.db, username, email)
        session = cherrypy.request.db

        user_group = vispa.config("usermanagement", "user_group", None)
        if user_group:
            user_group = Group.get_or_create_by_name(session, user_group)
            user_group.add_user(user, Group_User_Assoc.CONFIRMED)

        hash = None
        if vispa.config("web", "registration.autoactive", True):
            hash = user.hash

        elif vispa.config("web", "registration.sendmail", False):
            User.send_registration_mail(user.name, user.email, user.hash)

        return { "hash": hash }

    @cherrypy.expose
    @cherrypy.tools.user(on=False)
    @cherrypy.tools.method(accept="POST")
    def forgotpassword(self, username):
        if vispa.config("web", "forgot.use", False) and not cherrypy.session.get("is_guest", False):
            User.forgot_password(cherrypy.request.db, username)
        else:
            raise AjaxException("Password restoring not allowed!")

    @cherrypy.expose
    @cherrypy.tools.user(on=False)
    @cherrypy.tools.method(accept="POST")
    def setpassword(self, hash, password):
        user = User.set_password(cherrypy.request.db, hash, password)
        if user.active():
            cherrypy.session["user_id"] = unicode(user.id)
            cherrypy.session["user_name"] = user.name
            self.autoconnectworkspace(user, password)

    # json
    @cherrypy.expose
    @cherrypy.tools.method(accept="POST")
    def setjson(self, key, value, wid=None):
        db = cherrypy.request.db
        user_id = self.get("user_id")
        wid = int(wid) if wid else None
        JSONData.set_value(db,
            user_id      =user_id,
            key          =key,
            workspace_id =wid,
            value        =value
        )

    @cherrypy.expose
    @cherrypy.tools.method(accept="POST")
    def getjson(self, key, wid=None):
        db = cherrypy.request.db
        user_id = self.get("user_id")
        wid = int(wid) if wid else None
        return JSON.dumps(JSONData.get_value(db,
            user_id      =user_id,
            key          =key,
            workspace_id =wid
        ))

    # workspace
    @cherrypy.expose
    @cherrypy.tools.method(accept="POST")
    def addworkspace(self, name, host, login, key=None, cmd=None):
        if not vispa.config("workspace", "add", True):
            raise AjaxException("No permission to add a new Workspace!")

        db = self.get("db")
        user_id = self.get("user_id")

        # does the user already have a workspace with that name?
        workspaces = Workspace.get_user_workspaces(db, user_id)
        for workspace in workspaces:
            # case insensitive
            if name.lower() == workspace.name.lower():
                raise AjaxException("Workspace '%s' already in use!" % name)

        # add the workspace
        workspace = Workspace.add(db, user_id, name, host, login, key=key,
                                  command=cmd)
        return self._root.workspace_data(workspace)

    @cherrypy.expose
    @cherrypy.tools.method(accept="POST")
    def editworkspace(self, wid, name=None, host=None, login=None, key=None, cmd=None):
        if not vispa.config("workspace", "add", True):
            raise AjaxException("No permission to edit a Workspace!")

        db = cherrypy.request.db

        # can the user edit the workspace?
        workspace = Workspace.get_by_id(db, wid)
        if not isinstance(workspace, Workspace):
            raise AjaxException("Unknown Workspace!")
        if not workspace.can_edit(self.get("user_id")):
            raise AjaxException("No permission to edit this Workspace!")
        if workspace.user_id is None:
            raise AjaxException("No permission to edit this Workspace!")

        # edit it
        workspace.name    = name  or workspace.name
        workspace.host    = host  or workspace.host
        workspace.login   = login or workspace.login
        # these can be empty
        workspace.key     = workspace.key     if key   is None else key
        workspace.command = workspace.command if cmd   is None else cmd
        return self._root.workspace_data(workspace)

    @cherrypy.expose
    @cherrypy.tools.method(accept="POST")
    def deleteworkspace(self, wid):
        if not vispa.config("workspace", "add", True):
            raise AjaxException("No permission to delete a Workspace!")

        db = cherrypy.request.db

        # can the user edit the workspace?
        workspace = Workspace.get_by_id(db, wid)
        if not isinstance(workspace, Workspace):
            raise AjaxException("Unknown Workspace!")
        if not workspace.can_edit(self.get("user_id")):
            raise AjaxException("No permission to delete this Workspace!")

        # remove it
        success = Workspace.remove(db, wid)
        if not success:
            raise AjaxException("Couldn't remove this Workspace!")

    @cherrypy.expose
    @cherrypy.tools.method(accept="POST")
    def connectworkspace(self, wid, password=None):
        db       = cherrypy.request.db
        user     = cherrypy.request.user
        password = password or cherrypy.session.get("guest_password", None)

        # is workspace owned by the user?
        workspace = Workspace.get_by_id(db, wid)
        if not isinstance(workspace, Workspace) or not workspace.has_access(user):
            raise AjaxException("Unknown Workspace! (id: %s)" % wid)

        vispa.workspace.connect(workspace, user, db, password)

    def autoconnectworkspace(self, user, password):
        db = cherrypy.request.db

        for ws in Workspace.get_user_workspaces(db, user):
            if ws.auto_connect:
                vispa.workspace.connect(ws, user, db, password)

    @cherrypy.expose
    @cherrypy.tools.method(accept="POST")
    def disconnectworkspace(self, wid):
        db   = cherrypy.request.db
        user = cherrypy.request.user

        # is workspace owned by the user?
        workspace = Workspace.get_by_id(db, wid)
        if not isinstance(workspace, Workspace) or not workspace.has_access(user):
            raise AjaxException("Unknown Workspace! (id: %s)" % wid)

        # disconnect
        vispa.workspace.disconnect(workspace)

    @cherrypy.expose
    @cherrypy.tools.method(accept="GET")
    def getworkspacedata(self, wid=None):
        workspace = None
        if wid:
            db = cherrypy.request.db
            workspace = Workspace.get_by_id(db, wid)

        return self._root.workspace_data(workspace=workspace)

    # misc
    @cherrypy.expose
    def localuser(self):
        local = cherrypy.request.local
        remote = cherrypy.request.remote
        socket_uid = vispa.Netstat.get_socket_owner(
            local.ip or local.name,
            local.port,
            remote.ip or remote.name,
            remote.port
        )
        logger.info("Socket uid: %s" % socket_uid)
        server_uid = os.getuid()
        logger.info("Server uid: %s" % server_uid)

    @cherrypy.expose
    @cherrypy.tools.method(accept="POST")
    def feedback(self, content, anonymous):
        feedback_address = vispa.config("web", "feedback.address", "")
        if feedback_address == "":
            return
        subject = vispa.config("web", "feedback.subject", "VISPA Feedback")
        if self.convert(anonymous, bool):
            prefix = "Anonymous feedback:\n\n"
        else:
            uname = cherrypy.request.user.name
            umail = cherrypy.request.user.email
            prefix = "Feedback from user '%s' (%s):\n\n" % (uname, umail)
        vispa.send_mail(feedback_address, subject, prefix + content)
