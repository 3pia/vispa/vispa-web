# -*- coding: utf-8 -*-

import vispa
import cherrypy


class ErrorController(object):
    """ This class provides custom error-catching
        functions that are inserted into the cherrypy config.
    """

    STATUSMAP = {
        "400": "badrequest",
        "401": "unauthorized",
        "403": "forbidden",
        "404": "filenotfound",
        "408": "requesttimeout",
        "500": "internalservererror"
    }

    TMPL = "<html><head><meta http-equiv='refresh' content='0;url=%s' /></head></html>"

    def __init__(self):
        # dynamic function setting
        for code, name in self.STATUSMAP.iteritems():
            cherrypy.config.update({"error_page.%s" % code: self.__error})

            @cherrypy.expose
            @cherrypy.tools.render(template="errors/%s.mako" % code)
            def func():
                return self.get_error_data()

            setattr(self, name, func)

    @cherrypy.expose
    def index(self, *args, **kwargs):
        raise cherrypy.HTTPRedirect(vispa.url.dynamic("/"), status=301)

    def get_error_data(self):
        key = "error_data"
        if key in cherrypy.session:
            data = cherrypy.session[key]
            del cherrypy.session[key]
            return data
        else:
            return {}

    def __error(self, status, message, traceback, version):
        cherrypy.session["error_data"] = {
            "status": status,
            "message": message,
            "traceback": traceback,
            "version": version
        }

        page = self.STATUSMAP.get(status.split(" ")[0], "500")
        return self.TMPL % vispa.url.dynamic("error", page)