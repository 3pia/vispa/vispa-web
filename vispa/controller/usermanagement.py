# -*- coding: utf-8 -*-

# imports
from passlib.hash import sha256_crypt
from vispa import AjaxException
from vispa.controller import AbstractController
from vispa.models.group import Group, Group_User_Assoc, Group_Group_Assoc
from vispa.models.project import Project
from vispa.models.role import Role, Permission
from vispa.models.user import User
from vispa.models.workgroup import Workgroup
import cherrypy

from itertools import izip
from inspect import getargspec
from collections import deque

def DB():
    return cherrypy.request.db

def call_mix(func, *args, **kwargs):
    return func(*args, **{
        arg: kwargs[arg]
        for arg in getargspec(func)[0] if arg in kwargs
    })

class Rights(int):
    levels = [
        "none",
        "private",
        "protected",
        "public",
        "member",
        "manager",
        "admin",
        "forbidden"
    ]

    @staticmethod
    def has_user(user, listing):
        return any(
            user.id == (item.id if isinstance(item, User) else item.user_id)
            for item in listing
        )

    def test(self, obj):
        """
        Tests wether the object interaction is allowed with the given rights.
        """
        user = cherrypy.request.user

        if user.serveradmin:
            return True

        if self > self.manager:
            return False
        if obj == user:
            return True
        if user in getattr(obj, "get_managers", list)():
            return True

        if self > self.member:
            return False
        if getattr(obj, "is_member", lambda user: False)(user):
            return True

        return self <= {
            Group.PUBLIC: self.public,
            Group.PROTECTED: self.protected,
            Group.PRIVATE: self.private,
            None: self.none,
        }[getattr(obj, "privacy", None)]

    @staticmethod
    def extend(base, extension, delete=[]):
        ret = {}
        ret.update(base)
        ret.update(extension)
        for d in delete:
            del ret[d]
        return ret

for i, name in enumerate(Rights.levels):
    setattr(Rights, name, Rights(i))

class PermissionException(AjaxException):
    def __init__(self, obj):
        super(PermissionException, self).__init__(
            "Insufficient permissions to access %s \"%s\" with this operation"
            % (obj.__class__.__name__.lower(), obj.name), 403
        )

class BaseController(object):
    def __init__(self, table, rights={}, assocs=[]):
        self._table = table
        self._rights = rights
        for assoc in assocs:
            setattr(self, assoc._assoc, assoc)
            setattr(assoc, "_master", self)

    def _rga(self, rights, *names):
        # collect the object to work on
        tar = self
        tars = deque([tar])
        while tar and hasattr(tar, "_master"):
            tar = tar._master
            tars.appendleft(tar)
        rights = self._rights.get(rights, self._rights["__default__"])
        if type(rights) is not list:
            rights = [rights]
        good = [True] * len(rights)
        objs = deque()
        for tar, name, right in izip(tars, names, izip(*rights)):
            obj = tar._table.get_by_name(DB(), name)
            if obj is None:  # TODO: this is a info leak (about existence)
                raise AjaxException(
                    "There is no %s \"%s\""
                    % (tar._table.__name__.lower(), name), 404
                )
            good = [g and Rights(r).test(obj) for g, r in zip(good, right)]
            if True not in good:
                raise PermissionException(obj)
            objs.append(obj)
        return objs

    def _rt(self, rights):
        rights = self._rights.get(rights, self._rights["__default__"])
        if type(rights) is not list:
            rights = [rights]
        if not any(all(r.test(None) for r in right) for right in rights):
            raise AjaxException(403)

    def _rf(self, rights, obj_iter):
        rights = self._rights.get(rights, self._rights["__default__"])
        rights = Rights(rights[0] if type(rights) is tuple else rights)
        if type(rights) is not list:
            rights = [rights]
        return [obj for obj in obj_iter if any(r.test(obj) for r in rights)]

# base tables
class TableController(BaseController):
    """
    Basic controller for accessing items of a table.
    """
    def __init__(self, *args, **kwargs):
        self._setters = kwargs.pop("setters", [])
        super(TableController, self).__init__(*args, **kwargs)

    @cherrypy.expose
    def add(self, name):
        self._rt("create")
        self._table.create(DB(), name)

    @cherrypy.expose
    def list(self):
        isAdmin = cherrypy.request.user.serveradmin
        return [
            call_mix(entry.to_dict, user=cherrypy.request.user)
            for entry in self._rf("list", self._table.all(DB()))
            if isAdmin or getattr(entry, "status", 1) == 1
        ]

    @cherrypy.expose
    def info(self, name):
        base, = self._rga("info", name)
        return call_mix(base.to_dict, user=cherrypy.request.user)

    @cherrypy.expose
    def rename(self, name, new_name):
        base, = self._rga("rename", name)
        base.rename(new_name)

    @cherrypy.expose
    def remove(self, name):
        base, = self._rga("delete", name)
        base.delete()

    @cherrypy.expose
    def update(self, name, **kwargs):
        for key, value in kwargs.items():
            if key in self._setters:
                base, = self._rga("update_%s" % key, name)
                func = getattr(base, "set_%s" % key)
                func(value)
            else:
                raise AjaxException("Unknown property to updated: %s" % key, 404)

class AutojoinTableController(TableController):
    @cherrypy.expose
    def add(self, name):
        self._rt("create")
        entry = self._table.create(DB(), name)
        entry.add_manager(cherrypy.request.user)

class AssociationController(BaseController):
    def __init__(self, *args, **kwargs):
        self._assoc = kwargs.pop("assoc", [])
        super(AssociationController, self).__init__(*args, **kwargs)

    def _info(self, item):
        return {
            "name": item.name,
        }

    @cherrypy.expose
    def list(self, name):
        base, = self._rga("list", name)
        func = getattr(base, "get_%ss" % self._assoc)
        return [self._info(obj) for obj in call_mix(func, recursion_depth=0)]

    @cherrypy.expose
    def add(self, name, assoc_name):
        base, target = self._rga("add", name, assoc_name)
        func = getattr(base, "add_%s" % self._assoc)
        func(target)

    @cherrypy.expose
    def remove(self, name, assoc_name):
        base, target = self._rga("remove", name, assoc_name)
        func = getattr(base, "remove_%s" % self._assoc)
        func(target)

class AssociationToGroupController(AssociationController):
    def _info(self, item):
        return {
            "name": getattr(item, self._assoc).name,
            "confirmed": item.status == item.CONFIRMED,
        }

    @cherrypy.expose
    def add(self, name, assoc_name, password=""):
        try:
            base, target = self._rga("add", name, assoc_name)
            conf = True
        except PermissionException:
            base, target = self._rga("add_self", name, assoc_name)
            if base.privacy == Group.PUBLIC:
                conf = True
            elif base.password and password:
                if not sha256_crypt.verify(password, base.password):
                    raise AjaxException(403)
                conf = True
            elif base.privacy == Group.PROTECTED:
                conf = False
            else:
                raise PermissionException(base)

        func = getattr(base, "add_%s" % self._assoc)
        conft = Group_User_Assoc if self._table.__name__ == "User" else Group_Group_Assoc
        try:
            func(target, conft.CONFIRMED if conf else conft.UNCONFIRMED)
        except Exception as e:
            if not conf:
                raise
            if "already in group" not in e.message:  # TODO: this is fugly
                raise
            func = getattr(base, "confirm_%s" % self._assoc)
            func(target)

    @cherrypy.expose
    def confirm(self, name, assoc_name):
        base, target = self._rga("confirm", name, assoc_name)
        func = getattr(base, "confirm_%s" % self._assoc)
        func(target)

class AssociationToProjectController(AssociationController):
    def _info(self, item):
        return {
            "name": getattr(item, self._assoc).name,
            "roles": [
                role.name for role in item.roles
            ]
        }

class AssociationDeepController(AssociationController):
    @cherrypy.expose
    def list(self, name, mid_name):
        base, mid, = self._rga("list", name, mid_name)
        func = getattr(base, "get_%ss" % self._assoc)
        return [obj.name for obj in call_mix(func, mid, recursion_depth=0)]

    @cherrypy.expose
    def add(self, name, mid_name, assoc_name):
        base, mid, target = self._rga("add", name, mid_name, assoc_name)
        func = getattr(base, "add_%s" % self._assoc)
        func(mid, target)

    @cherrypy.expose
    def remove(self, name, mid_name, assoc_name):
        base, mid, target = self._rga("remove", name, mid_name, assoc_name)
        func = getattr(base, "remove_%s" % self._assoc)
        func(mid, target)

class UMAjaxController(AbstractController):

    def __init__(self):
        super(UMAjaxController, self).__init__()

        # some commonly used rights
        rights_admin = {"__default__": (Rights.admin,) * 3}
        rights_manager = {"__default__": (Rights.manager, Rights.none)}
        rights_assoc = {
            "list": (Rights.member,),
            "add": (Rights.manager, Rights.none),
            "remove": [
                (Rights.manager, Rights.none),  # managers can kick members
                (Rights.none, Rights.manager),  # anyone can leave on their own
            ],
            "__default__": (Rights.manager,),
        }

        # now define the controllers
        self.permission = TableController(table=Permission, rights=rights_admin)
        self.role = TableController(table=Role, rights=Rights.extend(rights_admin, {
            "list": (Rights.none,),
        }), assocs=[
            AssociationController(table=Permission, assoc="permission", rights=rights_admin),
        ])
        self.workgroup = AutojoinTableController(table=Workgroup, rights={
            "list": (Rights.member,),
            "info": (Rights.member,),
            "create": (Rights.none,),
            "delete": (Rights.manager,),
            "__default__": (Rights.manager,)
        }, assocs=[
            AssociationController(table=User, assoc="manager", rights=rights_manager),
            AssociationController(table=User, assoc="user", rights=rights_assoc)
        ])

        self.group = TableController(table=Group, rights={
            "list": (Rights.protected,),
            "info": (Rights.protected,),
            "create": (Rights.admin,),
            "delete": (Rights.admin,),
            "update_status": (Rights.admin,),
            "__default__": (Rights.manager,),
        }, assocs=[
            AssociationController(table=User, assoc="manager", rights=rights_manager),
            AssociationToGroupController(table=Group, assoc="parent_group", rights={
                "list": (Rights.public,),
                "__default__": (Rights.forbidden,),
            }),
        ] + [
            AssociationToGroupController(table=t, assoc=a, rights=Rights.extend(rights_assoc, {
                "list": (Rights.public,),
                "confirm": (Rights.manager, Rights.none),
                "add": (Rights.manager, Rights.none),
                "add_self": (Rights.none, Rights.manager),
            })) for t, a in [
                (User, "user"),
                (Group, "child_group"),
            ]
        ], setters=[
            "privacy",
            "status",
            "password",
        ])

        self.project = TableController(table=Project, rights={
            "list": (Rights.member,),
            "info": (Rights.member,),
            "create": (Rights.admin,),
            "delete": (Rights.admin,),
            "update_status": (Rights.admin,),
            "__default__": (Rights.manager,),
        }, assocs=[
            AssociationController(table=User, assoc="manager", rights=rights_manager),
        ] + [
            AssociationToProjectController(table=t, assoc=a, rights=rights_assoc, assocs=[
                AssociationDeepController(table=Role, assoc="%s_role" % a, rights=rights_admin)
            ]) for t, a in [
                (User, "user"),
                (Group, "group")
            ]
        ], setters=[
            "status"
        ])

    @cherrypy.expose
    def user_groups(self):
        return [grp.name for grp in cherrypy.request.user.get_groups()]

    @cherrypy.expose
    def user_projects(self, managed=False, no_global_project=False):
        return [pro.name for pro in cherrypy.request.user.get_projects(
                managed=not not managed,
                no_global_project=not not no_global_project
            )]
