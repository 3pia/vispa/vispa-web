# -*- coding: utf-8 -*-

# imports
import cherrypy
import vispa
from vispa.controller import AbstractController
from vispa.socketbus import SocketPublisher, PollingPublisher, add_session, \
    USE_SOCKETS, SUBSCRIBERS, POLLING_TIMESTAMPS, get_polling_publisher
from vispa import AjaxException
from time import time
import json


class BusController(AbstractController):

    def __init__(self):
        AbstractController.__init__(self, mount_static=False)

    @cherrypy.expose
    def index(self, *args, **kwargs):
        pass

    @cherrypy.expose
    @cherrypy.tools.user(redirect=False)
    @cherrypy.tools.ajax()
    def poll(self, timeoutms=10000):
        wid = cherrypy.request.private_params.get("_windowId")
        uid = cherrypy.request.user.id
        publisher = get_polling_publisher(wid, uid)
        if not isinstance(publisher, PollingPublisher):
            return []
        self.release()
        return publisher.fetch(int(timeoutms)*0.001)

    @cherrypy.expose
    def send(self, *args, **kwargs):
        # due to the application/x-www-form-urlencoded content type
        # msg is the first key of kwargs
        msg = kwargs.keys()[0]
        window_id = cherrypy.request.private_params.get("_windowId")
        user_id = self.get("user_id")
        publisher = get_polling_publisher(window_id, user_id)
        if isinstance(publisher, PollingPublisher):
            publisher.received_message(msg)


# overwrite the BusController's index?
if USE_SOCKETS:
    @cherrypy.expose
    @cherrypy.tools.websocket(handler_cls=SocketPublisher)
    def socket_hook(self, *args, **kwargs):
        # the user is connected via this controller function
        # on startup, so store user_id<->session_id information
        # in the publisher at "cherrypy.serving.request.ws_handler"
        publisher = cherrypy.serving.request.ws_handler
        setattr(publisher, "window_id", cherrypy.request.private_params.get("_windowId"))
        setattr(publisher, "user_id", self.get("user_id"))
        # call the publisher's "store" method to register it to the bus
        publisher.store()

    BusController.index = socket_hook
