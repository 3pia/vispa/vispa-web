# -*- coding: utf-8 -*-

# imports
import StringIO
import inspect
import logging
import os
import uuid

from vispa.controller import AbstractController, strongly_expire
from vispa.controller.ajax import AjaxController
from vispa.controller.bus import BusController
from vispa.controller.error import ErrorController
from vispa.controller.filesystem import FSController
from vispa.models.jsondata import JSONData
from vispa.models.user import User
from vispa.models.group import Group, Group_User_Assoc
from vispa.models.project import Project
from vispa.models.workspace import Workspace
import cherrypy
import vispa

import json as JSON
from vispa import socketbus
logger = logging.getLogger(__name__)


class RootController(AbstractController):

    def __init__(self, server):
        """
        The Constructor. Members from other classes
        are added as pages for cherrypy URL mapping.
        """
        AbstractController.__init__(self, mount_static=False)

        self._server = server

        static_path = os.path.join(os.path.dirname(inspect.getabsfile(vispa)), 'static')
        build_path = vispa.config("web", "build_path", None)
        if build_path:
            build_path = os.path.join(os.path.dirname(inspect.getabsfile(vispa)), build_path, 'static')
            if not os.path.isdir(build_path):
                logger.warning("RootController: build_path '%s' not found, falling back to default.", build_path)
            else:
                static_path = build_path
        self.mount_static(static_path)
        self.ajax = AjaxController(self)
        self.fs = FSController()
        self.extensions = AbstractController(mount_static=False)
        self.bus = BusController()

        if not vispa.config("web", "dev_mode", True):
            self.error = ErrorController()

        self.workspace_action = vispa.config("web", "workspace_action", "")
        self.use_websockets   = vispa.config("websockets", "enabled", False)
        self.client_log_level = vispa.config("web", "logging.level", "info")
        self.add_workspaces   = vispa.config("workspace", "add", True)
        self.use_feedback     = vispa.config("web", "feedback.address", "") != ""
        self.cache_bust       = vispa.config("web", "cache_bust", "")
        if self.cache_bust == "uuid":
             self.cache_bust = str(uuid.uuid4())
        elif self.cache_bust.startswith("mtime:"):
             self.cache_bust = str(os.path.getmtime(self.cache_bust[6:]))
        elif len(self.cache_bust) == 0:
             self.cache_bust = None

    @classmethod
    def expire_cookie(cls, name):
        cherrypy.response.cookie[name] = ""
        cherrypy.response.cookie[name]["expires"] = 0
        cherrypy.response.cookie[name]["max-age"] = 0

    def mount_extension_controller(self, mountpoint, controller):
        if hasattr(self.extensions, mountpoint):
            logger.warning("Controller mountpoint already exists: %s" % mountpoint)
        else:
            logger.info("Mounting controller '%s'" % os.path.join(os.sep, mountpoint))
            setattr(self.extensions, mountpoint, controller)

    def workspace_data(self, workspace=None, keys=None):
        db   = cherrypy.request.db
        user = cherrypy.request.user

        workspace_data = {}

        if workspace:
            workspaces = [workspace]
        else:
            workspaces = Workspace.get_user_workspaces(db, user)

        for ws in workspaces:
            data = ws.make_dict(keys=keys)

            conn = vispa.workspace._connection_pool.get(user, ws)
            data["connection_status"] = conn.status()[1] if conn else "disconnected"
            data["can_edit"] = ws.can_edit(user)

            if workspace == ws:
                return data

            workspace_data[ws.id] = data

        return workspace_data

    @cherrypy.expose
    @cherrypy.tools.render(template="sites/index.mako")
    @cherrypy.tools.method(accept="GET")
    @strongly_expire
    def index(self, *args, **kwargs):
        db = cherrypy.request.db
        user = cherrypy.request.user

        prefs = JSONData.get_values_by_key(db, user.id, key="preferences")
        sess  = JSONData.get_value(db, user.id, key="session", workspace_id=None)

        extensions = [
            name
            for name, ext in self._server._extensions.items()
            if not getattr(ext, "server_only", False)
        ]
        if vispa.config("usermanagement", "autosetup", False):
            allowed = user.has_permissions(
                Project.get_by_name(db, vispa.config("usermanagement", "global_project", None)),
                ["%s.allowed" % ext for ext in extensions],
                inexistentDefaultTo=True
            )
            extensions = [ext for ext, allow in zip(extensions, allowed) if allow]

        data = {
            "username"        : user.name,
            "usermail"        : user.email,
            "use_websockets"  : self.use_websockets,
            "add_workspaces"  : self.add_workspaces,
            "log_level"       : self.client_log_level,
            "use_feedback"    : self.use_feedback,
            "is_guest"        : cherrypy.session.get("is_guest", False),
            "is_admin"        : user.serveradmin,
            "workspace_action": self.workspace_action,
            "extensions"      : JSON.dumps(extensions),
            "cache_bust"      : self.cache_bust,
            "preferences"     : JSON.dumps(prefs),
            "session"         : JSON.dumps(sess),
        }

        return data

    @cherrypy.expose
    @cherrypy.tools.user(reverse=True)
    @cherrypy.tools.render(template="sites/login.mako")
    @cherrypy.tools.method(accept="GET")
    def login(self, *args, **kwargs):
        path = vispa.url.dynamic("?" + cherrypy.request.query_string)
        db = cherrypy.request.db

        if "user_id" in cherrypy.session:
            raise cherrypy.HTTPRedirect(path)

        # delete all cookies except for the session id
        session_key = cherrypy.serving.request.config.get("tools.sessions.name", "session_id")
        for key in cherrypy.response.cookie.keys():
            if key != session_key:
                self.expire_cookie(key)

        login = cherrypy.request.login
        if login and vispa.config("user", "remote.enabled", False):
            user = User.get_by_name(db, login)

            if user is None and vispa.config("user", "remote.auto_create", True):
                email_key = vispa.config("user", "remote.email_key", None)
                email_domain = vispa.config("user", "remote.email_domain", None)
                if email_key:
                    email = cherrypy.request.wsgi_environ.get(email_key, None)
                elif email_domain:
                    email = "%s@%s" % (login, email_domain)
                user = User(name=login, password=User.generate_hash(64), email=email, hash=User.generate_hash(32), status=User.ACTIVE)
                db.add(user)
                db.commit()
                vispa.fire_callback("user.register", user)
                vispa.fire_callback("user.activate", user)

            cherrypy.session["user_id"] = unicode(user.id)
            cherrypy.session["user_name"] = user.name
            vispa.fire_callback("user.login", user)
            raise cherrypy.HTTPRedirect(path)

        welcome_phrase = vispa.config("web", "text.welcome", "")
        login_text = vispa.config("web", "text.login", "")
        registration_text = vispa.config("web", "text.registration", "")
        forgot_text = vispa.config("web", "text.forgot", "")
        disclaimer_text = vispa.config("web", "text.disclaimer", "")
        use_forgot = vispa.config("web", "forgot.use", False)
        return {
            "welcome_phrase"   : welcome_phrase,
            "login_text"       : login_text,
            "registration_text": registration_text,
            "forgot_text"      : forgot_text,
            "disclaimer_text"  : disclaimer_text,
            "use_forgot"       : use_forgot,
            "cache_bust"       : self.cache_bust,
        }

    @cherrypy.expose
    @cherrypy.tools.user(on=False)
    @cherrypy.tools.method(accept="GET")
    def guest_login(self, *args, **kwargs):
        path = vispa.url.dynamic("?" + cherrypy.request.query_string)

        # redirect when the user is already logged in
        if "user_id" in cherrypy.session:
            raise cherrypy.HTTPRedirect(path)

        # return an error when guest logins are disabled
        if vispa.config("usermanagement", "enable_guest_login", False) is False:
            raise cherrypy.HTTPError(403, "Guest login not allowed!")

        db= cherrypy.request.db

        # actual guest login
        user, password = User.guest_login(db)
        for groupname in vispa.config("usermanagement", "guest_group", "guest"):
            group = Group.get_or_create_by_name(db, groupname)
            group.add_user(db, user, Group_User_Assoc.UNCONFIRMED)

        # update session
        cherrypy.session["user_id"] = unicode(user.id)
        cherrypy.session["user_name"] = user.name
        cherrypy.session["guest_password"] = password
        vispa.fire_callback("user.login", user)
        cherrypy.session["is_guest"] = True

        raise cherrypy.HTTPRedirect(path)

    @cherrypy.expose
    @cherrypy.tools.method(accept="GET")
    def logout(self, path="/"):
        vispa.fire_callback("user.logout", cherrypy.request.user)
        vispa.socketbus.remove_session(self.get("window_id"))
        cherrypy.lib.sessions.expire()

        # remove all cookies
        for key in cherrypy.response.cookie.keys():
            self.expire_cookie(key)

        raise cherrypy.HTTPRedirect(vispa.url.dynamic(path))

    @cherrypy.expose
    @cherrypy.tools.user(on=False)
    @cherrypy.tools.render(template="sites/password.mako")
    @cherrypy.tools.method(accept="GET")
    def password(self, hash, *args, **kwargs):
        user = User.get_by_hash(cherrypy.request.db, hash)
        data = {
            "welcome_phrase": vispa.config("web", "text.welcome", ""),
            "username": None if not isinstance(user, User) else user.name,
            "hash": hash,
            "cache_bust": self.cache_bust
        }
        return data

    @cherrypy.expose
    @cherrypy.tools.user(on=False)
    @cherrypy.tools.db(on=False)
    @cherrypy.tools.workspace(on=False)
    @cherrypy.tools.sessions(on=False)
    @cherrypy.tools.method(accept=["GET", "POST"])
    @strongly_expire
    def status(self, password=None):
        if password != vispa.config("web", "status_password", None):
            return """
<html>
<head>
    <title>VISPA Threads</title>
</head>
<body>
<h1>VISPA Threads</h1>
<form method="post">
<label for="password">Password:</label>
<input type="password" name="password" />
<input type="submit" value="Submit" />
</form>
</html>
"""
        else:
            s = StringIO.StringIO()
            vispa.dump_thread_status(s)
            return s.getvalue()

    @cherrypy.expose
    @cherrypy.tools.user(on=False)
    @cherrypy.tools.db(on=False)
    @cherrypy.tools.workspace(on=False)
    @cherrypy.tools.sessions(on=False)
    @cherrypy.tools.method(accept=["GET", "POST"])
    @strongly_expire
    def graceful_shutdown(self, password=None):
        if password != vispa.config("web", "status_password", None):
            return """
<html>
<head>
    <title>VISPA Graceful Shutdown</title>
</head>
<body>
<h1>VISPA Graceful Shutdown</h1>
<form method="post">
<label for="password">Password:</label>
<input type="password" name="password" />
<input type="submit" value="Submit" />
</form>
</html>
"""
        else:
            logger.warn(
                "Graceful shutdown started")
            _cb_graceful_shutdown(None, None)
            vispa.subscribe("bus.session_removed", _cb_graceful_shutdown)


def _cb_graceful_shutdown(window_id, user_id):
        remaining_session = len(socketbus.USER_SESSIONS)
        if remaining_session == 0:
            cherrypy.engine.exit()
        else:
            logger.warn(
                "Graceful shutdown: waiting for %d remaining session." %
                remaining_session)
