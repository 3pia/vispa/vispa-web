# -*- coding: utf-8 -*-

# imports
import inspect
import json
import logging
import os
import uuid

from vispa.models.workspace import Workspace
import cherrypy
import vispa

logger = logging.getLogger(__name__)


def strongly_expire(func):
    """
    Decorator that sends headers that instruct browsers and proxies not to cache.
    """
    def newfunc(*args, **kwargs):
        cherrypy.response.headers.update({
            "Expires"      : "Sun, 19 Nov 1978 05:00:00 GMT",
            "Cache-Control": "no-store, no-cache, must-revalidate, post-check=0, pre-check=0",
            "Pragma"       : "no-cache"
        })
        return func(*args, **kwargs)

    return newfunc


class StaticController(object):

    def __init__(self, path):
        isDevMode = vispa.config("web", "dev_mode", None)
        self._cp_config = {
           'tools.staticdir.on': True,
           'tools.staticdir.dir': path,
           'tools.db.on': False,
           'tools.user.on': False,
           'tools.permission.on': False,
           'tools.workspace.on': False,
           'tools.ajax.on': False,
           'tools.etags.on': True,
           'tools.sessions.on': False,
           'tools.expires.on': True,
           'tools.expires.force': True,
           'tools.expires.secs': 1 if isDevMode else 3600 * 24 * 365
        }


class AbstractController(object):

    def __init__(self, mount_static=True):
        self._object_cache = {}
        self._cp_config = {
           'tools.db.on': True,
           'tools.user.on': True,
           'tools.permission.on': True,
           'tools.permission.permissions': [],
        }
        if mount_static:
            self.mount_static()

    def mount_static(self, path=None, url='static'):
        path = path or 'static'
        static_path = os.path.join(os.path.dirname(inspect.getabsfile(self.__class__)), path)
        build_path = vispa.config("web", "build_path", None)
        if build_path:
            self_path = os.path.dirname(inspect.getabsfile(self.__class__))
            vispa_path = os.path.dirname(inspect.getabsfile(vispa))
            build_path = os.path.join(vispa_path, build_path)
            if self_path.startswith(vispa_path):
                self_path = self_path[len(vispa_path)+1:]
            build_path = os.path.join(build_path, self_path, path)
            if not os.path.isdir(build_path):
                logger.warning("Controller: build_path '%s' not found, fallback to default.", build_path)
            else:
                static_path = build_path

        logger.info("Mount static controller. path: %s, url: %s" % (static_path, url))
        setattr(self, url, StaticController(static_path))

    # a little helper
    def get(self, key, *args, **kwargs):
        key = key.lower()
        try:
            session = getattr(cherrypy, "session")
            request = getattr(cherrypy, "request")
            if key == "session_id":
                return session.id
            elif key == "user_id":
                return request.user.id
            elif key == "user_name":
                return request.user.name
            elif key == "db":
                return request.db
            elif key == "workspace":
                if len(args) == 0:
                    return request.workspace
                else:
                    return Workspace.get_by_id(request.db, int(args[0]))
            elif key == "profile_id":
                return session.get("profile_id", None)
            elif key == "profile":
                return request.profile
            elif key == "fs":
                workspace = request.workspace if len(args) == 0 else args[0]
                return vispa.workspace.get_instance(
                    "vispa.remote.filesystem.FileSystem", workspace=workspace,
                    init_args={
                        'workspaceid': workspace.id,
                        'userid': request.user.id
                    })
            elif key == "workspace_id":
                return request.private_params.get("_workspaceId", None)
            elif key == "view_id":
                return request.private_params.get("_viewId", None)
            elif key == "window_id":
                return request.private_params.get("_windowId", None)
            elif key == "combined_id":
                return session.id + request.private_params.get("_windowId", None) + request.private_params.get("_viewId", None)
            elif key == "proxy":
                if not hasattr(self, "extension") or not len(args):
                    return None
                return self.extension.get_workspace_instance(*args, **kwargs)
        except vispa.AjaxException:
            raise
        except:
            vispa.log_exception("AbstractController failed get(%s)" % key)
            return None
        return None

    # a little unicode helper
    def convert(self, value, flag):
        # convet lists, tuples and dicts by pseudo recursion
        if isinstance(value, list):
            return map(lambda elem: self.convert(elem, flag), value)
        elif isinstance(value, tuple):
            return tuple(map(lambda elem: self.convert(elem, flag), value))
        elif isinstance(value, dict):
            keys = map(lambda elem: str(elem), value.keys())
            values = map(lambda elem: self.convert(elem, flag), value.values())
            return dict(zip(keys, values))

        if value is None:
            return None

        # string, int, float
        if flag in [str, int, float]:
            try:
                return flag(value)
            except:
                raise Exception('TypeError: conversion to %s received bad argument!' % str(flag))
        # boolean
        if flag == bool:
            value = str(value).lower()
            if value == 'true':
                return True
            elif value == 'false':
                return False
            else:
                raise Exception("TypeError: boolean conversion expects 'true' or 'false'!")
        # error case
        raise Exception('TypeError: conversion with unknown type flag!')

    def cache(self, workspace_id, key):
        _key = (cherrypy.request.user.id, workspace_id, key)
        if _key in self._object_cache:
            return self._object_cache[_key]
        else:
            return None

    def set_cache(self, workspace_id, item, key=None):
        key = key or str(uuid.uuid4())
        _key = (cherrypy.request.user.id, workspace_id, key)
        self._object_cache[_key] = item
        return key

    def release_database(self):
        if hasattr(cherrypy.tools, "db"):
            cherrypy.tools.db.commit_transaction()
        if hasattr(cherrypy.request, "db"):
            cherrypy.request.db = None

    def release_session(self):
        if hasattr(cherrypy, "session"):
            cherrypy.session.save()
            cherrypy.session.loaded = False

    def release(self):
        self.release_database()
        self.release_session()
