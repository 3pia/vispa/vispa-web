# -*- coding: utf-8 -*-

from datetime import datetime
from sqlalchemy import Column, Table, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.orm.session import object_session
from sqlalchemy.types import Unicode, UnicodeText, DateTime, Integer
from vispa import AjaxException
from vispa.models import Base, ItemAccessor
from vispa.models.user import User

__all__ = ["Workgroup", "WorkgroupItem"]

workgroup_manager_association = Table('workgroup_manager_association', Base.metadata,
                                      Column('workgroup_id', Integer, ForeignKey('workgroup.id')),
                                      Column('user_id', Integer, ForeignKey('user.id')))

workgroup_user_association = Table('workgroup_user_association', Base.metadata,
                                   Column('workgroup_id', Integer, ForeignKey('workgroup.id')),
                                   Column('user_id', Integer, ForeignKey('user.id')))

class Workgroup(Base):

    """
    A workgroup is a lighter version of a project. It only contains users, no groups, and there is
    no explicit management of roles and permission, which means, all users have the same rights.
    The memberships are organized by managers.
    """

    __tablename__ = 'workgroup'
    id = Column(Integer, nullable=False, primary_key=True)
    name = Column(Unicode(255), nullable=False, unique=True)
    created = Column(DateTime, nullable=False, default=datetime.now)
    managers = relationship("User",
                            secondary=workgroup_manager_association,
                            backref="managed_workgroups")
    users = relationship("User", secondary=workgroup_user_association, backref="workgroups")
    items = relationship("WorkgroupItem", backref="workgroup")

    def to_dict(self, user=None):
        return {
            "id": self.id,
            "name": self.name,
            "created": self.created.isoformat(),
            "manager": None if user is None else user in self.get_managers(),
        }

    @staticmethod
    def get_by_id(session, gid):
        """
        Get a workgroup by its id.

        :param session: current session of database
        :para gid: id, which is looked for
        :returns: Workgroup or None
        """
        return session.query(Workgroup).filter_by(id=gid).first()

    @staticmethod
    def get_by_name(session, name):
        """
        Get a workgroup by its name.

        :param session: current session of database
        :para name: name, which is looked for
        :returns: Workgroup or None
        """
        return session.query(Workgroup).filter_by(name=unicode(name)).first()

    @staticmethod
    def get(session, workgroup):
        """
        Returns workgroup, whichs name or id is given. If the parameter workgroup is already an
        instance of Workroup it is directly returned.

        :param session: current session of database
        :param workgroup: name or id to look for. If workgroup is instance of Workroup, it is
                          directly returned
        :returns: Workgroup
        :raises AjaxException: if parameter workgroup is invalid of no workgroup can be found
        """
        if workgroup is None:
            raise AjaxException('invalid name', 400)
        if isinstance(workgroup, Workgroup):
            return workgroup
        else:
            retval = Workgroup.get_by_name(session, workgroup)
            if retval is None:
                retval = Workgroup.get_by_id(session, workgroup)
            if retval is None:
                raise AjaxException('workgroup %s not found' % workgroup, 404)
            return retval

    @staticmethod
    def all(session):
        """
        Get all existing workgroups.

        :param session: current session of database
        :returns: list of Workgroup objects
        """
        return session.query(Workgroup)

    def get_users(self):
        """
        Get users of the workgroup.

        :returns: list of User objects
        """
        return self.users

    def is_member(self, user):
        """
        Check if this user is a member.

        :param User user: The user to check.
        :return: Whether the user is a member.
        :rtype: Boolean
        """
        if not isinstance(user, User):
            raise TypeError("invalid type of user")
        return user in self.users

    def get_managers(self):
        """
        Get managers of workgroup.

        :returns: list of User objects
        """
        return self.managers

    @staticmethod
    def create(session, name):
        """
        Create new workgroup.

        :param session: current session of database
        :param name: name of the workgroup
        :raises AjaxException: if parameter name is invalid or already in use
        """
        # already exiting?
        if not name:
            raise AjaxException('invalid name', 400)
        if Workgroup.get_by_name(session, name) is not None:
            raise AjaxException('workgroup %s already exists' % name, 409)
        workgroup = Workgroup(name=name)
        session.add(workgroup)
        session.commit()
        return workgroup

    def rename(self, newname):
        """
        Rename workgroup.

        :param newname: new name of the workgroup
        :raises AjaxException: if newname is already in use
        """
        session = object_session(self)
        if Workgroup.get_by_name(session, newname) is not None:
            raise AjaxException("Workgroup %s already exists" % newname, 409)
        self.name = newname

    def delete(self):
        """
        Delete workgroup. It is also deleted from the database.
        """
        session = object_session(self)
        session.query(workgroup_user_association).filter_by(workgroup_id=self.id)\
            .delete(synchronize_session=False)
        session.query(workgroup_manager_association).filter_by(workgroup_id=self.id)\
            .delete(synchronize_session=False)
        session.delete(self)

    def add_user(self, user):
        """
        Add user to workgroup.

        :param user: user which has to be added
        :type user: User
        :raises TypeError: if user is not instance of User
        :raises AjaxException: if user is already in workgroup
        """
        if not isinstance(user, User):
            raise TypeError('invalid type of user')
        if user in self.users:
            raise AjaxException('user %s already in workgroup %s' % (user.name, self.name), 409)
        self.users.append(user)

    def remove_user(self, user):
        """
        Remove user from workgroup.

        :param user: user which has to be removed
        :type user: User
        :raises TypeError: if user is not instance of User
        :raises AjaxException: if user is not in workgroup
        """
        if not isinstance(user, User):
            raise TypeError('invalid type of user')
        if user not in self.users:
            raise AjaxException('user %s not in workgroup %s' % (user.name, self.name), 409)
        self.users.remove(user)

    def add_manager(self, user):
        """
        Add manager to workgroup.

        :param user: user which has to be added as manager
        :type user: User
        :raises TypeError: if user is not instance of User
        :raises AjaxException: if user is already manager of workgroup
        """
        if not isinstance(user, User):
            raise TypeError('invalid type of user')
        if user in self.managers:
            raise AjaxException('user %s already manager of workgroup %s' % (user.name, self.name), 409)
        self.managers.append(user)

    def remove_manager(self, manager):
        """
        Remove manager from workgroup.

        :param manager: manager which has to be removed
        :type manager: User
        :raises TypeError: if manager is not instance of User
        :raises AjaxException: if manager is not manager of workgroup
        """
        if not isinstance(manager, User):
            raise TypeError('invalid type of manager')
        if manager not in self.managers:
            raise AjaxException('user %s not manager of workgroup %s' % (manager.name, self.name), 409)
        self.managers.remove(manager)

    def get_items(self, itemtype=None):
        """
        Get WorkgroupItem.

        :param itemtype: optional selector on the item type
        :returns: list of WorkgroupItem objects
        """
        if itemtype:
            return [item for item in self.items if item.itemtype == itemtype]
        else:
            return self.items

    def item_accessor(self, session, prefix="", json=False):
        """
        Returns a new ItemAccessor (with the parameters passed down) for the WorkgroupItems.
        """
        return ItemAccessor(
            table=WorkgroupItem, foreignName="workgroup_id", foreignValue=self.id,
            session=session, prefix=prefix, json=json
        )

class WorkgroupItem(Base):

    """
    A workgroup item represents the actual content of a workgroup. It is simply an object
    consisting of a type (e.g. file) and its content (e.g. the path of the file)
    """

    __tablename__ = 'workgroup_items'
    id = Column(Integer, primary_key=True)
    workgroup_id = Column(Integer, ForeignKey('workgroup.id'), nullable=False)
    itemtype = Column(UnicodeText)
    content = Column(UnicodeText)

    @staticmethod
    def get_by_id(session, gid):
        """
        Get a workgroup item by its id.

        :param session: current session of database
        :param gid: given id, which is looked for
        :returns: WorkgroupItem or None
        """
        return session.query(WorkgroupItem).filter_by(id=gid).first()

    @staticmethod
    def get(session, item):
        """
        Returns item, whichs id given. If the parameter item is an instance of WorkgroupItem object,
        it is directly returned.

        :param session: current session of database
        :param item: id to look for. If item is an instance of WorkgroupItem, item is directly
                     returned
        :returns: WorkgroupItem
        :raises AjaxException: if parameter item is invalid or no WorkgroupItem can be found
        """
        if item is None:
            raise AjaxException('invalid item', 400)
        if isinstance(item, WorkgroupItem):
            return item
        else:
            retval = WorkgroupItem.get_by_id(session, item)
            if retval is None:
                raise AjaxException('unknown item with id %i' % int(item), 404)  # TODO: this seems wrong
            return retval

    @staticmethod
    def create(session, workgroup, itemtype, content):
        """
        Create an workgroup item. The item is added to the database.

        :param session: current session of database
        :param workgroup: workgroup of the new item
        :param itemtype: type of the item
        :param content: content of the item
        """
        workgroup_item = WorkgroupItem(
            workgroup_id=workgroup.id,
            itemtype=itemtype,
            content=content
        )
        session.add(workgroup_item)
        session.commit()
        return workgroup_item

    def set_content(self, content):
        """
        Set the content of the item.

        :param content: new content
        """
        self.content = content

    def delete(self):
        """
        Delete the item. It is also deleted from the database.
        """
        session = object_session(self)
        session.delete(self)

    def get_workgroup(self):
        """
        Get the workgroup of the item.

        :returns: Workgroup
        """
        return self.workgroup
