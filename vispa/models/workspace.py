# -*- coding: utf-8 -*-

# imports
from datetime import datetime
from sqlalchemy import Column, schema
from sqlalchemy.types import Unicode, Integer, DateTime, Boolean, Text,\
    UnicodeText
from vispa.models import Base, insertion_safe
from vispa.models.user import User
from vispa import AjaxException

__all__ = ["Workspace"]


class Workspace(Base):

    __tablename__ = "workspace"
    id = Column(Integer, nullable=False, primary_key=True)
    user_id = Column(Integer, schema.ForeignKey("user.id",
                                                ondelete="CASCADE",
                                                onupdate="CASCADE"),
                     nullable=True)
    name = Column(Unicode(128), nullable=False)
    host = Column(UnicodeText, nullable=False)
    login = Column(UnicodeText, nullable=True, default=None)
    key = Column(Text, nullable=True, default=None)
    command = Column(Text, nullable=True, default=None)
    created = Column(DateTime, nullable=True, default=datetime.now)
    auto_connect = Column(Boolean, nullable=False, default=False)
    login_credentials = Column(Boolean, nullable=False, default=False)
    __table_args__ = (schema.UniqueConstraint(user_id, name),)

    KEYS = ["id", "user_id", "name", "host", "login", "command", "created",
            "auto_connect", "key"]

    def make_dict(self, keys=None):
        converters = {
            "id"          : int,
            "user_id"     : int,
            "auto_connect": bool
        }
        d = {}
        for key in keys or Workspace.KEYS:
            if not hasattr(self, key):
                continue
            converter = converters.get(key, str)
            if key == "key":
                if self.key:
                    value = self.key[:10]
                    if value:
                        value += "..."
                else:
                    value = None
            else:
                value = getattr(self, key)
            try:
                d[key] = None if value is None else converter(value)
            except:
                d[key] = value

        return d

    def has_access(self, user):
        user_id = user if not isinstance(user, User) else user.id
        return (self.user_id is None) or (self.user_id == user_id)

    def can_edit(self, user):
        user_id = user if not isinstance(user, User) else user.id
        return (self.user_id is not None) and (self.user_id == user_id)

    def is_valid(self):
        # TODO
        return True

    @staticmethod
    def get_by_id(db, id):
        workspace = db.query(Workspace).filter_by(id=id).first()
        if isinstance(workspace, Workspace):
            return workspace
        return None

    @staticmethod
    def get_user_workspaces(db, user):
        user_id = user if not isinstance(user, User) else user.id

        user_workspaces = db.query(Workspace).filter_by(user_id=user_id)
        public_workspaces = db.query(Workspace).filter_by(user_id=None)
        return user_workspaces.union(public_workspaces).all()

    @staticmethod
    def get_user_workspace_count(db, user):
        user_id = user if not isinstance(user, User) else user.id

        user_workspaces = db.query(Workspace).filter_by(user_id=user_id)
        public_workspaces = db.query(Workspace).filter_by(user_id=None)
        return user_workspaces.union(public_workspaces).count()

    @staticmethod
    def add(db, user, name, host, login, key=None, command=None, add=True):
        user_id = user if not isinstance(user, User) else user.id

        if not name:
            raise AjaxException("No workspace name given")
        safe, msg = insertion_safe(name, host, login, key, command)
        if not safe:
            raise AjaxException(msg)
        entries = {
            "user_id": user_id,
            "name"   : name,
            "host"   : host,
            "login"  : login,
            "key"    : key,
            "command": command
        }
        workspace = Workspace(**entries)
        if add:
            db.add(workspace)
            db.commit()
        return workspace

    @staticmethod
    def remove(db, id):
        workspace = Workspace.get_by_id(db, id)
        if not isinstance(workspace, Workspace):
            return False
        db.delete(workspace)
        db.commit()
        return True

    @staticmethod
    def update(db, id, **kwargs):
        workspace = Workspace.get_by_id(db, id)
        if not isinstance(workspace, Workspace):
            return False
        safe, key = insertion_safe(**kwargs)
        if not safe:
            return False
        # test loop
        forbidden_keys = ["id", "user_id", "created"]
        for key, value in kwargs.items():
            if not hasattr(workspace, key):
                return False
        # real loop
        for key, value in kwargs.items():
            if str(key) in forbidden_keys:
                continue
            setattr(workspace, key, value)
        db.commit()
        return True
