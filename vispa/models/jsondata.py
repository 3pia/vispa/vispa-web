# -*- coding: utf-8 -*-

# imports
from sqlalchemy import Column, schema
from sqlalchemy.types import Integer, Unicode, DateTime, UnicodeText
from datetime import datetime
from vispa.models import Base

_all__ = ["JSONData"]

class JSONData(Base):

    __tablename__ = "json_data"
    id = Column(Integer, primary_key=True)
    user_id = Column(
        Integer,
        schema.ForeignKey(
            "user.id",
            ondelete="CASCADE",
            onupdate="CASCADE"),
        nullable=False)
    workspace_id = Column(
        Integer,
        schema.ForeignKey(
            "workspace.id",
            ondelete="CASCADE",
            onupdate="CASCADE"),
        nullable=True)
    key = Column(Unicode(255), nullable=False)
    value = Column(UnicodeText(), nullable=False, default=u"{}")
    timestamp = Column(DateTime, nullable=False, default=datetime.now)
    __table_args__ = (schema.UniqueConstraint(user_id, workspace_id, key),)

    def get_info_data(self):
        return {
            "time": (self.timestamp - datetime(1970, 1, 1)).total_seconds(),
            "data": self.value,
        }

    @staticmethod
    def get_item(db, user_id, key, workspace_id, create=False):
        json_data = db.query(JSONData).filter_by(user_id=user_id, key=key).filter(
            JSONData.workspace_id == workspace_id).first()
        if not json_data:
            if create:
                json_data = JSONData(
                    user_id=user_id, key=key, workspace_id=workspace_id, value=u"{}")
                db.add(json_data)
                db.commit()
            else:
                return None
        return json_data

    @staticmethod
    def get_value(db, user_id, key, workspace_id):
        json_data = JSONData.get_item(db,
            user_id      =user_id,
            key          =key,
            workspace_id =workspace_id,
            create       =False
        )
        return json_data.get_info_data() if json_data else None

    @staticmethod
    def get_values_by_key(db, user_id, key=None):
        matches = db.query(JSONData).filter_by(user_id=user_id, key=key).all()
        return {item.workspace_id: item.get_info_data() for item in matches}

    @staticmethod
    def set_value(db, user_id, key, workspace_id, value):
        # entry already exists?
        json_data = JSONData.get_item(db,
            user_id      =user_id,
            key          =key,
            workspace_id =workspace_id,
            create       =True
        )
        json_data.value = value
        json_data.timestmap = datetime.now()
        db.commit()
