# -*- coding: utf-8 -*-

from datetime import datetime
import logging
import os
import random
import re
import uuid
import string

from passlib.hash import sha256_crypt
from sqlalchemy import Column, func
from sqlalchemy.orm.session import object_session
from sqlalchemy.types import Unicode, DateTime, Integer, Boolean, UnicodeText
from vispa import AjaxException
from vispa.models import Base
from vispa.models.role import Permission
import vispa
logger = logging.getLogger(__name__)

__all__ = ["User"]

class User(Base):

    INACTIVE = 0
    ACTIVE = 1

    MIN_PW_LENGTH = 8
    NAME_LENGTH = [6, 30]
    PASSWORD_RESET_DELAY = 30  # minutes
    NAME_CHARS = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890_-+'
    FORBIDDEN_NAMES = ['data', 'guest', 'global', 'user', 'delete', 'select', 'insert',
                       'update', 'drop', 'vispa-admin', 'root', 'vispa', 'admin']

    __tablename__ = 'user'
    id = Column(Integer, nullable=False, primary_key=True)
    name = Column(Unicode(128), nullable=False, unique=True)
    password = Column(UnicodeText(), nullable=False)
    email = Column(Unicode(128), nullable=False, unique=True)
    created = Column(DateTime, nullable=False, default=datetime.now)
    last_request = Column(DateTime, nullable=False, default=datetime.now)
    last_password_reset = Column(DateTime, nullable=True, default=None)
    status = Column(Integer, nullable=False, default=ACTIVE)
    serveradmin = Column(Boolean, nullable=False, default=False)
    hash = Column(UnicodeText())

    @staticmethod
    def get_by_id(session, uid):
        return session.query(User).filter_by(id=uid).first()

    @staticmethod
    def get_by_name(session, name):
        return session.query(User).filter_by(name=unicode(name)).first()

    @staticmethod
    def get_or_create_by_name(session, name, **kwargs):
        user = User.get_by_name(session, name)
        if user is None:
            user = User(name=name, **kwargs)
            session.add(user)
            try:
                session.commit()
            except:
                session.rollback()
            if user.status == User.ACTIVE:
                vispa.fire_callback("user.activate", user)
        return user

    @staticmethod
    def get_by_email(session, email):
        return session.query(User).filter(
            func.lower(User.email) == func.lower(email)).first()

    @staticmethod
    def get_by_hash(session, hash):
        return session.query(User).filter_by(hash=hash).first()

    @staticmethod
    def get(session, user):
        if user is None:
            raise Exception('Invalid user')
        if isinstance(user, User):
            return user
        else:
            retval = User.get_by_name(session, user)
            if retval is None:
                retval = User.get_by_email(session, user)
            if retval is None:
                retval = User.get_by_hash(session, user)
            if retval is None:
                logger.info('Unknown user %s' % user)
                raise Exception('Unknown user %s' % user)
            return retval

    @staticmethod
    def all(session):
        return session.query(User)

    @staticmethod
    def update_last_request(session, uid):
        user = User.get_by_id(session, uid)
        if user:
            user.last_request = datetime.now()
        try:
            session.commit()
        except:
            session.rollback()

    @staticmethod
    def is_active(session, uid):
        user = User.get_by_id(session, uid)
        return user.status == User.ACTIVE

    def active(self):
        return self.status == User.ACTIVE

    @staticmethod
    def login(session, username, password):
        if username is None:
            raise AjaxException('Invalid username')
        if password is None:
            raise AjaxException('Invalid password')

        user = User.get_by_name(session, username)
        if user is None:
            user = User.get_by_email(session, username)
        if user is None:
            logger.info('Unknown user %s' % username)
            raise AjaxException('Wrong username or password')

        if not sha256_crypt.verify(password, user.password):
            raise AjaxException('Wrong username or password')

        if user.status != User.ACTIVE:
            raise AjaxException("Account is not active")

        vispa.fire_callback("user.login", user)

        return user

    @staticmethod
    def guest_login(session):
        name = "Guest-" + str(uuid.uuid4())[:23]

        # check by name
        while User.get_by_name(session, name) is not None:
            name = "Guest-" + str(uuid.uuid4())[:23]

        max_users = vispa.config('user', 'registration.max_users', 0)
        if max_users > 0:
            count = session.query(User).count()
            if count >= max_users:
                return 'The maximum number of registered users is reached!'

        password = str(uuid.uuid4())

        # register the user?
        user = User(
            name=name,
            password=User.encrypt_password(password),
            email=name,
            hash=User.generate_hash(32),
            status=User.ACTIVE)

        session.add(user)
        try:
            session.commit()
        except:
            session.rollback()

        vispa.fire_callback("user.register", user)
        vispa.fire_callback("user.activate", user)

        return user, password

    @staticmethod
    def encrypt_password(password):
        return unicode(sha256_crypt.encrypt(password, rounds=vispa.config('user', 'hash.rounds', 100000)), 'utf-8')


    @staticmethod
    def register(db, name, email):
        # check by name
        if User.get_by_name(db, name) is not None:
            raise AjaxException('Username or mail address already exists!')
        # check by email
        if User.get_by_email(db, email) is not None:
            raise AjaxException('Username or mail address already exists!')

        # max users?
        max_users = vispa.config('user', 'registration.max_users', 0)
        if max_users > 0:
            count = db.query(User).count()
            if count >= max_users:
                return 'The maximum number of registered users is reached!'

        # email valid?
        valid_hosts = [
            x.lower() for x in vispa.config(
                'user',
                'registration.mail_hosts',
                [])]
        emailvalid = True
        emailparts = email.split("@")
        if len(emailparts) != 2:
            emailvalid = False
        else:
            host = emailparts[1].lower()
            if len(valid_hosts) and host not in valid_hosts:
                emailvalid = False
        if not emailvalid:
            raise AjaxException('Please use a valid email address!')

        # check the name
        # => length
        if len(name) < User.NAME_LENGTH[0] or len(name) > User.NAME_LENGTH[1]:
            raise AjaxException(
                'The length of the username should be between %d and %d chars!' %
                tuple(
                    User.NAME_LENGTH))
        # => chars
        for char in name:
            if char not in User.NAME_CHARS:
                raise AjaxException(
                    'The char \'%s\' is not allowed!' %
                    char)
        # => forbidden?
        if name.lower() in User.FORBIDDEN_NAMES:
            raise AjaxException('This username is forbidden!')
        for pat in vispa.config('user', 'registration.forbidden_names', []):
            if re.search(pat, name, re.IGNORECASE):
                raise AjaxException('This username is forbidden!')


        # The userdata passed all checks

        # register the user?
        password = User.encrypt_password(User.generate_hash(64))
        user = User(name=name, password=password, email=email,
                    hash=User.generate_hash(32), status=User.INACTIVE)

        # auto activate user?
        autoactive = vispa.config("web", 'registration.autoactive', True)
        if autoactive:
            user.status = User.ACTIVE

        db.add(user)

        try:
            db.commit()
        except:
            db.rollback()

        vispa.fire_callback("user.register", user)

        if autoactive:
            vispa.fire_callback("user.activate", user)

        return user

    @staticmethod
    def set_password(db, hash, password):
        user = db.query(User).filter_by(hash=hash).first()
        if not isinstance(user, User):
            raise AjaxException("Invalid hash!")
        if len(password) < User.MIN_PW_LENGTH:
            raise AjaxException("Please select a password with a minimum "
                                   "length of %s!" % User.MIN_PW_LENGTH)
        user.hash = None
        user.password = User.encrypt_password(password)
        if user.status == User.INACTIVE:
            user.status = User.ACTIVE
            vispa.fire_callback("user.activate", user)

        try:
            db.commit()
        except:
            db.rollback()

        vispa.fire_callback("user.set_password", user)

        return user

    @staticmethod
    def send_registration_mail(name, email, hash):
        # create the mail content
        subject = vispa.config("web", "registration.subject",
                               "Your registration at VISPA")
        url = vispa.config("web", "password_url",
                           "http://localhost:4282/vispa/password")

        link = os.path.join(url, hash)
        content = """Thanks for your registration, %s!

\n
To finish your registration, click on the link below:
%s
\n\n
Your Vispa-Team!""" % (name, link)

        vispa.send_mail(email, subject, content)

    @staticmethod
    def forgot_password(db, name_or_mail):
        user = User.get_by_name(db, name_or_mail)
        if not isinstance(user, User):
            user = User.get_by_email(db, name_or_mail.lower())
        if not user or user.status == User.INACTIVE:
            raise AjaxException("This is not an active user!")

        now = datetime.now()
        last = user.last_password_reset
        delay = User.PASSWORD_RESET_DELAY
        if last and (now - last).total_seconds() < delay * 60:
            msg = "You only can reset your password every %s minutes!" % delay
            raise AjaxException(msg)
        user.last_password_reset = now

        hash = User.generate_hash(32)
        user.hash = hash

        try:
            db.commit()
        except:
            db.rollback()

        # create the mail content
        subject = vispa.config("web", "forgot.subject", "Your VISPA password")
        url = vispa.config("web", "password_url")

        link = os.path.join(url, hash)
        content = """Hi %s!
\n
You requested a password change. To set a new one, click on the link below:
%s
\n\n
Your Vispa-Team!""" % (user.name, link)

        vispa.send_mail(user.email, subject, content)
        return link

    @staticmethod
    def generate_hash(length=10, chars=unicode(string.ascii_uppercase + string.ascii_lowercase + string.digits, 'utf-8')):
        #return u"12345"
        return u''.join(random.SystemRandom().choice(chars) for _ in range(length))

    def get_group_ids(self, use_cache=True):
        """
        Retrieve all groups ids the user is a member of. Membership does not include being a mere
        manager. Also retireves all groups of which the user is a indirect member.

        :param Boolean use_cache: Whether to use caced retrun values. Default to True.
        :return: All group ids the user is a member of.
        :rtype: list[int]
        """
        ret = getattr(self, "_cached_group_ids", None)
        if use_cache and ret is not None:
            return ret

        from vispa.models.group import Group_User_Assoc, Group_Group_Assoc, Group
        session = object_session(self)

        done = set()
        todo = set(v[0] for v in session.query(Group_User_Assoc.group_id).filter(
            Group_User_Assoc.status == Group_User_Assoc.CONFIRMED,
            Group_User_Assoc.user_id == self.id,
            Group_User_Assoc.group_id == Group.id,
            Group.status == Group.ACTIVE
        ).all())

        while todo:
            done |= todo
            todo = set(v[0] for v in session.query(Group_Group_Assoc.parent_group_id).filter(
                Group_Group_Assoc.status == Group_Group_Assoc.CONFIRMED,
                Group_Group_Assoc.child_group_id.in_(todo),
                Group_Group_Assoc.parent_group_id == Group.id,
                Group.status == Group.ACTIVE
            ).all()) - done

        done = list(done)
        setattr(self, "_cached_group_ids", done)
        return done

    def get_groups(self):
        """
        Retrieve all groups the user is a member of. Membership does not include being a mere
        manager. Also retireves all groups of which the user is a indirect member.

        :return: All groups the user is a member of.
        :rtype: list[Group]
        """
        from vispa.models.group import Group

        return object_session(self).query(Group).filter(Group.id.in_(self.get_group_ids())).all()

    def get_projects(self, managed=False, no_global_project=False):
        """
        Retrive all projects the user is a member of. Membership does not include being a mere
        manager.

        :param Boolean managed: Also return projects that are just managed be the user. Defaults to
                                *False*.
        :param Boolean no_global_project: Filter out the global project. Defaults to *False*.
        :return: All projects the user is a member of.
        :rtype: list[Project]
        """
        from vispa.models.project import Project, Project_User_Assoc, Project_Group_Assoc, \
            project_manager_association

        ftemp = [
            (
                Project_Group_Assoc,
                Project_Group_Assoc.group_id.in_(self.get_group_ids())
            ),
            (
                Project_User_Assoc,
                Project_User_Assoc.user_id == self.id
            ),
        ]

        if managed:
            ftemp.append((
                project_manager_association,
                project_manager_association.c.user_id == self.id
            ))

        q = object_session(self).query(Project).filter_by(status=Project.ACTIVE)

        if no_global_project:
            global_project_name = vispa.config("usermanagement", "global_project", None)
            if global_project_name is not None:
                q = q.filter(Project.name != global_project_name)

        q = q.outerjoin(*[t for (t, f) in ftemp])
        q = q.filter(reduce(lambda a, b: a | b, (f for (t, f) in ftemp)))

        return q.all()

    def has_all_permissions(self, project, permissions, ignoreInexistent=False):
        """
        :param Project project: The project on which to check the permissions.
        :param list[str] permissions: A list of permission names to check.
        :param Boolean ignoreInexistent: Whether to ignore permissions which do not exist (in DB).
        :return: Whether the user has all permissions on the given project.
        :rtype: Boolean
        """
        missing = self.filter_permissions(project, permissions)

        if missing and ignoreInexistent and \
           not object_session(self).query(Permission).filter(Permission.name.in_(missing)).first():
            # all remainig permissions do actually not exist (their definition)
            return True

        # no permissions were missing
        return not missing

    def has_permissions(self, project, permissions, inexistentDefaultTo=False):
        """
        :param Project project: The project on which to chek the permissions.
        :param list[str] permissions: A list of permissions to check.
        :param Boolean inexistentDefaultTo: What inexistent (in DB) permissions default to.
        :return: A list of whether the user has the permission (corresponding to the input list).
        :rtype: list[Boolean]
        """
        missing = self.filter_permissions(project, permissions)

        if inexistentDefaultTo:
            missing = [v[0] for v in object_session(self).query(Permission.name).filter(
                Permission.name.in_(missing)
            ).all()]

        return [perm not in missing for perm in permissions]

    def filter_permissions(self, project, permissions):
        """
        Retruns a set of requested permissions (by name) that the user does not have.

        :param Project project: The project to check the permissions on.
        :param permissions: A list of permission-names to check for.
        :type permissions: list[basestring]
        :return: The set of permission that the user does not have.
        :rtype: set[basestring]
        :raises TypeError: project is not a Project
        :raises TypeError: permissions is not a iterable of basestring
        """
        from vispa.models.project import Project, Project_Group_Assoc, project_user_assoc_role_association
        from vispa.models.group import Group_User_Assoc, Group_Group_Assoc
        from vispa.models.role import Permission, role_permission_association

        if not isinstance(project, Project):
            raise TypeError("Invalid type of project")
        if any(not isinstance(s, basestring) for s in permissions):
            raise TypeError("Invalid type of permission")

        # set of permission we have not "found" yet
        permissions = set(permissions)
        session = object_session(self)

        # test user directly
        permissions -= set(v[0] for v in session.query(Permission.name).filter(
            project_user_assoc_role_association.c.user_id == self.id,
            project_user_assoc_role_association.c.project_id == project.id,
            project_user_assoc_role_association.c.role_id == role_permission_association.c.role_id,
            Permission.id == role_permission_association.c.permission_id,
        ).all())
        if not permissions:
            return permissions

        # active member of
        member = set(v[0] for v in session.query(Group_User_Assoc.group_id).filter_by(
            user_id=self.id,
            status=Group_User_Assoc.CONFIRMED
        ).all())

        for assoc in session.query(Project_Group_Assoc).filter_by(project_id=project.id).all():
            perms = set(p.name for p in assoc.get_permissions())

            # no relevant permissions in this group
            if not (permissions & perms):
                continue

            # get all sub-groups
            done = set()
            todo = set([assoc.group_id])
            while todo:
                done |= todo
                # if we are in one already there is no need to continue
                if member & done:
                    break
                todo = set(v[0] for v in session.query(Group_Group_Assoc.child_group_id).filter(
                    Group_Group_Assoc.status == Group_Group_Assoc.CONFIRMED,
                    Group_Group_Assoc.parent_group_id.in_(todo)
                ).all()) - done

            # are we a member of it or any of its childs
            if member & done:
                permissions -= perms
            if not permissions:
                return permissions

        return permissions

    def get_workgroups(self, member=True, manager=True):
        """
        Get all workgroups to which the user has the given affiliation.

        :param Boolean member: If true, include the workgroups in which the user is a member.
        :param Boolean manager: If true, include the workgroups of which the user is a manager.
        :return: A list of workgroups (without duplicates).

        """
        return list(
            set(self.workgroups if member else []) |
            set(self.managed_workgroups if manager else [])
        )
