# -*- coding: utf-8 -*-

from datetime import datetime
from sqlalchemy import Column, Table, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.orm.session import object_session
from sqlalchemy.types import Unicode, DateTime, Integer
from vispa import AjaxException
from vispa.models import Base
import vispa

__all__ = ["Permission", "Role"]

role_permission_association = Table('role_permission_association', Base.metadata,
                                    Column('role_id', Integer, ForeignKey('role.id')),
                                    Column('permission_id', Integer, ForeignKey('permission.id')))

class Permission(Base):

    """
    A Permission object is self-explanatory a permission for a user. It is only characerized by its
    name, which explains its meaning.
    """

    __tablename__ = 'permission'
    id = Column(Integer, nullable=False, primary_key=True)
    name = Column(Unicode(255), nullable=False, unique=True)
    created = Column(DateTime, nullable=False, default=datetime.now)

    def to_dict(self):
        return {
            "id": self.id,
            "name": self.name,
            "created": self.created.isoformat()
        }

    @staticmethod
    def get_by_id(session, gid):
        """
        Get a permission by its id.

        :param session: current session of database
        :param gid: given id, which is looked for
        :returns: Permission or None
        """
        return session.query(Permission).filter_by(id=gid).first()

    @staticmethod
    def get_by_name(session, name):
        """
        Get a permission by its name.

        :param session: current session of database
        :param name: given name, which is looked for
        :returns: Permission or None
        """
        return session.query(Permission).filter_by(name=unicode(name)).first()

    @staticmethod
    def get(session, permission):
        """
        Get a permissionby name. If permission is already a Permission object, it is directly
        returned.

        :param session: current session of database
        :param permission: name to look for. if type of permission is Permission, it is returned
        :returns: Permission
        :raises AjaxException: if permission parameter is invalid or no permission can be found
        """
        if permission is None:
            raise AjaxException("invalid permission", 400)
        if isinstance(permission, Permission):
            return permission
        else:
            retval = Permission.get_by_name(session, permission)
            if retval is None:
                raise AjaxException("permission %s not found" % permission, 404)
            return retval

    @staticmethod
    def all(session):
        """
        Get all existing permissions.

        :param session: current session of database
        :returns: list of Permission objects
        """
        return session.query(Permission)

    @staticmethod
    def create(session, name):
        """
        Create new permission.

        :param session: current session of database
        :param name: name of the permission
        :raises AjaxException: if name is invalid or already in use
        """
        # name valid and doesn't already exists?
        if not name:
            raise AjaxException("invalid name", 400)
        if Permission.get_by_name(session, name) is not None:
            raise AjaxException("permission %s already exists" % name, 409)
        # name valid -> create role
        permission = Permission(name=name)
        session.add(permission)
        session.commit()
        return permission

    @staticmethod
    def get_or_create_by_name(session, name):
        """
        Get or create a permission by name.

        :param session: current session of database
        :param name: name of the permission
        :returns: Permission
        """
        permission = Permission.get_by_name(session, name)
        if permission is None:
            Permission.create(session, name)
            permission = Permission.get_by_name(session, name)
        return permission

    def delete(self):
        """
        Delete permission. It is also deleted from the database.
        """
        session = object_session(self)
        session.query(role_permission_association).filter_by(permission_id=self.id)\
            .delete(synchronize_session=False)
        session.delete(self)

    def rename(self, newname):
        """
        Rename Permission.

        :param newname: new name of the permission
        :raises AjaxException: if newname is already in use
        """
        session = object_session(self)
        if Permission.get_by_name(session, newname) is not None:
            raise AjaxException("permission %s already exists" % newname, 409)
        self.name = newname

class Role(Base):

    """
    A Role is a collection of permissions (Permission objects).
    """

    __tablename__ = 'role'
    id = Column(Integer, nullable=False, primary_key=True)
    name = Column(Unicode(255), nullable=False, unique=True)
    created = Column(DateTime, nullable=False, default=datetime.now)
    permissions = relationship("Permission", secondary=role_permission_association)

    def to_dict(self):
        return {
            "id": self.id,
            "name": self.name,
            "created": self.created.isoformat(),
        }

    @staticmethod
    def get_by_id(session, gid):
        """
        Get a role by its id.

        :param session: current session of database
        :param gid: given id, which is looked for
        :returns: Role or None
        """
        return session.query(Role).filter_by(id=gid).first()

    @staticmethod
    def get_by_name(session, name):
        """
        Get a role by its name.

        :param session: current session of database
        :param name: given name, which is looked for
        :returns: Role or None
        """
        return session.query(Role).filter_by(name=unicode(name)).first()

    @staticmethod
    def get(session, role):
        """
        Get a role by name. If role is already a Role object, it is directly returned.

        :param session: current session of database
        :param role: name to look for. if type of role is Role, role is returned
        :returns: Role
        :raises AjaxException: if role parameter is invalid or no role can be found
        """
        if role is None:
            raise AjaxException("invalid role", 400)
        if isinstance(role, Role):
            return role
        else:
            retval = Role.get_by_name(session, role)
            if retval is None:
                raise AjaxException("role %s not found" % role, 404)
            return retval

    @staticmethod
    def all(session):
        """
        Get all existing permissions.

        :param session: current session of database
        :returns: list of Role objects
        """
        return session.query(Role)

    @staticmethod
    def create(session, name):
        """
        Create new role.

        :param session: current session of database
        :param name: name of the role
        :raises AjaxException: if name is invalid or already in use
        """
        # name valid and doesn't already exists?
        if not name:
            raise AjaxException("invalid name", 400)
        if Role.get_by_name(session, name) is not None:
            raise AjaxException("role %s already exists" % name, 409)
        # name valid -> create role
        role = Role(name=name)
        session.add(role)
        session.commit()
        return role

    @staticmethod
    def get_or_create_by_name(session, name):
        """
        Get or create a role by name.

        :param session: current session of database
        :param name: name of the role
        :returns: Role
        """
        role = Role.get_by_name(session, name)
        if role is None:
            Role.create(session, name)
            role = Role.get_by_name(session, name)
        return role

    def delete(self):
        """
        Delete role. It is also deleted from the database.
        """
        session = object_session(self)
        session.query(vispa.models.project.project_user_assoc_role_association)\
            .filter_by(role_id=self.id).delete(synchronize_session=False)
        session.query(vispa.models.project.project_group_assoc_role_association)\
            .filter_by(role_id=self.id).delete(synchronize_session=False)
        session.query(role_permission_association)\
            .filter_by(role_id=self.id).delete(synchronize_session=False)
        session.delete(self)

    def rename(self, newname):
        """
        Rename role.

        :param newname: new name of the role
        :raises AjaxException: if newname is already in use
        """
        session = object_session(self)
        if Role.get_by_name(session, newname) is not None:
            raise AjaxException("role %s already exists" % newname, 409)
        self.name = newname

    def add_permissions(self, permissions):
        """
        Add a list of permissions to the role.

        :param permissions: list of permissions
        :type permissions: list of Permission objects
        :raises AjaxTypeError: if type of permissions is invalid
        """
        if not isinstance(permissions, list):
            raise TypeError('Invalid type of permissions')
        for permission in permissions:
            if not isinstance(permission, Permission):
                raise TypeError('Invalid type of permission')
        self.permissions.extend(permissions)

    def set_permissions(self, permissions):
        """
        Sets the permissions of a role.

        :param permissions: list of permissions
        :type permissions: list of Permission objects
        :raises AjaxTypeError: if type of permissions is invalid
        """
        if not isinstance(permissions, list):
            raise TypeError('Invalid type of permissions')
        for permission in permissions:
            if not isinstance(permission, Permission):
                raise TypeError('Invalid type of permission')
        self.permissions = permissions

    def get_permissions(self):
        return self.permissions

    def add_permission(self, permission):
        self.add_permissions([permission])

    def remove_permssion(self, permission):
        if not isinstance(permission, Permission):
            raise TypeError('Invalid type of permission')
        if permission not in self.permissions:
            raise AjaxException('permission %s not in role %s' % (permission.name, self.name), 409)
        self.permission.remove(permission)
