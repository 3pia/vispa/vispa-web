from os.path import abspath, dirname
import logging

from alembic.script import ScriptDirectory
from alembic.config import Config
from alembic.environment import EnvironmentContext

import vispa.models

logger = logging.getLogger(__name__)


def migrate(db, revision="head"):
    script_location = dirname(abspath(__file__))
    logger.debug("script_location: %s" % script_location)
    config = Config()
    config.set_main_option("script_location", script_location)
    script = ScriptDirectory.from_config(config)

    def upgrade(rev, context):
        return script._upgrade_revs(revision, rev)

    with EnvironmentContext(
        config,
        script,
        fn=upgrade,
        destination_rev=revision
    ) as context:
        context.configure(
                connection=db,
                target_metadata=vispa.models.Base.metadata
                )
        try:
            with context.begin_transaction():
                context.run_migrations()
        except:
            vispa.log_exception()

