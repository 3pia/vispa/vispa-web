"""drop statistics

Revision ID: 3a3f32feff20
Revises: 468c7626c4c2
Create Date: 2015-11-19 11:27:24.861433

"""

# revision identifiers, used by Alembic.
revision = '3a3f32feff20'
down_revision = '468c7626c4c2'

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

def upgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('page_statistics')
    op.drop_table('access_statistics')
    ### end Alembic commands ###


def downgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.create_table('access_statistics',
    sa.Column('user_ip', sa.Unicode(length=60), nullable=False),
    sa.Column('user_agent', sa.Unicode(length=200), nullable=False),
    sa.Column('date', sa.Date(), nullable=False),
    sa.Column('pis', sa.Integer(), nullable=False),
    sa.PrimaryKeyConstraint('user_ip', 'user_agent', 'date')
    )
    op.create_table('page_statistics',
    sa.Column('page', sa.Unicode(length=128), nullable=False),
    sa.Column('date', sa.Date(), nullable=False),
    sa.Column('pis', sa.Integer(), nullable=False),
    sa.PrimaryKeyConstraint('page', 'date')
    )



    ### end Alembic commands ###
