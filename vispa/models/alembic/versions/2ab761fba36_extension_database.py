"""Extension Database

Revision ID: 2ab761fba36
Revises: 17d572bdfd2d
Create Date: 2015-07-13 15:37:11.904666

"""

# revision identifiers, used by Alembic.
revision = '2ab761fba36'
down_revision = '17d572bdfd2d'

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

def upgrade():
    op.create_table('extension_database',
                    sa.Column('extension_name', sa.Unicode(length=128), nullable=False),
                    sa.Column('data', sa.Text(), nullable=True),
                    sa.Column('timestamp', sa.DateTime(), nullable=False),
                    sa.PrimaryKeyConstraint('extension_name'),
                    )

def downgrade():
    op.drop_table('extension_database')
