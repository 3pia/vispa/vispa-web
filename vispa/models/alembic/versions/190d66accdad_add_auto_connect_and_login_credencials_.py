"""add auto_connect and login_credencials to workspace

Revision ID: 190d66accdad
Revises: 3e4b5288af61
Create Date: 2014-03-30 14:28:14.618331

"""
from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = '190d66accdad'
down_revision = '3e4b5288af61'


def upgrade():
    with op.batch_alter_table("workspace") as batch_op:
        batch_op.add_column(
            sa.Column(
                'login_credencials',
                sa.Boolean(),
                nullable=True))
        batch_op.add_column(
            sa.Column(
                'auto_connect',
                sa.Boolean(),
                nullable=True))


def downgrade():
    with op.batch_alter_table("workspace") as batch_op:
        batch_op.drop_column('login_credencials')
        batch_op.drop_column('auto_connect')
