"""add group

Revision ID: 2c7093ed3ed6
Revises: 350433110367
Create Date: 2014-05-13 17:44:51.149159

"""
from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = '2c7093ed3ed6'
down_revision = '350433110367'


def upgrade():
    op.create_table(
        'group',
        sa.Column(
            'id',
            sa.Integer(),
            nullable=False),
        sa.Column(
            'name',
            sa.Unicode(
                length=32),
            nullable=False),
        sa.Column(
            'created',
            sa.DateTime(),
            nullable=False),
        sa.PrimaryKeyConstraint('id'),
        sa.UniqueConstraint('name'))
    op.create_table(
        'grouplist', sa.Column(
            'group_id', sa.Integer(), nullable=True), sa.Column(
            'user_id', sa.Integer(), nullable=True), sa.ForeignKeyConstraint(
                ['group_id'], ['group.id'], ), sa.ForeignKeyConstraint(
                    ['user_id'], ['user.id'], ))


def downgrade():
    op.drop_table('grouplist')
    op.drop_table('group')
