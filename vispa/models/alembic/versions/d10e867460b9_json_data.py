"""JSON data

Revision ID: d10e867460b9
Revises: 459e6ec6f40d
Create Date: 2016-05-11 13:49:57.723307

"""

# revision identifiers, used by Alembic.
revision = 'd10e867460b9'
down_revision = '459e6ec6f40d'

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

def upgrade():
    op.create_table('json_data',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('user_id', sa.Integer(), nullable=False),
    sa.Column('workspace_id', sa.Integer(), nullable=True),
    sa.Column('key', sa.Unicode(length=255), nullable=False),
    sa.Column('value', sa.UnicodeText(), nullable=False),
    sa.Column('timestamp', sa.DateTime(), nullable=False),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], onupdate='CASCADE', ondelete='CASCADE'),
    sa.ForeignKeyConstraint(['workspace_id'], ['workspace.id'], onupdate='CASCADE', ondelete='CASCADE'),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('user_id', 'workspace_id', 'key')
    )
    op.drop_table('extension_preference')
    op.drop_table('vispa_shortcuts')
    op.drop_table('vispa_preference')
    op.drop_table('workspace_connection')
    op.drop_table('extension_shortcuts')
    op.drop_table('workspace_state')


def downgrade():
    op.create_table('workspace_state',
    sa.Column('workspace_id', sa.Integer(), nullable=False),
    sa.Column('user_id', sa.Integer(), nullable=False),
    sa.Column('state', sa.UnicodeText(), nullable=True),
    sa.Column('timestamp', sa.DateTime(), nullable=True),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], onupdate='CASCADE', ondelete='CASCADE'),
    sa.ForeignKeyConstraint(['workspace_id'], ['workspace.id'], onupdate='CASCADE', ondelete='CASCADE'),
    sa.PrimaryKeyConstraint('workspace_id', 'user_id'),
    )
    op.create_table('extension_shortcuts',
    sa.Column('user_id', sa.Integer(), nullable=False),
    sa.Column('key', sa.Unicode(length=255), nullable=False),
    sa.Column('value', sa.UnicodeText(), nullable=False),
    sa.Column('timestamp', sa.DateTime(), nullable=False),
    sa.Column('created', sa.DateTime(), nullable=False),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], onupdate='CASCADE', ondelete='CASCADE'),
    sa.PrimaryKeyConstraint('user_id', 'key'),
    )
    op.create_table('workspace_connection',
    sa.Column('workspace_id', sa.Integer(), nullable=False),
    sa.Column('user_id', sa.Integer(), nullable=False),
    sa.Column('host', sa.UnicodeText(), nullable=False),
    sa.Column('tempdir', sa.UnicodeText(), nullable=True),
    sa.Column('timestamp', sa.DateTime(), nullable=True),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], onupdate='CASCADE', ondelete='CASCADE'),
    sa.ForeignKeyConstraint(['workspace_id'], ['workspace.id'], onupdate='CASCADE', ondelete='CASCADE'),
    sa.PrimaryKeyConstraint('workspace_id', 'user_id'),
    )
    op.create_table('vispa_preference',
    sa.Column('user_id', sa.Integer(), nullable=False),
    sa.Column('section', sa.Unicode(length=255), nullable=False),
    sa.Column('value', sa.UnicodeText(), nullable=False),
    sa.Column('timestamp', sa.DateTime(), nullable=False),
    sa.Column('created', sa.DateTime(), nullable=False),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], onupdate='CASCADE', ondelete='CASCADE'),
    sa.PrimaryKeyConstraint('user_id', 'section'),
    )
    op.create_table('vispa_shortcuts',
    sa.Column('user_id', sa.Integer(), nullable=False),
    sa.Column('key', sa.Unicode(length=255), nullable=False),
    sa.Column('value', sa.UnicodeText(), nullable=False),
    sa.Column('timestamp', sa.DateTime(), nullable=False),
    sa.Column('created', sa.DateTime(), nullable=False),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], onupdate='CASCADE', ondelete='CASCADE'),
    sa.PrimaryKeyConstraint('user_id', 'key'),
    )
    op.create_table('extension_preference',
    sa.Column('user_id', sa.Integer(), nullable=False),
    sa.Column('key', sa.Unicode(length=255), nullable=False),
    sa.Column('value', sa.UnicodeText(), nullable=False),
    sa.Column('timestamp', sa.DateTime(), nullable=False),
    sa.Column('created', sa.DateTime(), nullable=False),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], onupdate='CASCADE', ondelete='CASCADE'),
    sa.PrimaryKeyConstraint('user_id', 'key'),
    )
    op.drop_table('json_data')
