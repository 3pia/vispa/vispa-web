"""remove workspace port

Revision ID: 2a4a3fd96ecf
Revises: 190d66accdad
Create Date: 2014-04-02 16:26:51.748774

"""
from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = '2a4a3fd96ecf'
down_revision = '190d66accdad'


def upgrade():
    with op.batch_alter_table("workspace") as batch_op:
        batch_op.drop_column('port')


def downgrade():
    with op.batch_alter_table("workspace") as batch_op:
        batch_op.add_column(sa.Column('port', sa.Integer(), nullable=False))
