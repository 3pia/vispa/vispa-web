"""nullable user id in workspace

Revision ID: 5887b0150ba1
Revises: 43c7de99ad41
Create Date: 2014-04-04 15:31:13.469672

"""
from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = '5887b0150ba1'
down_revision = '43c7de99ad41'


def upgrade():
    with op.batch_alter_table("workspace") as batch_op:
        batch_op.alter_column(
            'user_id',
            existing_type=sa.Integer(),
            nullable=True)


def downgrade():
    with op.batch_alter_table("workspace") as batch_op:
        batch_op.alter_column('user_id',
                              existing_type=sa.Integer(),
                              nullable=False)
