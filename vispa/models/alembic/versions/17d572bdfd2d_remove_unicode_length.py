"""remove unicode length

Revision ID: 17d572bdfd2d
Revises: 2c7093ed3ed6
Create Date: 2014-09-12 11:59:28.725938

"""
from alembic import op
from sqlalchemy.types import Unicode, UnicodeText
from sqlalchemy.sql.elements import quoted_name


# revision identifiers, used by Alembic.
revision = '17d572bdfd2d'
down_revision = '2c7093ed3ed6'


def upgrade():
    with op.batch_alter_table("user") as batch_op:
        batch_op.alter_column(
            "name",
            type_=Unicode(128),
            existing_type=Unicode(30),
            existing_nullable=False)
        batch_op.alter_column(
            "password",
            type_=UnicodeText,
            existing_type=Unicode(128),
            existing_nullable=False)
        batch_op.alter_column(
            "email",
            type_=Unicode(128),
            existing_type=Unicode(100),
            existing_nullable=False)
        batch_op.alter_column(
            "hash",
            type_=UnicodeText,
            existing_type=Unicode(100),
            existing_nullable=True)

    with op.batch_alter_table("workspace") as batch_op:
        batch_op.alter_column(
            "name",
            type_=Unicode(128),
            existing_type=Unicode(100),
            existing_nullable=False)
        batch_op.alter_column(
            "host",
            type_=UnicodeText,
            existing_type=Unicode(100),
            existing_nullable=False)
        batch_op.alter_column(
            "login",
            type_=UnicodeText,
            existing_type=Unicode(100),
            existing_nullable=True)

    with op.batch_alter_table("vispa_preference") as batch_op:
        batch_op.alter_column(
            "section",
            type_=Unicode(128),
            existing_type=Unicode(64),
            existing_nullable=False)
        batch_op.alter_column(
            "value",
            type_=UnicodeText,
            existing_type=Unicode(300),
            existing_nullable=False)

    with op.batch_alter_table("extension_preference") as batch_op:
        batch_op.alter_column(
            quoted_name("key", True),
            type_=Unicode(128),
            existing_type=Unicode(64),
            existing_nullable=False)
        batch_op.alter_column(
            "value",
            type_=UnicodeText,
            existing_type=Unicode(300),
            existing_nullable=False)

    with op.batch_alter_table("vispa_shortcuts") as batch_op:
        batch_op.alter_column(
            quoted_name("key", True),
            type_=Unicode(128),
            existing_type=Unicode(64),
            existing_nullable=False)
        batch_op.alter_column(
            "value",
            type_=UnicodeText,
            existing_type=Unicode(300),
            existing_nullable=False)

    with op.batch_alter_table("extension_shortcuts") as batch_op:
        batch_op.alter_column(
            quoted_name("key", True),
            type_=Unicode(128),
            existing_type=Unicode(64),
            existing_nullable=False)
        batch_op.alter_column(
            "value",
            type_=UnicodeText,
            existing_type=Unicode(300),
            existing_nullable=False)


def downgrade():
    with op.batch_alter_table("user") as batch_op:
        batch_op.alter_column(
            "name",
            type_=Unicode(30),
            existing_type=Unicode(128),
            existing_nullable=False)
        batch_op.alter_column(
            "password",
            type_=Unicode(128),
            existing_type=UnicodeText,
            existing_nullable=False)
        batch_op.alter_column(
            "email",
            type_=Unicode(100),
            existing_type=Unicode(128),
            existing_nullable=False)
        batch_op.alter_column(
            "hash",
            type_=Unicode(100),
            existing_type=UnicodeText,
            existing_nullable=True)

    with op.batch_alter_table("workspace") as batch_op:
        batch_op.alter_column(
            "name",
            type_=Unicode(100),
            existing_type=Unicode(128),
            existing_nullable=False)
        batch_op.alter_column(
            "host",
            type_=Unicode(100),
            existing_type=UnicodeText,
            existing_nullable=False)
        batch_op.alter_column(
            "login",
            type_=Unicode(100),
            existing_type=UnicodeText,
            existing_nullable=True)

    with op.batch_alter_table("vispa_preference") as batch_op:
        batch_op.alter_column(
            "section",
            type_=Unicode(64),
            existing_type=Unicode(128),
            existing_nullable=False)
        batch_op.alter_column(
            "value",
            type_=Unicode(300),
            existing_type=UnicodeText,
            existing_nullable=False)

    with op.batch_alter_table("extension_preference") as batch_op:
        batch_op.alter_column(
            quoted_name("key", True),
            type_=Unicode(64),
            existing_type=Unicode(128),
            existing_nullable=False)
        batch_op.alter_column(
            "value",
            type_=Unicode(300),
            existing_type=UnicodeText,
            existing_nullable=False)

    with op.batch_alter_table("vispa_shortcuts") as batch_op:
        batch_op.alter_column(
            quoted_name("key", True),
            type_=Unicode(64),
            existing_type=Unicode(128),
            existing_nullable=False)
        batch_op.alter_column(
            "value",
            type_=Unicode(300),
            existing_type=UnicodeText,
            existing_nullable=False)

    with op.batch_alter_table("extension_shortcuts") as batch_op:
        batch_op.alter_column(
            quoted_name("key", True),
            type_=Unicode(64),
            existing_type=Unicode(128),
            existing_nullable=False)
        batch_op.alter_column(
            "value",
            type_=Unicode(300),
            existing_type=UnicodeText,
            existing_nullable=False)
