"""init

Revision ID: 3e4b5288af61
Revises: None
Create Date: 2014-03-27 16:21:23.472862

"""
from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = '3e4b5288af61'
down_revision = None


def upgrade():

    op.create_table('user',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('name', sa.Unicode(length=30), nullable=False),
                    sa.Column(
                        'password',
                        sa.Unicode(
                            length=128),
                        nullable=False),
                    sa.Column('email', sa.Unicode(length=100), nullable=False),
                    sa.Column('created', sa.DateTime(), nullable=False),
                    sa.Column('last_request', sa.DateTime(), nullable=False),
                    sa.Column('status', sa.Integer(), nullable=False),
                    sa.Column('hash', sa.Unicode(length=100), nullable=True),
                    sa.PrimaryKeyConstraint('id'),
                    sa.UniqueConstraint('email'),
                    sa.UniqueConstraint('name')
                    )
    op.create_table('access_statistics',
                    sa.Column(
                        'user_ip',
                        sa.Unicode(
                            length=60),
                        nullable=False),
                    sa.Column(
                        'user_agent',
                        sa.Unicode(
                            length=200),
                        nullable=False),
                    sa.Column('date', sa.Date(), nullable=False),
                    sa.Column('pis', sa.Integer(), nullable=False),
                    )
    op.create_table('page_statistics',
                    sa.Column('page', sa.Unicode(length=128), nullable=False),
                    sa.Column('date', sa.Date(), nullable=False),
                    sa.Column('pis', sa.Integer(), nullable=False),
                    )
    op.create_table('workspace',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('user_id', sa.Integer(), nullable=False),
                    sa.Column('name', sa.Unicode(length=100), nullable=False),
                    sa.Column('host', sa.Text(), nullable=False),
                    sa.Column('port', sa.Integer(), nullable=False),
                    sa.Column('login', sa.Unicode(length=100), nullable=True),
                    sa.Column('key', sa.Text(), nullable=True),
                    sa.Column('command', sa.Text(), nullable=True),
                    sa.Column('created', sa.DateTime(), nullable=True),
                    sa.ForeignKeyConstraint(
                        ['user_id'],
                        ['user.id'],
                        onupdate='CASCADE',
                        ondelete='CASCADE'),
                    sa.PrimaryKeyConstraint('id'),
                    sa.UniqueConstraint('user_id', 'name')
                    )
    op.create_table('extension_shortcuts',
                    sa.Column('user_id', sa.Integer(), nullable=False),
                    sa.Column('key', sa.Unicode(length=64), nullable=False),
                    sa.Column('value', sa.Text(), nullable=False),
                    sa.Column('timestamp', sa.DateTime(), nullable=False),
                    sa.Column('created', sa.DateTime(), nullable=False),
                    sa.ForeignKeyConstraint(
                        ['user_id'],
                        ['user.id'],
                        onupdate='CASCADE',
                        ondelete='CASCADE'),
                    sa.PrimaryKeyConstraint('user_id', 'key')
                    )
    op.create_table('vispa_preference',
                    sa.Column('user_id', sa.Integer(), nullable=False),
                    sa.Column(
                        'section',
                        sa.Unicode(
                            length=64),
                        nullable=False),
                    sa.Column('value', sa.Text(), nullable=False),
                    sa.Column('timestamp', sa.DateTime(), nullable=False),
                    sa.Column('created', sa.DateTime(), nullable=False),
                    sa.ForeignKeyConstraint(
                        ['user_id'],
                        ['user.id'],
                        onupdate='CASCADE',
                        ondelete='CASCADE'),
                    sa.PrimaryKeyConstraint('user_id', 'section')
                    )
    op.create_table('vispa_shortcuts',
                    sa.Column('user_id', sa.Integer(), nullable=False),
                    sa.Column('key', sa.Unicode(length=64), nullable=False),
                    sa.Column('value', sa.Unicode(length=300), nullable=False),
                    sa.Column('timestamp', sa.DateTime(), nullable=False),
                    sa.Column('created', sa.DateTime(), nullable=False),
                    sa.ForeignKeyConstraint(
                        ['user_id'],
                        ['user.id'],
                        onupdate='CASCADE',
                        ondelete='CASCADE'),
                    sa.PrimaryKeyConstraint('user_id', 'key')
                    )
    op.create_table('extension_preference',
                    sa.Column('user_id', sa.Integer(), nullable=False),
                    sa.Column('key', sa.Unicode(length=64), nullable=False),
                    sa.Column('value', sa.Text(), nullable=False),
                    sa.Column('timestamp', sa.DateTime(), nullable=False),
                    sa.Column('created', sa.DateTime(), nullable=False),
                    sa.ForeignKeyConstraint(
                        ['user_id'],
                        ['user.id'],
                        onupdate='CASCADE',
                        ondelete='CASCADE'),
                    sa.PrimaryKeyConstraint('user_id', 'key')
                    )
    op.create_table('workspace_state',
                    sa.Column('workspace_id', sa.Integer(), nullable=False),
                    sa.Column('user_id', sa.Integer(), nullable=False),
                    sa.Column('state', sa.Text(), nullable=True),
                    sa.Column('timestamp', sa.DateTime(), nullable=True),
                    sa.ForeignKeyConstraint(
                        ['user_id'],
                        ['user.id'],
                        onupdate='CASCADE',
                        ondelete='CASCADE'),
                    sa.ForeignKeyConstraint(
                        ['workspace_id'],
                        ['workspace.id'],
                        onupdate='CASCADE',
                        ondelete='CASCADE'),
                    sa.PrimaryKeyConstraint('workspace_id', 'user_id')
                    )
    op.create_table('workspace_connection',
                    sa.Column('workspace_id', sa.Integer(), nullable=False),
                    sa.Column('user_id', sa.Integer(), nullable=False),
                    sa.Column('host', sa.Text(), nullable=False),
                    sa.Column('tempdir', sa.Text(), nullable=True),
                    sa.Column('timestamp', sa.DateTime(), nullable=True),
                    sa.ForeignKeyConstraint(
                        ['user_id'],
                        ['user.id'],
                        onupdate='CASCADE',
                        ondelete='CASCADE'),
                    sa.ForeignKeyConstraint(
                        ['workspace_id'],
                        ['workspace.id'],
                        onupdate='CASCADE',
                        ondelete='CASCADE'),
                    sa.PrimaryKeyConstraint('workspace_id', 'user_id')
                    )


def downgrade():

    op.drop_table('workspace_connection')
    op.drop_table('workspace_state')
    op.drop_table('extension_preference')
    op.drop_table('vispa_shortcuts')
    op.drop_table('vispa_preference')
    op.drop_table('extension_shortcuts')
    op.drop_table('workspace')
    op.drop_table('page_statistics')
    op.drop_table('access_statistics')
    op.drop_table('user')
