"""fix credentials

Revision ID: 43c7de99ad41
Revises: 2a4a3fd96ecf
Create Date: 2014-04-02 16:32:42.374051

"""
from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = '43c7de99ad41'
down_revision = '2a4a3fd96ecf'


def upgrade():
    with op.batch_alter_table("workspace") as batch_op:
        batch_op.alter_column(
            'login_credencials',
            new_column_name='login_credentials',
            existing_type=sa.Boolean())


def downgrade():
    with op.batch_alter_table("workspace") as batch_op:
        batch_op.alter_column(
            'login_credentials',
            new_column_name='login_credencials',
            existing_type=sa.Boolean())
