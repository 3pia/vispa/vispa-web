"""add last password stamp to user table

Revision ID: 350433110367
Revises: 5887b0150ba1
Create Date: 2014-04-11 10:05:49.919687

"""
from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = '350433110367'
down_revision = '5887b0150ba1'


def upgrade():
    with op.batch_alter_table("user") as batch_op:
        batch_op.add_column(
            sa.Column(
                'last_password_reset',
                sa.DateTime(),
                nullable=True))


def downgrade():
    with op.batch_alter_table("user") as batch_op:
        batch_op.drop_column('user', 'last_password_reset')
