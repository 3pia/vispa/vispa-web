from __future__ import with_statement
import os
import sys
from alembic import context
from sqlalchemy import create_engine, pool
from logging.config import fileConfig
from logging import basicConfig

url = context.config.get_section_option('database', 'sqlalchemy.url')

try:
    fileConfig(context.config.config_file_name)
except:
    basicConfig()

try:
    from vispa import models
except:
    vispa_path = os.path.abspath(__file__)
    vispa_path = os.path.dirname(vispa_path)
    vispa_path = os.path.dirname(vispa_path)
    vispa_path = os.path.dirname(vispa_path)
    vispa_path = os.path.dirname(vispa_path)
    sys.path.insert(0, vispa_path)
    from vispa import models
from vispa.models import *
target_metadata = models.Base.metadata


def run_migrations_offline():
    context.configure(url=url)

    with context.begin_transaction():
        context.run_migrations()


def run_migrations_online():
    engine = create_engine(url, poolclass=pool.NullPool)

    connection = engine.connect()
    context.configure(connection=connection, target_metadata=target_metadata)

    try:
        with context.begin_transaction():
            context.run_migrations()
    finally:
        connection.close()

if context.is_offline_mode():
    run_migrations_offline()
else:
    run_migrations_online()
