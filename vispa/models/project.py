# -*- coding: utf-8 -*-

from datetime import datetime
from sqlalchemy import Column, Table, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.orm.session import object_session
from sqlalchemy.types import Unicode, UnicodeText, DateTime, Integer
from vispa import AjaxException
from vispa.models import Base, ItemAccessor
from vispa.models.role import Role
from vispa.models.user import User
import vispa

__all__ = ["Project_User_Assoc", "Project_Group_Assoc", "Project", "ProjectItem"]

project_manager_association = Table('project_manager_association', Base.metadata,
                                    Column('project_id', Integer, ForeignKey('project.id')),
                                    Column('user_id', Integer, ForeignKey('user.id')))

project_user_assoc_role_association = Table('project_user_assoc_role_association', Base.metadata,
                                            Column('project_id', Integer, ForeignKey('project.id')),
                                            Column('user_id', Integer, ForeignKey('user.id')),
                                            Column('role_id', Integer, ForeignKey('role.id')))

project_group_assoc_role_association = Table('project_group_assoc_role_association', Base.metadata,
                                             Column('project_id', Integer, ForeignKey('project.id')),
                                             Column('group_id', Integer, ForeignKey('group.id')),
                                             Column('role_id', Integer, ForeignKey('role.id')))

class Project_User_Assoc(Base):

    """
    The Project_User_Assoc object is an association object, which connects a project and a user. It
    has a many-to-many relationship to roles, which gives the user roles inside the project. For the
    association table, the project and the user id are used.
    """

    __tablename__ = 'project_user_assoc'
    project_id = Column(Integer, ForeignKey('project.id'), primary_key=True)
    user_id = Column(Integer, ForeignKey('user.id'), primary_key=True)
    user = relationship("User", backref="projects")
    roles = relationship("Role",
                         secondary=project_user_assoc_role_association,
                         primaryjoin="and_(Project_User_Assoc.project_id==project_user_assoc_role_a"
                         "ssociation.c.project_id, Project_User_Assoc.user_id==project_user_assoc_r"
                         "ole_association.c.user_id)")

    def get_permissions(self):
        """
        Get the permissions of this Project User connection.

        :returns: set of Permission objects
        """
        permissions = set([])
        for role in self.roles:
            permissions.update(role.permissions)
        return permissions

class Project_Group_Assoc(Base):

    """
    The Project_Group_Assoc object is an association object, which connects a project and a group.
    It has a many-to-many relationship to roles, which gives the group roles inside the project. For
    the association table, the project and the group id are used.
    """

    __tablename__ = 'project_group_assoc'
    project_id = Column(Integer, ForeignKey('project.id'), primary_key=True)
    group_id = Column(Integer, ForeignKey('group.id'), primary_key=True)
    group = relationship("Group", backref="projects")
    roles = relationship("Role", secondary=project_group_assoc_role_association,
                         primaryjoin="and_(Project_Group_Assoc.project_id==project_group_assoc_role"
                         "_association.c.project_id, Project_Group_Assoc.group_id==project_group_as"
                         "soc_role_association.c.group_id)")

    def get_permissions(self):
        """
        Get the permissions of this Project Group connection.

        :returns: set of Permission objects
        """
        permissions = set([])
        for role in self.roles:
            permissions.update(role.permissions)
        return permissions

class Project(Base):

    """
    A project connects users and groups to some content (ProjectItems) and also assigns permissions
    to them via roles. Permissions can be e.g. read and write rights on the content.
    """

    INACTIVE = 0
    ACTIVE = 1
    DELETED = 2

    __tablename__ = 'project'
    id = Column(Integer, nullable=False, primary_key=True)
    name = Column(Unicode(255), nullable=False, unique=True)
    created = Column(DateTime, nullable=False, default=datetime.now)
    status = Column(Integer, nullable=False, default=ACTIVE)
    managers = relationship("User",
                            secondary=project_manager_association,
                            backref="managed_projects")
    users = relationship("Project_User_Assoc", backref="project")
    groups = relationship("Project_Group_Assoc", backref="project")
    items = relationship("ProjectItem", backref="project")

    def to_dict(self, user=None):
        return {
            "id": self.id,
            "name": self.name,
            "created": self.created.isoformat(),
            "status": self.status,
            "manager": None if user is None else user in self.get_managers(),
        }

    @staticmethod
    def get_by_id(session, gid):
        """
        Get a project by its id.

        :param session: current session of database
        :param gid: given id, which is looked for
        :returns: Project object or None if inexistent
        """
        return session.query(Project).filter_by(id=gid).first()

    @staticmethod
    def get_by_name(session, name):
        """
        Get a project by its name.

        :param session: current session of database
        :param name: name which is looked for
        :returns: Project object or None if inexistent
        """
        return session.query(Project).filter_by(name=unicode(name)).first()

    @staticmethod
    def get(session, project):
        """
        Get a project by name. If the project parameter is an instance of Project, it is directly
        returned.

        :param session: current session of database
        :param project: name to look for. If project is instance of Project it is directly returned
        :returns: Project object
        :raises AjaxException: if project parameter is invalid or no project can be found
        """
        if project is None:
            raise AjaxException('invalid name', 400)
        if isinstance(project, Project):
            return project
        else:
            retval = Project.get_by_name(session, project)
            if retval is None:
                raise AjaxException('project %s not found' % project, 404)
            return retval

    @staticmethod
    def all(session):
        """
        Returns all existing projects.

        :param session: current session of database
        :returns: list of Project objects
        """
        return session.query(Project)

    @staticmethod
    def create(session, name):
        """
        Create new project.

        :param session: current session of database
        :param name: name of the project
        :raises AjaxException: if name is invalid or project with same name already exists
        """
        # name valid and doesn't already exists?
        if name is None:
            raise AjaxException("invalid name", 400)
        if Project.get_by_name(session, name) is not None:
            raise AjaxException("project %s already exists" % name, 409)
        # name valid -> create project
        project = Project(name=name)
        session.add(project)
        session.commit()
        return project

    @staticmethod
    def get_or_create_by_name(session, name):
        """
        Get or create a project by its name.

        :param session: current session of database
        :param name: name of the project
        :returns: Project
        """
        project = Project.get_by_name(session, name)
        if project is None:
            Project.create(session, name)
            project = Project.get_by_name(session, name)
        return project

    def delete(self):
        """
        Delete project. Internally, the delete flag is set, its not really deleted.
        """
        self.status = Project.DELETED

    def rename(self, newname):
        """
        Rename project.

        :param newname: new name of the project
        :raises AjaxException: if newname is invalid or project with newname already exists
        """
        session = object_session(self)
        if not newname:
            raise AjaxException('invalid name', 400)
        newname = str(newname)
        if Project.get_by_name(session, newname) is not None:
            raise AjaxException('project %s already exists' % newname, 409)
        self.name = newname

    def set_status(self, status):
        """
        Set status of project.

        :param status: new status of the project, either 0 for inactive or 1 for active
        :raises AjaxException: if status is invalid
        """
        status = int(status)
        # activeness interger valid?
        if status not in [Project.INACTIVE, Project.ACTIVE]:
            raise AjaxException('invalid status', 400)
        self.status = status

    def get_users(self):
        """
        Returns users of project.

        :returns: list of Project_User_Assoc objects
        """
        return self.users

    def add_user(self, user):
        """
        Adds user to project without any roles.

        :param user: concerning user
        :type user: User
        :raises TypeError: if user is not instance of User
        :raises AjaxException: if user already in project
        """
        session = object_session(self)
        if not isinstance(user, User):
            raise TypeError('invalid type of user')
        # check if Project_User_Assoc object exists
        assoc = session.query(Project_User_Assoc).filter_by(user_id=user.id) \
            .filter_by(project_id=self.id).first()
        if assoc is not None:
            raise AjaxException("user %s already in project %s" % (user.name, self.name), 409)
        assoc = Project_User_Assoc(project_id=self.id, user_id=user.id)
        assoc.user = user
        self.users.append(assoc)
        session.add(assoc)
        session.commit()

    def remove_user(self, user):
        """
        Removes user from Project. The concerning Project_User_Assoc is deleted from the database.

        :param user: concerning user
        :type user: User
        :raises TypeError: if user is not instance of User
        :raises AjaxException: if user not in project
        """
        session = object_session(self)
        if not isinstance(user, User):
            raise TypeError('invalid type of user')
        # get Project_User_Assoc object
        assoc = session.query(Project_User_Assoc).filter_by(user_id=user.id) \
            .filter_by(project_id=self.id).first()
        if assoc is None:
            raise AjaxException("user %s not in project %s" % (user.name, self.name), 409)
        session.delete(assoc)

    def is_member(self, user):
        """
        Check if this user is a member.

        :param User user: The user to check.
        :return: Whether the user is a member.
        :rtype: Boolean
        """
        if not isinstance(user, User):
            raise TypeError("invalid type of user")
        project_group_ids = set(assoc.group.id for assoc in self.groups)
        user_group_ids = set(user.get_group_ids())
        return not not (project_group_ids & user_group_ids)

    def get_groups(self):
        """
        Get groups of project.

        :returns: list of Project_Group_Assoc objects
        """
        return self.groups

    def has_group(self, group):
        """
        Check if group is already in project.

        :param group: concerning group
        :type group: Group
        :returns: bool whether group is in project
        :raises TypeError: if group is not instance of Group
        """
        if not isinstance(group, vispa.models.group.Group):
            raise TypeError('invalid type of group')
        return group in [x.group for x in self.get_groups()]

    def add_group(self, group):
        """
        Adds group to project without any roles. The necessary Project_Group_Assoc object is added
        to the database.

        :param group: concerning group
        :type group: Group
        :raises TypeError: if group is not instance of Group
        """
        session = object_session(self)
        if not isinstance(group, vispa.models.group.Group):
            raise TypeError('invalid type of group')
        # get Project_Group_Assoc object
        assoc = session.query(Project_Group_Assoc).filter_by(group_id=group.id) \
            .filter_by(project_id=self.id).first()
        if assoc is not None:
            raise AjaxException("group %s already in project %s" % (group.name, self.name), 409)
        assoc = Project_Group_Assoc(project_id=self.id, group_id=group.id)
        assoc.group = group
        self.groups.append(assoc)
        session.add(assoc)
        session.commit()

    def remove_group(self, group):
        """
        Removes group from Project.

        :param group: concerning group
        :type group: Group
        :raises TypeError: if group is not instance of Group
        :raises AjaxException: if group is not in project
        """
        session = object_session(self)
        if not isinstance(group, vispa.models.group.Group):
            raise TypeError('invalid type of group')
        # get Project_Group_Assoc object
        assoc = session.query(Project_Group_Assoc).filter_by(group_id=group.id) \
            .filter_by(project_id=self.id).first()
        if assoc is None:
            raise AjaxException("group %s not in project %s" % (group.name, self.name), 409)
        session.delete(assoc)

    def get_managers(self):
        """
        Get managers of project.

        :returns: list of User objects
        """
        return self.managers

    def add_manager(self, user):
        """
        Adds new manager to project.

        :param user: concerning user
        :type user: User
        :raises TypeError: if user is not instance of User
        """
        if not isinstance(user, User):
            raise TypeError('invalid type of user')
        self.managers.append(user)

    def remove_manager(self, manager):
        """
        Removes manager from Project.

        :param manager: concerning manager
        :type manager: User
        :raises TypeError: if manager is not instance of User
        :raises AjaxException: if manager is not manager of project
        """
        if not isinstance(manager, User):
            raise TypeError('invalid type of manager')
        # is given manager manager of project?
        if manager not in self.managers:
            raise AjaxException("user %s is not manager of project %s" % (manager.name, self.name), 409)
        self.managers.remove(manager)

    def get_roles_of_user(self, user):
        """
        Return roles of user.

        :param user: concerning user
        :type user: User
        :returns: list of Role objects
        :raises TypeError: if user is not instance of User
        :raises AjaxException: if user not in project
        """
        if not isinstance(user, User):
            raise TypeError('invalid type of user')
        # get Project_User_Assoc object
        assoc = None
        for x in self.users:
            if x.user_id == user.id:
                assoc = x
        if assoc is None:
            raise AjaxException("user %s not in project %s" % (user.name, self.name), 409)
        return assoc.roles

    def get_roles_of_group(self, group):
        """
        Return roles of group.

        :param group: concerning group
        :type group: Group
        :returns: list of Role objects
        :raises TypeError: if group is not instance of Group
        :raises AjaxException: if group not in project
        """
        if not isinstance(group, vispa.models.group.Group):
            raise TypeError('invalid type of group')
        # get Project_Group_Assoc object
        assoc = None
        for x in self.groups:
            if x.group_id == group.id:
                assoc = x
        if assoc is None:
            raise AjaxException("group %s not in project %s" % (group.name, self.name), 409)
        return assoc.roles

    def set_roles_of_user(self, user, roles):
        """
        Sets the roles of a user in a project.

        :param user: concerning user
        :type user: User
        :param roles: list of roles
        :type roles: list of Role objects
        :raises TypeError: if type if user or roles is invalid
        :raises AjaxException: if user not in project
        """
        if not isinstance(user, User):
            raise TypeError('invalid type of user')
        if not isinstance(roles, list):
            raise TypeError('Invalid type of roles')
        for role in roles:
            if not isinstance(role, Role):
                raise TypeError('Invalid type of roles')
        assoc = None
        for x in self.users:
            if x.user_id == user.id:
                assoc = x
        if assoc is None:
            raise AjaxException("user %s not in project %s" % (user.name, self.name), 409)
        assoc.roles = roles

    def add_roles_to_user(self, user, roles):
        """
        Add role to a user.

        :param user: concerning user
        :type user: User
        :param roles: list of roles
        :type roles: list of Role objects
        :raises TypeError: if type if user or roles is invalid
        :raises AjaxException: if user not in project
        """
        if not isinstance(user, User):
            raise TypeError('invalid type of user')
        if not isinstance(roles, list):
            raise TypeError('Invalid type of roles')
        for role in roles:
            if not isinstance(role, Role):
                raise TypeError('Invalid type of roles')
        assoc = None
        for x in self.users:
            if x.user_id == user.id:
                assoc = x
        if assoc is None:
            raise AjaxException("user %s not in project %s" % (user.name, self.name), 409)
        assoc.roles.extend(roles)

    def get_user_roles(self, user):
        return self.get_roles_of_user(user=user)

    def add_user_role(self, user, role):
        if not isinstance(user, User):
            raise TypeError('invalid type of user')
        if not isinstance(role, Role):
            raise TypeError('Invalid type of roles')
        for assoc in self.users:
            if assoc.user_id == user.id:
                if role in assoc.roles:
                    raise AjaxException("user %s has role %s already" % (user.name, role.name), 409)
                assoc.roles.append(role)
                break
        else:
            raise AjaxException("user %s not in project %s" % (user.name, self.name), 409)

    def remove_user_role(self, user, role):
        if not isinstance(user, User):
            raise TypeError('invalid type of user')
        if not isinstance(role, Role):
            raise TypeError('Invalid type of roles')
        for assoc in self.users:
            if assoc.user_id == user.id:
                assoc.roles.remove(role)
                break
        else:
            raise AjaxException("user %s not in project %s" % (user.name, self.name), 409)

    def get_group_roles(self, group):
        return self.get_roles_of_group(group=group)

    def add_group_role(self, group, role):
        if not isinstance(group, vispa.models.group.Group):
            raise TypeError('invalid type of group')
        if not isinstance(role, Role):
            raise TypeError('Invalid type of roles')
        for assoc in self.groups:
            if assoc.group_id == group.id:
                if role in assoc.roles:
                    raise AjaxException("group %s has role %s already" % (group.name, role.name), 409)
                assoc.roles.append(role)
                break
        else:
            raise AjaxException("group %s not in project %s" % (group.name, self.name), 409)

    def remove_group_role(self, group, role):
        if not isinstance(group, vispa.models.group.Group):
            raise TypeError('invalid type of group')
        if not isinstance(role, Role):
            raise TypeError('Invalid type of roles')
        for assoc in self.groups:
            if assoc.group_id == group.id:
                assoc.roles.remove(role)
                break
        else:
            raise AjaxException("group %s not in project %s" % (group.name, self.name), 409)

    def set_roles_of_group(self, group, roles):
        """
        Sets the roles of a group in a project.

        :param group: concerning group
        :type group: Group
        :param roles: list of roles
        :type roles: list of Role objects
        :raises TypeError: if type if group or roles is invalid
        :raises AjaxException: if group not in project
        """
        if not isinstance(group, vispa.models.group.Group):
            raise TypeError('invalid type of group')
        if not isinstance(roles, list):
            raise TypeError('Invalid type of roles')
        for role in roles:
            if not isinstance(role, Role):
                raise TypeError('Invalid type of roles')
        assoc = None
        for x in self.groups:
            if x.group_id == group.id:
                assoc = x
        if assoc is None:
            raise AjaxException("group %s not in project %s" % (group.name, self.name), 409)
        assoc.roles = roles

    def add_roles_to_group(self, group, roles):
        """
        Add role to group.

        :param group: concerning group
        :type group: Group
        :param roles: list of roles
        :type roles: list of Role objects
        :raises TypeError: if type if group or roles is invalid
        :raises AjaxException: if group not in project
        """
        if not isinstance(group, vispa.models.group.Group):
            raise TypeError('invalid type of group')
        if not isinstance(roles, list):
            raise TypeError('Invalid type of roles')
        for role in roles:
            if not isinstance(role, Role):
                raise TypeError('Invalid type of roles')
        assoc = None
        for x in self.groups:
            if x.group_id == group.id:
                assoc = x
        if assoc is None:
            raise AjaxException("group %s not in project %s" % (group.name, self.name), 409)
        assoc.roles.extend(roles)

    def get_items(self, itemtype=None):
        """
        Get ProjectItems.

        :param itemtype: optional selector on the item type
        :returns: list of ProjectItem objects
        """
        if itemtype:
            return [item for item in self.items if item.itemtype == itemtype]
        else:
            return self.items

    def item_accessor(self, session, prefix="", json=False):
        """
        Returns a new ItemAccessor (with the parameters passed down) for the ProjectItems.
        """
        return ItemAccessor(
            _table=ProjectItem, _foreignName="project_id", _foreignValue=self.id,
            session=session, prefix=prefix, json=json
        )

class ProjectItem(Base):

    """
    A project item represents the actual content of a project. It is simply an object
    consisting of a type (e.g. file) and its content (e.g. the path of the file)
    """

    __tablename__ = 'project_items'
    id = Column(Integer, primary_key=True)
    project_id = Column(Integer, ForeignKey('project.id'), nullable=False)
    itemtype = Column(UnicodeText)
    content = Column(UnicodeText)

    @staticmethod
    def get_by_id(session, gid):
        """
        Get a ProjectItem by its id.

        :param session: current session of database
        :param gid: given id, which is looked for
        :returns: ProjectItem or None if inexistent
        """
        return session.query(ProjectItem).filter_by(id=gid).first()

    @staticmethod
    def get(session, item):
        """
        Get a ProjectItem by its id. If the item is already a ProjectItem object, it is directly
        returned.

        :param session: current session of database
        :param item: id to look for. if type of item is ProjectItem, item is returned
        :returs: ProjectItem
        :raises AjaxException: if item is invalid or no item can be found
        """
        if item is None:
            raise AjaxException('invalid item', 400)
        if isinstance(item, ProjectItem):
            return item
        else:
            retval = ProjectItem.get_by_id(session, item)
            if retval is None:
                raise AjaxException('unknown item with id %i' % int(item), 404)  # TODO: this look wrong
            return retval

    @staticmethod
    def create(session, project, itemtype, content):
        """
        Create an project item. The item is added to the database.

        :param session: current session of database
        :param project: project of the new item
        :param itemtype: type of the item
        :param content: content of the item
        """
        project_item = ProjectItem(
            project_id=project.id,
            itemtype=itemtype,
            content=content
        )
        session.add(project_item)
        session.commit()
        return project_item

    def set_content(self, content):
        """
        Set the content of the item.

        :param content: new content
        """
        self.content = content

    def delete(self, session):
        """
        Delete the item. It is removed from the database.

        :param session: current session of database
        """
        session.delete(self)

    def get_project(self):
        """
        Get the project of the item.

        :returns: Project
        """
        return self.project
