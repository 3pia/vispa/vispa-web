# -*- coding: utf-8 -*-

from datetime import datetime
from passlib.hash import sha256_crypt
from sqlalchemy import Column, Table, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.orm.session import object_session
from sqlalchemy.types import Unicode, DateTime, Integer, UnicodeText
from vispa import AjaxException, fire_callback
from vispa.models import Base
from vispa.models.project import Project
from vispa.models.user import User

__all__ = ["Group_User_Assoc", "Group_Group_Assoc", "Group"]

group_manager_association = Table('group_manager_association', Base.metadata,
                                  Column('group_id', Integer, ForeignKey('group.id')),
                                  Column('user_id', Integer, ForeignKey('user.id')))

class Group_User_Assoc(Base):

    """
    The Group_User_Assoc object is a association object representing the membership of a user in a
    group. In addition to the membership itself, it has a status flag, which indicates, whether the
    membership is confirmed or not.
    """

    CONFIRMED = 0
    UNCONFIRMED = 1

    __tablename__ = 'group_user_assoc'
    status = Column(Integer, nullable=False, default=UNCONFIRMED)
    group_id = Column(Integer, ForeignKey('group.id'), primary_key=True)
    user_id = Column(Integer, ForeignKey('user.id'), primary_key=True)
    user = relationship("User", backref="groups")

class Group_Group_Assoc(Base):

    """
    The Group_Group_Assoc object is a association object representing the membership of one group in
    another, called child group and parnet group. In addition to the membership itself, it has a
    status flag, which indicates whether the membership is confirmed or not.
    """

    CONFIRMED = 0
    UNCONFIRMED = 1

    __tablename__ = 'group_group_assoc'
    status = Column(Integer, nullable=False, default=UNCONFIRMED)
    parent_group_id = Column(Integer, ForeignKey('group.id'), primary_key=True)
    child_group_id = Column(Integer, ForeignKey('group.id'), primary_key=True)
    child_group = relationship("Group", foreign_keys=child_group_id, backref="parent_groups")

class Group(Base):

    """
    A group is a collection of users and other groups. Every group has an id, a unique name and a
    creation timestamp. Furthermore there is a privacy integer as follows:
        0 - public: the group and its members can be seen by everyone and everyone can join
        1 - protected: the group is visible, members only for other members,
        joining via request to group manager
        2 - private: group and members are invisible, joining via password
    A group is organized by managers, who can edit the name, privacy etc. as well as the memberships
    of users and group. Additionally, the managers can join the group into a parentgroup.
    """

    PUBLIC = 0
    PROTECTED = 1
    PRIVATE = 2

    INACTIVE = 0
    ACTIVE = 1
    DELETED = 2

    __tablename__ = 'group'
    id = Column(Integer, nullable=False, primary_key=True)
    name = Column(Unicode(255), nullable=False, unique=True)
    created = Column(DateTime, nullable=False, default=datetime.now)
    privacy = Column(Integer, nullable=False, default=PUBLIC)
    password = Column(UnicodeText())
    status = Column(Integer, nullable=False, default=ACTIVE)
    managers = relationship("User", secondary=group_manager_association, backref="managed_groups")
    users = relationship("Group_User_Assoc", backref="group")
    child_groups = relationship("Group_Group_Assoc",
                                primaryjoin=id == Group_Group_Assoc.parent_group_id,
                                backref="parent_group")

    def to_dict(self, user=None):
        has_password = self.password and not sha256_crypt.verify("", self.password)
        member = None
        if user:
            for a in self.users:
                if user.id == a.user_id:
                    member = a.status
                    break
            else:
                member = 2
        return {
            "id": self.id,
            "name": self.name,
            "created": self.created.isoformat(),
            "privacy": self.privacy,
            "password": has_password,
            "status": self.status,
            "manager": None if user is None else user in self.get_managers(),
            "member": member,
        }

    @staticmethod
    def get_by_name(session, name):
        """
        Get a group by its name.

        :param session: current session of database
        :param name: name to look for
        :returns: Group or None if inexistent
        """
        return session.query(Group).filter_by(name=unicode(name)).first()

    @staticmethod
    def get_by_id(session, gid):
        """
        Get a group by its id.

        :param session: current session of database
        :param gid: id to look for
        :returns: Group or None if inexistent
        """
        return session.query(Group).filter_by(id=gid).first()

    @staticmethod
    def get(session, group):
        """
        Get a group by name or id. If the group parameter is an instance of Group, it is directly
        returned.

        :param session: current session of database
        :param group: name or id to look for. if group is instance of Group, group is returned
        :returns: Group
        :raises AjaxException: if group parameter invalid or no group can be found
        """
        if not group:
            raise AjaxException('invalid name', 400)
        if isinstance(group, Group):
            return group
        else:
            retval = Group.get_by_name(session, group)
            if retval is None:
                retval = Group.get_by_id(session, group)
            if retval is None:
                raise AjaxException('group %s not found' % group, 404)
            return retval

    @staticmethod
    def create(session, name, privacy=PUBLIC, password=""):
        if name is None:
            raise AjaxException('invalid name', 400)
        # privacy integer valid?
        privacy = int(privacy)
        if privacy not in [Group.PUBLIC, Group.PROTECTED, Group.PRIVATE]:
            raise AjaxException('invalid privacy', 400)
        if Group.get_by_name(session, name) is not None:
            raise AjaxException("group %s already exists" % name, 409)
        # parameters valid -> create and add group
        group = Group(name=name,
                      privacy=privacy,
                      password=User.encrypt_password(password))
        session.add(group)
        session.commit()
        return group

    @staticmethod
    def get_or_create_by_name(session, name, privacy=PUBLIC, password=""):
        """
        Get a group by name or create it as public group, if it does not exists.

        :param session: current session of database
        :param name: name of the group
        :returns: Group
        :raises AjaxException: if no group with name exists and either name or privacy are invalid
        """
        group = Group.get_by_name(session, name)
        if group is None:
            group = Group.create(session, name, privacy=privacy, password=password)
        return group

    @staticmethod
    def all(session):
        """
        Get all groups.

        :param session: current session of database
        :returns: list of all Group objects
        """
        return session.query(Group)

    def delete(self, session=None):
        """
        Delete group. Internally, the delete flag is set, its not deleted from the database.
        """
        self.status = Group.DELETED

    def rename(self, newname):
        """
        Rename group.

        :param newname: new name of the group
        :raises AjaxException: if newname is invalid or already exitent
        """
        session = object_session(self)
        if not newname:
            raise AjaxException('invalid name', 400)
        newname = str(newname)
        if Group.get_by_name(session, newname) is not None:
            raise AjaxException('group %s already exists' % newname, 409)
        self.name = newname

    def set_privacy(self, privacy):
        """
        Set privacy of group.

        :param privacy: new privacy of the group (0, 1, or 2)
        :raises AjaxException: if privacy is invalid
        """
        privacy = int(privacy)
        # privacy integer valid?
        if privacy not in [Group.PUBLIC, Group.PROTECTED, Group.PRIVATE]:
            raise AjaxException('invalid privacy', 400)
        self.privacy = privacy

    def set_status(self, status):
        """
        Set status of group.

        :param status: new status of the group, either 0 for inactive or 1 for active
        :raises AjaxException: if status is invalid
        """
        status = int(status)
        # activeness interger valid?
        if status not in [Group.INACTIVE, Group.ACTIVE]:
            raise AjaxException('invalid status', 400)
        self.status = status

    def set_password(self, password):
        """
        Set the password of a group.

        :param password: new password
        :raises AjaxException: if password is invalid
        """
        password = str(password)
        if password is None:
            raise AjaxException('invalid password', 403)
        self.password = User.encrypt_password(password)

    def get_managers(self):
        """
        Get managers of the group.

        :returns: list of User objects, which are managers of the group
        """
        return self.managers

    def add_manager(self, user):
        """
        Add manager to group.

        :param user: user which has to be added
        :type user: User
        :raises TypeError: if user is not instance of User
        :raises AjaxException: if user is already manager of the group‚
        """
        if not isinstance(user, User):
            raise TypeError('invalid type of user')
        if user in self.managers:
            raise AjaxException('user %s already manager of group %s' % (user.name, self.name), 409)
        self.managers.append(user)

    def remove_manager(self, manager):
        """
        Remove manager from group.

        :param manager: manager which has to be removed
        :type manager: User
        :raises TypeError: if manager is not instance of User
        :raises AjaxException: if manager is not manager of the group
        """
        if not isinstance(manager, User):
            raise TypeError('invalid type of manager')
        if manager not in self.managers:
            raise AjaxException('user %s not manager of group %s' % (manager.name, self.name), 409)
        self.managers.remove(manager)

    def get_users(self, recursion_depth=-1):
        """
        Get all users of the group. The recursion depth for the child groups can be given as
        additional argument. E.g. 0 is only the group itself, 1 also includes all direct child
        groups. -1 belongs to infinite recursion depth. Notice: since loops in groups are permitted,
        there is only a finite number of recusions for a finite number of groups.

        :param recursion_depth: number of recursions for child groups
        :returns: Set of Group_User_Assoc objects
        """
        group_user_assocs = set(self.users)
        if recursion_depth != 0:
            for child_group in self.child_groups:
                group_user_assocs.update(child_group.child_group.get_users(recursion_depth-1))
        return group_user_assocs

    def add_user(self, user, confirmed=Group_User_Assoc.UNCONFIRMED):
        """
        Add user to group. The necessary Group_User_Assoc object is added to the database.
        By default, the membership is unconfirmed.

        :param user: user which has to be added
        :type user: User
        :raises TypeError: if user is not instance of User
        :raises AjaxException: if user already in group
        """
        session = object_session(self)
        if not isinstance(user, User):
            raise TypeError('invalid type of user')
        if not session.query(Group_User_Assoc).filter_by(group_id=self.id) \
                .filter_by(user_id=user.id).first() is None:
            raise AjaxException('user %s already in group %s' % (user.name, self.name), 409)
        assoc = Group_User_Assoc(group_id=self.id, user_id=user.id, status=confirmed)
        self.users.append(assoc)
        assoc.user = user
        session.add(assoc)
        try:
            session.commit()
        except:
            session.rollback()
        fire_callback("user.group.membership", user, self, confirmed == Group_User_Assoc.CONFIRMED)

    def confirm_user(self, user):
        """
        Confirm a user in a private group.

        :param user: concerning user
        :type user: User
        :raises TypeError: if user is not instance of User
        :raises AjaxException: if user not in group
        """
        if not isinstance(user, User):
            raise TypeError('invalid type of user')
        for x in self.users:
            if x.user_id == user.id:
                assoc = x
        if assoc is None:
            raise AjaxException('user %s not in group %s' % (user.name, self.name), 409)
        assoc.status = Group_User_Assoc.CONFIRMED
        fire_callback("user.group.membership", user, self, True)

    def remove_user(self, user):
        """
        Remove user from group. The concerning Group_User_Assoc object is deleted from the database.

        :param user: user which has to be removed
        :type user: User
        :raises TypeError: if user is not instance of User
        :raises AjaxException: if user not in group
        """
        session = object_session(self)
        if not isinstance(user, User):
            raise TypeError('invalid type of user')
        assoc = session.query(Group_User_Assoc).filter_by(group_id=self.id) \
            .filter_by(user_id=user.id).first()
        if assoc is None:
            raise AjaxException('user %s not in group %s' % (user.name, self.name), 409)
        session.delete(assoc)
        fire_callback("user.group.membership", user, self, False)

    def is_member(self, user):
        """
        Check if this user is a member.

        :param User user: The user to check.
        :return: Whether the user is a member.
        :rtype: Boolean
        """
        if not isinstance(user, User):
            raise TypeError("invalid type of user")
        return self.id in user.get_group_ids()

    def get_child_groups(self, recursion_depth=-1):
        """
        Get child groups of a parent group. This function works as get_users regarding the recursion
        depth.

        :param recursion_depth: number of steps for recursion
        :returns: set of Group_Group_Assoc objects
        """
        group_group_assocs = set(self.child_groups)
        if recursion_depth != 0:
            for child_group in self.child_groups:
                group_group_assocs.update(child_group.child_group \
                                          .get_child_groups(recursion_depth-1))
        return group_group_assocs

    def get_parent_groups(self, recursion_depth=-1):
        """
        Get parent groups (confirmed and unconfirmed) of a child group. This function
        works as get_child_groups but in opposite direction. The recursion is only
        done for confirmed memberships.

        :param recursion_depth: number of steps for recursion
        :returns: set of Group_Group_Assoc objects
        """
        group_group_assocs = set(self.parent_groups)
        if recursion_depth != 0:
            for parent_group in self.parent_groups:
                if parent_group.status == Group_Group_Assoc.CONFIRMED:
                    group_group_assocs.update(parent_group.parent_group \
                                              .get_parent_groups(recursion_depth-1))
        return group_group_assocs

    def add_child_group(self, child_group, confirmed=Group_Group_Assoc.UNCONFIRMED):
        """
        Add child group to parent group. Loops in groups are permitted. The necessary
        Group_Group_Assoc object is added to the database.

        :param child_group: concerning child group
        :type child_group: Group
        :raises TypeError: of child_group is not instance of Group
        :raises AjaxException: if a loop is detected
        :raises AjaxException: if child_group is already in the group
        """
        session = object_session(self)
        if not isinstance(child_group, Group):
            raise TypeError('invalid type of child_group')
        # no loops in groups
        if self is child_group:
            raise AjaxException('group loop detected', 508)
        if self in [x.child_group for x in child_group.get_child_groups()]:
            raise AjaxException('group loop detected', 508)
        # already child group?
        if session.query(Group_Group_Assoc).filter_by(parent_group_id=self.id) \
                .filter_by(child_group_id=child_group.id).first() is not None:
            raise AjaxException('group %s already in group %s' % (child_group.name, self.name), 409)
        assoc = Group_Group_Assoc(parent_group_id=self.id,
                                  child_group_id=child_group.id,
                                  status=confirmed)
        self.child_groups.append(assoc)
        assoc.child_group = child_group
        session.add(assoc)
        session.commit()

    def confirm_child_group(self, child_group):
        """
        Confirm a child group in a private parent group.

        :param child_group: concerning child group
        :type child_group: Group
        :raises TypeError: of child_group is not instance of Group
        :raises AjaxException: if child_group is not in the group
        """
        if not isinstance(child_group, Group):
            raise TypeError('invalid type of child_group')
        for x in self.child_groups:
            if x.child_group_id == child_group.id:
                assoc = x
        if assoc is None:
            raise AjaxException('group %s not in group %s' % (child_group.name, self.name), 409)
        assoc.status = Group_Group_Assoc.CONFIRMED
        return True

    def remove_child_group(self, child_group):
        """
        Remove child group from parent group. The concerning Group_Group_Assoc object
        is deleted form the database.

        :param child_group: concerning child group
        :type child_group: Group
        :raises TypeError: of child_group is not instance of Group
        :raises AjaxException: if child_group is not in the group
        """
        session = object_session(self)
        if not isinstance(child_group, Group):
            raise TypeError('invalid type of child_group')
        assoc = session.query(Group_Group_Assoc).filter_by(parent_group_id=self.id) \
            .filter_by(child_group_id=child_group.id).first()
        if assoc is None:
            raise AjaxException('group %s not in group %s' % (child_group.name, self.name), 409)
        session.delete(assoc)

    def get_projects(self):
        """
        Returns active projects of the group and all (confirmed) parent groups.

        :returns: set of Project_Group_Assoc objects
        """
        project_group_assocs = set([x for x in self.projects if x.project.status == Project.ACTIVE])
        # recursion
        for parent_group in self.parent_groups:
            if parent_group.status == Group_Group_Assoc.CONFIRMED:
                project_group_assocs.update(parent_group.get_projects())
        return project_group_assocs
