# -*- coding: utf-8 -*-

import os
import logging
import vispa
import sqlalchemy
from sqlalchemy.ext.declarative import declarative_base
import json

logger = logging.getLogger(__name__)

Base = declarative_base()

__all__ = ['group', 'jsondata', 'project', 'role', 'user', 'workgroup', 'workspace']

FORBIDDEN_PHRASES = [u"drop ", u"select ", u"dump ", u"insert ", u"delete ",
                     u"update ", u"drop\\ ", u"select\\ ", u"dump\\ ",
                     u"insert\\ ", u"delete\\ ", u"update\\ "]
FORBIDDEN_CHARS = [u"´", u"`"]



def insertion_safe(*args, **kwargs):
    for arg in list(args) + kwargs.values():
        if isinstance(arg, dict):
            arg = arg.keys()
        if not isinstance(arg, list):
            arg = [arg]
        for elem in arg:
            if not isinstance(elem, (str, unicode)):
                continue
            # 1. check: forbidden phrases
            for phrase in FORBIDDEN_PHRASES:
                if elem.lower().find(phrase) >= 0:
                    return False, elem
            # 2. check: forbidden chars
            for char in FORBIDDEN_CHARS:
                if elem.lower().find(char) >= 0:
                    return False, elem
    return True, None

class ItemAccessor(object):
    """
    An helper to provide dictonary like access to ProjectItem or WorkgroupItem objects. Items are
    expected to be unique, if not the behaviour becomes undefined.
    Item values are directly accessed via [] and may be read, set, deleted or iterated over.

    :param _table: The table to act upon. Set by the factory.
    :param _foreignName: The name of the foreign relation. Set by the factory.
    :param _foreignValue: The value of the foreign relation. Set by the factory.
    :param session: A SQLAlchemy session.
    :param str prefix: A prefix for all items. It will be automatically applied and stripped.
    :param Boolean json: Whether the items content should be automatically json de-/encoded.
    """
    def __init__(self, _table, _foreignName, _foreignValue, session, prefix, json=False):
        if not prefix:
            raise RuntimeError("a prefix must be given")
        self.__dict__.update(locals())
        self.__dict__.pop("self")
        self._selector_foreign = getattr(self._table, _foreignName) == self._foreignValue
        self._selector_itemtype = self._table.itemtype.like("%s%%" % self.prefix)
        self._reset_cache()

    def _reset_cache(self):
        self.objects = dict()
        self._decoded = dict()
        self._got_all = False

    def _get_object(self, key):
        if key not in self.objects:
            obj = self.session.query(self._table).filter(
                self._selector_foreign,
                self._table.itemtype == (self.prefix + key)
            ).first()
            self.objects[key] = obj
        else:
            obj = self.objects[key]
        return obj

    def _query_all(self):
        return self.session.query(self._table).filter(
            self._selector_foreign,
            self._selector_itemtype
        )

    def load_all(self):
        """
        Load all items. This should be called if one expects to access multiple items, since it will
        only cause on query to be used.
        """
        for obj in self._query_all().all():
            key = obj.itemtype[len(self.prefix):]
            if key not in self.objects:
                self.objects[key] = obj
        self._got_all = True

    def delete_all(self):
        """
        Delete all items, even if there are duplicates for a itemtype.
        """
        self._query_all().delete()
        self._reset_cache()

    def sub_accessor(self, prefix, json=None):
        """
        Instanciates a ItemAccessor with an addional prefix (appended on the current one).
        They will not share any caches.

        :param str prefix: The additional prefix.
        :param json: If it is not None (default) this value will be used instead of the current one.
        :type json: Boolean or None
        :return: The new ItemAccessor.
        :rtype: ItemAccessor
        """
        if not prefix:
            raise RuntimeError("a prefix must be given")
        return ItemAccessor(
            _table=self._table,
            _foreignName=self._foreignName,
            _foreignValue=self._foreignValue,
            session=self.session,
            prefix=self.prefix + prefix,
            json=self.json if json is None else json
        )

    def __getitem__(self, key):
        if not isinstance(key, basestring):
            raise TypeError("key needs to be a basestring")

        obj = self._get_object(key)

        if obj is None:
            return obj
        if self.json:
            if key not in self._decoded:
                self._decoded[key] = json.loads(obj.content)
            return self._decoded[key]
        else:
            return obj.content

    def __setitem__(self, key, value):
        if not isinstance(key, basestring):
            raise TypeError("key needs to be a basestring")

        if self.json:
            value = json.dumps(value)

        obj = self._get_object(key)
        if obj is None:
            info = dict(itemtype=(self.prefix + key), content=value)
            info[self._foreignName] = self._foreignValue
            obj = self._table(**info)
            self.session.add(obj)
            self.session.commit()
            self.objects[key] = obj
        else:
            obj.content = value

    def __delitem__(self, key):
        if not isinstance(key, basestring):
            raise TypeError("key needs to be a basestring")

        obj = self._get_object(key)
        if obj is not None:
            self.session.delete(obj)
            del self.objects[key]
            if self.json and key in self._decoded:
                del self._decoded[key]

    def __iter__(self):
        return self.iterkeys()

    def iterkeys(self):
        if not self._got_all:
            self.load_all()
        return iter(self.objects)

    def itervalues(self):
        return (self[key] for key in self.iterkeys())

    def iteritems(self):
        return ((key, self[key]) for key in self.iterkeys())

def open_database(var_dir=None):
    var_dir = var_dir or ""
    sa_identifier = vispa.config('database', 'sqlalchemy.url',
                                 'sqlite:///%s' % os.path.join(var_dir, "vispa.db"))
    pool_size = vispa.config('database', 'sqlalchemy.pool_size', 10)
    max_overflow = vispa.config(
        'database',
        'sqlalchemy.max_overflow',
        10)
    # https://github.com/mitsuhiko/flask-sqlalchemy/issues/2
    # http://docs.sqlalchemy.org/en/latest/core/pooling.html#dealing-with-disconnects
    pool_recycle = vispa.config(
        'database',
        'sqlalchemy.pool_recycle',
        7200)
    logger.info('Use database %s.' % sa_identifier)
    try:
        engine = sqlalchemy.create_engine(
            sa_identifier,
            echo=False,
            pool_size=pool_size,
            pool_recycle=pool_recycle,
            max_overflow=max_overflow)
    except TypeError:
        engine = sqlalchemy.create_engine(
            sa_identifier,
            echo=False)
    return engine
