## error/401.mako

<%! from vispa import url %>

<%inherit file="/error/base.mako" />
<%namespace name="base" file="/base.mako" />

<%block name="error_message">
  ${message or "You are not authorized to see this page!"}
</%block>

<%block name="error_code">401</%block>
