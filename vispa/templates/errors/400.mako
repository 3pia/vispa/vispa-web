## error/400.mako

<%! from vispa import url %>

<%inherit file="/error/base.mako" />
<%namespace name="base" file="/base.mako" />

<%block name="error_message">
  ${message or "Bad request!"}
</%block>

<%block name="error_code">400</%block>
