## error/500.mako

<%! from vispa import url %>

<%inherit file="base.mako" />
<%namespace name="base" file="/base.mako" />

<%block name="error_message">
  ${message or "An internal server error occured!"}
</%block>

<%block name="error_code">500</%block>
