## error/408.mako

<%! from vispa import url %>

<%inherit file="/error/base.mako" />
<%namespace name="base" file="/base.mako" />

<%block name="error_message">
  ${message or "Request timeout!"}
</%block>

<%block name="error_code">408</%block>
