## error/404.mako

<%! from vispa import url %>

<%inherit file="/error/base.mako" />
<%namespace name="base" file="/base.mako" />

<%block name="error_message">
  ${message or "The requested path was not found!"}
</%block>

<%block name="error_code">404</%block>