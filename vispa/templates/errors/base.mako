## errors/base.mako

<%! from vispa import url %>

<%inherit file="/vispa.mako" />
<%namespace name="base" file="/base.mako" />

<%block name="js_inline_post">
  <%
    parent.js_inline_post()
  %>

  require(["jquery", "css!styles/common.css"], function($) {
    $("#back").click(function() {
      window.history.back();
    });
    $("#base").click(function() {
      window.location.href = "${url.dynamic("/")}";
    });
  });
</%block>

<div class="container">
  <div class="page-header">
    <h1>
      <div class="vispa-table-outer">
        <div class="vispa-table-inner">
          <img src="${url.static('img/vispa/header_ball.png')}" style="margin: -4px 10px 0 0;" />
          Sorry, but there was an error (${next.error_code()})!
        </div>
      </div>
    </h1>
  </div>
  <p class="lead">
    ${next.error_message()}
  </p>
  <p>
    You may return and try again or go to the VISPA main page.
  </p>
  <p>
    <button role="button" class="btn btn-default" id="back">
      <i class="glyphicon glyphicon-arrow-left"></i>
      <span>Go Back</span>
    </button>
    <button role="button" class="btn btn-primary" id="base">
      <span>VISPA</span>
      <i class="glyphicon glyphicon-arrow-right"></i>
    </button>
  </p>
</div>
