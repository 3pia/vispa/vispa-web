## sites/login.mako

<%! import os.path%>
<%! from vispa import url %>

<%inherit file="/vispa.mako" />
<%namespace name="base" file="/base.mako" />

<%block name="js_inline_post">
  <%
    parent.js_inline_post()
  %>

  // use template variables to create the vispa config
  var vispaConfig = {
    url: {
      dynamicBase: "${base_dynamic}",
      staticBase : "${base_static}"
    },
    useForgot    : ${"true" if use_forgot else "false"}
  };

  requirejs(['${base_static}js/config.js?'+urlArgs], function () {
    require(["vispa/login"], function(VispaLogin) {
      window.vispa = new VispaLogin(vispaConfig);
    });
  });

</%block>

## Main GUI Markup
<div class="container">

  <h2>
    <div class="vispa-table-outer">
      <div class="vispa-table-inner">
        <a href="https://vispa.physik.rwth-aachen.de/" class="vispa-header-logo">
          <img src="${url.static('img/vispa/header_ball.png')}" />
        </a>
        ${welcome_phrase}
      </div>
    </div>
  </h2>

  <hr />

  <ul class="nav nav-tabs hidden" id="tabs">
    <li class="active">
      <a href="#login-pane" data-toggle="tab">Login</a>
    </li>
    <li>
      <a href="#register-pane" data-toggle="tab">Register</a>
    </li>
    <li>
      <a href="#forgot-pane" data-toggle="tab">Forgot password?</a>
    </li>
  </ul>

  <div class="tab-content">
    <div class="tab-pane active" id="login-pane">
      <div class="well">

        <!-- login -->
        <div class="row">
          <div class="col-md-4 col-md-offset-1">
            <h3>Sign in</h3>
            <form class="form-login" role="form">
              <input type="text" id="login-name" class="form-control" placeholder="Username or email address" required autofocus />
              <input type="password" id="login-password" class="form-control" placeholder="Password" required />
              <a href="#forgot-pane" class="pull-right" data-toggle="tab" class="${'' if use_forgot else 'hidden'}">Forgot password?</a>
              <div class="clearfix"></div>
              <div id="login-buttons">
                <button class="btn btn-primary" type="submit">Sign in</button>
                <span class="space">or</span>
                <a href="#register-pane" data-toggle="tab">create an account</a>
              </div>
            </form>
            <div class="alert alert-danger" id="login-alert" style="display:none;">
              <i class="glyphicon glyphicon-warning-sign"></i>
              <span data-bind="alert"></span>
            </div>
          </div>
          <div class="col-md-6 col-md-offset-1">
            <div class="side_text">
              ${login_text}
            </div>
          </div>
        </div>
        <!-- end login -->

      </div>
    </div>
    <div class="tab-pane" id="register-pane">
      <div class="well">

        <!-- register -->
        <div class="row">
          <div class="col-md-4 col-md-offset-1">
            <h3>Create an account</h3>
            <form class="form-register" role="form">
              <input type="text" id="register-name" class="form-control" placeholder="Username" required />
              <input type="email" id="register-email" class="form-control" placeholder="Email address" required/>
              <button class="btn btn-success" type="submit">Create account</button>
              <span class="space">or</span>
              <a href="#login-pane" data-toggle="tab">login</a>
            </form>
            <div class="alert alert-danger" id="register-alert" style="display:none;">
              <i class="glyphicon glyphicon-warning-sign"></i>
              <span data-bind="alert"></span>
            </div>
            <div class="alert alert-success" id="register-success" style="display:none;">
              <i class="glyphicon glyphicon-ok"></i>
              <span>Further instructions have been sent to your mail address! Redirecting in 4 seconds ...</span>
            </div>
          </div>
          <div class="col-md-6 col-md-offset-1">
            <div class="side_text">
              ${registration_text}
            </div>
          </div>
        </div>
        <!-- end register -->

      </div>
    </div>
    <div class="tab-pane" id="forgot-pane">
      <div class="well">

        <!-- forgot password -->
        <div class="row">
          <div class="col-md-4 col-md-offset-1">
            <h3>Forgot your password?</h3>
            Type your username or your email address to reset your password.
            <form class="form-forgot" role="form">
              <input type="text" id="forgot-name" class="form-control" placeholder="Username or email address" required />
              <button class="btn btn-info" type="submit">Send</button>
              <span class="space">or</span>
              <a href="#login-pane" data-toggle="tab">login</a>
            </form>
            <div class="alert alert-danger" id="forgot-alert" style="display:none;">
              <i class="glyphicon glyphicon-warning-sign"></i>
              <span data-bind="alert"></span>
            </div>
            <div class="alert alert-success" id="forgot-success" style="display:none;">
              <i class="glyphicon glyphicon-ok"></i>
              <span>Further instructions have been sent to your mail address! Redirecting in 4 seconds ...</span>
            </div>
          </div>
          <div class="col-md-6 col-md-offset-1">
            <div class="side_text">
              ${forgot_text}
            </div>
          </div>
        </div>
        <!-- end forgot password -->

      </div>
    </div>
  </div>

  % if disclaimer_text:
  <div class="imprint">
    <hr />
    ${disclaimer_text}
  </div>
  % endif

</div>
