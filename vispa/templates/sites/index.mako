## sites/index.mako

<%! import os.path%>
<%! from vispa import url %>
<%! import cherrypy%>

<%inherit file="/vispa.mako" />
<%namespace name="base" file="/base.mako" />

<%block name="meta">
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <meta name="apple-mobile-web-app-status-bar-style" content="black" />
</%block>

<%block name="js_inline_post">
  <%
    parent.js_inline_post()
  %>

  // use template variables to create the template data object
  var socketUrl = window.location.protocol == "https:" ? "wss" : "ws";
  socketUrl += "://" + window.location.host + "${url.dynamic('bus/index')}";

  var vispaConfig = {
    global: {
      devMode        : ${"true" if dev_mode else "false"},
      logLevel       : "${log_level}",
      useFeedback    : ${"true" if use_feedback else "false"},
      workspaceAction: "${workspace_action}",
      max_request_body_size: "${cherrypy.server.max_request_body_size}",
      url: {
        dynamicBase : "${base_dynamic}",
        staticBase  : "${base_static}"
      },
      socket: {
        url      : socketUrl,
        forcePoll: ${"false" if use_websockets else "true"},
        pollUrl  : "${url.dynamic('bus/poll')}",
        sendUrl  : "${url.dynamic('bus/send')}"
      }
    },
    user: {
      name           : "${username}",
      addWorkspaces  : ${"true" if add_workspaces else "false"},
      isGuest        : ${"true" if is_guest else "false"},
      isAdmin        : ${"true" if is_admin else "false"},
      extensions     : ${extensions},
    },
    preferences: ${preferences},
    session    : ${session},
  };

  requirejs(['${base_static}js/config.js?'+urlArgs], function () {
      require(["vispa/vispa"], function(Vispa) {
      var vispa = window.vispa = new Vispa(vispaConfig);
    });
  });

</%block>

## Main GUI Markup
<div class="vispa-sidebar"></div>

<div class="vispa-body">

  <!-- background -->
  <div class="vispa-table-outer vispa-logo">
    <div class="vispa-table-inner vispa-welcome">
      <img src="${base_static}img/vispa/Vispa2.0Logo_300.png" />
    </div>
  </div>
  <!-- end background -->

  <!-- logger -->
  <div class="vispa-logs-wrapper vispa-logs-wrapper-left">
    <div class="vispa-logs"></div>
  </div>
  <!-- end logger -->

</div>
