## sites/password.mako

<%! import os.path%>
<%! from vispa import url %>

<%inherit file="/vispa.mako" />
<%namespace name="base" file="/base.mako" />

<%block name="js_inline_post">
  <%
    parent.js_inline_post()
  %>

    // use template variables to create the vispa config
    var vispaConfig = {
      url: {
        dynamicBase: "${base_dynamic}",
        staticBase : "${base_static}",
      }
    };

    var passwordHash = "${hash}";
    var passwordUser = ${"'%s'" % username if username else "null"};

    //Load common code that includes config, then load the app logic for this page.
    requirejs(['${base_static}js/config.js?'+urlArgs], function () {
      require(["vispa/password"], function(VispaPassword) {
        window.vispa = new VispaPassword(vispaConfig, passwordUser, passwordHash);
      });
    });

</%block>

## Main GUI Markup
<div class="container">

  <h2>
    <div class="vispa-table-outer">
      <div class="vispa-table-inner">
        <a href="${url.dynamic('/')}" class="vispa-header-logo">
          <img src="${url.static('img/vispa/header_ball.png')}" />
        </a>
        ${welcome_phrase}
      </div>
    </div>
  </h2>

  <hr />

  <div class="well">

  <!-- login -->
  <div class="row">
    % if username:
      <div class="col-lg-4 col-lg-offset-1">
        <h3>Set a password</h3>
        <form class="form-password" role="form">
          <input type="password" id="password1" class="form-control" placeholder="Password" required autofocus />
          <input type="password" id="password2" class="form-control" placeholder="Confirm password" required />
          <button class="btn btn-primary" type="submit">Set password</button>
          <span class="space">or</span>
          <a href="${url.dynamic('login')}">login</a>
        </form>
      </div>
    % else:
      <script>
        setTimeout(function() {
          window.location.href = "${url.dynamic('/')}";
        }, 4000);
      </script>
      <div class="col-lg-6 col-lg-offset-3">
        <h3>Invalid request!</h3>
        <div class="alert alert-danger">
          Redirecting in 4 seconds ...
        </div>
      </div>
    % endif
  </div>
  <!-- end login -->

</div>
