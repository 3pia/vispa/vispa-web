## base.mako
<%! from vispa import url %>

<!DOCTYPE html>
<html>

  <head>

    <meta charset="utf-8" />
    <%block name="meta" />

    <title><%block name="title" /></title>

    <%block name="favicon" />

    <%block name="css_imports" />

    <style>
      <%block name="css_inline" />
    </style>

    <noscript>
      <%block name="noscript" />
    </noscript>

  </head>

  <body <%block name="body_attrs" />>

    ${next.body()}

    <script>
      <%block name="js_inline_pre" />
    </script>

    <%block name="js_imports_pre" />

    <script>
      <%block name="js_inline_post" />
    </script>

    <%block name="js_imports_post" />

  </body>

</html>

<%def name="import_css(file)">
  <link rel="stylesheet" type="text/css" href=${file} />
</%def>

<%def name="import_js(file, deferred=False)">
  % if deferred:
    <script src=${file} deferred="deferred"></script>
  % else:
    <script src=${file}></script>
  % endif
</%def>
