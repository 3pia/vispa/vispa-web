## vispa.mako

<%! import os.path%>
<%! from vispa import url %>

<%inherit file="/base.mako" />
<%namespace name="base" file="/base.mako" />

<%block name="body_attrs"><% parent.body_attrs() %>autofocus</%block>

<%block name="title">VISPA</%block>

<%block name="favicon">
  <link rel="shortcut icon" href="${url.static('img/ico/favicon.ico')}">
</%block>

<%block name="js_inline_pre">
    var baseUrl = "${base_static}";

    % if cache_bust:
    var urlArgs = 'bust=${cache_bust}';
    % else:
    var urlArgs = '';
    % endif

</%block>

<%block name="js_imports_pre">
    % if cache_bust:
    <script src="${base_static}vendor/requirejs/require.js?bust=${cache_bust}"></script>
    % else:
    <script src="${base_static}vendor/requirejs/require.js"></script>
    % endif
</%block>

<%block name="css_inline">
  body > div.blocker {
    position: absolute;
    height: 100%;
    width: 100%;
    background-color: white;
    z-index: 100;
    visibility: visible;
    opacity: 1;
    transition: visibility 0s linear 0.05s, opacity 0.05s linear;
  }
</%block>

<div class="blocker"></div>

${next.body()}
