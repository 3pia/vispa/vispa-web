# -*- coding: utf-8 -*-

# imports
import cherrypy
import logging
import vispa
from vispa.models.project import Project

logger = logging.getLogger(__name__)


class PermissionTool(cherrypy.Tool):
    """
    Tool to require permissions on the global Project or a Project identified by the value request
    parameter. In both cases, it will set the cherrypy.request.project to the Project the
    permissions where checked on.

    use from config file:
    [/path/to/protected/resource]
    tools.permission.on = True
    tools.permission.permissions = ['myextension.read', 'myextension.write']

    use as decorator:
    @cherrypy.expose
    @cherrypy.tools.permission(permissions=['myextension.resources'])
    def resource(self):
        return "Hello, %s!" % cherrypy.request.login

    :param permissions: Permission name(s) which the user must all have.
    :type permissions: str or list[str]
    :param string: Name of the request parameter to parse the project name from. If emptry, will
        always use the global project.
    :param Boolean ignoreInexistent: when true, permissions are ignored if they do not exist (in DB)
    :raises HTTPError(500): if the global Project is not defined
    :raises HTTPError(400): if the request did not specifiy a project
    :raises HTTPError(404): if the Project was not found or is not active
    :raises HTTPError(403): if the user does not have the requested permissions
    """

    def __init__(self):
        """
        Needs to be called after User and Workspace tools: set priority > 70 (WorkspaceTool)
        """
        super(PermissionTool, self).__init__("before_handler", self._check, priority=80)

    def _check(self, permissions, project="", ignoreInexistent=False):
        if not vispa.config("usermanagement", "autosetup", False):
            return

        # transform the params
        if isinstance(permissions, basestring):
            permissions = [permissions]

        # some variables
        req = cherrypy.request
        session = getattr(req, "db", None)
        if session is None:
            return

        if not project:
            projectName = vispa.config("usermanagement", "global_project", None)
            if projectName is None:
                raise cherrypy.HTTPError(500)
        else:
            params = req.private_params if project.startswith("_") else req.params
            projectName = params.get(project, None)
            if projectName is None:
                raise cherrypy.HTTPError(400)

        # get the actual objects of interest
        pro = Project.get_by_name(session, projectName)
        if pro is None or pro.status != Project.ACTIVE:
            raise cherrypy.HTTPError(404)

        # test for permissions
        if permissions:
            if not req.user.has_all_permissions(pro, permissions, ignoreInexistent):
                raise cherrypy.HTTPError(403)

        # save back the project
        req.project = pro
