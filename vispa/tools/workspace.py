# -*- coding: utf-8 -*-

import cherrypy
import vispa
from vispa.models.workspace import Workspace
import logging

logger = logging.getLogger(__name__)


class WorkspaceTool(cherrypy.Tool):
    def __init__(self):
        cherrypy.Tool.__init__(self, "before_handler", self.before_handler,
                               priority=70)

    def _setup(self):
        cherrypy.Tool._setup(self)

    def before_handler(self, **conf):
        request = cherrypy.request
        db = request.db

        request.workspace = None
        wid = request.private_params.get("_workspaceId", None)

        if wid is None:
            logger.debug("No workspace selected")
        else:
            workspace = Workspace.get_by_id(db, wid)
            if not isinstance(workspace, Workspace):
                logger.debug("Selected Workspace (%s) not found" % wid)
            else:
                logger.debug("Workspace: %s (%d)" %
                             (workspace.name, workspace.id))
                request.workspace = workspace
