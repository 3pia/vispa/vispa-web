# -*- coding: utf-8 -*-

import cherrypy
from sqlalchemy.orm import scoped_session, sessionmaker


class SqlAlchemyTool(cherrypy.Tool):

    def __init__(self, engine):
        """
        The SA tool is responsible for associating a SA session
        to the SA engine and attaching it to the current request.
        Since we are running in a multithreaded application,
        we use the scoped_session that will create a session
        on a per thread basis so that you don't worry about
        concurrency on the session object itself.

        This tools binds a session to the engine each time
        a requests starts and commits/rollbacks whenever
        the request terminates.
        """
        cherrypy.Tool.__init__(self, 'before_handler',
                               self.bind_session, priority=60)

        self._engine = engine
        assert self._engine != None
        self._session = scoped_session(sessionmaker(autoflush=True,
                                                    autocommit=False))

    def _setup(self):
        cherrypy.Tool._setup(self)
        cherrypy.request.hooks.attach('on_end_resource',
                                      self.commit_transaction, priority=40)

    def bind_session(self):
        """
        Attaches a session to the request's scope by requesting
        the SA plugin to bind a session to the SA engine.
        """
        assert self._engine != None
        self._session.configure(bind=self._engine)
        cherrypy.request.db = self._session()
        assert cherrypy.request.db != None

    def commit_transaction(self):
        """
        Commits the current transaction or rolls back
        if an error occurs. Removes the session handle
        from the request's scope.
        """
        db = getattr(cherrypy.request, 'db', None)
        if db is not None:
            try:
                db.commit()
            except:
                db.rollback()
            finally:
                db.close()
                self._session.remove()
