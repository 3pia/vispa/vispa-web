# -*- coding: utf-8 -*-

"""
Cherrypy tool that moves parameters that start with an underscore from the querystring to
request.private_params.
"""

import cherrypy
import vispa
import logging

logger = logging.getLogger(__name__)


class PrivateParameterFilter(cherrypy.Tool):

    def __init__(self):
        cherrypy.Tool.__init__(self, "before_handler", self.before_handler, priority=55)

    def before_handler(self):
        req = cherrypy.request
        private_params = {}
        for key in req.params.keys():
            if key.startswith("_"):
                private_params[key] = req.params.pop(key)
        req.private_params = private_params
