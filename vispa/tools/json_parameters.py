# -*- coding: utf-8 -*-

import cherrypy
import logging
import json
import vispa

logger = logging.getLogger(__name__)


class JsonParameters(cherrypy.Tool):

    def __init__(self):
        cherrypy.Tool.__init__(self, "before_handler", self.before_handler, priority=56)

    def before_handler(self):
        req = cherrypy.request

        # this tool only accepts encoded json
        if "application/json" not in req.headers["Content-Type"].lower():
            return

        if req.method == "POST":
            data = req.body.fp.read()
        elif req.method in ["GET", "HEAD"]:
            data = req.params.keys().pop(0)

        try:
            if data in req.params.keys():
                del req.params[data]
            data = json.loads(data)
            req.params.update(data)
        except:
            vispa.log_exception()
