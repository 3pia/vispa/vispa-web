# -*- coding: utf-8 -*-

__all__ = ["MakoTool"]


import cherrypy
from mako import exceptions


class MakoTool(cherrypy.Tool):

    def __init__(self, common_data=None):
        super(MakoTool, self).__init__("before_handler", self.callable, priority=100)

    def callable(self, template=None, common_data=None, **kwargs):
        # get template
        mako_template = cherrypy.engine.publish("lookup_template", template).pop()
        if not mako_template:
            return exceptions.html_error_template().render(error="Template not present")

        if common_data is None:
            common_data = {}

        mako_data = common_data.copy()

        # get the original handler
        innerfunc = cherrypy.serving.request.handler

        # create a wrapper for it
        def wrapper(*args, **kwargs):
            request = cherrypy.serving.request

            # call the original handler
            data = innerfunc(*args, **kwargs)
            if not isinstance(data, dict):
                error = "Template data is no dict: %s" % str(data)
                return exceptions.html_error_template().render(error=error)

            # combine data
            mako_data.update(data)

            # actual rendering
            try:
                return mako_template.render(**mako_data)
            except:
                error = "Error while rendering template with given data: %s" % str(data)
                return exceptions.html_error_template().render(error=error)

        # set the new handler
        cherrypy.serving.request.handler = wrapper
