# -*- coding: utf-8 -*-

"""
Definition of the vispa ajax tool.
"""


import cherrypy
import json
import vispa
from vispa import log_exception, config, AjaxException
from vispa.controller import strongly_expire
from rpyc.core.vinegar import GenericException


class AjaxTool(cherrypy.Tool):
    """
    Ajax tool that takes the output of a wrapper inner function
    and returns a json encoded dictionary containing the following entries:
    - code: A response code, basically an appropriate http status. The status of the
      cherrypy response object is also set to this value. Thus, the code is 200 if
      there were no errors.
    - data: The attached payload with an arbitrary type.
    - message: In case of an error, i.e. code != 200, an additional error message. On success,
      i.e. code = 200, this should be empty.
    - alert: A boolean that desribes whether the error message should be shown in the GUI using a
      dialog or something similar.
    """

    def __init__(self):
        super(AjaxTool, self).__init__("before_handler", self.callable, priority=50)

    def callable(self, encoded=False):
        """
        The callable of this tool. If *encoded* is *True*, the return value of the wrapped inner
        function is expected to be already json encoded. Otherwise, it will be encoded using
        ``json.dumps()``.
        """
        innerfunc = cherrypy.serving.request.handler

        def wrapper(*args, **kwargs):
            response = cherrypy.serving.response

            # define the result object
            # exclude the data entry as it may be already encoded
            # the actual result creation is done after the try-except block
            result = {
                "code"   : 200,
                "message": None,
                "alert"  : False
            }
            data = "null"

            try:
                try:
                    data = innerfunc(*args, **kwargs)
                    if not encoded:
                        data = json.dumps(data)

                except (AjaxException, GenericException) as e:
                    remote_name = "vispa.remote.AjaxException"
                    if isinstance(e, GenericException) and type(e).__name__ != remote_name:
                        raise

                    result["code"]    = e.code
                    result["message"] = e.message
                    result["alert"]   = e.alert

                except Exception as e:
                    raise

            except cherrypy.CherryPyException:
                raise

            except Exception as e:
                result["code"]    = 500
                result["message"] = "an unhandled exception occured"
                result["alert"]   = True

                if config("vispa", "devmode", False):
                    result["message"] += ": " + str(e) 

                log_exception()

            # changes to the response object
            response.headers["Content-Type"] = "application/json;charset=utf-8"
            #response.status = result["code"]

            # json encode the result
            result = '%s, "data": %s}' % (json.dumps(result)[:-1], data)

            return result

        # actual wrapping
        cherrypy.serving.request.handler = strongly_expire(wrapper)
