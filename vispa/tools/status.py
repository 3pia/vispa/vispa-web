import threading
import time

import cherrypy


class ThreadStatus(object):

    start = None
    end = None
    url = None

    def __init__(self, threadid):
        self.id = threadid

    def last_req_time(self):
        if self.end is None:
            return time.time() - self.start
        else:
            return self.end - self.start

    def idle_time(self):
        if self.end is None:
            return 0
        return time.time() - self.end


class StatusMonitor(cherrypy.Tool):
    """Register the status of each thread."""

    def __init__(self):
        self._point = 'on_start_resource'
        self._name = 'status'
        self._priority = 50
        self.seen_threads = {}

    def callable(self):
        threadID = threading._get_ident()
        ts = self.seen_threads.setdefault(threadID, ThreadStatus(threadID))
        ts.start = cherrypy.response.time
        ts.url = cherrypy.url()
        ts.end = None

    def unregister(self):
        """Unregister the current thread."""
        threadID = threading._get_ident()
        if threadID in self.seen_threads:
            self.seen_threads[threadID].end = time.time()

    def _setup(self):
        cherrypy.Tool._setup(self)
        cherrypy.request.hooks.attach('on_end_resource', self.unregister)
