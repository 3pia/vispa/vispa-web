# -*- coding: utf-8 -*-

"""
Definition of the vispa user tool.
"""

# imports
import cherrypy
import os
import logging
import vispa
from vispa import AjaxException
from vispa.models.user import User

logger = logging.getLogger(__name__)


class UserTool(cherrypy.Tool):
    """
    The user tool checks whether the session contains the field "user_id". If it exists, a reference
    to the corresponding user is stored as "cherrypy.request.user". Otherwise, the request is either
    redirected or a 401 error is returned using the ajax tool.
    """

    def __init__(self):
        super(UserTool, self).__init__("before_handler", self._fetch, priority=65)

    def _setup(self):
        super(UserTool, self)._setup()
        cherrypy.request.hooks.attach("before_finalize", self._cleanup, priority=35)

    def _fetch(self, redirect_url, redirect_url_reverse, redirect=True, reverse=False):
        """
        Actual tool logic. If a user is not logged-in, i.e. no valid user id can be found in the
        session, access to the requested resource is refused (401). Then, the user is redirected to
        *redirect_url* if *redirect* is *True*, or a json formatted error is returned otherwise.
        When *reverse* is *True*, the above rules flip and *redirect_url_reverse* is used instead.
        However, in this case *redirect* is treated as *True* regardless of the value passed in the
        arguments.
        """
        # prepare some values
        if reverse:
            redirect = True
            url = redirect_url_reverse
        else:
            url = redirect_url

        # get the user id and determine wheter access is granted or forbidden
        user_id = cherrypy.session.get("user_id", None)
        has_access = False

        if reverse and user_id is None:
            has_access = True
        elif user_id is not None:
            user = User.get_by_id(cherrypy.request.db, user_id)
            if isinstance(user, User):
                has_access = True
                cherrypy.request.user = user
                # update the users last request time
                User.update_last_request(cherrypy.request.db, user.id)

        # when access is forbidden, create a handler that replaces the original
        # one in order to stop the request via raise an exception
        if not has_access:
            def handler(*args, **kwargs):
                if redirect:
                    logger.debug("Redirect %s" % url)
                    raise cherrypy.HTTPRedirect(url)
                else:
                    raise cherrypy.HTTPError(401)

            cherrypy.serving.request.handler = handler

    def _cleanup(self):
        cherrypy.request.user = None
