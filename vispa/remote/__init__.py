from json import dumps
connection = None
log = None
send = None


def send_topic(topic, data=None, window_id=None, user_id=None):
    data = dumps({"data": data, "topic": topic}, ensure_ascii=True)
    send(window_id=window_id, user_id=user_id, encode_json=False, data=data)


# copy of vispa.AjaxException
class AjaxException(Exception):
    """
    AjaxException that is handled by the ajax tool and that can be raised in controller methods.
    *message* is the error message to show. *code* should be an integer that represents a specific
    type of exception. If *code* is *None* and *message* is an integer representing a http status
    code, the error message is set to the standard error message for that http error. If *alert* is
    *True*, the message is shown in a dialog in the GUI.
    """

    def __init__(self, message, code=None, alert=True):
        """
        __init__(message, code=500, alert=True)
        """
        if code is None:
            if isinstance(message, int) and message in response_codes:
                code    = message
                message = response_codes[code][0]
            else:
                code = 500

        super(AjaxException, self).__init__(message)

        self.code  = code
        self.alert = alert


def raise_ajax(fn=None, **kwargs):
    """
    Decorator that transforms raised exceptions into AjaxExceptions.
    """
    def decorator(fn):
        def wrapper(*args, **_kwargs):
            try:
                return fn(*args, **_kwargs)
            except Exception as e:
                if isinstance(e, AjaxException):
                    raise
                else:
                    raise AjaxException(str(e), **kwargs)
        return wrapper
    return decorator if fn is None else decorator(fn)
