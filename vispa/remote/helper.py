
def truncated_utf8(utf8_bytes):
    """ returns the bytes containing complete and truncated bytes """
    if not utf8_bytes:
        return b"", b""

    # https://en.wikipedia.org/wiki/UTF-8#Description
    
    # last byte is 1 byte code pint
    if not utf8_bytes[-1] & 0b10000000:
        return utf8_bytes, b""
    else:
        # find initial code point
        for i in [-2, -3, -4]:
            if utf8_bytes[i] & 0b11000000:
                return utf8_bytes[:i+1], utf8_bytes[i+1:]
        raise Exception("invalid utf-8 string")
    
class UTF8Reader(object):
    
    def __init__(self, f):
        self._file = f
        self._remainder = None
        
    def read(self, n=-1):
        utf8_bytes = self._file.read(n)
        if utf8_bytes is None:
            return None
        if self._remainder:
            data, self._remainder = truncated_utf8(self._remainder + utf8_bytes)
        else:
            data, self._remainder = truncated_utf8(utf8_bytes)
        return data
    
    
class UTF8Buffer(object):
    """
    Buffers incoming UTF8 encoded data, and prevents chunks from being created in the middle of a
    multi byte sequence.
    """
    def __init__(self):
        self.buffer = ""

    def fill(self, data):
        """
        Fill the buffer with the given *data*.
        """
        self.buffer += data

    def read(self, count=None):
        """
        Get a maximum of *count* bytes from the buffer. UTF8 multibyte characters will not be
        broken apart. If *count* is *None* the current buffer length is used.
        """
        if count is None:
            count = len(self.buffer)
        else:
            count = min(int(count), len(self.buffer))
        if count <= 0:
            return ""
        for offset in range(4): # longest multibyte sequence is 4 bytes
            if offset >= count: # we would attempt to cut everything off
                return "" # just return nothing and keep buffering
            selected = self.buffer[:count-offset]
            try:
                # this will fail if we have a truncated multibyte sequence
                selected.decode("utf-8")
            except UnicodeDecodeError:
                continue
            else:
                self.buffer = self.buffer[count-offset:] # get the remainder of the buffer
                break
        else:
            # everything failed (most likely there are illegal chars)
            selected = self.buffer.decode('utf-8','ignore').encode("utf-8")
            self.buffer = ""
        return selected

    def passThru(self, data, count=None):
        """
        A convenience function for adding data to the buffer and reading up to *count* of it again.
        If *count* is *None* the current buffer length is used.
        """
        self.fill(data)
        return self.read(count=count)
