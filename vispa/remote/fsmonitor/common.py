# Copyright (c) 2010 Luke McCarthy <luke@iogopro.co.uk>
#
# This is free software released under the MIT license.
# See COPYING file for details, or visit:
# http://www.opensource.org/licenses/mit-license.php
#
# The file is part of FSMonitor, a file-system monitoring library.
# https://github.com/shaurz/fsmonitor

class FSMonitorError(Exception):
    pass

class FSMonitorOSError(OSError, FSMonitorError):
    pass

class FSEvent(object):
    def __init__(self, watch, action, name=""):
        self.watch = watch
        self.name = name
        self.action = action

    @property
    def action_name(self):
        return self.action_names[self.action]

    @property
    def path(self):
        return self.watch.path

    @property
    def user(self):
        return self.watch.user

    Access      = 0x001
    Modify      = 0x002
    Attrib      = 0x004
    Create      = 0x008
    Delete      = 0x010
    DeleteSelf  = 0x020
    MoveFrom    = 0x040
    MoveTo      = 0x080
    MoveSelf    = 0x100
    All         = 0x1FF

    action_names = {
        Access     : "access",
        Modify     : "modify",
        Attrib     : "attrib",
        Create     : "create",
        Delete     : "delete",
        DeleteSelf : "delete self",
        MoveFrom   : "move from",
        MoveTo     : "move to",
        MoveSelf   : "move self",
    }
