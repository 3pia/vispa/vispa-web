# -*- coding: utf-8 -*-

"""
Basic functionality for the VISPA platform
"""

from StringIO import StringIO
from fileinput import filename
import csv
import inspect
import logging
import os
import re
import signal
import smtplib
import socket
import sys
import tempfile
import thread
import token
import tokenize
import traceback
import copy

import cherrypy
from cherrypy.lib.httputil import response_codes

from . import url

from smtplib import SMTP
from email.MIMEText import MIMEText
from email.Header import Header
from email.Utils import parseaddr, formataddr

try:
    import ConfigParser as cp
except:
    import configparser as cp

logger = logging.getLogger(__name__)

# http://stackoverflow.com/a/24517154
from .version import __version__
VERSION = __version__
RELEASE_VERSION, _, BUGFIX_VERSION = __version__.rpartition(".")
# http://semver.org/
MAJOR_VERSION, MINOR_VERSION, PATCH_VERSION = __version__.split(".")

_datapath = 'data'
_configpath = 'conf'
_codepath = os.path.dirname(inspect.getfile(inspect.currentframe()))
_codepath = os.path.dirname(_codepath)


def datapath(*args):
    """
    returns the path relative to the datapath
    """
    return os.path.join(_datapath, *args)


def set_datapath(p):
    global _datapath
    _datapath = p


def configpath(*args):
    return os.path.join(_configpath, *args)


def set_configpath(p):
    global _configpath
    _configpath = os.path.abspath(p)


def codepath(*args):
    return os.path.join(_codepath, *args)


def set_codepath(p):
    global _codepath
    _codepath = p


def config_files(directory):
    conf_d_dir = configpath(directory)
    if os.path.isdir(conf_d_dir):
        config_files = [f for f in os.listdir(conf_d_dir) if f.endswith(".ini") and os.path.isfile(os.path.join(conf_d_dir, f))]
        config_files.sort()
        return [os.path.join(conf_d_dir, f) for f in config_files]
    else:
        return []

def setup_config(conf_dir):
    logger.info('Using %s as config dir.' % conf_dir)
    set_configpath(conf_dir)

    logger.info('Read config file: %s', configpath('vispa.ini'))
    config.read(configpath('vispa.ini'))

    # load all ini files
    _config_files = config_files('vispa.d')
    logger.info('Read config files: %s', _config_files)
    config.read(_config_files)


class VispaConfigParser(cp.SafeConfigParser):
    def __call__(self, section, option, default=None):
        if self.has_option(section, option):
            value = self.get(section, option)
            # try to cast according to 'default'
            if default is None:
                return value
            # boolean
            if isinstance(default, bool):
                if value == 'True':
                    return True
                elif value == 'False':
                    return False
            # int
            if isinstance(default, int):
                return int(value)
            # long
            if isinstance(default, long):
                return long(value)
            # string
            if isinstance(default, str):
                return str(value)
            # list
            if isinstance(default, list):
                l = []
                g = tokenize.generate_tokens(StringIO(value).readline)
                for toknum, tokval, _, _, _  in g:
                    if toknum == token.NAME or toknum == token.NUMBER:
                        l.append(tokval)
                    elif toknum == token.STRING:
                        l.append(tokval[1:-1])
                return l

            # tuple
            if isinstance(default, tuple):
                if len(value):
                    return tuple(value.split(','))
                else:
                    return ()
            # TODO: dict
            return value
        else:
            return default

# setup a config parser for "vispa.ini"
config = VispaConfigParser()

# a little publish/subscribe system
_callbacks = {}


def subscribe(topic, callback):
    if callback not in _callbacks.get(topic, []):
        _callbacks.setdefault(topic, []).append(callback)
register_callback = subscribe


def publish(topic, *args, **kwargs):
    values = []
    for callback in _callbacks.get(topic, []):
        try:
            value = callback(*args, **kwargs)
            values.append(value)
        except Exception:
            values.append(None)
            log_exception()
    return values
fire_callback = publish


def exception_string():
    exc_type, exc_value, exc_tb = sys.exc_info()
    st = traceback.format_exception(exc_type, exc_value, exc_tb)
    return  ''.join(st)


def log_exception(prefix=""):
    sys.stderr.write(prefix + exception_string())


class Netstat(object):
    """
    Parses /dev/net/tcp to determine which user owns the specified port
    """

    _proc_net_tcp_re = re.compile("\W+([0-9]+):\W+([0-9A-F]+):([0-9A-F]+)"
                                  "\W+([0-9A-F]+):([0-9A-F]+)\W+([0-9A-F]+)"
                                  "\W+([0-9A-F]+):([0-9A-F]+)"
                                  "\W+([0-9A-F]+):([0-9A-Z]+)"
                                  "\W+([0-9A-F]+)\W+([0-9A-F]+)"
                                  "\W+([0-9A-F]+).*")

    _IDX_LOCAL_IP = 2
    _IDX_LOCAL_PORT = 3
    _IDX_REMOTE_IP = 4
    _IDX_REMOTE_PORT = 5
    _IDX_USERID = 12

    @staticmethod
    def _ipv4_to_hex(ip):
        parts = ip.split(".")
        parts.reverse()
        return "".join(["%02X" % int(part) for part in parts])

    @staticmethod
    def _port_to_hex(port):
        return "%02X" % port

    @staticmethod
    def _get_socket_owner_file(filename, local_ip, local_port,
                               remote_ip, remote_port):
        f = open(filename, 'r')
        userid = None
        for line in f:
            m = Netstat._proc_net_tcp_re.match(line)
            if not m:
                continue
            if m.group(Netstat._IDX_LOCAL_IP) != local_ip:
                continue
            if m.group(Netstat._IDX_LOCAL_PORT) != local_port:
                continue
            if m.group(Netstat._IDX_REMOTE_IP) != remote_ip:
                continue
            if m.group(Netstat._IDX_REMOTE_PORT) != remote_port:
                continue
            userid = int(m.group(Netstat._IDX_USERID))
            break
        f.close()
        return userid

    @staticmethod
    def _get_socket_owner_ipv4(local_ip, local_port, remote_ip, remote_port):
        local_port = Netstat.port_to_hex(local_port)
        local_ip = Netstat.ipv4_to_hex(local_ip)
        remote_port = Netstat.port_to_hex(remote_port)
        remote_ip = Netstat.ipv4_to_hex(remote_ip)
        return Netstat.get_socket_owner_file('/proc/net/tcp',
                                             local_ip, local_port,
                                             remote_ip, remote_port)

    @staticmethod
    def get_socket_owner(local_ip, local_port, remote_ip, remote_port):
        """
        Returns the system id of the local user which established the
        specifiec connection

        :param local_ip: ip address of the local peer
        :param local_ip: port the local peer
        :param remote_ip: ip address of the remote peer
        :param remote_ip: port the remote peer
        :rtype: user id of the user who opened this port or None
        """
        # prepare adresses
        _locals = socket.getaddrinfo(local_ip, local_port,
                                     0, 0, socket.SOL_TCP)
        # [(2, 1, 6, '', ('127.0.0.1', 4282))]
        _remotes = socket.getaddrinfo(remote_ip, remote_port,
                                      0, 0, socket.SOL_TCP)
        # [(2, 1, 6, '', ('127.0.0.1', 53726))]

        for local in _locals:
            for remote in _remotes:
                if remote[0] != local[0]:
                    continue
                if remote[4][0] != local[4][0]:
                    continue
                if remote[0] == socket.AF_INET:
                    return Netstat.get_socket_owner_ipv4(local[4][0],
                                                         local[4][1],
                                                         remote[4][0],
                                                         remote[4][1])
                if remote[0] == socket.AF_INET6:
                    return Netstat.get_socket_owner_ipv6(local[4][0],
                                                         local[4][1],
                                                         remote[4][0],
                                                         remote[4][1])
        return None


class AjaxException(Exception):
    """
    AjaxException that is handled by the ajax tool and that can be raised in controller methods.
    *message* is the error message to show. *code* should be an integer that represents a specific
    type of exception. If *code* is *None* and *message* is an integer representing a http status
    code, the error message is set to the standard error message for that http error. If *alert* is
    *True*, the message is shown in a dialog in the GUI.
    """

    def __init__(self, message, code=None, alert=True):
        """
        __init__(message, code=500, alert=True)
        """
        if code is None:
            if isinstance(message, int) and message in response_codes:
                code    = message
                message = response_codes[code][0]
            else:
                code = 500

        super(AjaxException, self).__init__(message)

        self.code  = code
        self.alert = alert


# the bus
bus = None


def send_mail(addr, subject="", content="", sender_addr=None, smtp_host=None,
              smtp_port=None):
    """Send an email.

    All arguments should be Unicode strings (plain ASCII works as well).

    Only the real name part of sender and recipient addresses may contain
    non-ASCII characters.

    The email will be properly MIME encoded and delivered though SMTP to
    localhost port 25.  This is easy to change if you want something different.

    The charset of the email will be the first one out of US-ASCII, ISO-8859-1
    and UTF-8 that can represent all the characters occurring in the email.
    """

    if smtp_host is None:
        smtp_host = config("mail", "smtp.host", "localhost")
    if smtp_port is None:
        smtp_port = config("mail", "smtp.port", 0)
    if sender_addr is None:
        sender_addr = config("mail", "sender_address", "noreply@example.org")

    # Header class is smart enough to try US-ASCII, then the charset we
    # provide, then fall back to UTF-8.
    header_charset = 'ISO-8859-1'

    # We must choose the body charset manually
    for body_charset in 'US-ASCII', 'ISO-8859-1', 'UTF-8':
        try:
            content.encode(body_charset)
        except UnicodeError:
            pass
        else:
            break

    # Split real name (which is optional) and email address parts
    sender_name, sender_addr = parseaddr(sender_addr)
    recipient_name, recipient_addr = parseaddr(addr)

    # We must always pass Unicode strings to Header, otherwise it will
    # use RFC 2047 encoding even on plain ASCII strings.
    sender_name = str(Header(unicode(sender_name), header_charset))
    recipient_name = str(Header(unicode(recipient_name), header_charset))

    # Make sure email addresses do not contain non-ASCII characters
    sender_addr = sender_addr.encode('ascii')
    recipient_addr = recipient_addr.encode('ascii')

    # Create the message ('plain' stands for Content-Type: text/plain)
    msg = MIMEText(content.encode(body_charset), 'plain', body_charset)
    msg['From'] = formataddr((sender_name, sender_addr))
    msg['To'] = formataddr((recipient_name, recipient_addr))
    msg['Subject'] = Header(unicode(subject), header_charset)

    # Send the message via SMTP to localhost:25
    smtp = SMTP(smtp_host, smtp_port)
    smtp.sendmail(sender_addr, addr, msg.as_string())
    smtp.quit()


def thread_stacktraces():
    frames = sys._current_frames()
    data = {}
    for thread_id, frame in frames.iteritems():
        stacktrace = StringIO()
        traceback.print_stack(frame, file=stacktrace)
        data[thread_id] = stacktrace.getvalue()
    return data


def dump_thread_status(f=None):
    # open the file
    if f == None:
        f = config('web', 'status_filename', None)

    if isinstance(f, (str, unicode)):
        f = f % {'pid': os.getpid()}
        f = open(f, 'a')

    if not hasattr(f, 'write'):
        return

    # header
    f.write("""
    <html>
    <head>
        <title>CherryPy Status</title>
    </head>
    <body>
    <h1>CherryPy Status</h1>
    """)

    # worker table
    f.write("""
    <table>
      <tr>
        <th>Thread ID</th>
        <th>Idle Time</th>
        <th>Last Request Time</th>
        <th>URL</th>
      </tr>
    """)
    threads = copy.deepcopy(cherrypy.tools.status.seen_threads.values())
    threads.sort(key=lambda thread: (thread.idle_time(), thread.last_req_time()))
    for thread in threads:
        f.write("""
        <tr>
          <th><a href=\"#%s\">%s</a></th>
          <td>%.4f</td>
          <td>%.4f</td>
          <td>%s</td>
         </tr>""" % (thread.id, thread.id, thread.idle_time(),
                     thread.last_req_time(), thread.url))
    f.write("</table>")

    # threads
    for thread_id, st in thread_stacktraces().items():
        f.write("""
        <h3><a id=\"%s\">%s</a></h3>
        <pre>\n%s</pre>""" % (thread_id, thread_id, st))

    f.write("""
    </body>
    </html>
    """)

    if hasattr(f, 'flush'):
        f.flush()


def dump_thread_status_on_signal(signal, stack):
    f = os.path.join(tempfile.gettempdir(),
                     "cherrypy-status-%d.html" % os.getpid())
    dump_thread_status(f)


def setup_thread_dump():
    signal.signal(signal.SIGUSR2, dump_thread_status_on_signal)
