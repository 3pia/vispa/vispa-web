# -*- coding: utf-8 -*-

from threading import Thread
import StringIO
import imp
import logging
import os.path
import signal
import sys
import tempfile
import time
import traceback
import zipfile

# make i/o binary
sys.stdin = os.fdopen(sys.stdin.fileno(), 'rb', 0)
sys.stdout = os.fdopen(sys.stdout.fileno(), 'wb', 0)

# setup logging
logging.basicConfig(
    level=logging.DEBUG,
    stream=sys.stderr,
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s")
logger = logging.getLogger("vispa.workspace-loader")

# use provided loglevel
try:
    loglevel = int(sys.stdin.readline())
except:
    loglevel = 0
logging.getLogger().setLevel(loglevel)

# read zipped packages
length = int(sys.stdin.readline())
zipbuffer = sys.stdin.read(length)
iobuffer = StringIO.StringIO(zipbuffer)
packagezip = zipfile.ZipFile(iobuffer)


class InMemoryZipImporter:

    def __init__(self, packagezip):
        self.__packagezip = packagezip

    def _file_exists(self, filename):
        try:
            self.__packagezip.getinfo(filename)
            return True
        except KeyError:
            return False

    def _module_info(self, fullname):
        '''
        returns filename, is package, is bytecode
        '''
        base = fullname.replace(".", "/")

        # check compiled packages
        for suffix in ["/__init__.pyc", "/__init__.pyo"]:
            filename = base + suffix
            if self._file_exists(filename):
                return filename, True, True

        # check source packages
        filename = base + "/__init__.py"
        if self._file_exists(filename):
            return filename, True, False

        # check compiled modules
        for suffix in [".pyc", ".pyo"]:
            filename = base + suffix
            if self._file_exists(filename):
                return filename, False, True

        # check source modules
        filename = base + ".py"
        if self._file_exists(filename):
            return filename, False, False

        # not found
        return None, False, False

    def _get_code(self, filename):
        filehandle = self.__packagezip.open(filename)
        code = compile(filehandle.read(), filename, "exec")
        return code

    def find_module(self, fullname, path=None):
        logger.debug("find module %s, path %s" % (fullname, path))
        filename, _, isbytecode = self._module_info(fullname)
        if filename is None or isbytecode:
            return None
        else:
            return self

    def load_module(self, fullname):
        filename, ispkg, _ = self._module_info(fullname)

        code = self._get_code(filename)
        mod = sys.modules.setdefault(fullname, imp.new_module(fullname))
        # mod.__file__ = "<%s>" % self.__class__.__name__
        # mod.__file__ = "zip:/" + fullname.replace(".", "/")
        mod.__file__ = fullname.replace(".", "/")
        mod.__loader__ = self
        if ispkg:
            mod.__path__ = []
            mod.__package__ = fullname
        else:
            mod.__package__ = fullname.rpartition('.')[0]
        exec(code, mod.__dict__)
        return mod

# install memory loader
sys.meta_path.insert(0, InMemoryZipImporter(packagezip))


def thread_stacktraces():
    frames = sys._current_frames()
    data = {}
    for thread_id, frame in frames.iteritems():
        stacktrace = StringIO.StringIO()
        traceback.print_stack(frame, file=stacktrace)
        data[thread_id] = stacktrace.getvalue()
    return data


def dump_thread_status(f=None):
    if isinstance(f, (str, unicode)):
        f = f % {'pid': os.getpid()}
        f = open(f, 'w')

    if not hasattr(f, 'write'):
        return

    # header
    f.write("""
    <html>
    <head>
        <title>VISPA Workspace Loader Threads</title>
    </head>
    <body>
    <h1>VISPA Workspace Loader Threads</h1>
    """)

    # threads
    for thread_id, st in thread_stacktraces().items():
        f.write("""
        <h3><a id=\"%s\">%s</a></h3>
        <pre>\n%s</pre>""" % (thread_id, thread_id, st))

    f.write("""
    </body>
    </html>
    """)

    if hasattr(f, 'flush'):
        f.flush()


def dump_thread_status_on_signal(signal, stack):
    try:
        f = os.path.join(tempfile.gettempdir(),
                         "vispa-workspace-loader-%d.html" % os.getpid())
        dump_thread_status(f)
    except Exception as e:
        sys.stderr.write((str(e)))


# install dump threads signal
signal.signal(signal.SIGUSR2, dump_thread_status_on_signal)


import rpyc
import vispa.remote


class Service(rpyc.Service):

    def exposed_getmodule(self, name):
        """imports an arbitrary module"""
        return __import__(name, None, None, "*", -1)

class RemoteHandler(logging.Handler):

    def __init__(self, handler):
        logging.Handler.__init__(self)
        self._handler = rpyc.async(handler)

    def emit(self, record):
        msg = self.format(record)
        self._handler(record.name, record.levelno, msg)

def main_loop(fin, fout):
    # connect the rpyc server to fifo
    stream = rpyc.PipeStream(fin, fout)
    channel = rpyc.Channel(stream, False)
    service = Service() if rpyc.version.version[0] >= 4 else  Service
    vispa.remote.connection = rpyc.Connection(
        service, channel, {
            'allow_public_attrs': True,
            'allow_setattr': True,
            'sync_request_timeout': 300,
        })
    vispa.remote.log = vispa.remote.connection.root.log
    vispa.remote.send = vispa.remote.connection.root.send

    exising_handlers = []
    try:
        exising_handlers = logging.getLogger().handlers
        logging.getLogger().handlers = [RemoteHandler(vispa.remote.log)]
        logger.info("connected, serving")
    except:
        logging.getLogger().handlers = exising_handlers
        logger.exception("main_loop")
        return

    logger.info("daemon loop")

    try:
        vispa.remote.connection.serve_threaded()
    except EOFError:
        logging.getLogger().handlers = exising_handlers
        logger.exception("main_loop")
        pass
    except Exception:
        logging.getLogger().handlers = exising_handlers
        logger.exception("main_loop")
    finally:
        logging.getLogger().handlers = exising_handlers
        vispa.remote.connection.close()

    logger.debug("daemon done")


# notify subprocesses that we are on a workspace
os.environ['VISPA_WORKSPACE'] = "1"

# prepare stdin/stdout to be used by RPYC

# send magic string to notify server that the following output is valid
sys.stdout.flush()
sys.stderr.flush()

# redirect python sys.stdout and ALL output to err to /dev/null
ssh_stdin = sys.stdin

# keep original connection to ssh
ssh_stdout = os.fdopen(os.dup(sys.stdout.fileno()), "wb")

# keep fd so ssh does not shutdown
ssh_stderr = os.fdopen(os.dup(sys.stderr.fileno()), "wb")

# redirect all upcoming output to /dev/null
if loglevel == logging.DEBUG:
    filename = os.getenv("TMPDIR", "/tmp")
    filename = os.path.join(filename, "workspace-loader-%d-out.txt" % os.getpid())
    os.dup2(os.open(filename, os.O_WRONLY | os.O_NONBLOCK | os.O_CREAT), sys.stdout.fileno())
else:
    os.dup2(os.open(os.devnull, os.O_WRONLY | os.O_NONBLOCK), sys.stdout.fileno())

# redirect fd 2 to /dev/null. this even works for 3rd party libraries like ROOT
if loglevel == logging.DEBUG:
    filename = os.getenv("TMPDIR", "/tmp")
    filename = os.path.join(filename, "workspace-loader-%d-err.txt" % os.getpid())
    os.dup2(os.open(filename, os.O_WRONLY | os.O_NONBLOCK | os.O_CREAT), sys.stderr.fileno())
else:
    os.dup2(os.open(os.devnull, os.O_WRONLY | os.O_NONBLOCK), sys.stderr.fileno())

ssh_stdout.write("START_RPYC".encode("ascii"))
ssh_stdout.flush()

# start main loop in thread
t = Thread(target=main_loop, args=(ssh_stdin, ssh_stdout))
t.daemon = True
t.start()

# wait for main loop to finish
while t.is_alive():
    t.join(0.5)
    time.sleep(0.5)
