# -*- coding: utf-8 -*-
"""
Functions to connect and control connections to workers
"""

from StringIO import StringIO
import inspect
import logging
import os
import random
import select
import shutil
import subprocess
import sys
import tempfile
import threading
import time
import zipfile
import Queue
from threading import Thread


from vispa import AjaxException
from vispa.models.user import User
from vispa.models.workspace import Workspace
import cherrypy
import paramiko
import rpyc.core.stream
from rpyc.version import version as rpyc_version
import vispa.remote
import types
logger = logging.getLogger(__name__)

if rpyc_version[0] >= 4:
    from rpyc.lib import Timeout

RPYC_COMPRESSION = False

_remote_files = []
_zip_file = None


def _create_zip():
    logger.debug("create workspace zip")
    zfile, zpath = tempfile.mkstemp()
    zfile = os.fdopen(zfile, "w")
    z = zipfile.ZipFile(zfile, "w", compression=zipfile.ZIP_DEFLATED)

    remotefolders = set()
    remotepackages = set()
    for localfilename, remotefilename in _remote_files:
        if remotefilename.startswith("/"):
            remotefilename = remotefilename[1:]
        logger.debug("put: '%s' -> '%s'"
                     % (localfilename, remotefilename))
        remotefolder = os.path.dirname(remotefilename)
        remotepackagecomponents = remotefilename.split(os.sep)[:-1]
        p = ""
        for c in remotepackagecomponents:
            p = os.path.join(p, c)
            remotefolders.add(p)
        if os.path.basename(remotefilename) == "__init__.py":
            remotepackages.add(remotefolder)
        z.write(localfilename, remotefilename)

    missing_packages = remotefolders.difference(remotepackages)
    for p in missing_packages:
        logger.debug("add __init__.py: %s" % (p))
        z.writestr(os.path.join(p, "__init__.py"), "")

    z.close()
    zfile.close()
    return zpath


def add_remote_file(localfile, remotefile):
    _remote_files.append((localfile, remotefile))


def add_remote_files(files):
    for tpl in files:
        if not isinstance(tpl, tuple):
            logger.warn("add_remote_files requires a list of tuples. found %s"
                        % str(file))
        else:
            _remote_files.append(tpl)


def directory_files(local, remote, **kwargs):
    files = kwargs.get('files', [])
    l = len(local) + 1
    for directory, _dirnames, filenames in os.walk(local):
        remote_dir = os.path.join(remote, directory[l:])
        for filename in filenames:
            if not filename.endswith('.py'):
                continue
            localfilename = os.path.join(directory, filename)
            remotefilename = os.path.join(remote_dir, filename)
            files.append((localfilename, remotefilename))
    return files


def add_directory_files(local, remote, **kwargs):
    files = directory_files(local, remote)
    _remote_files.extend(files)
    # logger.debug(_remote_files)


def package_files(pkg, target_path, **kwargs):
    root = os.path.dirname(inspect.getfile(pkg))
    return directory_files(root, target_path, **kwargs)


def add_package_files(pkg, target=None):
    if not target:
        target = pkg.__name__.replace('.', '/')
    files = package_files(pkg, target)
    _remote_files.extend(files)
    # logger.info(_remote_files)

add_package_files(vispa.remote)
add_package_files(rpyc)


class LoggingService(rpyc.Service):

    def exposed_log(self, name, level, msg):
        logging.getLogger(name).log(level, msg)

    def exposed_send(self, *args, **kwargs):
        vispa.bus.send(*args, **kwargs)


class LocalConnectionFeeder(threading.Thread):

    callbacks = {}

    def __init__(self):
        threading.Thread.__init__(self)
        self.daemon = True

    def run(self):
        while True:
            rlist, _, _ = select.select(self.callbacks.keys(), [], [], 1)
            for fd in rlist:
                if fd in self.callbacks:
                    try:
                        self.callbacks[fd]()
                    except:
                        logger.exception("LocalConnectionFeeder callback")
                        del self.callbacks[fd]


class LocalConnectionImpl(object):

    _local_connection_feeder = None

    def __init__(self, command, **kwargs):
        if command[0] == "python2" and not sys.platform.startswith("java"):
            command[0] = sys.executable
        logger.info("local connection: %s" % " ".join(command))
        self.__proc = subprocess.Popen(command, stdin=subprocess.PIPE,
                                       stdout=subprocess.PIPE,
                                       stderr=None,
                                       bufsize=0,
                                       close_fds=True,
                                       universal_newlines=False)

        self.stdout = self.__proc.stdout
        self.stdout_fileno = self.stdout.fileno()
        self.stderr = self.__proc.stderr
        self.stdin = self.__proc.stdin

    def __del__(self):
        self.close()

    def close(self):
        self.__proc.terminate()
        self.__proc.wait() # this can deadlock like this

        cf = self._local_connection_feeder
        if cf and self.stdout_fileno in cf.callbacks:
            del cf.callbacks[self.stdout_fileno]

        self.stdin.close()

    def print_stderr(self, timeout=0):
        return
        fno = self.stderr.fileno()
        while True:
            r, _, _ = select.select([fno], [], [], timeout)
            if r and r[0] == fno:
                data = os.read(fno, 4096)
                if data:
                    sys.stderr.write(data)
                else:
                    return
            else:
                return

    def writeln(self, data):
        fno = self.stdin.fileno()
        _, w, _ = select.select([], [fno], [], 1)
        if w:
            os.write(fno, data)
            os.write(fno, "\n")

    def stream(self):
        return rpyc.PipeStream(self.stdout, self.stdin)

    def set_on_feed(self, cb):
        if self._local_connection_feeder is None:
            self._local_connection_feeder = LocalConnectionFeeder()
            self._local_connection_feeder.callbacks[self.stdout_fileno] = cb
            self._local_connection_feeder.start()



class WrappedChannelFile(object):

    def __init__(self, file_, how):
        self.__file = file_
        self.__how = how

    def close(self):
        try:
            self.__file.channel.shutdown(self.__how)
        except:
            pass
        return self.__file.close()

    def fileno(self):
        return self.__file.channel.fileno()

    def __getattr__(self, name):
        return getattr(self.__file, name)


def _BufferedPipe_feed(self, data):
    self.original_feed(data)
    if hasattr(self, "on_feed"):
        self.on_feed()


class SSHConnectionImpl(object):

    """
    An SSH transport for Pushy, which uses Paramiko.
    """
    TIMEOUT = 30

    class FileStream(rpyc.core.stream.Stream):

        def __init__(self, file_in, file_out):
            self._file_in = file_in
            self._file_out = file_out
            self._in_buffer_event = threading.Event()
            self._file_in.channel.in_buffer.set_event(
                self._in_buffer_event)

        def close(self):
            logger.debug("filestream closed")

        def closed(self):
            return self._file_out.channel.closed

        def read(self, count):
            try:
                data = self._file_out.read(count)
                if len(data) < count:
                    raise EOFError("read too few bytes: %d < %d" % (len(data), count))
                return data
            except:
                c, i, t = sys.exc_info()
                raise EOFError, i.args, t

        def write(self, data):
            try:
                return self._file_in.write(data)
            except:
                c, i, t = sys.exc_info()
                raise EOFError, i.args, t

        def fileno(self):
            raise NotImplementedError

        def poll(self, timeout):
            if rpyc_version[0] >= 4:
                timeout = Timeout(timeout).timeleft()
            return self._in_buffer_event.wait(timeout)

    def __init__(self, command, address,
                 missing_host_key_policy="warning",
                 **kwargs):
        """
        @param missing_host_key_policy: A string describing which policy to
                       use ('autoadd', 'warning', or 'reject'), or an
                       object of type paramiko.MissingHostKeyPolicy.
        @param kwargs: Any parameter that can be passed to
                       U{paramiko.SSHClient.connect<
                         http://www.lag.net/paramiko/docs/
                         paramiko.SSHClient-class.html#connect>}.
                         - port
                         - username
                         - password
                         - pkey
                         - key_filename
                         - timeout
                         - allow_agent
                         - look_for_keys
        """
        self.__client = paramiko.SSHClient()
        # only do this when connecting as local user, SLOW!!
        if False:
            self.__client.load_system_host_keys()

        # Set the missing host key policy.
        if isinstance(missing_host_key_policy, str):
            missing_host_key_policy = missing_host_key_policy.lower()
            if missing_host_key_policy == "autoadd":
                self.__client.set_missing_host_key_policy(
                    paramiko.AutoAddPolicy())
            elif missing_host_key_policy == "warning":
                self.__client.set_missing_host_key_policy(
                    paramiko.WarningPolicy())
            else:
                if missing_host_key_policy != "reject":
                    import warnings
                    warnings.warn("Unknown missing host key policy: " +
                                  missing_host_key_policy)
                self.__client.set_missing_host_key_policy(
                    paramiko.RejectPolicy())
        else:
            # Assume it is a user-defined policy object.
            self.__client.set_missing_host_key_policy(
                missing_host_key_policy)

        if 'key' in kwargs and kwargs['key']:
            key = kwargs['key']
            if key.find('RSA') >= 0:
                logger.info("use RSA private key")
                kwargs['pkey'] = paramiko.RSAKey.from_private_key(
                    StringIO(key),
                    kwargs['password'])
                del kwargs['password']
            elif key.find('DSA') >= 0:
                logger.info("use DSA private key")
                kwargs['pkey'] = paramiko.DSSKey.from_private_key(
                    StringIO(key),
                    kwargs['password'])
                del kwargs['password']
            else:
                logger.info("invalid key, try password")

        host, port = self.address2host_port(address)

        # Create the connection.
        connect_args = {"hostname": host, "port": port, "timeout": 1, "compress": False}

        # TODO: check for local user
        local_user = False
        if local_user:
            connect_args['look_for_keys'] = True
            connect_args['allow_agent'] = True
        else:
            connect_args['look_for_keys'] = False
            connect_args['allow_agent'] = False

        for name in ("port", "username", "password", "pkey",
                     "key_filename", "timeout", "allow_agent",
                     "look_for_keys"):
            if name in kwargs:
                connect_args[name] = kwargs[name]
        logger.debug("connect via ssh to workspace: %s" % address)
        self.__client.connect(**connect_args)

        # Join arguments into a string
        args = command
        for i in range(len(args)):
            if " " in args[i]:
                args[i] = "'%s'" % args[i]
        command = " ".join(args)
        command = '/bin/sh -c \"exec $SHELL -l -c \\\"%s\\\"\"' % command
        logger.debug("execute remote command: %s" % command)
        stdin, stdout, stderr = self.__client.exec_command(command)
        stdout.channel.in_buffer.original_feed = stdout.channel.in_buffer.feed
        stdout.channel.in_buffer.feed = types.MethodType(
            _BufferedPipe_feed,
            stdout.channel.in_buffer)
        stdin.channel.settimeout(self.TIMEOUT)
        self.stdin = WrappedChannelFile(stdin, 1)
        self.stdout = WrappedChannelFile(stdout, 0)
        self.stderr = WrappedChannelFile(stderr, 0)
        logger.debug("ssh connection established")

    def address2host_port(self, host):
        host_port = host.split(':')
        host = host_port[0].strip()
        port = int(host_port[1]) if len(host_port) > 1 else 22
        return host, port

    def __del__(self):
        self.close()

    def close(self):
        if hasattr(self, "stdin"):
            self.stdin.close()
            self.stdout.close()
            self.stderr.close()
        self.__client.close()

    def print_stderr(self):
        c = self.stderr.channel
        while c.recv_stderr_ready():
            sys.stderr.write(c.recv_stderr(4096))
#         if data:
#             got_data = True
#             sys.stderr.write(data)
#
#         session = self.stderr.channel
#         while 1:
#             r, _, _ = select.select([session], [], [], 0.1)
#             if not r:
#                 continue
#             got_data = False
#             if session.recv_stderr_ready():
#                 data = r[0].recv_stderr(4096)
#                 if data:
#                     got_data = True
#                     sys.stderr.write(data)
#             if not got_data:
#                 break

    def writeln(self, data):
        #         c = self.stdin.channel
        #         _, w, _ = select.select([], [c], [], 1)
        #         if w:
        self.stdin.write(data)
        self.stdin.write("\n")

    def stream(self):
        return self.FileStream(self.stdin, self.stdout)

    def set_on_feed(self, cb):
        self.stdout.channel.in_buffer.on_feed = cb

    def client(self):
        return self.__client


class Connection(object):

    DISCONNECTED = 0
    CONNECTING = 1
    CONNECTED = 2
    DISCONNECTING = 3

    STATUS_MESSAGES = {
        DISCONNECTED: 'disconnected',
        CONNECTING: 'connecting',
        CONNECTED: 'connected',
        DISCONNECTING: 'disconnecting'}

    def __init__(self, userid, workspaceid, host, **kwargs):
        self.__userid = userid
        self.__workspaceid = workspaceid
        self.__host = host
        self.__python = kwargs.get('python', 'python2') or "python2"
        self.__username = kwargs.get('username', None)
        self.__server = None
        self.__tempdir = kwargs.get("tempdir", "")
        self.__rpyc = None
        self.__prepared = False
        self.__password = kwargs.get("password", "")
        self.__key = kwargs.get("key", "")
        self.__serving_thread = None
        self._connection = None
        self._status = Connection.DISCONNECTED

    def _send_file(self, filename):
        logger.debug("Send file to remote connection: %s" % filename)
        size = os.stat(filename).st_size
        self._connection.stdin.write("%d\n" % size)
        with open(filename) as f:
            shutil.copyfileobj(f, self._connection.stdin)

    def _readline(self):
        irdy, _, _ = select.select([self._connection.stdout], [], [], 10)
        if irdy:
            return self._connection.stdout.readline()
        else:
            return ""

    def find_magic_string(self):
        logger.debug("find magic string")
        MAGIC_STRING = "START_RPYC".encode("ascii")
        MAGIC_STRING_LENGTH = len(MAGIC_STRING)
        buf = b''

        while True:
            # fill buffer to needed size
            remaining = MAGIC_STRING_LENGTH - len(buf)
            buf += self._connection.stdout.read(remaining)

            # compare
            if buf == MAGIC_STRING:
                return

            # jump to next S
            try:
                buf = buf[buf.index(MAGIC_STRING[0], 1):]
            except:
                buf = b''


    def connected(self, timeout=10):
        _timeout = timeout
        while _timeout > 0 and self._status == Connection.CONNECTING:
            logger.debug("workspace connecting, wait...")
            time.sleep(0.1)
            _timeout -= 0.1

        if self._status == Connection.CONNECTED:
            return True
        else:
            return False

    def active(self):
        if self._status in [Connection.DISCONNECTED, Connection.DISCONNECTING]:
            return False
        elif self._status == Connection.CONNECTED:
            try:
                self.__rpyc.ping(data="vispa")
                return True
            except:
                self.close()
                return False
        else:
            return True

    @staticmethod
    def send_workspace_status(userid, workspaceid, status):
        topic = "workspace." + Connection.STATUS_MESSAGES[status]
        data = {"workspaceId": workspaceid}
        vispa.bus.send_topic(topic, user_id=userid, data=data)

    def send_status(self):
        Connection.send_workspace_status(self.__userid, self.__workspaceid,
                                         self._status)

    def status(self, new_status=None):
        if new_status is None:
            return self._status, self.STATUS_MESSAGES[self._status]
        else:
            changed = self._status != new_status
            self._status = new_status
            if changed:
                self.send_status()
            return self._status

    def poll(self, timeout=0):
        if self.__rpyc and self._status == Connection.CONNECTED:
            try:
                return self.__rpyc.poll(timeout)
            except:
                self.close()
                return False
        else:
            return False

    def open(self, **kwargs):
        self.status(Connection.CONNECTING)
        global _zip_file
        if not _zip_file:
            _zip_file = _create_zip()

        bootstrap_script = \
            "import sys;exec(sys.stdin.read(int(sys.stdin.readline())))"

        command = [self.__python, "-u", "-c", bootstrap_script]

        try:
            if self.__host == "local:":
                if vispa.config('workspace', 'allow_local', True):
                    self._connection = LocalConnectionImpl(command)
                else:
                    raise AjaxException("local workspaces to allowed!")
            else:
                if not self.__password and not self.__key:
                    vispa.bus.send_topic(
                        "workspace.password_required",
                        user_id=self.__userid,
                        data={
                            "workspaceId": self.__workspaceid})
                    self.status(Connection.DISCONNECTED)
                    return
                try:
                    self._connection = SSHConnectionImpl(command, self.__host,
                                                         password=self.__password,
                                                         key=self.__key,
                                                         username=self.__username)
                except (paramiko.PasswordRequiredException):
                    vispa.bus.send_topic(
                        "workspace.passphrase_required",
                        user_id=self.__userid,
                        data={
                            "workspaceId": self.__workspaceid})
                    self.status(Connection.DISCONNECTED)
                    return
                except (paramiko.AuthenticationException, paramiko.SSHException):
                    vispa.bus.send_topic(
                        "workspace.password_required",
                        user_id=self.__userid,
                        data={
                            "workspaceId": self.__workspaceid})
                    self.status(Connection.DISCONNECTED)
                    return
                except Exception:
                    self.status(Connection.DISCONNECTED)
                    logger.exception("SSH implementation")
                    # raise AjaxException("Failed to setup SSH Connection!")
                    return

            loader_file = os.path.join(os.path.dirname(__file__),
                                       "workspace_loader.py")
            # send in memory loader
            self._send_file(loader_file)

            self._connection.stdin.write("%d\n" % logger.getEffectiveLevel())

            # send packages
            self._send_file(_zip_file)

            # wait for remote site finish
            self._connection.print_stderr()

            self.find_magic_string()

            # send
            logger.info("connect rpyc")
            channel = rpyc.Channel(self._connection.stream(), False)
            service = LoggingService() if rpyc_version[0] >= 4 else  LoggingService
            self.__rpyc = rpyc.Connection(service, channel, {
                "allow_public_attrs": True,
                "sync_request_timeout": 300,
            })

            logger.info("connected rpyc")
            self._connection.print_stderr()

            logger.info("setup remote logger")
            rlogger = self.__rpyc.root.getmodule('logging').getLogger()
            rlogger.setLevel(logger.getEffectiveLevel())

            logger.info("start serving thread")
            self.__serving_thread = Thread(target=self._serve_all)
            self.__serving_thread.daemon = True
            self.__serving_thread.start()
        except AjaxException:
            raise
        except:
            self.status(Connection.DISCONNECTED)
            logger.exception("SSH implementation")
        else:
            logger.info("connection open")
            self.status(Connection.CONNECTED)

    def _serve_all(self):
        try:
            self.__rpyc.serve_all()
        except:
            logger.exception("Connection._server_all")
        finally:
            logger.debug("Connection._server_all done")
            self.close()

    def close(self):
        if self._status in [Connection.DISCONNECTED, Connection.DISCONNECTING]:
            return

        self.status(Connection.DISCONNECTING)

        _instance_pool.clear(self.__userid, self.__workspaceid)

        logger.debug("close rpyc")
        try:
            if self.__rpyc:
                self.__rpyc.close()
                self.__rpyc = None
        except:
            vispa.log_exception()

        logger.debug("close transport")
        if hasattr(self, '_connection') and self._connection:
            try:
                self._connection.close()
            except:
                vispa.log_exception()
            self._connection = None

        self.status(Connection.DISCONNECTED)
        logger.debug("connection closed")

    def stdin(self):
        return self._connection.stdin

    def stdout(self):
        return self._connection.stdout

    def __del__(self):
        self.close()

    def rpyc(self):
        return self.__rpyc

    def errors(self):
        for i, line in enumerate(self._connection.stderr):
            line = line.rstrip()
            logger.info("%d: %s" % (i, line))

    def tempdir(self):
        return self.__tempdir

    def host(self):
        return self.__host


class ConnectionPool(object):

    SERVE_CONNECTION_INTERVAL = 0.2

    def __init__(self):
        self._connections = {}

        vispa.register_callback('bus.all_user_sessions_removed', self.clear)
        vispa.register_callback('stop', self.clear)

    def __del__(self):
        self.clear()

    def _connect_host(self, user, workspace, host, **kwargs):
        python = None
        if workspace.command and len(workspace.command):
            python = workspace.command
        logger.info("spawn remote process: '%s' using command '%s'"
                    % (host, python))
        username = workspace.login or user.name
        connection = Connection(
            user.id,
            workspace.id,
            host,
            python=python,
            username=username,
            key=workspace.key,
            **kwargs)
        threading.Thread(target=connection.open).start()
        time.sleep(0.01)
        return connection

    def _connect(self, user, workspace, password=None):
        logger.debug("connect new workspace")
        if workspace.host:
            hosts = workspace.host.split(',')
        else:
            hosts = ['local:']
        while len(hosts) > 0:
            idx = random.randint(0, len(hosts) - 1)
            host = hosts.pop(idx).strip()
            connection = self._connect_host(
                user,
                workspace,
                host,
                password=password)
            if connection:
                return connection
        return None

    def connect(self, user, workspace, password=None):
        key = (user.id, workspace.id)
        if key in self._connections and self._connections[key].active():
            logger.debug(
                "workspace already conected: %d - %d" %
                (user.id, workspace.id))
            self._connections[key].send_status()
            return self._connections[key]

        connection = self._connect(user, workspace, password)
        if connection:
            self._connections[key] = connection
            return connection
        else:
            return None

    def get(self, user, workspace, **kwargs):
        key = (user.id, workspace.id)
        if key in self._connections:
            logger.debug("return pooled connection: %d - %d"
                         % (user.id, workspace.id))
            connection = self._connections[key]
            if kwargs.get('wait', True) is False or connection.connected():
                return connection
            else:
                return None
        else:
            logger.debug("workspace not connected: %d - %d"
                         % (user.id, workspace.id))
            return None

    def clear(self, user=None, workspace=None):
        uid = user.id if type(user) == User else user
        wid = workspace.id if type(workspace) == Workspace else workspace
        for _uid, _wid in self._connections.keys():
            if uid and uid != _uid:
                continue
            if wid and wid != _wid:
                continue
            logger.info("clear client %d %d" % (_uid, _wid))
            # TODO: put client.close in a Thread
            key = (_uid, _wid)
            if key in self._connections:
                c = self._connections[key]
                del self._connections[key]
                c.close()

            return

        # not found, send topic anyway
        if user and workspace:
            Connection.send_workspace_status(user.id, workspace.id,
                                             Connection.DISCONNECTED)

    def connections_of_user(self, user):
        """ Returns a list of tuples: (workspaceId, connection) """
        l = []
        for key in self._connections.iterkeys():
            if user.id != key[0]:
                continue
            l.append((key[1], self._connections[key]))
        return l

    def get_workspace_connections(self, user, workspaces):
        """ return a list of tuples with the workspace
        and the assosiated connection or None """
        return [(workspace, self._connections.get((user.id, workspace.id), None))
                for workspace in workspaces]

_connection_pool = ConnectionPool()


class InstancePool(object):
    # TODO: make Pool thread safe

    def __init__(self):
        self._instances = {}

    def __del__(self):
        for key in self._instances.keys():
            del self._instances[key]

    def get(self, _user, _workspace,
            classname=None, key=None, init_args=None, **kwargs):
        client = _connection_pool.get(_user, _workspace)
        if client is None:
            raise AjaxException("not connected")
        if classname:
            _key = (_user.id, _workspace.id, classname, key)
            logger.debug("get: %s", str(_key))
            if _key in self._instances:
                logger.debug("return pooled class %s" % classname)
                return self._instances[_key]
            cls = classname.split('.')
            module = ".".join(cls[:-1])
            name = cls[-1]
            m = client.rpyc().root.getmodule(module)
            rcls = getattr(m, name)
            if rcls:
                logger.debug("return new class %s" % classname)
                if isinstance(init_args, dict):
                    instance = rcls(**init_args)
                else:
                    instance = rcls()
                self._instances[_key] = instance
                return instance
            else:
                logger.info("class %s not found" % classname)
                return None
        else:
            return client.rpyc().root

    def clear(self, user=None, workspace=None, classname=None, key=None):
        userid = user.id if isinstance(user, User) else user
        workspaceid = workspace.id if isinstance(
            workspace,
            Workspace) else workspace
        for _uid, _wid, _classname, _key in self._instances.keys():
            if user and userid != _uid:
                continue
            if workspace and workspaceid != _wid:
                continue
            if classname and classname != _classname:
                continue
            if key and key != _key:
                continue
            logger.info("clear class %d %d %s %s"
                        % (_uid, _wid, str(_classname), str(_key)))
            del self._instances[(_uid, _wid, _classname, _key)]

_instance_pool = InstancePool()


def get_instance(classname,
                 key=None, user=None, workspace=None,
                 db=None, init_args=None):
    """
    Returns a pooled reference to a class instance of type `classname`.
    """
    if not db:
        db = cherrypy.request.db

    if not user:
        user = cherrypy.request.user
    elif not isinstance(user, User):
        user = User.get_by_id(db, int(user))

    if not workspace:
        workspace = cherrypy.request.workspace
    elif not isinstance(workspace, Workspace):
        workspace = Workspace.get_by_id(db, int(workspace))

    return _instance_pool.get(user, workspace, classname, key, init_args)


def clear_instance(classname, key=None, user=None, workspace=None, db=None):
    """
    Remove all instances of type `classname` from the pool
    """
    if not db:
        db = cherrypy.request.db

    if not user:
        user = cherrypy.request.user
    elif not isinstance(user, User):
        user = User.get_by_id(db, int(user))

    if not workspace:
        workspace = cherrypy.request.workspace
    elif not isinstance(workspace, Workspace):
        workspace = Workspace.get_by_id(db, int(workspace))

    return _instance_pool.clear(user, workspace, classname, key)


def connect(workspace=None, user=None, db=None, password=None):
    """
    Connect the selected workspace.
    """
    if not db:
        db = cherrypy.request.db

    if not user:
        user = cherrypy.request.user
    elif not isinstance(user, User):
        user = User.get_by_id(db, int(user))

    if not workspace:
        workspace = cherrypy.request.workspace
    elif not isinstance(workspace, Workspace):
        workspace = Workspace.get_by_id(db, int(workspace))

    if not workspace.has_access(user):
        raise vispa.AjaxException("Access to workspace not allowed!")

    return _connection_pool.connect(user, workspace, password=password)


def disconnect(workspace=None, user=None, db=None):
    """
    Disconnect the selected workspace and remove all pooled instances.
    """
    if not db:
        db = cherrypy.request.db

    if not user:
        user = cherrypy.request.user
    elif not isinstance(user, User):
        user = User.get_by_id(db, int(user))

    if not workspace:
        workspace = cherrypy.request.workspace
    elif not isinstance(workspace, Workspace):
        workspace = Workspace.get_by_id(db, int(workspace))

    _instance_pool.clear(user, workspace)
    _connection_pool.clear(user, workspace)


def disconnect_all(user=None, db=None):
    """
    Disconnect all workspaces and pooled instances of the selected user.
    """
    if not db:
        db = cherrypy.request.db

    if not user:
        user = cherrypy.request.user
    elif not isinstance(user, User):
        user = User.get_by_id(db, int(user))

    _instance_pool.clear(user)
    _connection_pool.clear(user)


def module(modulename, user=None, workspace=None, db=None):
    """
    Returns a reference to a remote module
    """
    if not db:
        db = cherrypy.request.db

    if not user:
        user = cherrypy.request.user
    elif not isinstance(user, User):
        user = User.get_by_id(db, int(user))

    if not workspace:
        workspace = cherrypy.request.workspace
    elif not isinstance(workspace, Workspace):
        workspace = Workspace.get_by_id(db, int(workspace))

    _rpyc = _connection_pool.get(user, workspace).rpyc()
    return _rpyc.root.getmodule(modulename)
