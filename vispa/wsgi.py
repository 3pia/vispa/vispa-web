import logging
import logging.config
import os
import cherrypy

vispa_server = None

cherrypy.config.update({'environment': 'embedded'})

def application(environ, start_response):
    global vispa_server
    if vispa_server is None:
        configdir = os.environ.get(
            'VISPA_CONFIG_PATH',
            '/etc/vispa')
        configdir = environ.get(
            'VISPA_CONFIG_PATH',
            configdir)

        datadir = os.environ.get(
            'VISPA_DATA_PATH',
            '/var/lib/vispa')
        datadir = environ.get(
            'VISPA_DATA_PATH',
            datadir)

        logging_conf = os.path.join(configdir, 'logging.ini')
        if os.path.isfile(logging_conf):
            logging.config.fileConfig(logging_conf)
        logger = logging.getLogger(__name__)

        from vispa import server
        vispa_server = server.Server(
            configdir=configdir,
            vardir=datadir)
        cherrypy.server.unsubscribe()
        cherrypy.engine.start()
        logger.info("server startet.")
    return cherrypy.tree(environ, start_response)
