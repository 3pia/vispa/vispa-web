"""
Functions to translate a relative path into a valid url
"""
import re
import os
import vispa


def clean(url):
    """
    Clean the url, multiple fix slashes and strip whitespaces
    """
    # fix triple slashes
    url = url.replace(u'///', u'/')

    # fix double slashes, preserve protocol
    url = re.sub(u'(?<!:)//', u'/', url)

    return url.strip()


def join(*args):
    """
    Join all parameters into one clean url path.
    """
    return clean(u'/'.join(args))


def dynamic(*parts, **kwargs):
    """
    Create an absolute URL to non static content, e.g. controllers
    """
    base_dynamic = vispa.config(u'web', u'base_dynamic', None)
    if not base_dynamic:
        base_dynamic = vispa.config(u'web', u'base', u'/')

    url = join(base_dynamic, *parts)
    encoding = kwargs.get('encoding', None)
    if encoding:
        return url.encode(encoding)
    else:
        return url


def static(*parts, **kwargs):
    """
    Create an absolute URL to static content, e.g. images
    """
    base_static = vispa.config(u'web', u'base_static', None)
    if not base_static:
        base_static = vispa.config(u'web', u'base', u'/')

    relative_url = join(*parts)

    if relative_url.startswith('/'):
        relative_url = relative_url[1:]

    if kwargs.get('staticdir', True):
        relative_url = join(u'static', relative_url)

    extension = kwargs.get('extension', None)
    if extension:
        relative_url = join(u'extensions', extension, relative_url)

    url = join(base_static, relative_url)
    if kwargs.get('timestamp', True):
        # ToDo: find correct extension path
        path = join(u'vispa', relative_url)
        try:
            t = os.path.getmtime(vispa.codepath(path))
        except:
            t = 0
        url = u'%s?%s' % (url, unicode(t))

    encoding = kwargs.get('encoding', None)
    if encoding:
        return url.encode(encoding)
    else:
        return url
