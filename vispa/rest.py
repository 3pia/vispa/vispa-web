# -*- coding: utf-8 -*-

"""
Dispatcher and controller for implementing a REST api in cherrypy.
"""

import cherrypy
import urllib
import re
from collections import OrderedDict


__all__ = ["RESTDispatcher", "RESTController", "GET", "POST", "DELETE"]


class RESTDispatcher(cherrypy.dispatch.Dispatcher):
    """
    Dispatches request to handler functions that are decorated with resolvers. Use it in conjunction
    with the RESTController class.
    Example:

    .. code-block:: python

       class Controller(RESTController):

           @POST("user/:id(^\d+$)/name/:name")
           def set_user_name_by_id(self, id, new_name):
               pass

           @POST("user/:name/name/:name")
           def set_user_name_by_name(self, name, new_name):
               pass

    Calls to POST /user/4/name/tom will be routed to the first handler,
    calls to POST /user/tom/name/tim to the second one. Thus, the order of handler definitions is
    important for the dispatcher to take precedence rules into account. 
    """

    _last_handler_id = 0

    @classmethod
    def _handler_id(cls):
        """
        Creates a new, unique id for a handler.
        Ids are important to resolve the order of handler definition.
        """
        id = cls._last_handler_id
        cls._last_handler_id += 1
        return id

    @classmethod
    def _resolver(cls, method):
        """
        Returns a function that wraps handlers to define their dispatching behavior.

        .. code-block:: python

           GET = RESTDispatcher._resolver("GET")

           # now, GET can be used as a decorator
           @GET("some/resource/:param")
           def ...
        """
        def wrapper(fmt):
            def decorator(fn):
                fn.exposed = True
                fn.format  = fmt
                fn.method  = method.upper()
                fn.id      = cls._handler_id()
                fn.resources, fn.params, fn.regexps = cls._parse_format(fmt)
                fn.nparts  = len(fn.resources) + len(fn.params)
                return fn
            return decorator
        return wrapper

    @staticmethod
    def _is_handler(obj):
        """
        Checks whether an object is a valid REST handler, i.e. a method wrapped by e.g. a GET
        decorator.
        """
        return hasattr(obj, "__call__") and getattr(obj, "exposed", False) and hasattr(obj, "id")

    @staticmethod
    def _parse_format(fmt):
        """
        Parses an url format and returns a tuple consisting of three OrderedDict's: resources,
        params and regexps. Example: /user/:id(^\d+$)/name/:name
        -> resources: {0: "user", 2: "name"}
        -> params   : {1: "id", 3: "name"}
        -> regexps  : {1: /^\d+$/, 3: None}
        """
        resources = OrderedDict()
        params    = OrderedDict()
        regexps   = OrderedDict()

        for i, p in enumerate(fmt.strip("/").split("/")):
            if p[0] != ":":
                resources[i] = p
            else:
                param, _, expr = p[1:].partition("(")
                regexp = None
                if expr and expr[-1] == ")":
                    regexp = re.compile(expr[:-1])
                params[i]  = param
                regexps[i] = regexp

        return resources, params, regexps

    def find_handler(self, path):
        """
        Implements cherrypy.dispatch.Dispatcher.find_handler(path).
        """
        request = cherrypy.serving.request

        node, parts = self.find_node(path)

        if node is None:
            return None, []

        # get possible handlers, filter by method and nparts
        check = lambda h: h.nparts == len(parts) and h.method == request.method
        handlers = [h for h in node._handlers.values() if check(h)]

        if len(handlers) == 0:
            return None, []

        handler = None
        params  = {}

        for h in handlers:
            is_valid = True
            for i, resource in h.resources.items():
                if parts[i] != resource:
                    is_valid = False
                    break
            if not is_valid:
                continue
            _params = {}
            for (i, param), regexp in zip(h.params.items(), h.regexps.values()):
                value = parts[i]
                if regexp is not None and not regexp.match(value):
                    is_valid = False
                    break
                _params[param] = value
            if is_valid:
                handler = h
                params  = _params
                break

        if handler:
            request.config.update(getattr(handler, "_cp_config", {}))

        request.params = params
        
        return handler, []

    def find_node(self, path):
        """
        Trails the handler tree to find the correct RESTController instance.
        """
        request   = cherrypy.serving.request
        app       = request.app
        node      = app.root
        REST_node = None

        config = {}
        config.update(getattr(node, "_cp_config", {}))
        config.update(app.config.get("/", {}))

        parts   = [urllib.unquote(p) for p in path.strip("/").split("/")]
        curpath = ""

        while len(parts) > 0:
            part     = parts[0]
            subnode  = getattr(node, part, None)

            curpath += "/" + part
            config.update(app.config.get(curpath, {}))

            if subnode is None:
                break

            config.update(getattr(subnode, "_cp_config", {}))

            if isinstance(subnode, RESTController):
                REST_node = subnode
            elif REST_node is not None:
                break

            node = subnode
            parts.pop(0)

        request.config = config

        return REST_node, parts


# by default, attach GET, POST and DELETE decorators to RESTDispatcher
GET    = RESTDispatcher._resolver("GET")
POST   = RESTDispatcher._resolver("POST")
DELETE = RESTDispatcher._resolver("DELETE")


class RESTController(object):
    """
    RESTController that should be used in conjunction the a RESTDispatcher.
    It's only task is to prepare handlers at the end of its initialization.
    """

    def __init__(self):
        super(RESTController, self).__init__()

        # find and store all handlers
        # id -> handler
        self._handlers = {}
        for fn in self._get_REST_handlers():
            self._handlers[fn.id] = fn

    def _get_REST_handlers(self):
        """
        Prepares handlers by filling the self._handler dict.
        """
        members  = [getattr(self, name) for name in dir(self)]
        handlers = [member for member in members if RESTDispatcher._is_handler(member)]
        return sorted(handlers, key=lambda m: m.id)
