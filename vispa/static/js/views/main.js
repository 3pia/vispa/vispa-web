/**
 * views/main
 * =================
 */

define([
  "jquery",
  "async",
  "vue/vue",
  "vispa/vue/base",
  "vispa/vue/menu",
  "vispa/vue/fastmenu",
  "vispa/views/base",
  "vispa/common/color",
  "vispa/utils",
  "require",
  "text!html/index/mainview/content.html",
  "text!html/index/mainview/tab.html"
], function(
  $,
  async,
  Vue,
  VueBase,
  Menu,
  FastMenu,
  BaseView,
  Color,
  Utils,
  require,
  tContent,
  tTab
) {

  var MainViewTab = Vue.extend({
    template: tTab.trim(),
    computed: {
      instance: {
        cached: false,
        get: function() {
          return this.$options.instance;
        },
      },
      closeIconFinal: function() {
        return this.closeIcon || "fa-times";
      },
      color: function() {
        if (!vispa.wsManager.useColor) return "";
        return this.instance.workspace ?
          this.instance.workspace.color :
          vispa.wsManager.prefs.get("color");
      },
      style: function() {
        var c = this.color;
        if(!c) return "";
        c = new Color(c);
        switch(vispa.wsManager.prefs.get("colorStyle")){
          case "background":
            c.setAlpha(0.65);
            c = c.toString();
            return "background:linear-gradient(to left,"+c+","+c+")";
          case "border":
            if (c.value.s)
              return "border-color:"+c.toString();
          default:
            return "";
        }
      },
      isCurrentWs: function() {
        var ws = this.instance.workspace;
        if (!ws) return true; // no own workspace
        var currentWsId = vispa.wsManager._vueData.current;
        if (!currentWsId) return true;
        return ws.id === currentWsId;
      },
      justIcon: function() {
        return vispa.wsManager.prefs.get("otherStyle")==="collapse" && !this.isCurrentWs;
      },
      title: function () {
        var ws = this.instance.workspace;
        var ret = [];
        if (this.labelFull)
          ret.push(this.labelFull);
        if (ws)
          ret.push("Workspace: " + ws.name);
        return ret.join("\n");
      },
    },
    methods: {
      focus: function(event) {
        this.instance.focus();
      },
      close: function(event) {
        this.instance.close();
      },
      mayClose: function(event) {
        if (event.which == 2) {
          this.close();
        }
      },
    },
    components: {
      "menu-button": FastMenu.button,
    },
  });

  var MainViewContent = Vue.extend({
    template: tContent.trim(),
    components: {
      "fast-menu": FastMenu.bar,
    },
    computed: {
      instance: {
        cached: false,
        get: function() {
          return this.$options.instance;
        },
      },
    },
    methods: {
      focus: function(event) {
        if (!this.instance.isFocused())
          this.instance.focus();
      },
    },
  });

  // shimmed events
  var EventsShimmed = ["focus","blur","show","hide","close","resize"];

  /**
   * .. js:class:: MainView(args, viewArgs, baseArgs)
   *
   *    :extends: BaseView
   *
   *    The view used for most things. It's instances are managed by a :js:class:`ViewManager`
   *    instance and allow for abitrary position and size within the grid.
   *
   *    :param Object args: See :js:class:`BaseView`.
   *    :param Object viewArgs: See :js:class:`BaseView` and
   *      :js:func:`attach` of :js:class:`ViewManager`.
   *    :param Object baseArgs: See :js:class:`BaseView`.
   */
  var MainView = BaseView._extend($.extend({},
  {

    init: function init(args, viewArgs) {
      init._super.apply(this, arguments);

      this._loadingTimeout = null;

      var useFastmenu = !!(this.prefs && this.prefs.vue.get("fastmenu"));

      // Vue now
      Utils.setROP(this, "vue", {});
      this.vue.tab = new MainViewTab({
        instance: this,
        data: {
          badge: "",
          fastmenu: useFastmenu,
          iconClass: this._class._members.iconClass || "fa-file-o",
          closeIcon: "",
          labelFull: "",
          labelShort: this._class._members.label || "",
          menuItems: $.extend(
            {
              generalDiv: {
                divider: true,
                position: 1000,
              },
              close: {
                label: "Close",
                iconClass: "fa-times",
                callback: function(){
                  this.$root.instance.close();
                },
                position: 1010,
              },
            },
            Menu.objClone(this._class._members.menu)
          ),
        },
      });
      this.vue.node = new MainViewContent({
        instance: this,
        data: {
          fastmenu: useFastmenu,
          loading : false,
          spinner : vispa.staticURL("img/maingui/loader6.gif"),
          classTag: this.extension.name.toLowerCase() +
              "-" + this._class._members.name.toLowerCase(),
        },
      });

      // mount it
      this.vue.tab.$mount();
      this.vue.node.$mount();

      // save the nodes
      Utils.setROP(this, "$node",             $(this.vue.node.$el));
      Utils.setROP(this, "$messageContainer", $(this.vue.node.$els.msg));

      // setup events
      this.on("focus", function() {
        vispa.mainMenu.$options.tabber = this.parent;
        this.vue.tab.badge = "";
        var ns = this.$messageContainer[0].children;
        for(var i=ns.length-1; i>=0; i--) {
          if (ns[i].__vue__ && ns[i].__vue__.focus) {
            this.blur();
            ns[i].__vue__.focus();
            break;
          }
        }
      });
      this.on("blur", function() {
        var ns = this.$messageContainer[0].children;
        for(var i in ns) {
          if (ns[i].__vue__ && ns[i].__vue__.blur) {
            ns[i].__vue__.blur();
          }
        }
      });
      this.on("pushedMessage", function() {
        this.vue.tab.badge = this.messageCount || "";
      });

      // register shimmed events
      for (var i=0; i < EventsShimmed.length; i++) {
        var event = EventsShimmed[i];
        var handler = "on" + event[0].toUpperCase() + event.substr(1);
        this.on(event, this[handler]);
      }

      // finally attach
      vispa.mainViewManager.attach(this, $.extend({
        tabber : viewArgs.callee ? viewArgs.callee.parent : undefined,
        rightOf: (viewArgs.rightOf || viewArgs.leftOf) ? undefined : viewArgs.callee,
      }, viewArgs));

      // manuall trigger ready on vue instances
      this.vue.tab.$emit("hook:attached");
      this.vue.node.$emit("hook:attached");

      // render on next tick
      this.setLoading(true);
      setTimeout((function(){
        this.render(this.$content);
        this.setLoading(false);
      }).bind(this), 10);
    },

    /** indent: 1
     * .. js:attribute:: $tab read-only
     *
     *    :type: jQuery
     *
     *    The jQuery object holding the tab associated wit this instance.
     */
    $tab: {
      descriptor: true,
      get: function() {
        return $(this.vue.tab.$el);
      }
    },

    /** indent: 1
     * .. js:attribute:: $content read-only
     *
     *    :type: jQuery
     *
     *    The jQuery object containing the node that is holding the views content.
     */
    $content: {
      descriptor: true,
      get: function() {
        return $(this.vue.node.$els.content);
      }
    },

    /** indent: 1
     * .. js:attribute:: menu read-only
     *
     *    :type: MenuButton
     *
     *    The vue instance representing the menu of this view.
     */
    menu: {
      descriptor: true,
      get: function() {
        return this.vue.tab.$refs.menu;
      }
    },

    /// transparent override
    close: function close(force) {
      if (force !== true && this.onBeforeClose() === false) return;
      close._super.call(this);
    },

    /// transparent override
    destroy: function destroy() {
      this.vue.tab.$destroy();
      this.vue.node.$destroy();
      destroy._super.call(this);
    },

    /** indent: 1
     * .. js:function:: isManaged()
     *
     *    :return: Wether this viwe is managed. If it is not applicable (no :js:attr:`master`) will
     *    	return *null*.
     *    :rtype: Bool|null
     */
    isManaged: function() {
      return this.master && this.master.isManaged ? this.master.isManaged(this) : null;
    },

    /** indent: 1
     * .. js:function:: isVisible()
     *
     *    :return: Wether this view has focus. If it applicable (not :js:func:`isManaged`) will
     *    	return *null*.
     *    :rtype: Bool|null
     */
    isVisible: function() {
      return this.isManaged() ? this.master.isVisible(this) : null;
    },

    /** indent: 1
     * .. js:function:: isFocused()
     *
     *    :return: Wether this view has focus. If it applicable (not :js:func:`isManaged`) will
     *    	return *null*.
     *    :rtype: Bool|null
     */
    isFocused: function() {
      return this.isManaged() ? this.master.isFocused(this) : null;
    },

    /** indent: 1
     * .. js:attribute:: icon
     *
     *    :type: String
     *
     *    The CSS class names assigned to the icon of the tab.
     */
    icon: {
      descriptor: true,
      set: function(icon) {
        this.vue.tab.iconClass = icon;
      },
      get: function() {
        return this.vue.tab.iconClass;
      }
    },

    /** indent: 1
     * .. js:attribute:: label
     *
     *    :type: String
     *
     *    The label that is displayed in the tab. Setting this is the same as calling
     *    :js:func:`setLabel` with the new label and *false* as second parameter.
     */
    label: {
      descriptor: true,
      set: function(label) {
        this.setLabel(label, false);
      },
      get: function() {
        return this.vue.tab.labelFull || this.vue.tab.labelShort;
      }
    },

    /** indent: 1
     * .. js:function:: setLabel(label[, isPath])
     *
     *    Sets the label to a new value. If is path ist true it will intrepret the name as a path.
     *    The path will be shortend as much as possible while avoiding the labels of peers.
     *
     *    :param String label: The new label to be set.
     *    :param Bool isPath: Wether the label ist to be interpreted as a path.
     */
    setLabel: function setLabel(label, isPath) {
      var split = (label || "").split("/");
      var path = label;
      if (isPath && split.length>1 && this.isManaged()) {
        var peers = this.master.instances;
        var peerLabels = [];
        for (var i=0; i<peers.length; i++) {
          if (peers[i] != this) {
            peerLabels = peers[i].label;
          }
        }
        // jshint shadow:true
        for (var i=1; i<=split.length; i++) {
          path = split.slice(-i).join("/");
          if (!~peerLabels.indexOf(path) && path) {
            // if only a / remains to the full label we use it
            if (i+1==split.length && !split[0]) {
              path = label;
            }
            break;
          }
        }
      }

      this.vue.tab.labelFull = (label == path) ? "" : label;
      this.vue.tab.labelShort = path;
    },

    /** indent: 1
     * .. js:attribute:: modified
     *
     *    :type: Bool
     *
     *    Wether the tabs closing icon (cross) should indicate that the contents of this
     *    were modified but not yet saved by replacing it with an appropriate icon (pencil).
     */
    modified: {
      descriptor: true,
      set: function(modified) {
        this.vue.tab.closeIcon = modified ? "fa-pencil" : "";
      },
      get: function() {
        return !!~this.vue.tab.closeIcon.indexOf("fa-pencil");
      }
    },

    /** indent: 1
     * .. js:function:: setLoading(loading[, delay])
     *
     *    Behaves just like the normal setLoading with the only difference that you can provide
     *    a delay to the visual representation of loading (when not loading previously). It is
     *    inthendes to be used with low delays (such as 0.2s) to hold up the illusion of a
     *    very responsive UI.
     *
     *    :param Bool loading: Wether to increase or decrease the internal loading semaphore.
     *    :param Number delay: The delay in seconds until the loading indicatior will be shown
     *       after it was not already.
     */
    setLoading: function setLoading(loading, delay) {
      var self = this;

      setLoading._super.call(this, loading, function(ld) {
        self.vue.node.loading = ld;
      }, delay);
    },

    /** indent: 1
     * .. note::
     *    All following functions are to be overridden as need. Calling their *_super* is not
     *    required.
     */

    /** indent: 1
     * .. js:function:: getFragment()
     *
     *    Returns the fragment representing the the most important parts of the state of this view.
     *    Should use :js:func:`getState` to aquire the information for assembling the fragment.
     *
     *    :return: The fragment.
     *    :rtype: String
     */
    getFragment: function() {
      return "";
    },

    /** indent: 1
     * .. js:function:: applyFragment(fragment)
     *
     *    Loades a state from the fragment given. Should us :js:func:`state.set` to forward the
     *    information aquired though the fragment. It must silently fail if the given fragment is
     *    malformed in any way.
     *
     *    :param String fragment: The fragment the state is to be reconstructed from.
     */
    applyFragment: function(fragment) {
    },

    /** indent: 1
     * .. js:function:: onFocus(opts)
     *
     *    Bound to the *focus* event.
     *
     *    :param opts: The same parameter as provided in :js:func:`focus` and it's *focus* event.
     */
    onFocus: function () {
    },

    /** indent: 1
     * .. js:function:: onBlur()
     *
     *    Bound to the *blur* event.
     */
    onBlur: function () {
    },

    /** indent: 1
     * .. js:function:: onShow(opts)
     *
     *    Bound to the *show* event.
     *
     *    :param opts: The same parameter as provided in :js:func:`show` and in it's *show* event.
     */
    onShow: function () {
    },

    /** indent: 1
     * .. js:function:: onHide()
     *
     *    Bound to the *hide* event.
     */
    onHide: function () {
    },

    /** indent: 1
     * .. js:function:: onClose()
     *
     *    Bound to the *close* event.
     */
    onClose: function () {
    },

    /** indent: 1
     * .. js:function:: onBeforeClose()
     *
     *    This function is **always** called by :js:func:`close` to determine wether the closing of
     *    the view should be allowed. For example, if there are unsafed changes one use this
     *    function the check for them and trigger the safe process to prevent the changes from
     *    being lost.
     *
     *    :return: If *false* is returned the closeing will not be performed.
     *    :rtype: Bool
     */
    onBeforeClose: function () {
      return true;
    },

    /** indent: 1
     * .. js:function:: onResize()
     *
     *    Bound to the *resize* event.
     */
    onResize: function () {
    },

    _serialize: function () {
      return [
        this.master._getFocusIndex(this),  // 0
        this.parent._getActiveIndex(this), // 1
        this.workspaceId,                  // 2
        this.extension.name,               // 3
        this.name,                         // 4
        this.state.obj,                    // 5
        this.icon,                         // 6
        this.label,                        // 7
      ];
    },
  }),{
    init: function init() {
      init._super.apply(this, arguments);

      /** indent: 1
       * .. js:attribute:: mainMenuEntry
       *
       *    :type: Object|null
       *
       *		If available, this will be an automatically created object that can be used in the main
       *		menu.
       */
      if (!this.mainMenuEntry && this.menuPosition) {
        this.mainMenuEntry = {};
      }
      var e = this.mainMenuEntry;
      if (e) {
        e.label = e.label || this.label;
        e.iconClass = e.iconClass || this.iconClass;
        e.position = e.position || this.menuPosition;
        e.needWs = this.workspace !== null;
        if (!e.callback) {
          var viewClass = this._class._instanceClass;
          if (this.menuFFcallback) {
            e.callback = vispa.mainMenu.spawnFromFile.bind(vispa.mainMenu,
              viewClass, this.menuFFselector, this.menuFFcallback);
          } else {
            e.callback = vispa.mainMenu.spawn.bind(vispa.mainMenu, viewClass, {});
          }
        }
      } else {
        e = null;
      }
      Utils.setROP(this, "mainMenuEntry", e);
    },

    _unserialize: function (tabber, data) {
      var Workspace = require("vispa/workspace"); // cyclice dependecy ?
      var Extension = require("vispa/extension"); // cyclice dependecy ?

      // check whether we have everything
      var ws   = Workspace._members.byId(data[2]);
      var ext  = Extension._members.byName(data[3]);
      if (!ext) return;
      var view = ext.getView(data[4]);
      if (!view) return;

      var viewArgs = {
        tabber     : tabber,
        focusIndex : data[0],
        activeIndex: data[1],
      };
      if ((ws === null || ws.state === "connected") && !vispa.sessionManager.lazy) {
        view._members.spawn(data[5], viewArgs, {
          workspace: ws,
        });
      } else {
        vispa.spawnInstance("core", "PlaceholderView", {
          ws    : ws,
          view  : view,
          state : data[5],
          icon  : data[6],
          label : data[7],
        }, viewArgs);
      }
    },

    name: "MainView"
  });

  return MainView;
});
