/**
 * views/dialog
 * =================
 */
define([
  "jquery",
  "vispa/views/base",
  "vispa/vue/menu",
  "vispa/vue/fastmenu",
  "vispa/common/dialog",
  "vispa/utils",
  "text!html/index/dialogview/header.html",
  "text!html/index/dialogview/body.html"
], function(
  $,
  BaseView,
  Menu,
  FastMenu,
  Dialog,
  Utils,
  tHeader,
  tBody
) {

  var DialogViewDialog = Dialog.extend({
    computed: {
      instance: {
        cached: false,
        get: function() {
          return this.$options.instance;
        },
      },
    },
    methods: {
      close: function () {
        if (this.instance)
          this.instance._close();
      },
    },
    partials: {
      header: tHeader,
      body: tBody,
    },
    components: {
      "menu-button": FastMenu.button,
      "fast-menu":   FastMenu.bar,
    },
  });

  /**
   * .. js:class:: DialogView(args, viewArgs, baseArgs)
   *
   *    :extends: BaseView
   *
   *    This view is opened in a dialog. Usually within a :js:class:`MainView`.
   *
   *    :param Object args: See :js:class:`BaseView`.
   *    :param Object viewArgs: See :js:class:`BaseView`.
   *    :param Object baseArgs: See :js:class:`BaseView`.
   */
  var DialogView = BaseView._extend({

    init: function init(args, viewArgs) {
      init._super.apply(this, arguments);

      this._hasFocus = false;

      Utils.setROP(this, "callee", viewArgs.callee || undefined);
      var c = this.callee;
      while(c && c.callee) {
        c = c.callee;
      }
      Utils.setROP(this, "calleeRoot", c || undefined);

      if (this.callee) {
        this.$messageContainer = this.callee.$messageContainer;
      }

      // link events
      this.on("focus", this.show);
      var useFastmenu = !!(this.prefs && this.prefs.vue && this.prefs.vue.get("fastmenu"));

      // use a dialog
      this.vue = {};
      this.vue.node = this._dialog = new DialogViewDialog($.extend(true,
        {},
        this.dialogExtraOpts,
        {
          instance: this,
          computed: {
            hasBody: function() {
              return true;
            },
          },
          created: function() {
            // prevent auto mountpoint creation of the base Dialog Vue
            this.$options.el = undefined;
          },
          data: {
            fastmenu: false,
            loading : false,
            spinner : vispa.staticURL("img/maingui/loader6.gif"),
            menuItems: $.extend(
              {
                generalDiv: {
                  divider: true,
                  position: 1000,
                },
                close: {
                  label: "Close",
                  iconClass: "fa-times",
                  callback: function(){
                    this.$root.instance.close();
                  },
                  position: 1010,
                },
              },
              Menu.objClone(this._class._members.menu)
            ),
          },
        }
      ));

      // mount it now, since we need this._dialog available
      this._dialog.$mount();
      this._dialog.$appendTo(this.$messageContainer[0], undefined, false);
      this._dialog.fastmenu = useFastmenu && false; // TODO: broken so we disable this

      // render on next tick
      setTimeout((function(){
        this.render(this.$content);
      }).bind(this),0);
    },

    setWidth: function(width) {
      // if (this._dialog)
      //   this._dialog.setWidth(width);
    },

    /** indent: 1
     * .. js:attribute:: dialogExtraOpts
     *
     *    :type: Object
     *
     *    An object providing the base vue options for the underlying *DialogViewDialog*.
     */
    dialogExtraOpts: {
      descriptor: true,
      get: function() {
        return {};
      },
    },

    /** indent: 1
     * .. js:attribute:: $content read-only
     *
     *    :type: jQuery
     *
     *    The jQuery object containing the node that is holding the views content.
     */
    $content: {
      descriptor: true,
      get: function() {
        return $(this._dialog.$els.content);
      }
    },

    /** indent: 1
     * .. js:attribute:: menu read-only
     *
     *    :type: MenuButton
     *
     *    The vue instance representing the menu of this view.
     */
    menu: {
      descriptor: true,
      get: function() {
        return this._dialog.$refs.menu;
      }
    },

    /** indent: 1
     * .. js:function:: isVisible()
     *
     *    :return: Wether this view is visible. Always true.
     *    :rtype: Bool
     */
    isVisible: function() {
      return true;
    },

    /** indent: 1
     * .. js:function:: isFocused()
     *
     *    :return: Wether this view has focus.
     *    :rtype: Bool
     */
    isFocused: function() {
      return this._hasFocus;
    },

    /// transparent override
    focus: function focus(opts) {
      var c = this.calleeRoot;
      if (c && c.isFocused && c.isFocused()) {
        c.blur();
      }

      this._hasFocus = true;
      return focus._super.call(this, opts);
    },

    show: function show(opts) {
      this._dialog.isShown = true;
    },

    blur: function blur() {
      this._hasFocus = false;
      return blur._super.call(this);
    },

    hide: function() {
      this._dialog.isShown = false;
      this.close();
    },

    close: function() {
      this._dialog.close();
    },

    _close: function(callback) {
      // trigger the actual closing process using late fake _super calls
      this.blur();
      DialogView.prototype.hide._super.call(this);
      DialogView.prototype.close._super.call(this);

      // break a cyclic reference
      this._dialog.$options.instance = undefined;

      Utils.callback(callback)();

      // will refocus the parent if this had focus
      var c = this.calleeRoot;
      if (c && c.focus) {
        c.focus();
      }
    },

    /// transparent override
    destroy: function destroy() {
      this._dialog.$destroy(true, true);
    },

    /** indent: 1
     * .. js:attribute:: icon
     *
     *    :type: String
     *
     *    The CSS class names assigned to the icon of the dialog.
     */
    icon: {
      descriptor: true,
      set: function(icon) {
        this._dialog.iconClass = icon;
      },
      get: function() {
        return this._dialog.iconClass;
      }
    },

    /** indent: 1
     * .. js:attribute:: label
     *
     *    :type: String
     *
     *    The label of the dialog, displayed as title
     */
    label: {
      descriptor: true,
      set: function(label) {
        this._dialog.title = label;
      },
      get: function() {
        return this._dialog.title;
      }
    },

    /** indent: 1
     * .. js:function:: setLabel(label)
     *
     *    Sets the label. For compatiblity with :js:class:`MainView`s :js:func:`setLabel`.
     *
     *    :param String label: The label to be set.
     */
    setLabel: function (label) {
      this.label = label;
    },

    /** indent: 1
     * .. js:function:: setLoading(loading[, delay])
     *
     *    Behaves just like the normal setLoading with the only difference that you can provide
     *    a delay to the visual representation of loading (when not loading previously). It is
     *    inthendes to be used with low delays (such as 0.2s) to hold up the illusion of a
     *    very responsive UI.
     *
     *    :param Bool loading: Wether to increase or decrease the internal loading semaphore.
     *    :param Number delay: The delay in seconds until the loading indicatior will be shown
     *       after it was not already.
     */
    setLoading: function setLoading(loading, delay) {
      var self = this;

      setLoading._super.call(this, loading, function(ld) {
        self._dialog.loading = ld;
      }, delay);
    },

  },{
    name: "DialogView"
  });


  return DialogView;

});
