/**
 * views/side
 * =================
 */

define([
  "jquery",
  "vue/vue",
  "vispa/views/base",
  "vispa/utils",
], function (
  $,
  Vue,
  BaseView,
  Utils
) {

  /**
   * .. js:class:: SidebarView(args, viewArgs, baseArgs)
   *
   *    :extends: BaseView
   *
   *    The view used for the sidebar. Every view is a singleton
   *
   *    :param Object args: An Object containing any parameters for the specific View.
   *      A subset of it is usually used to initialize the state of the view.
   */
  var SidebarView = BaseView._extend($.extend({}, {

    init: function init(args) {
      var self = this;
      init._super.apply(self, arguments);

      /** indent: 1
       * .. js:attribute:: toggle read-only
       *
       *    :type: jQuery
       *
       *    The VueComponent of the toggle.
       */
      Utils.setROP(self, "toggle", self._class._members.sidebar.$refs.toggles[self.name]);

      /** indent: 1
       * .. js:attribute:: $content read-only
       *
       *    :type: jQuery
       *
       *    The jQuery object containing the node that is holding the views content.
       */
      Utils.setROP(self, "$content", $("#" + self.toggle.id, self.toggle.$root.$el));
    },

    /** indent: 1
     * .. js:function:: showSidebar()
     *
     *    Will show the whole sidebar, same as setting *vispa.sidebar.active = true*.
     */
    showSidebar: function show() {
      this.toggle.$root.active = true;
    },

    /** indent: 1
     * .. js:function:: hide()
     *
     *    Will hide the whole sidebar, same as setting *vispa.sidebar.active = false*.
     */
    hideSidebar: function hide() {
      this.toggle.$root.active = false;
    },

    /** indent: 1
     * .. js:function:: show()
     *
     *    Will show the view, same as putting the toggle to state *selected = true*. It also calls
     *    :js:func:`show` of :js:class:`BaseView`.
     */
    show: function show() {
      show._super.apply(this, arguments);
      this.toggle.selected = true;
    },

    /** indent: 1
     * .. js:function:: hide()
     *
     *    Will hide the view, same as putting the toggle to state *selected = false*. It also calls
     *    :js:func:`hide` of :js:class:`BaseView`.
     */
    hide: function hide() {
      hide._super.apply(this, arguments);
      this.toggle.selected = false;
    },

    /** indent: 1
     * .. js:function:: updatePopovers()
     *
     *    Update popovers. Shortcut for sidebar.updatePopovers.
     */
     updatePopovers: function () {
      this.toggle.$root.updatePopovers();
     }

  }), {
    _register: function _register(extension) {
      // fill preferences with default to avoid redundancy
      if (this.preferences) {
        if (!this.preferences.label && this.label) {
          this.preferences.label = this.label;
        }
        if (!this.preferences.iconClass && this.iconClass) {
          this.preferences.iconClass = this.iconClass;
        }
      }

      _register._super.call(this, extension);

      // add menu entry
      var self = this;
      self.id = self.extension.name + "-" + self.name;
      self.sidebar = vispa.sidebar;
      Vue.set(vispa.sidebar.items, self.name, {
        label: self.label || self.name,
        iconClass: self.iconClass || "fa-bars",
        callback: function () {
          if (self.singleton === true) self.spawn({}, {}, {});
        },
        position: self.position || 1000,
        selected: false,
        contentItem: true,
        id: self.id
      });
    },
    name: "SidebarView",
    singleton: true
  });

  return SidebarView;

});
