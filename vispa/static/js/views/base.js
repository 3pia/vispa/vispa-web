/**
 * views/base
 * =================
 */

define([
  "jquery",
  "vispa/ui/uinodecollection",
  "vispa/messenger",
  "vispa/common/dialog",
  "vispa/utils",
  "vispa/mixins/ajax",
  "vispa/mixins/link",
  "vispa/mixins/load",
  "vispa/mixins/logger",
  "vispa/mixins/preferences",
  "vispa/mixins/shortcuts",
  "vispa/mixins/socket",
  "vispa/mixins/state",
  "require"
], function(
  $,
  UINodeCollection,
  Messenger,
  Dialog,
  Utils,
  AjaxMixin,
  LinkMixin,
  LoadMixin,
  LoggerMixin,
  PreferencesMixin,
  ShortcutsMixin,
  SocketMixin,
  StateMixin,
  require
) {

  var runningId = 0;

  /**
   * .. js:class:: BaseView(args, viewArgs, baseArgs)
   *
   *    The base class for all views. The decendants are expected to implement the details of the
   *    visual representation.
   *
   *    :extends: UINodeCollection
   *    :mixins: AjaxMixin
   *    :mixins: LinkMixin
   *    :mixins: LoadMixin
   *    :mixins: LoggerMixin
   *    :mixins: PreferencesMixin
   *    :mixins: ShortcutsMixin
   *    :mixins: SocketMixin
   *    :mixins: StateMixin
   *
   *    :param Object args: An Object containing any parameters for the specific View.
   *      A subset of it is usually used to initialize the state of the view.
   *    :param Object viewArgs: An Object containing parameters of interest (e.g. position) for the
   *      view kind (Dialog, Main, SideBar).
   *    :param BaseView viewArgs.callee: The view that opened this view. May be *undefined* if not
   *      applicable.
   *    :param Object baseArgs: An Object containing parameters of interest for this class.
   *    :param Workspace baseArgs.workspace: The :js:class:`Workspace` this view is to be linked
   *    	to.
   *    :param String baseArgs.baseContext: See :js:attr:`baseContext`. If not present will be
   *      generated automatically.
   *    :param String baseArgs.defaultQuery: See :js:attr:`defaultQuery`. If not present it will
   *    	be generated automatically.
   *
   *    :throws Error: If the *baseArgs* is not an object or it is missing critical parameters.
   *    :throws Error: If the :js:attr:`maxInstances` is reached.
   *
   *    .. note::
   *       All classes extending this one can simply call the the init super by using::
   *
   *          init._super.apply(this, arguments);
   *
   *    .. note::
   *       The shortcuts are disabled by default.
   */
  var BaseView = UINodeCollection._extend($.extend({},
    AjaxMixin,
    LinkMixin,
    LoadMixin,
    LoggerMixin,
    PreferencesMixin,
    ShortcutsMixin,
    SocketMixin,
    StateMixin,
  {
    init: function init(args, viewArgs, baseArgs) {
      init._super.apply(this, Array.prototype.slice.call(arguments, 3));
      var mem = this._class._members;

      if(!$.isPlainObject(baseArgs)) {
        throw new Error("baseArgs must be a simple object!");
      }

      if (mem.maximumAllowedInstances <= mem._instances.length) {
        throw new Error("the maxInstances was reached");
      }

      var ws = mem.workspace;

      if (mem.singleton) {
        if (mem.count) {
          throw new Error("this view is an already existing singleton");
        } else {
          mem.singleton = this;
        }
        ws = null;
      }

      if (ws!==null && !baseArgs.workspace) {
        throw new Error("this view needs to be bound to a workspace");
      }

      // LinkMixin goes first since we may set defaultValues for baseArgs based upon its values
      LinkMixin.init.call(this,
        ws!==null ? baseArgs.workspace : null
      );

      // set the id
      /** indent: 1
       * .. js:attribute:: id read-only
       *
       *    :type: String
       *
       *    The id of the view.
       */
      Utils.setROP(this, "id", this.name + "-" + (mem._instanceCounter++));
      /** indent: 1
       * .. js:attribute:: rid read-only
       *
       *    :type: Number
       *
       *    A running id.
       */
      Utils.setROP(this, "rid", runningId++);
      /** indent: 1
       * .. js:attribute:: baseContext read-only
       *
       *    :type: String
       *
       *    An identifier string used for :js:attr:`logger`, :js:mixin:`SocketMixin`,
       *    and :js:mixin:`ShortcutsMixin`.
       */
      Utils.setROP(this, "baseContext", "view." + this.extension.name + "." + this.id);

      // compile the defaultQuery
      var defaultQuery = baseArgs.defaultQuery;
      if (!defaultQuery) {
        defaultQuery = vispa.defaultQuery;
        if (defaultQuery) {
          defaultQuery += "&";
        }
        defaultQuery += "_viewId=" + this.id;
        if (this.workspace) {
          defaultQuery += "&_workspaceId=" + this.workspace.id;
        }
      }

      // now setup remaining mixins
      AjaxMixin.init.call(this,
        baseArgs.dynamicBase || (this.extension||vispa).dynamicBase,
        baseArgs.staticBase  || (this.extension||vispa).staticBase,
        defaultQuery
      );

      PreferencesMixin.init.call(this, mem.prefrencesCategory, mem.prefrencesGroup);
      LoggerMixin     .init.call(this, this.baseContext);
      ShortcutsMixin  .init.call(this, this.baseContext);
      SocketMixin     .init.call(this, "extension."+this.id+".socket");
      StateMixin      .init.call(this, this.id);
      LoadMixin       .init.call(this);

      // fill internal variables
      this.$messageContainer = vispa.$messageContainer;
      this._state = {};

      // register
      mem._instances.push(this);

      // finish setup
      this.shortcuts.disable();
    },

    /** indent: 1
     * .. js:function:: processArgs(args, viewArgs, baseArgs)
     *
     *    For singletons, this function will be called automatically when instanciation of it was
     *    reuested again by spawn, in order to be able to provess the arguments anyway. Will be
     *    called before foucs.
     *
     *    .. note::
     *       This function should be overridden if needed.
     *
     *    .. note::
     *       For information on the arguments, see :js:func:`init`.
     *
     */
    processArgs: function(args, viewArgs, baseArgs) {
    },

    /** indent: 1
     * .. js:attribute:: messageContainer
     *
     *    :type: JQuery
     *
     *    The JQuery object (containing exactly one DOM node) to contain the messages/dialogs.
     *
     *    :throws Error: If attempting to set it to an invalid value.
     */
    $messageContainer: {
      descriptor: true,
      enumerable: true,
      get: function() {
        return this._$messageContainer;
      },
      set: function(newContainer) {
        if (!(newContainer instanceof $)) {
          throw new Error("newContainer must be an jQuery object");
        }
        if (newContainer.length != 1) {
          throw new Error("newContainer must contain exactly one DOM node");
        }
        this._$messageContainer = newContainer;
      }
    },

    /** indent: 1
     * .. js:attribute:: messageCount read-only
     *
     *    :type: Number
     *
     *    The of buffered messages for this view in the *vispa.messenger*.
     */
    messageCount: {
      descriptor: true,
      enumerable: true,
      get: function() {
        return vispa.messenger.messageCount(this.baseContext);
      },
    },

    _pushMessage: function(data) {
      data = $.extend({
        target: this._$messageContainer,
        context: this.baseContext
      }, data);

      vispa.messenger._pushMessage(data);
      this.emit("pushedMessage");

      return this;
    },

    /** indent: 1
     * .. js:function:: alert(message[, opts])
     *
     *    See :js:func:`alert` of :js:class:`Dialog`
     */
    alert  : Dialog.alert,

    /** indent: 1
     * .. js:function:: prompt(message, callback[, opts])
     *
     *    See :js:func:`prompt` of :js:class:`Dialog`
     */
    prompt : Dialog.prompt,

    /** indent: 1
     * .. js:function:: confirm(message, callback[, opts])
     *
     *    See :js:func:`confirm` of :js:class:`Dialog`
     */
    confirm: Dialog.confirm,

    /** indent: 1
     * .. js:function:: dialog(opts)
     *
     *    See :js:func:`dialog` of :js:class:`Dialog`
     */
    dialog : Dialog.dialog,

    /** indent: 1
     * .. js:function:: focus(opts)
     *
     *    Will focus the view. A :js:class:`ViewManager` may also call :js:func:`show` in the
     *    process.
     *
     *    :param opts: This may be used to pass further options to the view manager.
     *
     *    :emits focus(opts): This event is used by the :js:class:`ViewManager` and can also be
     *      used by others.
     */
    focus: function(opts) {
      this.shortcuts.enable();
      this.emit("focus", opts);
    },

    /** indent: 1
     * .. js:function:: blur()
     *
     *    Will blur (lose focus) the view.
     *
     *    :emits blur: This event is used by the :js:class:`ViewManager` and can also be used
     *      by others.
     */
    blur: function() {
      this.shortcuts.disable();
      this.emit("blur");
    },

    /** indent: 1
     * .. js:function:: show(opts)
     *
     *    Will show the view. If it is not :js:attr:`loaded` is will call :js:func:`load`
     *    automatically with itself provided as callback.
     *
     *    :param opts: This may be used to pass further options to the view manager.
     *
     *    :emits show(opts): This event is used by the :js:class:`ViewManager` and can also be
     *      used by others.
     */
    show: function(opts) {
      vispa.messenger.addContext(this.baseContext);
      this.emit("show", opts);
    },

    /** indent: 1
     * .. js:function:: hide()
     *
     *    Will hide the view. A :js:class:`ViewManager` may also call :js:func:`blur` in the
     *    process.
     *
     *    :emits hide: This event is used by the :js:class:`ViewManager` and can also be used
     *      by others.
     */
    hide: function() {
      vispa.messenger.removeContext(this.baseContext);
      this.emit("hide");
    },

    /** indent: 1
     * .. js:function:: close(force)
     *
     *    Will close the view. A :js:class:`ViewManager` may also call :js:func:`blur` and
     *    :js:func:`hide` in the process. Impliest the destruction of the instance - it must not
     *    be used thereafter.
     *
     *    :param Bool force: If this is *true* :js:func:`onBeforeClose` will not be consulted.
     *
     *    :emits close: This event is used by the :js:class:`ViewManager` and can also be used by
     *      others.
     */
    close: function() {
      this.emit("close");
      this.destroy();
    },

    /** indent: 1
     * .. js:function:: destroy()
     *
     *    Destroys the instance. It must not be used thereafter.
     */
    destroy: function() {
      PreferencesMixin.destroy.call(this);
      StateMixin      .destroy.call(this);
      LinkMixin       .destroy.call(this);
      SocketMixin     .destroy.call(this);
      var index = this._class._members._instances.indexOf(this);
      if (~index) {
        this._class._members._instances.splice(index, 1);
      }
      if (this._class._members.singleton) {
        this._class._members.singleton = true;
      }
    },

    /** indent: 1
     * .. js:function:: render(node)
     *
     *    The function to render/refresh the displayed content.
     *
     *    :param JQuery node: The JQUery object containing the one DOM node in which the content
     *    	is to be renderd.
     *
     *    .. note::
     *       Is to be overridden.
     */
    render: function(node) {
    },

    /** indent: 1
     * .. js:attribute:: name read-only
     *
     *    :type: String
     *
     *    The name of the view. Same as *_class._members.name*.
     */
    name: {
      descriptor: true,
      get: function() {
        return this._class._members.name;
      }
    },

    /** indent: 1
     * .. js:attribute:: extension read-only
     *
     *    :type: Extension
     *
     *    The extension of the view. Same as *_class._members.extension*.
     */
    extension:{
      descriptor: true,
      get: function() {
        return this._class._members.extension;
      }
    },

    /** indent: 1
     * .. js:function:: spawnInstance(extName, viewName[, args[, viewArgs[, baseArgs]]])
     *
     *    Spawn a new view from the given parameters.
     *
     *    :param String extName: The name of the extension that has the view.
     *    :param String viewName: The anem of the view to open.
     *    :param Object args: The args to be provided to the view.
     *    :param Object viewArgs: The viewArgs to be provided to the view.
     *    :param Object baseArgs: The baseArgs to be provided to the view.
     *
     *    :return: The view that was opened. It is most likely not ready to use yet.
     *    :rtype: BaseView
     *
     *    :throws Error: If either the extension of the view was not found.
     */
    spawnInstance: function (extName, viewName, args, viewArgs, baseArgs) {
      var extClass = require("vispa/extension"); // cyclice dependecy
      var ext  = extClass._members.byName(extName);
      if (!ext ) throw new Error("extension does not exist", extName);
      var view = ext.getView(viewName);
      if (!view) throw new Error("view does not exist", viewName);
      return view._members.spawn(
        args || {},
        $.extend({
          callee: this,
        }, viewArgs),
        $.extend({
          workspace: this.workspace
        }, baseArgs)
      );
    },

    /** indent: 1
     * .. js:function:: fileURL(path)
     *
     *    Retrieves the URL to directly read a file from the workspace.
     *
     *    :return: The full URL.
     *    :rtype: String
     *
     *    :throws Error: if this view is not bound to a workspace
     */
    fileURL: function(path) {
      if (!this.workspace) throw new Error("This view is not bound to workspace");
      return this.dynamicURL("/fs/getfile?_workspaceId="+this.workspaceId+"&path="+path);
    },

    _warnDisconnect: function() {
      this._warnDisconnectDialog = this._warnDisconnectDialog || this.confirm(
        "This tab's workspace has been disconnected. It will not work properly anymore.",
        function (ok) {
          this._warnDisconnectDialog = undefined;
          if (ok) this.close();
        }.bind(this), {
          title: "Workspace disconnected",
          yesLabel: "Close",
          noLabel: "Keep open",
        }
      );
    },

  }),$.extend({},
  {
    init: function() {
      this._instances = [];
      this._instanceCounter = 0;

      // freeze read-only properties
      Utils.setROP(this, "name",         this.name);
      Utils.setROP(this, "maxInstances", this.maxInstances);
      Utils.setROP(this, "extension",    this.extension);
    },

    _register: function(extension) {
      // update a ROP
      Utils.setROP(this, "extension", extension);

      if (this.preferences) {
        // fill preferences with default to avoid redundancy
        if (!this.preferences.label && this.label) {
          this.preferences.label = this.label;
        }
        if (!this.preferences.iconClass && this.iconClass) {
          this.preferences.iconClass = this.iconClass;
        }
        if ($.isArray(this.fastmenu)) {
          this.preferences.items.fastmenu = {
            value: this.fastmenu,
            level: 5,
            type: "object",
            global: true,
          };
        }
        // register preferences
        vispa.preferences.addGroup(this.extension.name, this.name, this.preferences);
      }
      if (this.fileHandlers) {
        $.each(this.fileHandlers, function (index, handler) {
          var key = this.extension.name + "-" + this.name + "-" + String(index);
          var self = this;
          handler.spawn = function (args) {
            vispa.mainMenu.spawn(self._class._instanceClass, args);
          };
          vispa.fileHandler.addFileHandler(key, handler);
        }.bind(this));
      }
    },

    /** indent: 1
     * .. js:attribute:: prefrencesCategory
     *    :type: String
     *
     *    The prefrences category name. Defaults to *this.extension.name*.
     *
     *    .. note::
     *       Can be redefined if necessary.
     */
    prefrencesCategory: {
      descriptor: true,
      get: function() {
        return this.extension.name;
      },
    },

    /** indent: 1
     * .. js:attribute:: prefrencesGroup
     *    :type: String
     *
     *    Thie prefrences group name. Defaults to *this.name*.
     *
     *    .. note::
     *       Can be redefined if necessary.
     *
     */
    prefrencesGroup: {
      descriptor: true,
      get: function() {
        return this.name;
      },
    },

    /** indent: 1
     * .. js:function:: spawn(args, viewArgs, baseArgs[, opts])
     *
     *    A singleton safe spawing method for a new view. For an excistant singleton only the
     *    *focus* method will be called.
     *
     *    :param Object args: Passed on to :js:class:`baseView` instanciation (if required).
     *    :param Object viewArgs: Passed on to :js:class:`baseView` instanciation (if required).
     *    :param Object baseArgs: Passed on to :js:class:`baseView` instanciation (if required).
     *    :param Object opts: Options passed to the :js:func:`focus` methods.
     *
     *    :return: The view spawned (or actived in the case of a existing singleton).
     *    :rtype: BaseView
     */
    spawn: function(args, viewArgs, baseArgs, opts) {
      var inst;
      if (this.singleton && this.singleton.show) {
        inst = this.singleton;
        inst.processArgs(args, viewArgs, baseArgs);
        inst.focus(opts);
      } else {
        inst = new this._class._instanceClass(args, viewArgs, baseArgs);
      }
      return inst;
    },

    /** indent: 1
     * .. js:attribute:: name read-only class-member
     *
     *    :type: String
     *
     *    The name of the view. Must be overridden by extending classes.
     *
     *    :throws Error: If it is not overridden.
     */
    name: "BaseView",

    /** indent: 1
     * .. js:attribute:: maxInstances read-only class-member
     *
     *    :type: Number
     *    :value: Infinity
     *
     *    The maximum allowed count of concurrent intances. Should be overridden as needed.
     */
    maxInstances: Infinity,

    /** indent: 1
     * .. js:attribute:: extension read-only class-member
     *
     *    :type: Extension|null
     *    :value: null
     *
     *    The extension this view is linked to by default. Will be updated automatically when
     *    registering the view to an extension.
     */
    extension: null,

    /** indent: 1
     * .. js:attribute:: instances read-only class-member
     *
     *    :type: Array(BaseView)
     *
     *    A list of all current instances of this class.
     */
    instances: {
      descriptor: true,
      get: function () {
        return this._instances.slice();
      },
    },

    /** indent: 1
     * .. js:attribute:: count read-only class-member
     *
     *    :type: Number
     *
     *    The count of instances of this class currently active.
     */
    count: {
      descriptor: true,
      get: function () {
        return this._instances.length;
      },
    },

    /** indent: 1
     * .. js:function:: closeAllInstances()
     *
     *    Closes all open instance of this viewClass.
     */
    closeAllInstances: function () {
      // loop backwards since the closing of an instance should remove the entry from the array
      // and thus shorten it
      for(var i = this._instances.length; i >= 0; i--) {
        this._instance[i].close(true);
      }
    },

    /** indent: 1
     * .. js:attribute:: workspace class-member
     *
     *    :type:
     *
     *		The default workspace binding type for this view. If it is *null* this view will only be
     *		global (no workspace binding). If it is a number, it will always be bound to a
     *		workspace. In any other case, it can be both.
     */
    workspace: -1,
  }));

  return BaseView;
});
