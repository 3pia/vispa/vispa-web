/**
 * mixins/load
 * ===========
 */

define(["jquery"], function($) {

  /**
   * .. js:mixin:: LoadMixin()
   *
   *    Mixin that provides load state functionality.
   */
  var LoadMixin = {

    init: function() {
      this._loadingCount   = 0;
      this._loadingTimeout = undefined;
    },

    /** indent: 1
     * .. js:attribute:: loading
     *
     *    :type: Boolean
     *
     *    Is *true* if any loading (not necessarly the inital loading) is still in process.
     *    Setting its value is the same as calling setLoading with this value.
     */
    loading: {
      descriptor: true,
      set: function(loading) {
        this.setLoading(loading);
      },
      get: function() {
        return !!this._loadingCount;
      }
    },

    /** indent: 1
     * .. js:function:: setLoading(loading[, delay[, setter]])
     *
     *    In-/Decreses the internal loading counter. May be overloaded to visually represent
     *      the loading process.
     *
     *    :param Bool loading: A logical *true* value will increase the loading count,
     *      anything else will decrease it.
     *    :param Number|null delay: An optional delay to use for calling the setter.
     *    :param Function setter: A function to update the visual loading state.
     *      This parameter only to be used by the views extending the :js:class:`BaseView`.
     *
     *    :emits loadingDone: When the internal loading counter has reach 0 again.
     */
    setLoading: function(loading, setter, delay) {
      var wasLoading = this.loading;

      if (loading) {
        this._loadingCount++;
      } else {
        this._loadingCount--;
        if (this._loadingCount < 0) {
          this._loadingCount = 0;
        }
        if (this._loadingCount === 0 && $.isFunction(this.emit)) {
          this.emit("loadingDone");
        }
      }

      if ((typeof setter === "function") && (wasLoading !== this.loading)) {
        if (this._loadingTimeout) {
          clearTimeout(this._loadingTimeout);
          this._loadingTimeout = undefined;
        }
        if (delay === null) {
          delay = 0.25;
        }
        if (delay && delay > 0) {
          var self = this;
          this._loadingTimeout = setTimeout(function(){
            setter(self.loading);
            self._loadingTimeout = undefined;
          }, delay/1000);
        } else {
          setter(this.loading);
        }
      }
    },

    /** indent: 1
     * .. js:attribute:: loaded read-only
     *
     *    :type: bool
     *
     *    Is *true* if the initial loading has finished.
     *
     *    .. note::
     *       Is to be overidden.
     */
    loaded: {
      descriptor: true,
      value: true,
      configurable: true,
      enumerable: true,
    },

    /** indent: 1
     * .. js:function:: load(callback)
     *
     *    This function is expected to start the inital loading process.
     *
     *    .. note::
     *       Is to be overidden.
     *
     *    :callback Error|null err: A possible error.
     *
     *    :emits loaded: Once loading has finished after the callback was called.
     */
    load: function(callback) {
      callback(null);
    },

  };


  return LoadMixin;
});
