/**
 * mixins/ajax
 * ===========
 */

define(["vispa/utils"], function(Utils) {

  /**
   * .. js:mixin:: AjaxMixin(dynamicBase, staticBase[, defaultQuery])
   *
   *    Mixin that provides ajax functionality, such as GET and POST methods as well as cached
   *    file retrieval.
   *
   *    :param String dynamicBase: See :js:attr:`dynamicBase`.
   *    :param String staticBase: See :js:attr:`staticBase`.
   *    :param String|Function defaultQuery: Defaults to ``""``. See :js:attr:`defaultQuery`.
   */
  var AjaxMixin = {

    init: function(dynamicBase, staticBase, defaultQuery) {
      // check dynamicBase
      if (typeof(dynamicBase) != "string") {
        throw new Error("invalid type for dynamicBase");
      }

      // check staticBase
      if (typeof(staticBase) != "string") {
        throw new Error("invalid type for staticBase");
      }

      // check defaultQuery
      if (defaultQuery === undefined) {
        defaultQuery = function() { return ""; };
      } else if (typeof(defaultQuery) == "string") {
        var _defaultQuery = defaultQuery;
        defaultQuery = function() { return _defaultQuery; };
      } else if (!$.isFunction(defaultQuery)) {
        throw new Error("invalid type for defaultQuery");
      }

      // make sure they end in a slash
      dynamicBase = cutTrailingSlash(dynamicBase) + "/";
      staticBase  = cutTrailingSlash(staticBase ) + "/";

      /** indent: 1
       * .. js:attribute:: dynamicBase read-only
       *
       *    :type: String
       *
       *    The dynamic base for ajax calls.
       */
      Utils.setROP(this, "dynamicBase", dynamicBase);

      /** indent: 1
       * .. js:attribute:: staticBase read-only
       *
       *    :type: String
       *
       *    The static base for file retrievals via :js:func:`getFile`.
       */
      Utils.setROP(this, "staticBase",  staticBase);

      this._defaultQuery = defaultQuery;
    },

    /** indent: 1
     * .. js:attribute:: defaultQuery read-only
     *
     *    :type: String|Function
     *
     *    The default query string when sending ajax requests.
     *
     *    .. note::
     *       When this value is assigned to a function, the getter still returns a string by
     *       evaluating the function without arguments.
     */
    defaultQuery: {
      descriptor: true,
      get: function() {
        var q = this._defaultQuery();
        if (typeof(q) != "string") {
          throw new Error("invalid type for defaultQuery");
        }
        return q;
      }
    },

    /** indent: 1
     * .. js:function:: staticURL(url)
     *
     *    Returns the fully prefixed URL for a static resource.
     *    If the given url starts with a slash it will resturn a global URL
     *    (same as calling this function on the vispa object).
     *
     *    :param String url: The URL of the resource to get.
     *
     *    :return: The fully prefixed URL.
     *    :rtype: String
     */
    staticURL: function(url) {
      return (url[0]=="/" ? vispa : this).staticBase + cutLeadingSlash(url);
    },

    /** indent: 1
     * .. js:function:: dynamicURL(url)
     *
     *    Returns the fully prefixed URL for a dynamic resource (usually ajax).
     *    If the given url starts with a slash it will resturn a global URL
     *    (same as calling this function on the vispa object).
     *
     *    :param String url: The URL of the resource to get.
     *
     *    :return: The fully prefixed URL.
     *    :rtype: String
     */
    dynamicURL: function(url) {
      return (url[0]=="/" ? vispa : this).dynamicBase + cutLeadingSlash(url);
    },

    /** indent: 1
     * .. js:function:: applyDefaultQuery(url)
     *
     *    Applies the defaultQuery to the given URL.
     *
     *    :param String url: The URL do be augmented with the defaultQuery.
     *    :return: The augmented URL.
     *    :rtype: String
     */
    applyDefaultQuery: function(url) {
      var qs = this.defaultQuery;
      if (qs) {
        url += (~url.indexOf("?") ? "&" : "?") + qs;
      }
      return url;
    },

    /** indent: 1
     * .. js:function:: getFile(url[, callback])
     *
     *    Retrieves the content of a file. This function caches file contents identified by *url*.
     *
     *    :param String url: The URL to a file to retrieve, relative to :js:attr:`staticBase`.
     *    :callback Error|null err: A possible error.
     *    :callback String content: The content of the retrieved file.
     */
    getFile: function(url, callback) {
      Utils.asyncRequire("text!"+this.staticURL(url))(Utils.callback(callback));
    },

    /** indent: 1
     * .. js:function:: ajax(method, url[, data][, callback])
     *
     *    Ajax wrapper method that represents the core of any ajax call. It should be used instead
     *    of jQuery's methods since vispa implements the callback-as-last-argument style instead of
     *    using jqXHR/Deferred objects.
     *
     *    :param String method: The http method to use. Currently, only "GET", "POST" and "DELETE"
     *       are supported.
     *    :param String url: The url of the endpoint to use, relative to the dynamic base.
     *    :param Object|String data: The data to send. When a string is passed, it is assumed that
     *       it is an encoded json object and the content type will be set accordingly. When an
     *       object is passed, it is forwarded to jQuery's ajax method the normal way.
     *    :callback Error|null err: A possible error.
     *    :callback data: The value of the *data* attribute of the response.
     *    :callback Object res: The response object.
     */
    ajax: function(method, url, data, callback) {
      var self = this;

      // default arguments
      if (callback === undefined) {
        if ($.isFunction(data)) {
          callback = data;
          data = {};
        } else {
          callback = function(){};
        }
      }

      // if url is an array, join it
      if ($.isArray(url)) {
        url = url.map(encodeURIComponent).join("/");
      }

      // get prefixed URL
      url = this.dynamicURL(url);

      // add default query string
      url = this.applyDefaultQuery(url);

      // build the request
      var req = {
        type: method,
        url : url,
        data: data
      };

      // json encoded?
      if (typeof(data) == "string") {
        req.contentType = "application/json";
      }

      // perform the request
      $.ajax(req).always(function(jqXHRFail, statusString, jqXHRSuccess) {
        var jqXHR = statusString == "success" ? jqXHRSuccess : jqXHRFail;

        var res = jqXHR.responseJSON;
        if (res === undefined) {
          res = {
            code   : jqXHR.status,
            data   : jqXHR.responseText,
            message: jqXHR.status == 200 ? null : jqXHR.statusText,
            alert  : false
          };
        }

        if (res.code == 200) {
          callback(null, res.data, res);
        } else {
          if (res.alert && $.isFunction(self.alert)) {
            self.alert(res.message);
          }
          var err = new Error(res.message);
          callback(err, null, res);
        }
      });
    },

    /** indent: 1
     * .. js:function:: GET(...)
     *
     *    Convenience wrapper around :js:func:`ajax` for sending GET requests.
     */
    GET: function() {
      var args = Array.prototype.slice.call(arguments);
      this.ajax.apply(this, ["GET"].concat(args));
    },

    /** indent: 1
     * .. js:function:: POST(...)
     *
     *    Convenience wrapper around :js:func:`ajax` for sending POST requests.
     */
    POST: function() {
      var args = Array.prototype.slice.call(arguments);
      this.ajax.apply(this, ["POST"].concat(args));
    },

    /** indent: 1
     * .. js:function:: DELETE(...)
     *
     *    Convenience wrapper around :js:func:`ajax` for sending DELETE requests.
     */
    DELETE: function() {
      var args = Array.prototype.slice.call(arguments);
      this.ajax.apply(this, ["DELETE"].concat(args));
    }
  };


  /*
   * .. js:function:: cutLeadingSlash(url)
   *
   *    Cuts the leading slash from a *url*.
   *
   *    :param String url: The URL to use.
   *    :return: The new URL.
   *    :rtype: String
   */
  var cutLeadingSlash = function(url) {
    return url[0] == "/" ? url.substr(1) : url;
  };


  /*
   * .. js:function:: cutTrailingSlash(url)
   *
   *    Cuts the trailing slash from a *url*.
   *
   *    :param String url: The URL to use.
   *    :return: The new URL.
   *    :rtype: String
   */
  var cutTrailingSlash = function(url) {
    return url.substr(-1) == "/" ? url.substr(0, url.length - 1) : url;
  };


  return AjaxMixin;
});
