/**
 * mixins/shortcuts
 * ================
 */

define(["vispa/utils"], function(Utils) {

  var scHanldler = function(func, event) {
    if (!func.call(this, event)) {
      event.preventDefault();
    }
  };

  /**
   * ShortcutsMixin
   * ------------------
   *
   * .. js:mixin:: ShortcutsMixin(shortcutsContext)
   *
   *    Mixin that steers shortcuts.
   *
   *    :param String shortcutsContext: See :js:attr:`shortcutsContext`.
   */
  var ShortcutsMixin = {

    init: function(shortcutsContext) {
      // check and set shortcutsContext
      if (typeof(shortcutsContext) != "string") {
        throw new Error("invalid type for shortcutsContext");
      }

      /** indent: 1
      * .. js:attribute:: shortcutsGroup read-only
      *
      *    :type: String
      *
      *    The `shortcuts group <https://github.com/riga/jquery.shortcuts>`_ that shortcuts are
      *    registered to.
      */
      Utils.setROP(this, "shortcuts", $.Shortcuts(shortcutsContext));

      this.applyShortcuts();

      if (this.prefs) {
        this.prefs._watchAutoShortcuts(this.applyShortcuts.bind(this));
      }
    },

    /** indent: 1
     * .. js:function:: applyShortcuts()
     *
     *    Applies the current shortcuts. All shortcuts will be reset at the beginning of this
     *      function.
     */
    applyShortcuts: function() {
      this.shortcuts.empty();
      // parse preferences (if there are any)
      if (this.prefs) {
        var sc = this.prefs.autoShortcuts;
        for (var i in sc) {
          var k = Utils.shortcutPP(i);
          if (k) {
            this.shortcuts.add(k, scHanldler.bind(this, sc[i]));
          }
        }
      }
    },
  };

  return ShortcutsMixin;
});
