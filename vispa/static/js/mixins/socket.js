/**
 * mixins/socket
 * =============
 */

define(["jclass", "vispa/utils"], function(JClass, Utils) {

  /**
   * SocketMixin
   * ---------------
   *
   * .. js:mixin:: SocketMixin(socketContext)
   *
   *    Mixin that provides websocket functionality.
   *
   *    :param String socketContext: See :js:attr:`SocketProxy.socketContext`.
   */
  var SocketMixin = {

    init: function(socketContext) {
      /** indent: 1
       * .. js:attribute:: socket read-only
       *
       *    :type: SocketProxy
       *
       *    The contained socket proxy instance.
       */
      Utils.setROP(this, "socket", new SocketProxy(socketContext));
    },

    /// transparent override
    destroy: function() {
      this.socket._destroy();
    },
  };


  /**
   * SocketProxy
   * -----------
   *
   * .. js:class:: SocketProxy(socketContext)
   *
   *    .. warning::
   *       This class is not exportet and its instances are only available throug the implementers of the :js:mixin:`SocketMixin`.
   *
   *    A lightweight socket proxy implementation that basically yields the emitter-like
   *    :js:func:`emit` and :js:func:`on` methods. Internally, these calls are forwarded to the
   *    global ``vispa.socket`` instance with *socketContext* being prefixed to all events.
   *
   *    :param String socketContext: See :js:attr:`socketContext`.
   */
  var SocketProxy = JClass._extend({

    init: function(socketContext) {
      if (typeof(socketContext) != "string") {
        throw new Error("invalid type for socketContext");
      }

      /** indent: 1
       * .. js:attribute:: socketContext read-only
       *
       *    :type: String
       *
       *    The socket context which is used to as a prefix for all events.
       */
      Utils.setROP(this, "socketContext", socketContext);
    },

    /** indent: 1
     * .. js:function:: emit(event, ...)
     *
     *    Emits an *event* with all supplied arguments.
     *
     *    :param String event: The name of the event to emit to.
     */
    emit: function(event) {
      var args = Array.prototype.slice.call(arguments, 1);
      event = this.socketContext + "." + event;
      vispa.socket.emit.apply(vispa.socket, args);
    },


    /** indent: 1
     * .. js:function:: on(event, callback)
     *
     *    Listens to an *event* with a *callback*.
     *
     *    :param String event: The name of the event to listen to.
     *    :param Function callback: The callback function that is invoked with all arguments
     *       supplied in the corresponding event emission.
     */
    on: function(event, callback) {
      event = this.socketContext + "." + event;
      vispa.socket.on(event, callback);
    },

    _destroy: function() {
      vispa.socket.removeAllListeners(this.socketContext + ".**");
    },
  });


  return SocketMixin;
});
