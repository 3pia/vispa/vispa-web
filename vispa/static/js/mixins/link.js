/**
 * mixins/link
 * ===========
 */

define(["vispa/workspace", "vispa/utils"], function(Workspace, Utils) {

  /**
   * .. js:mixin:: LinkMixin(workspace)
   *
   *    Mixin that provides *optional* links to a :js:class:`Workspace`.
   *
   *    :param Workspace workspace: See :js:attr:`workspace`.
   */
  var LinkMixin = {

    init: function(workspace) {
      // check workspace
      if (!(workspace instanceof Workspace) && workspace !== null) {
        throw new Error("invalid type for workspace");
      }

      /** indent: 1
       * .. js:attribute:: workspace read-only
       *
       *    :type: Workspace
       *
       *    The workspace this object is bound to.
       */
      Utils.setROP(this, "workspace", workspace);

      // register instance
      if (this.workspace) {
        this.workspace._registerInstance(this);
      }
    },

    /// transparent override
    destroy: function() {
      // unregister instance
      if (this.workspace) {
        this.workspace._unregisterInstance(this);
      }
    },

    /** indent: 1
     * .. js:attribute:: workspaceId read-only
     *
     *    :type: Number|null
     *
     *    The linked workspace Id or *null*.
     */
    workspaceId: {
      descriptor: true,
      get: function() {
        return this.workspace ? this.workspace.id : null;
      }
    },
  };


  return LinkMixin;
});
