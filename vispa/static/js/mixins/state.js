/**
 * mixins/state
 * ============
 */

define([
  "jquery",
  "emitter",
  "vispa/utils",
], function(
  $,
  Emitter,
  Utils
) {

  /**
   * .. js:mixin:: StateMixin()
   *
   *    Mixin that defines the state getter and loader.
   */
  var StateMixin = {
    init: function() {
      /** indent: 1
       * .. js:attribute:: state read-only
       *
       *    :type: StateProxy
       *
       *    The object managing the state,
       */
      Utils.setROP(this, "state", new StateProxy(this));
    },

    /// transparent override
    destroy: function() {
      this.state._destroy();
    },

    // convienience functions
    getState: function() {
      return this.state.get.apply(this.state, arguments);
    },
    setState: function() {
      return this.state.set.apply(this.state, arguments);
    },
    setupState: function() {
      return this.state.setup.apply(this.state, arguments);
    },
  };

  /**
   * .. js:class:: StateProxy(base)
   *
   *    :extends: Emitter
   *
   *    Provides the state handling functions.
   *
   *    .. warning::
   *       There is no need to instanciate this object directly,
   *       this is all handled by the state mixin.
   *
   *    :param Emitter base: The instance of which the state is managed.
   *       Used for informing it about state changes.
   */
  var StateProxy = Emitter._extend({
    init: function init(base) {
      init._super.call(this);

      if (!(base instanceof Emitter)) throw new Error("base must be an emitter");
      /** indent: 1
       * .. js:attribute:: obj read-only
       *
       *    :type: Object
       *
       *    The object holding the state information. Manipulating its values will not cause
       *    the appropriate events to be emitted.
       */
      Utils.setROP(this, "obj" , {});
      /** indent: 1
       * .. js:attribute:: base read-only
       *
       *    :type: Emitter
       *
       *    The instance of which the state is managed.
       */
      Utils.setROP(this, "base", base);
    },

    _destroy: function() {
    },

    /** indent: 1
     * .. js:function:: get(stateKey[, defaultValue])
     *
     *    Retieves the value of a state. Returns undefined if it is not set.
     *
     *    :param String stateKey: The key of the value to be retrieved.
     *    :param Any defaultValue: The value to return if the given state key is not defined.
     *
     *    :return: The value for the given *stateKey*.
     *    :rtype: Any
     */
    get: function(stateKey, defaultValue) {
      var value = this.obj[stateKey];
      return value===undefined ? defaultValue : value;
    },

    /** indent: 1
     * .. js:function:: set(stateKey, value[, delay])
     *
     *		Sets the value of a state key, then triggers the events in the order they are documented
     *		in. Finally calls :js:func:`save` and returns the final value.
     *
     *    :param String stateKey: The key of the valuze to set.
     *    :param Any value: The value to set. Must be serializable by JSON to be saved.
     *    :param Number delay: :js:func:`save` is called with this paramerter.
     *
     *    :return: The new value for the *stateKey*. (This may actuall differ from what was
     *       intendet to be set, if it was changed during the events.)
     *    :rtype: Any
     *
     *    :emits *stateKey*(newValue, oldValue): Events of the appropriate *stateKey* will be
     *       emitted when its value changed.
     *    :emits changedState(stateKey, newValue, oldValue): Is emitted for state changes - but
     *       on the *base* object.
     */
    set: function(stateKey, value, delay) {
      var oldFragment = this._fragment;
      var oldValue    = this.obj[stateKey];
      this.obj[stateKey] = value;
      this.emit(stateKey, value, oldValue);
      this.base.emit("changedState", stateKey, value, oldValue);
      if (oldFragment && oldFragment!==this._fragment && this._fragmentActive) {
        vispa.saveFragment();
      }
      this.save(delay);
      return this.obj[stateKey];
    },

    /** indent: 1
     * .. js:function:: setup(source[, auxSource])
     *
     *    Will update the internal state with the values from the given parameters.
     *
     *    :param Object source: An object containing the values to apply to the state.
     *    :param Object auxSource: An additonal object containing the values to apply to the state.
     *
     *    :emits changedState(*null*): Is always emitted afterwards - but on the *base* object.
     */
    setup: function(source, auxSource) {
      var oldFragment = this._fragment;
      $.extend(this.obj, source, auxSource);
      this.base.emit("changedState", null);
      if (oldFragment!==this._fragment && this._fragmentActive) {
        vispa.saveFragment();
      }
    },

    /** indent: 1
     * .. js:function:: save([delay])
     *
     *    Save the current state.
     *
     *    :param Number delay: The maximum delay in seconds to save the current state to the
     *       server. For more details see :js:func:`save` of :js:class:`SessionManager`.
     */
    save: function(delay) {
      vispa.sessionManager.save(delay);
    },

    /// just for internacl use
    _fragment: {
      descriptor: true,
      get: function() {
        return this.base.getFragment ? this.base.getFragment() : undefined;
      },
    },

    _fragmentActive: {
      descriptor: true,
      get: function() {
        return this.base.isFocused ? this.base.isFocused() : true;
      }
    },
  });

  return StateMixin;
});
