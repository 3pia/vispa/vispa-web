/**
 * mixins/preferences
 * ==================
 */

define([
  "jquery",
  "jclass",
  "vispa/utils"
], function(
  $,
  JClass,
  Utils
) {
  /**
   * PreferencesMixin
   * --------------------
   *
   * .. js:mixin:: PreferencesMixin(category, group[, prefs])
   *
   *    Mixin that provides preference functionality.
   *
   *    :param String category: Preferences category.
   *    :param String group: Preferences group.
   *    :param Object prefs: The config for the preferences to create. See :js:class:`PrefGroup`s
   *      *data* parameter for details.
   */
  var PreferencesMixin = {

    init: function(category, group, prefs) {
      if (prefs) {
        vispa.preferences.addGroup(category, group, prefs);
      }
      var vue = vispa.preferences.getGroup(category, group);

      if (vue) {
        /** indent: 1
         * .. js:attribute:: prefs
         *
         *    :type: PreferencesProxy
         *
         *    All prefrences related function are collected here.
         */
        Utils.setROP(this, "prefs", new PreferencesProxy(vue, this));
      }

      // TODO: we need to call applyPreferences on any changes (this could become deprecated/removed)
    },

    /** indent: 1
     * .. js:function:: applyPreferences(key, value)
     *
     *    This function will be bound to the *changed* event of the prefrences object. This is also
     *    triggered for non-auto applied shortcus.
     *
     *    :param String key: The key of the setting changed. May be undefined if multiple settings
     *    	changed at once - e.g. after loading the from the DB.
     *    :param value: The new value of the setting changed. Only available if the *key* was set.
     */
    applyPreferences: function(key, value) {
      // interface method, no workload
    },

    /// transparent override
    destroy: function() {
      if (this.prefs) {
        this.prefs._destroy();
      }
    },
  };

  var PreferencesProxy = JClass._extend({
    init: function(vue, base) {
      /** indent: 1
      * .. js:attribute:: prefs.vue
      *
      *    :type: PrefsGroup (Vue)
      *
      *    The preferences group.
      */
      Utils.setROP(this, "vue", vue);

      this._base = base;
      this._prefsWatches = []; // collect teardown functions for watchers
    },
    _destroy: function() {
      var unwatch;
      while (unwatch = this._prefsWatches.pop()) {
        unwatch();
      }
    },

    /** indent: 1
     * .. js:function:: prefs.get(key)
     *
     *    Retrieves the value of a preference item given by key.
     *
     *
     *    :param String key: The key of the preferences item.
     *    :return: The current value of the preferences item.
     */
    get: function(key) {
      var prefItem = this.vue.get(key);
      if (!prefItem) {
        throw new Error("there is no preference with the given key");
      }
      return prefItem.get(this._base.workspace);
    },

    /** indent: 1
     * .. js:function:: prefs.set(key[, value[, global]])
     *
     *    Retrieves the value of a preference item given by key.
     *
     *    :param String key: The key of the preferences item.
     *    :param value: The new value for the preferences item.
     *    :param Bool global: The value will be set globally, ignoring eventual binding.
     */
    set: function(key, value, global) {
      var prefItem = this.vue.get(key);
      if (!prefItem) {
        throw new Error("there is no preference with the given key");
      }
      var ws = global||prefItem.data.global ? undefined : this._base.workspace;
      prefItem.set(ws, value);
    },

    /** indent: 1
     * .. js:function:: prefs.watch(key, callback)
     *
     *    Watches a specific prefrences items value for changes.
     *
     *    .. note::
     *       This uses the *$watch* method of *vuejs* internally.
     *
     *    :param String key: The key of the preferences item to watch. If *null* will watch all
     *      preferences of the entire group.
     *    :callback oldValue: The old value of the preference before it was changed.
     *    :callback newValue: The new value of the preference after it was changed.
     */
    watch: function(key, callback) {
      var prefItem;
      if (key === null) {
        prefItem = this.vue;
      } else {
        prefItem = this.vue.get(key);
        if (!prefItem) {
          throw new Error("there is no preference with the given key");
        }
      }
      if (!$.isFunction(callback)) {
        throw new Error("callback is no function");
      }

      this._prefsWatches.push(prefItem.watch(this._base.workspace, callback.bind(this._base)));
    },

    /** indent: 1
     * .. js:function:: prefs.forceWorkspace(key)
     *
     *    Makes sure that the given preference will be a workspace specific one. This only works
     *    for non-primitive types (*Array*, *Object*).
     *
     *    :param String key: The key of the preference.
     *
     *    :throws Error: If there is no such prefrence.
     *    :throws Error: If *key* referes to a global only prefernce.
     *    :throws Error: If it failed to froce the preference to be workspace specific.
     */
    forceWorkspace: function(key) {
      var prefItem = this.vue.get(key);
      if (!prefItem)
        throw new Error("there is no preference with the given key");
      if (prefItem.data.global)
        throw new Error(key+" is a global preference");
      var ws = this._base.workspace;
      var g = prefItem.get(null);
      var l = prefItem.get(ws);
      if (g == l)
        prefItem.set(ws, JSON.parse(JSON.stringify(g)));
      l = prefItem.get(ws);
      if (g == l)
        throw new Error("failed to force workspace specific preference");
    },

    _watchAutoShortcuts: function(callback) {
      this._prefsWatches.push(this.vue.watchAutoShortcuts(this._base.workspace, callback));
    },

    /** indent: 1
     * .. js:attribute:: prefs.autoShortcuts read-only
     *
     *    :type: Object(shortcutDefinition => callback)
     *
     *		All shortcuts do be automatically applied (by :js:mixin:`ShortcutsMixin`).
     */
    autoShortcuts: {
      descriptor: true,
      get: function() {
        return this.vue.autoShortcuts(this._base.workspace);
      },
    },
  });

  return PreferencesMixin;
});
