/**
 * mixins/logger
 * =============
 */

define(["jquery"], function($) {

  /**
   * LoggerMixin
   * --------------------
   *
   * .. js:mixin:: LoggerMixin(loggerContext)
   *
   *    Mixin that provides logger functionality. It is save to be initalized multiple times.
   *
   *    :param String loggerContext: See *JQuery.Logger()*.
   */
  var LoggerMixin = {
    init: function(loggerContext) {
      this._logger = $.Logger(loggerContext);
    },

    /** indent: 1
     * .. js:attribute:: logger read-only
     * 
     *    :type: JQuery.Logger
     *
     *    The logger for this view.
     */
    logger: {
      descriptor: true,
      get: function() {
        return this._logger;
      },
    },

    /** indent: 1
     * .. js:function:: createSubLogger(name)
     *
     *    Creates a new *JQuery.Logger* as a sub-logger of the current :js:attr:`logger`.
     *
     *    :param String name: The name to append to the logger identifier. A leading "." is not requried.
     *    :return: The new sub-logger.
     *    :rtype: JQuery.Logger
     */
    createSubLogger: function(name) {
      var loggerName = this._logger.namespace() + "." + name;
      loggerName = loggerName.replace(/\.\.+/g, ".");
      loggerName = loggerName.replace(/^\.+/g, "");
      loggerName = loggerName.replace(/\.+$/g, "");
      return $.Logger(loggerName);
    }
  };

  return LoggerMixin;
});
