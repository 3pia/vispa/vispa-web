// first config useed for optimizer, set actual baseUrl later
require.config({
    paths: {
        vispa       : "js",
        vendor      : "vendor",
        jclass      : "vendor/jclass/jclass",
        emitter     : "js/common/emitter",
        jquery      : "vendor/jquery/js/jquery",
        bootstrap   : "vendor/bootstrap/js/bootstrap",
        ace         : "vendor/ace",
        async       : "vendor/async/async",
        transparency: "vendor/transparency/transparency",
        vue         : "vendor/vue",
        extensions  : "../extensions",
        styles      : "css",
        xterm       : "vendor/xterm",
    },
    shim: {
        "vendor/jquery/plugins/touchpunch/jquery.touchpunch":         ["jquery", "vendor/jquery/plugins/ui/jquery.ui"],
        "vendor/jquery/plugins/history/jquery.history":               ["jquery"],
        "vendor/jquery/plugins/hotkeys/jquery.hotkeys":               ["jquery"],
        "vendor/jquery/plugins/fileupload/jquery.fileupload":         ["jquery"],
        "vendor/jquery/plugins/shortcuts/jquery.shortcuts":           ["jquery"],
        "vendor/jquery/plugins/localesorter/jquery.localesorter":     ["jquery"],
        "vendor/jquery/plugins/polling/jquery.polling":               ["jquery"],
        "vendor/jquery/plugins/ellipsis2/jquery.ellipsis2":           ["jquery"],
        "vendor/jquery/plugins/logger/jquery.logger":                 ["jquery"],
        "bootstrap":                                                  ["jquery"],
        "vendor/bootstrap/plugins/select/bootstrap.select":           ["bootstrap"],
        "vendor/bootstrap/plugins/slider/bootstrap.slider":           ["bootstrap"],
        "vendor/bootstrap/plugins/colorpicker/bootstrap-colorpicker": ["bootstrap"],
        "vendor/transparency/transparency":                           ["jquery"],
        "ace/ext-language_tools":                                     { deps: ["ace/ace"] },
    },
    map: {
        "*": {
            css : "vendor/requirejs/plugins/require-css",
            text: "vendor/requirejs/plugins/text"
        }
    }
});

require.config({
   baseUrl: baseUrl,
   urlArgs: urlArgs
});



require(["jquery", "transparency"], function($, Transparency) {
    // configure transparency to only use data-bind as matcher key
    Transparency.matcher = function(element, key) {
        return element.el.getAttribute("data-bind") == key;
    };

    // use transparency as our rendering engine
    $.fn.render = Transparency.jQueryPlugin;
});

require([
    "vendor/jquery/plugins/ui/jquery.ui",
    "vendor/jquery/plugins/mobile/jquery.mobile",
    "vendor/jquery/plugins/touchpunch/jquery.touchpunch",
    "vendor/jquery/plugins/cookie/jquery.cookie",
    "vendor/jquery/plugins/history/jquery.history",
    "vendor/jquery/plugins/fileupload/jquery.fileupload",
    "vendor/bootstrap/plugins/select/bootstrap.select",
    "vendor/bootstrap/plugins/slider/bootstrap.slider",
    "vendor/bootstrap/plugins/colorpicker/bootstrap-colorpicker",
    "css!styles/icons.css",
    "css!vendor/jquery/plugins/ui/jquery.ui.css",
    "css!vendor/bootstrap/css/bootstrap.css",
    "css!vendor/bootstrap/plugins/select/bootstrap.select.css",
    "css!vendor/bootstrap/plugins/slider/bootstrap.slider.css",
    "css!vendor/bootstrap/plugins/colorpicker/bootstrap-colorpicker.css",
    "css!vendor/fuelux/css/fuelux.css",
    "css!vendor/fontawesome/css/font-awesome.css"
]);
