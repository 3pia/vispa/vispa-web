define([
  "jquery",
  "jclass",
  "vispa/mixins/ajax",
  "vispa/common/dialog",
  "css!styles/password.css"
], function(
  $,
  JClass,
  AjaxMixin,
  Dialog
) {

  var VispaPassword = JClass._extend($.extend({},
  AjaxMixin,
  {

    init: function(config, user, hash) {
      this.config = config;
      this.user   = user;
      this.hash   = hash;

      // start mixins
      AjaxMixin.init.call(this,
        this.config.url.dynamicBase,
        this.config.url.staticBase
      );

      if (this.user) { // ???
        var self = this;
        $(".form-password button[type='submit']").click(function(event) {
          event.preventDefault();
          self.submit();
        });

        $("#password1").focus();
      }
    },

    submit: function() {
      var self = this;

      var pass1 = $("#password1").val();
      if (!pass1) {
        $("#password1").focus();
        return this;
      }

      var pass2 = $("#password2").val();
      if (!pass2) {
        $("#password2").focus();
        return this;
      }

      if (pass1 != pass2) {
        window.dialog = Dialog.alert("Your passwords don't match!", {
          onClose: function() {
            $("#password1").focus();
          },
        });
        return;
      }

      this.POST("ajax/setpassword", {
        hash: this.hash,
        password: pass1
      }, function (err, data) {
        if (err) {
          var dialog = Dialog.alert(err.message, {
            onClose: function() {
              $("#password1").focus();
            },
          });
        } else {
          window.location.href = self.dynamicURL("/");
        }
      });
    }
  }));

  return VispaPassword;
});
