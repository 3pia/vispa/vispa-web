define([
  "jquery",
  "vue/vue",
  "vue/vue-strap",
  "vispa/vue/base",
  "vispa/vue/menu",
  "vispa/vue/slider",
  "vispa/vue/colorpicker",
  "vispa/workspace",
  "text!html/index/prefs/manipulator.html",
], function(
  $,
  Vue,
  VueStrap,
  VueBase,
  Menu,
  VueSlider,
  VueColorPicker,
  Workspace,
  template
){
  var addWorkspaceSetting = function() {
    var item = this.root.$parent.$parent; // menuItem->menuRoot->prefsManip->prefsItem
    var ws = this.data.ws;
    var def = item.get(ws);
    if (def===undefined)
      def = item.value;
    item.set(ws, def);
  };

  var PrefsManipulator = Vue.extend({
    template: template.trim(),
    props: ["data", "wsId"],
    ready: function() {
      if(this.$els.slider)
        $(this.$els.slider).slider();
    },
    computed: {
      path: {
        cached: false,
        get: function() {
          return this.$parent.path;
        },
      },
      def: function() {
        var r;
        if (this.isWs)
          r = this.$parent.get(this.wsId, false);
        if (r===undefined)
          r = this.$parent.get(null, false);
        if (r===undefined)
          r = this.data.value;
        return r;
      },
      type: function() {
        var type = this.data.type;
        if (type)
          type = type.toLowerCase();
        else
          type = "";
        if (type == "bool" || type=="boolean")
          return "bool";
        if (type == "color")
          return "color";
        if (this.data.range)
          return "range";
        if (this.data.options)
          return "options";
        return "text";
      },
      val: {
        get: function() {
          return this.$root.get(this.path, this.wsId, true);
        },
        set: function(val) {
          if (val === undefined)
            this.$root.del(this.path, this.wsId);
          switch(this.data.type) {
            case "float":
              val = parseFloat(val);
              break;
            case "int":
            case "integer":
              val = parseInt(val);
              break;
            case "bool":
            case "boolean":
              val = String(val).toLowerCase()=="true";
              break;
          }
          if (val === this.def && !this.isWs) // just keep the global config space clean
            this.$root.del(this.path, this.wsId);
          else
            this.$root.set(this.path, this.wsId, val);
        }
      },
      valDef: {
        get: function() {
          var r = this.val;
          if (r===undefined)
            r = this.def;
          return r;
        },
        set: function(val) {
          this.val = val;
        }
      },
      isWs: function() {
        return this.wsId !== null && !isNaN(this.wsId);
      },
      label: function() {
        return this.isWs ? Workspace._members.byId(this.wsId).name : "Global";
      },
      pattern: function() {
        if ((this.data.type||"").toLowerCase() == "shortcut" || this.data.callback) {
          return '\\s*(((mac|win|lin|linux):)?(key(up|down|press):)?((meta|ctrl|alt|shift)\\+)*\\w+\\s*(\\s+|$))*';
        }
      },
      optionsObj: function() {
        var d = this.data.options;
        var r = [];
        if (this.type=="bool") {
          if(!(d instanceof Array) || d.length<2) {
            d = ["Enabled", "Disabled"];
          }
          return [
            {value: true,  label:d[0]},
            {value: false, label:d[1]},
          ];
        }
        if (d instanceof Array) {
          for (var i=0; i<d.length; i++) {
            r.push({
              label: d[i].label || String(d[i]),
              value: d[i].value || d[i],
            });
          }
        } else {
          var k = Object.keys(d);
          k.sort();
          for (var i=0; i<k.length; i++) {
            r.push({value:k[i], label:d[k[i]]});
          }
        }
        return r;
      },
      wsAddItems: function() {
        var wsUsed = this.$root.wsList(this.path);
        var wsAll = Workspace._members._vueData.workspaces;
        var ret = {};
        var wsNames = [];
        for (var i in wsAll) {
          i = parseInt(i);
          if (isNaN(i)) continue;
          if (~wsUsed.indexOf(i)) continue;
          var wsName = wsAll[i].config.name;
          wsNames.push(wsName);
          ret["ws"+i] = {
            label: wsName,
            callback: addWorkspaceSetting,
            ws: i,
          };
        }
        wsNames.sort();
        // jshint shadow:true
        for (var i in ret)
          ret[i].position = wsNames.indexOf(ret[i].label);
        return ret;
      },
    },
    methods: {
      reset: function() {
        this.val = this.def;
      },
      remove: function() {
        this.val = undefined;
      },
    },
    components: {
      "menu-button"   : Menu.button,
      "vs-select"     : VueStrap.select,
      "vs-check-group": VueStrap.buttonGroup,
      "vs-check-btn"  : VueStrap.checkbox,
      "vs-radio-group": VueStrap.buttonGroup,
      "vs-radio-btn"  : VueStrap.radio,
      "vs-slider"     : VueSlider,
      "vs-colorpicker": VueColorPicker,
    },
  });

  return PrefsManipulator;
});
