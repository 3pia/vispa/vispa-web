define([
  "jquery",
  "vue/vue",
  "vispa/vue/base",
  "vispa/prefs/group",
  "text!html/index/prefs/category.html",
], function(
  $,
  Vue,
  VueBase,
  PrefsGroup,
  template
){
  var PrefsCategory = Vue.extend({
    template: template.trim(),
    props: ["data", "key"],
    computed: {
      path: function() {
        return this.key;
      },
      justOne: function() {
        return Object.keys(this.items).length<2;
      }
    },
    mixins: [
      VueBase.compProxy("data", [
        "items",
        "label",
      ]),
      VueBase.itemsAcc,
    ],
    components: {
      "prefs-group": PrefsGroup,
    }
  });

  return PrefsCategory;
});
