define([
  "jquery",
  "vue/vue",
  "vispa/vue/base",
  "vispa/prefs/item",
  "text!html/index/prefs/group.html",
], function(
  $,
  Vue,
  VueBase,
  PrefsItem,
  template
){
  var PrefsGroup = Vue.extend({
    template: template.trim(),
    props: ["data", "key"],
    mixins: [
      VueBase.compProxy("data", [
        "items",
        "label",
        "iconClass",
        "description",
      ]),
      VueBase.itemsAcc,
    ],
    computed: {
      path: function() {
        return this.$parent.path+"."+this.key;
      },
      showLevel: function() {
        return this.$root.showLevel;
      },
      hasContent: function() {
        for (var i in this.items) {
          if ((this.items[i].level||0) <= this.showLevel)
            return true;
        }
        return false;
      },
    },
    methods: {
      watch: function(ws, callback) {
        return this.$watch(function(){
          return this.$root.get(this.path, ws, undefined);
        }, callback, {deep: true});
      },
      autoShortcuts: function(ws) {
        var sc = {};
        for(var i in this.$refs.items) {
          var item = this.$refs.items[i];
          if (item.type == "shortcut" && !item.noAutoApply && item.callback) {
            var k = item.get(ws);
            if (k) {
              sc[k] = item.callback;
            }
          }
        }
        return sc;
      },
      watchAutoShortcuts: function(ws, callback) {
        return this.$watch(function(){
          return this.autoShortcuts(ws);
        }, callback, {deep: true});
      },
    },
    components: {
      "prefs-item": PrefsItem,
    }
  });

  return PrefsGroup;
});
