define([
  "jquery",
  "vue/vue",
  "vue/vue-strap",
  "vispa/vue/base",
  "vispa/vue/nav",
  "vispa/workspace",
  "vispa/prefs/category",
  "vispa/utils",
  "text!html/index/prefs/manager.html",
], function(
  $,
  Vue,
  VueStrap,
  VueBase,
  Nav,
  Workspace,
  PrefsCategory,
  Utils,
  template
){
  var PrefsManager = Vue.extend({
    template: template.trim(),
    // props: [],
    created: function() {
      this.$mount();

      $(this.$els.content).scrollspy({
        target: "#"+(this.$els.nav.id = "prefsNav_"+Utils.uuid(10))
      });
    },
    data: function() {return {
      items: {
        core: {
          label: "VISPA Core",
          items: {},
        },
      },
      data: {
        user: vispa.jsondata.syncObj("preferences"),
      },
      showLevel: 1,
    };},
    computed: {
      isDevMode: function() {
        return !!vispa.args.global.devMode;
      },
    },
    methods: {
      // global managment functions
      addCategory: function(category, label) {
        if (this.items[category]) {
          this.items[category].label = label;
        } else {
          VueBase.runSync(function(){
            Vue.set(this.items, category, {
              label: label,
              items: {},
            });
          }, this);
        }
      },
      addGroup: function(category, group, config) {
        if (!this.items[category]) {
          throw new Error("Preferences category "+category+" does not exists.")
        }
        if (this.items[category].items[group]) {
          throw new Error("Preferences for "+category+":"+group+" are already registered");
        }
        // needs to be syncronousbecause following calls need the vue instances the data implies
        VueBase.runSync(function(){
          Vue.set(this.items[category].items, group, config);
        }, this);
      },
      getGroup: function(category, group) {
        return this.$refs.items[category].$refs.items[group];
      },
      // prefs value interaction functions
      get: function(path, ws, user) {
        ws = Workspace._members.getId(ws, null);
        if (user === undefined) {
          // user settings come first
          var r;
          if (ws>=0 && ((r = this.get(path,   ws, true)) !==undefined)) return r;
          if (         ((r = this.get(path, null, true)) !==undefined)) return r;
          // now seversettings
          if (ws>=0 && ((r = this.get(path,   ws, false))!==undefined)) return r;
          if (         ((r = this.get(path, null, false))!==undefined)) return r;
          // extension default has to be folded in at a later point in time
        } else {
          var d = this.data;
          d = user ? d.user : d.server;
          if (!d) return;
          d = d[ws];
          if (!d) return;
          // this internally uses expressions
          try {
            return Vue.parsers.path.getPath(d, path);
          } catch (e){}
        }
      },
      set: function(path, ws, value) {
        ws = Workspace._members.getId(ws, null);
        ws = ws===null ? ".null." : "["+ws+"].";
        this.$set("data.user"+ws+path, value);
      },
      del: function(path, ws) {
        this.set(path, ws, undefined);
      },
      // these are helpers
      wsList: function(path) {
        var ks = [];
        for(var i in this.data.user) {
          if (i ===  null)  continue;
          if (i === "null") continue;
          i = parseInt(i);
          if (isNaN(i))     continue;
          if (i < 0)        continue;
          if (this.get(path, i, true)!==undefined)
            ks.push(i);
        }
        for(var i in (this.data.server || {})) {
          if (i ===  null)  continue;
          if (i === "null") continue;
          i = parseInt(i);
          if (isNaN(i))     continue;
          if (i < 0)        continue;
          if (~ks.indexOf(i)) continue;
          if (this.get(path, i, false)!==undefined)
            ks.push(i);
        }
        ks.sort();
        ks.unshift(null);
        return ks;
      },
    },
    watch: {
      "data.user": {
        deep: true,
        handler: function() {
          var obj = vispa.jsondata.syncObj("preferences", this.data.user, true);
          if (this.data.user !== obj) {
            this.data.user = obj;
          }
        },
      },
    },
    components: {
      "prefs-category": PrefsCategory,
      "vs-radio-group": VueStrap.buttonGroup,
      "vs-radio-btn": VueStrap.radio,
    },
  });

  return PrefsManager;
});
