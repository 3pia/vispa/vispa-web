define([
  "jquery",
  "vue/vue",
  "vispa/vue/base",
  "vispa/prefs/manipulator",
  "text!html/index/prefs/item.html",
], function(
  $,
  Vue,
  VueBase,
  PrefsManipulator,
  template
){
  var PrefsItem = Vue.extend({
    template: template.trim(),
    props: ["data", "key"],
    computed: {
      path: function() {
        return this.$parent.path + "." + this.key;
      },
      wsList: function() {
        return this.$root.wsList(this.path);
      },
    },
    methods: {
      get: function(ws, user) {
        var r = this.$root.get(this.path, ws, user);
        if (r===undefined && user===undefined)
          return this.value;
        else
          return r;
      },
      set: function(ws, value) {
        this.$root.set(this.path, ws, value);
      },
      del: function(ws) {
        this.$root.del(this.path, ws);
      },
      watch: function(ws, callback) {
        // jshint shadow:true
        var ws = ws;
        return this.$watch(function(){
          return this.get(ws);
        }, callback);
      },
    },
    mixins: [
      VueBase.compProxy("data", [
        "type",
        "position",
        "level", // 0, everywhere,
        "label",
        "description",
        "check", // a function for type checking
        "range", // the range description (min, max[, step=1.0])
        "options", // list of options
        "noAutoApply", // do not autoapply this shortcut
        "global", // not specific for workspaces
        "value", // default value
        "callback", // the callback for a shortcut
      ]),
    ],
    components: {
      "prefs-manipulator": PrefsManipulator,
    }
  });

  return PrefsItem;
});
