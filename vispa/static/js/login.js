define([
  "jquery",
  "jclass",
  "vispa/mixins/ajax",
  "css!styles/login.css"
], function(
  $,
  JClass,
  AjaxMixin
) {

  var VispaLogin = JClass._extend($.extend({},
  AjaxMixin,
  {

    init: function(config) {
      var self = this;
      this.config = config;

      // start mixins
      AjaxMixin.init.call(this,
        this.config.url.dynamicBase,
        this.config.url.staticBase
      );

      // setup tabs
      $("#tabs li > a").click(function(event) {
        $(this).tab("show");
      });

      // catch submit actions
      $.each(["login", "register", "forgot"], function(i, fn) {
        $(".form-" + fn + " button[type='submit']").click(function(event) {
          event.preventDefault();
          self[fn]();
        });
      });

      // connect events
      $("a[data-toggle='tab']").on("shown.bs.tab", function(event) {
        var target = $(this).attr("href").split("-")[0] + "-name";
        $(target).focus();
      });

      // initially, fire a shown.bs.tab event to catch hashtags
      var hash = window.location.hash;
      if (hash && ~["#login-pane", "#register-pane", "#forgot-pane"].indexOf(hash)) {
        $("a[href='" + hash + "']").tab("show");
      }
    },

    login: function() {
      var self = this;

      var user = $("#login-name").val();
      if (!user) {
        $("#login-name").focus();
        return this;
      }

      var pass = $("#login-password").val();
      if (!pass) {
        $("#login-password").focus();
        return this;
      }

      this.POST("ajax/login", {username: user, password: pass}, function(err, res) {
        if (err) {
          $("#login-alert").fadeIn(100).render({alert: err.message});
          $("#login-name").focus();
        } else {
          window.location.href = self.dynamicURL(location.search);
        }
      });
    },

    register: function() {
      var self = this;

      var user = $("#register-name").val();
      if(!user) {
        $("#register-name").focus();
        return this;
      }

      var email = $("#register-email").val();
      if (!email) {
        $("#register-email").focus();
        return this;
      }

      this.POST("ajax/register", {username: user, email: email}, function(err, res) {
        if (err) {
          $("#register-alert").fadeIn(100).render({alert: err.message});
          $("#register-name").focus();
        } else {
          if (res.hash) {
            setTimeout(function() {
              window.location.href = self.dynamicURL("password/" + res.hash);
            }, 1000);
          } else {
            $("#register-success").fadeIn(100);
            setTimeout(function() {
              window.location.href = self.dynamicURL("/");
            }, 4000);
          }
        }
      });
    },

    forgot: function() {
      var self = this;

      if (!this.config.useForgot) {
        return this;
      }

      var user = $("#forgot-name").val();
      if (!user) {
        $("#forgot-name").focus();
        return this;
      }

      this.POST("ajax/forgotpassword", {username: user}, function(err, res) {
        if (err) {
          $("#forgot-alert").fadeIn(100).render({alert: err.message});
          $("#forgot-name").focus();
        } else {
          $("#forgot-success").fadeIn(100);
          setTimeout(function() {
            window.location.href = self.url.dynamic("/");
          }, 4000);
        }
      });
    }
  }));

  return VispaLogin;
});
