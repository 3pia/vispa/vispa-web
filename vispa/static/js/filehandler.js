/**
 * filehandler
 * =================
 */
define([
  "jclass",
  "vispa/utils",
  "vispa/vue/menu",
], function(
  JClass,
  Utils,
  Menu
) {

  var funcEval = function(obj, prop) {
    var val = obj[prop];
    if (typeof val === "function")
      return val.call(obj);
    else
      return val;
  }

  /**
   * .. js:class:: FileHandler()
   *
   *    The file handler for vispa. One can add :js:class:`MenuItem` like objects, which can be
   *    used within a menu (like headless) or directly. :js:func:`getFileHandlers` then returns all
   *    registered file handlers with the property *value* set to the filename, which is given to
   *    the function as parameter. The handlers themself should update themself via the filename.
   *    Classes deriving from :js:class:`BaseView` can set the member property *fileHandlers*,
   *    which must be a list of :js:class:`MenuItem` like objects. These are automatically added to
   *    the file handler. Additionally. :js:class:`BaseView` adds :js:func:`spawn` to the handler
   *    so that inside :js:func:`callback` *this.spawn* can be used to easily spawn an instance.
   */

  var FileHandler = JClass._extend({

    init: function init() {
      init._super.call(this, arguments);

      Utils.setROP(this, "_menu", new Menu.headless({
        data: {
          items: {}
        },
      }));
      this._menu.$mount();
    },

    /** indent: 1
     * .. js:function:: addFileHandler(key, handler)
     *
     *    Add a single file handler with key.
     */
    addFileHandler: function (key, handler) {
      handler.value = handler.value || "";
      this._menu.set(key, handler);
    },

    /** indent: 1
     * .. js:function:: addFileHandlers(handlers)
     *
     *    Add a dict of file handlers.
     */
    addFileHandlers: function (handlers) {
      $.each(handlers, this.addFileHandler);
    },

    /** indent: 1
     * .. js:function:: getFileHandlers(file)
     *
     *    Get all file handlers. Each is updated previously with *handler.value = file*.
     */
    getFileHandlers: function (file) {
      var handlers = this._menu.$refs.items;
      for(var i in handlers) {
        handlers[i].$set("value", file);
      }
      return this._menu.items;
    },

    /** indent: 1
     * .. js:function:: getDefaultFileHandler(file)
     *
     *    Get the file handler with the highest position or *null* if it has no callback, e.g. for
     *    a divider or a header.
     */
    getDefaultFileHandler: function (file) {
      this.getFileHandlers(file);
      var handlers = this._menu.$refs.items;
      handlers = Object.keys(handlers).map(function(key){return handlers[key]});
      handlers = handlers.filter(function(handler){
        return !handler.hidden;
      });
      handlers.sort(function (a, b) {
        return a.position - b.position;
      });
      var defaultFileHandler = handlers[0];
      return defaultFileHandler.callback ? defaultFileHandler : null;
    },

    /** indent: 1
     * .. js:function:: runDefaultHandler(file, view)
     *
     *    Runs the default file handler if available for the given view.
     *
     *    :param String file: The file to run the handler for.
     *    :param BaseView view: The view that intends to open the file.
     */

    runDefaultHandler: function(file, view) {
      var handler = this.getDefaultFileHandler(file);
      if(!handler.callback) return;
      handler.callback.call({$root: {view: view}, value: file});
    },

  });

  return FileHandler;
});
