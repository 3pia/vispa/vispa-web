/**
 * Workspace
 * =========
 */

define([
  "jquery",
  "emitter",
  "async",
  "vispa/utils",
  "vispa/mixins/logger",
  "vispa/mixins/preferences",
  "vispa/common/dialog",
  "vue/vue",
], function($, Emitter, async, Utils, LoggerMixin, PreferencesMixin, Dialog, Vue) {

  /**
   * .. js:class:: Workspace(config)
   *
   *    :extends: Emitter
   *    :mixins: LoggerInferface
   *
   *    The abstract representation of a workspace and it's functions.
   *
   *    .. warning::
   *       This class is not to be instanciated manually. Instead use its class-members functions.
   *
   *    :param Object config: The configuration of the workspace.
   */
  var Workspace = Emitter._extend($.extend({},
    LoggerMixin,
  {

    init: function init(config) {
      init._super.call(this);

      // is this a duplcate
      if (this._class._members._workspaces[this.id]) {
        throw new Error("this workspace id is already in use");
      }

      // set static config
      /** indent: 1
       * .. js:attribute:: id read-only
       *
       *    :type: Number
       *
       *    The id of the workspace.
       */
      Utils.setROP(this, "id", config.id);

      // start mixins
      LoggerMixin.init.call(this, "workspace."+this.id);

      // register with members
      this._class._members._workspaces[this.id] = this;

      // own data holders
      this._connectCallbacks    = [];
      this._disconnectCallbacks = [];
      this._instances           = [];

      // initial state
      this._vueData = {
        config      : $.extend({}, config),
        serverConfig: $.extend({}, config),
        state: "",
        id   : this.id,
      };
      Vue.set(this._class._members._vueData.workspaces, this.id, this._vueData);

      this.on("stateChanged", function() {
        if (this._passwordDialog) {
          this._passwordDialog.abort();
        }
        if (this.state === "connected") {
          var m = this._class._members;
          if (m.current === null)
            m.current = this.id;
        }
      });

      this._setState(config.connection_status || "disconnected");
      // create listeners
      var connectWithPass = function(type) {
        this.emit("disconnected");

        this.logger.debug(type + " required");

        this._passwordDialog = Dialog.prompt("", function(result) {
          this._passwordDialog = undefined;
          if (result === null) return;
          this._connect(result);
        }.bind(this), {
          title   : "Enter workspace " + type + " for " + this.name,
          password: true,
          okLabel : "Connect",
        });
      };

      this.on("password_required",   connectWithPass.bind(this, "password"));
      this.on("passphrase_required", connectWithPass.bind(this, "SSH key passphrase"));

      this.on("connecting", function() {
        this._setState("connecting");
      });

      this.on("connected", function() {
        this._setState("connected");
        this._finishConnect(null);
      });

      this.on("disconnecting", function() {
        this._setState("disconnecting");
      });

      this.on("disconnected", function() {
        this._setState("disconnected");
        this._instances.map(function (inst) {
          inst._warnDisconnect();
        });
        this._finishDisconnect(null);
      });

      // autoconnect ?
      if (config.auto_connect)
        this.connect();
    },

    /** indent: 1
     * .. js:attribute:: name read-only
     *
     *    :type: String
     *
     *    The name of the workspace.
     */
    name: {
      descriptor: true,
      get: function() {
        return this._class._members._vueData.workspaces[this.id].serverConfig.name;
      }
    },

    /** indent: 1
     * .. js:attribute:: state read-only
     *
     *    :type: String
     *
     *    The connectivity state of the workspace. Possible values are:
     *    - disconnected
     *    - requestedConnect
     *    - connecting
     *    - connected
     *    - requestedDisonnect
     *    - disconnecting
     *
     *    :emits stateChanged: If this value changed.
     */
    state: {
      descriptor: true,
      get: function() {
        return this._state;
      }
    },

    /** indent: 1
     * .. js:attribute:: color
     *
     *    :type: String
     *
     *    The assigned color of this workspace.
     */
    color: {
       descriptor: true,
       get: function() {
         return this._class._members.prefs.vue.get("color").get(this.id);
       },
       set: function(newColor) {
         this._class._members.prefs.vue.get("color").set(this.id, newColor);
       },
     },

    /* indent: 1
     * .. js:function:: _setState(state, onlyIfOldStateIn)
     *
     *    Sets the state to a new value. Also updates the icons.
     *
     *    :param String state: The new state to be set to.
     *    :param Array(String)|String onlyIfOldStateIn: The state will be changed only if the old
     *    	state is in this array.
     *      If it is a string it is interpreded as if it was an *Array* containing this string.
     *      If it is not an *Array* the condition is ignored.
     *
     *    :throws Error: If attempting to go to a undefined state.
     *
     *    :emits stateChanged: When the state has changed. This is to be used to update additional
     *      renderings of the workspaces state (like in the sidebar view workspace selector
     *      dropdowns)
     */
    _setState: function(state, onlyIfOldStateIn) {
      if (typeof onlyIfOldStateIn == "string" && onlyIfOldStateIn != this._state) {
        return;
      }
      if (onlyIfOldStateIn instanceof Array && !~onlyIfOldStateIn.indexOf(this._state)) {
        return;
      }

      this.logger.debug(state);

      var changed = this._state != state;

      this._state         = state;
      this._vueData.state = state;

      if (changed)
        this.emit("stateChanged");
    },

    /** indent: 1
     * .. js:function:: connect([callback[, password]])
     *
     *    Connect the workspace.
     *
     *    :callback String|null err: Will contain the error description or *null* if the connect
     *      was successfull. Consider using the *connected* event instead.
     *    :param String password: May contain the password/SSH passphrase used for connecting.
     */
    connect: function(callback, password) {
      if (this._state == "connected") {
        if ($.isFunction(callback)) callback(null);
        return;
      }

      if ($.isFunction(callback)) {
        this._connectCallbacks.push(callback);
      } else if (~["requestedDisconnect", "disconnecting"].indexOf(this._state) &&
        !this._connectCallbacks.length) {
        // make sure that we have at least one connect callback as a marker for a request connect
        // during disconnecting
        this._connectCallbacks.push(function(){});
      }

      if (this._state == "disconnected" && !this._passwordDialog) {
        this._connect(password || null);
      }
    },

    _connect: function(password) {
      var self = this;
      this._setState("requestedConnect", "disconnected");
      vispa.POST("ajax/connectworkspace", {
        wid     : this.id,
        password: password
      }, function(err) {
        if (!err) return;
        self._setState("disconnected", "requestedConnect");
        self._finishConnect(err);
      });
    },

    _finishConnect: function (err) {
      for(var i = 0; i < this._connectCallbacks.length; i++) {
        this._connectCallbacks[i](err);
        // only the first registered callback will recieve the error information
        if (err) {
          break;
        }
      }
      this._connectCallbacks.length = 0;

      // start queued up disconnect
      if (this._disconnectCallbacks.length) {
        this.disconnect();
      }
    },

    /** indent: 1
     * .. js:function:: disconnect([callback])
     *
     *    Disconnects the workspace.
     *
     *    :callback String|null err: Will contain the error description or *null* if the disconnect
     *      was successfull. Consider using the *disconnected* event instead.
     */
    disconnect: function(callback) {
      var self = this;

      if (this._state == "disconnected") {
        if (this._passwordDialog) { // abort connecting with password
          this._passwordDialog.abort();
        }
        if ($.isFunction(callback)) callback(null);
        return;
      }

      if ($.isFunction(callback)) {
        this._disconnectCallbacks.push(callback);
      } else if (~["requestedConnect", "connecting"].indexOf(this._state) &&
        !this._disconnectCallbacks.length) {
        // make sure that we have at least one disconnect callback as a marker for a request
        // disconnect during connecting
        this._disconnectCallbacks.push(function(){});
      }

      if (this._state == "connected" && !this._passwordDialog) {
        // TODO: close all instances (wait before actually disconnecting)
        vispa.POST("ajax/disconnectworkspace", {
          wid: this.id
        }, function(err) {
          // there used to be a manual disconnect event triggering since there would be no event
          // when we were already disconnected, we shouldn't need this anymore
          if (!err) return;
          self._setState("connected", "requestedDisconnect");
          self._finishDisconnect(err);
        });
      }
    },

    _finishDisconnect: function (err) {
      for(var i = 0; i < this._disconnectCallbacks.length; i++) {
        this._disconnectCallbacks[i](err);
        // only the first registered callback will recieve the error information
        if (err) {
          break;
        }
      }
      this._disconnectCallbacks.length = 0;

      // start queued up connect
      if (this._connectCallbacks.length) {
        this.connect();
      }
    },

    /** indent: 1
     * .. js:function:: edit(newConfig[, callback[, connect]])
     *
     *    Edit a current workspace. Will be disconnected if it is not already.
     *
     *    :param Object newConfig: The new config for the workspace.
     *    :param String newConfig.name: The new workspace name.
     *    :param String newConfig.host: The new host name.
     *    :param String newConfig.login: The new login user name.
     *    :param String newConfig.key: The new private key used for authentification.
     *    :param String newConfig.command: The new command.
     *    :param Bool connect: Connect when edit has finished.
     *
     *    :callback String|null err: Will contain the error description or *null*.
     */
    edit: function(newConfig, callback, connect) {
      if (this._state != "disconnected") {
        this.disconnect(this.edit.bind(this, newConfig, callback, true));
      }

      var self = this;
      callback = Utils.callback(callback);
      vispa.POST("ajax/editworkspace", {
        name   : newConfig.name,
        host   : newConfig.host,
        login  : newConfig.login,
        key    : newConfig.key,
        command: newConfig.command,
        wid    : this.id,
      }, function(err, config){
        if (err) {
          callback(err);
          return;
        }

        // not setting .config since this is the currently edited on,
        // it should line up with the save one if everything went ok
        self._vueData.serverConfig = $.extend({}, config);
        // overwrite key since server will report truncated version for safety
        self._vueData.config.key = self._vueData.serverConfig.key;
        if (connect) {
          self.connect(callback);
        } else {
          callback(null);
        }
      });
    },

    /** indent: 1
     * .. js:function:: remove([callback])
     *
     *   Removes the workspace. A success implies the destruction of the instance and it must not
     *   be used thereafter.
     *
     *    :callback String|null err: Will contain the error description or *null* if the remove was
     *      successfull
     */
    remove: function(callback) {

      if (this._state != "disconnected") {
        this.disconnect(this.remove.bind(this, callback));
      }

      callback = Utils.callback(callback);
      // tell the server
      vispa.POST("ajax/deleteworkspace", {
        wid: this.id
      }, function(err) {
        if (err) {
          return callback(err);
        }

        delete this._class._members._workspaces[this.id];
        this._class._members._vueData.workspaces[this.id] = undefined;
        delete this._class._members._vueData.workspaces[this.id];

        this.logger.info("removed");
        this.emit("removed");
        callback(null);
      }.bind(this));
    },

    // used by the LinkMixin
    _registerInstance: function(instance) {
      if (!$.isFunction(instance.close)) return;
      if (~this._instances.indexOf(instance)) return;

      this._instances.push(instance);
    },

    // used by the LinkMixin
    _unregisterInstance: function(instance) {
      var index = this._instances.indexOf(instance);
      if (!~index) return;

      this._instances.splice(index, 1);
    },

    /** indent: 1
     * .. js:function:: closeAllInstances()
     *
     *    Forces all known open instances to be closed. This is syncronus.
     */
    closeAllInstances: function() {
      // loop backwards since the closing of an instance should remove the entry from the array
      // thus shorten it
      for(var i = this._instances.length; i >= 0; i--) {
        this._instance[i].close(true);
      }
    },
  }),$.extend({},
  PreferencesMixin,
  {
    init: function() {
      // these are used by vue rendering, they must be available from the start
      this._vueData = {
        workspaces: {},
        current: null,
      };
    },

    /** indent: 1
     * .. js:function:: initPrefs()
     *
     *    Registers the preferences with *vispa.preferences*.
     */
    initPrefs: function() {
      PreferencesMixin.init.call(this, "core", "workspace", {
        label: "Workspaces",
        iconClass: "fa-server",
        items: {
          colorStyle: {
            position: 2,
            level: 1,
            label: "Workspace color style",
            description: "How a workspaces color is presented in a tab of a workspace.",
            type: "string",
            value: "none",
            options: ["none","background","border"],
            global: true,
            flat: true,
          },
          colorEvenSingle: {
            position: 3,
            level: 1,
            label: "Color even single workspace",
            description: "Apply color even when only a single workspace is used.",
            type: "boolean",
            value: false,
            global: true,
          },
          otherStyle: {
            position: 4,
            level: 1,
            label: "Other workspace style",
            description: "The style of tabs that have a different workspace from the currently" +
              " focused tab.",
            type: "string",
            value: "none",
            options: ["none","darkened","collapse"],
            global: true,
            flat: true,
          },
          color: {
            position: 1,
            level: 1,
            label: "Workspace color",
            description: "The assigned color for a workspace.",
            type: "color",
            value: "white",
          },
        },
      });
    },

    /** indent: 1
     * .. js:function:: load(callback) class-member
     *
     *    Loades the workspaces from the DB and registers for the socket event redistribution.
     *
     *    :callback String|null err: *null* on success else the error description.
     */
    load: function(callback) {
      // prevent multiple starts
      if (this._workspaces) return;
      this._workspaces = {};

      // setup listeners
      var genericListener = function(topic, data) {
        var workspace = this.byId(data.workspaceId);
        if (workspace) {
          workspace.emit(topic);
        }

        // re-emit certain topics
        if (~["connected", "disconnected"].indexOf(topic)) {
          vispa.callbacks.emit(topic + "Workspace", data.workspaceId);
        }
      };
      var topics = ["password_required", "passphrase_required", "connecting", "connected",
                    "disconnecting", "disconnected"];

      for(var i = 0; i < topics.length; i++) {
        vispa.socket.on("workspace." + topics[i], genericListener.bind(this, topics[i]));
      }

      callback = Utils.callback(callback);
      // initially, get workspace data, listen to topics, setup the gui
      vispa.GET("ajax/getworkspacedata", function(err, workspaceData) {
        if (err) return callback(err);

        for (var i in workspaceData) {
          new Workspace(workspaceData[i]);
        }

        callback(null);
      });

    },

    /** indent: 1
     * .. js:function:: getId(workspace, fallback) class-member
     *
     *    Retrieves the workspace id if possible or return the fallback value.
     *
     *    :param Workspace|Number workspace: A Workspace instance or a valud workspace id.
     *    :param any fallback: The fallback value.
     *    :return: The workspace if available or the fallback value.
     *    :rtype: Number
     */
    getId: function(workspace, fallback) {
      if (workspace instanceof Workspace)
        return workspace.id;

      var id = parseInt(workspace);
      if (isNaN(id) || id<0)
        return fallback;

      if (this._workspaces[id])
        return id;

      return fallback;
    },

    /** indent: 1
     * .. js:function:: byId(id) class-member
     *
     *    Retrieves the workspace by id.
     *
     *    :return: the workspace or *null* if there no workspace for the id given
     *    :rtype: Workspace
     */
    byId: function(id) {
      id = parseInt(id);

      return isNaN(id) ? null : (this._workspaces[id] || null);
    },

    /** indent: 1
     * .. js:function:: byName(name) class-member
     *
     *    Retrieves the first workspace with the given name.
     *
     *    :return: the workspace or *null* if there no such workspace
     *    :rtype: Workspace
     */
    byName: function(name) {
      for (var i in this.workspaces) {
        if (this._workspaces[i].name == name) {
          return this.workspaces[i];
        }
      }
      return null;
    },

    /** indent: 1
     * .. js:attribute:: current
     *
     *    :type: Workspace|null
     *
     *    The current workspace. This is the workspace new vies might be bound to - it is not the
     *    workspace of the currently active view/thread.
     */
    current: {
      descriptor: true,
      get: function() {
        return this._workspaces[this._vueData.current] || null;
      },
      set: function(newWorkspace) {
        this._vueData.current = this.getId(newWorkspace, null);
      },
    },

    /** indent: 1
     * .. js:attribute:: workspaces read-only class-member
     *
     *    :type: Ojbect(id => Workspace)
     *
     *    The workspace directory (id => workspace). Safe against alteration.
     */
    workspaces: {
       descriptor: true,
       get: function() {
         return $.extend({}, this._workspaces);
       }
     },

    /** indent: 1
     * .. js:attribute:: workspaceCount
     *
     *    :type: Number
     *
     *    The number of workspaces.
     */
    workspaceCount: {
      descriptor: true,
      get: function() {
        return Object.keys(this._workspaces).length;
      }
    },

    /** indent: 1
     * .. js:attribute:: activeWorkspaceCount
     *    :type: Number
     *
     *    The number of connected (more precisely: not disconnected) workspaces.
     */
    activeWorkspaceCount: {
      descriptor: true,
      get: function() {
        var n = 0, ws = this._vueData.workspaces;
        for (var i in ws)
          n += (ws[i].state === "disconnected") ? 0 : 1;
        return n;
      },
    },

    /** indent: 1
     * .. js:attribute:: useColor
     *    :type: Bool
     *
     *    Whether the workspaces should use the coloring.
     */
    useColor: {
      descriptor: true,
      get: function() {
        return !!this.prefs.get("colorEvenSingle") || this.activeWorkspaceCount>1;
      },
    },

    disconnectAll: function(callback) {
      callback = Utils.callback(callback);

      var dcTasks = [];
      for(var i in this._workspaces) {
        var ws = this._workspaces[i];
        if(ws.state == "connected")
          dcTasks.push(ws.disconnect.bind(ws));
      }

      if (dcTasks.length)
        async.parallel(dcTasks, callback);
      else
        callback(null);
    },

    /** indent: 1
     * .. js:function:: createNew(config[, callback]) class-member
     *
     *    Create a new workspace and saves it.
     *
     *    :param Object config: A plain object holding the informations for the new Workspace.
     *      See :js:class:`Workspace` constructor argument.
     *    :callback String|null err: The error information or *null* if is was a sucess.
     *    :callback Workspace workspace: The newly created workspace.
     */
    createNew: function(config, callback) {
      callback = $.isFunction(callback) ? callback : function(){};

      vispa.POST("ajax/addworkspace", config, function(err, data) {
        if (err) {
          return callback(err);
        }

        callback(null, new Workspace(data));
      });
    },

  }));

  return Workspace;
});
