/**
 * vue/menu
 * =================
 */
define([
  "jquery",
  "vue/vue",
  "vispa/vue/base",
  "text!html/index/vue/menu-item.html",
  "text!html/index/vue/menu-root.html",
  "text!html/index/vue/menu-button.html",
  "text!html/index/vue/menu-context.html",
], function(
  $,
  Vue,
  VueBase,
  Titem,
  Troot,
  Tbutton,
  Tcontext
){
  Vue.partial("menu-root", Troot.trim());

  /**
   * .. js:class:: MenuItem()
   *
   *    :extends: Vue
   *    :mixins: VueBase.itemsAcc
   *    :param config object: The config using the same options as can be provided to
   *    	sub-:js:class:`MenuItem`s via :js:attr:`items`.
   *
   *    An menu item. This class should not be instanciated directly.
   *
   *    .. js:attribute:: header
   *
   *       :type: Boolean
   *
   *       Whether this item is a header. Default is *false*. If it is *true* :js:attr:`label`
   *       and :js:attr:`iconClass` will be used for its contnet.
   *
   *    .. js:attribute:: divider
   *
   *       :type: Boolean
   *
   *       Whether this item is a divider. Default is *false*.
   *
   *    .. js:attribute:: disabled
   *
   *       :type: Boolean
   *
   *       Whether this entry should be disabled. Default is *false*.
   *
   *    .. js:attribute:: label
   *
   *       :type: String
   *
   *       The label of the entry. Must be provided.
   *
   *    .. js:attribute:: iconClass
   *
   *       :type: String (icon class-name)
   *
   *       The class-name for the icon to use (if any). Default is *""*.
   *
   *    .. js:attribute:: auxClass
   *
   *       :type: String (class-names)
   *
   *       Auxiliary class names for the menu item for additional styling.
   *
   *    .. js:attribute:: position
   *
   *       :type: Number
   *
   *       Defines the ordering of the entries. They will be sorted in ascending order.
   *
   *    .. js:attribute:: hidden
   *
   *       :type: Boolean
   *
   *       Whether this item is hidden (not shown at all).
   *
   *    .. js:attribute:: callback
   *
   *       :type: Function
   *
   *       This function will be called with *this* referring to the :js:class:`MenuItem`. If it
   *       returns *true* the menu will be kept open.
   *
   *    .. js:attribute:: items
   *
   *       :type: Object (key => config)
   *
   *       Contains configuration of subitems by key. All the above mentioned attributes can be
   *       included in a config.
   *
   *    .. js:attribute:: data
   *
   *       :type: Object
   *
   *       The original data object, this way auxiliary data provided can be accessed.
   *
   *    .. js:attribute:: root
   *
   *       :type: MenuButton
   *
   *       The root instance containing the menu.
   */
  var MenuItem = Vue.extend({
    template: Titem.trim(),
    props: ["data", "key"],
    created: function(){
      if(this.items===undefined)
        this.items = {};
      if(this.data.position === undefined)
        this.position = 0;
    },
    computed: {
      class: function() {
        if (this.data.header)  return "dropdown-header";
        if (this.data.divider) return "divider";
        var c = [];
        this.disabled && c.push("disabled");
        this.selected && c.push("selected");
        this.hasSubmenu && c.push("dropdown-submenu");
        this.auxClass && c.push(this.auxClass);
        return c.join(" ");
      },
      hasSubmenu: function() {
        return !!(this.items && Object.keys(this.items).length);
      },
      root: {
        cached: false,
        get: function() {
          return this.$parent ? this.$parent.root : undefined;
        },
      },
      selected: {
        get: function() {
          switch (this.selectable) {
            case true:
              var v = this.$parent.value;
              return Vue.util.isArray(v) ? !!~v.indexOf(this.value) : undefined;
            case false:
              return this.$parent.value === this.value;
            default:
              return this.data.selected;
          }
        },
        set: function(sel) {
          sel = !!sel;
          switch (this.value!==undefined ? this.selectable : undefined) {
            case true:
              if (!Vue.util.isArray(this.$parent.value)) {
                this.$parent.value = [];
              }
              var val = this.value;
              var arr = this.$parent.value;
              var idx = arr.indexOf(val);
              if (sel != !!~idx) {
                if (sel) {
                  arr.push(val);
                } else {
                  arr.splice(idx, 1);
                }
              }
              break;
            case false:
              this.$parent.value = sel ? this.value : undefined;
              break;
            default:
              this.data.selected = sel;
              break;
          }
        }
      },
      selectable: function() {
        return this.$parent && this.$parent.multiple;
      },
    },
    methods: {
      clicked: function(event) {
        if (this.data.header || this.data.divider || this.disabled || this.hasSubmenu)
          event.stopPropagation();
        else {
          if (this.selected !== undefined || this.selectable !== undefined) {
            this.selected = !this.selected;
          }
          if (this.data.callback && this.data.callback.call(this, event))
            event.stopPropagation();
        }
        event.preventDefault();
      },
    },
    mixins: [
      VueBase.itemsAcc,
      VueBase.compProxy("data", [
        "header",
        "divider",
        "callback",
        "items",
        "selected",
        "value",
        "multiple",
        "position",
        "auxClass"
      ],[
        "hidden",
        "disabled",
        "label",
        "iconClass"
      ]),
    ],
  });
  MenuItem.options.components["menu-item"] = MenuItem;

  /**
   * .. js:class:: MenuButton()
   *
   *    :extends: Vue
   *    :mixins: VueBase.itemsAcc
   *    :param config object: The config of the MenuButton. See attributes for more details.
   *
   *    A MenuButton with a Menu attached.
   *
   *    .. js:attribute:: label
   *
   *       :type: String
   *
   *       The label of the button. Must be provided.
   *
   *    .. js:attribute:: dropup
   *
   *       :type: Boolean
   *
   *       Whether this is a dropup menu. Default false.
   *
   *    .. js:attribute:: iconClass
   *
   *       :type: String (icon class-name)
   *
   *       The class-name of the icon for the button (displayed before the label).
   *
   *    .. js:attribute:: btnClass
   *
   *       :type: String (class-names)
   *
   *       The class-names to be in included. Should be provided.
   *
   *    .. js:attribute:: class
   *
   *       :type: String (class-names)
   *
   *       The class-names for the MenuButton wrapper. May be used to integrate it into a input
   *       group.
   *
   *    .. js:attribute:: items
   *
   *       :type: Object (menu definition)
   *
   *       See :js:class:`MenuItem`.
   */
  var MenuButton = Vue.extend({
    template: Tbutton.trim(),
    props: [
      "btnClass",
      "iconClass",
      "label",
      "dropup",
      "items",
      "class",
      "disabled",
      // for selectable compatiblity
      "value",
      "multiple"
    ],
    created: function(){
      if(this.items===undefined)
        this.$set("items", {});
    },
    computed: {
      finalBtnClass: function() {
        var r = ["dropdown-toggle"];
        if (this.btnClass)
          r.push(this.btnClass);
        if (!this.length || this.disabled)
          r.push("disabled");
        return r.join(" ");
      },
      finalClass : function() {
        var r = [this.dropup ? "dropup" : "dropdown"];
        if (this.class)
          r.push(this.class);
        return r.join(" ");
      },
      root: {
        cached: false,
        get: function() {
          return this;
        },
      },
      selectable: function() {
        return this.$parent && this.$parent.multiple;
      },
    },
    mixins: [
      VueBase.itemsAcc,
    ],
    components: {
      "menu-item": MenuItem,
    }
  });

  /**
   * .. js:class:: MenuHeadless(config)
   *
   *    :extends: Vue
   *    :mixins: VueBase.itemsAcc
   *    :param config object: The config of the MenuHeadless. See attributes for more details.
   *
   *    Just a headless menu, which is to be dynamically $attacheTo a menu head, to provide the
   *    same menu in multiple locations.
   *
   *    .. js:attribute:: items
   *
   *       :type: Object (menu definition)
   *
   *       See :js:class:`MenuItem`.
   */
  var MenuHeadless = Vue.extend({
    template: Troot.trim(),
    created: function(){
      if(this.items===undefined)
        this.$set("items", {});
    },
    computed: {
      root: {
        cached: false,
        get: function() {
          return this;
        },
      },
    },
    mixins: [
      VueBase.itemsAcc,
    ],
    components: {
      "menu-item": MenuItem,
    }
  });

  /**
   * .. js:class:: MenuContext(config)
   *    :extends: Vue
   *    :mixins: VueBase.itemsAcc
   *    :param config object: The config of the MenuContext. See attributes for more details.
   *
   *    A menu that can be opened as a context-menu.
   *
   *    .. js:attribute:: items
   *
   *       :type: Object (menu definition)
   *
   *       See :js:class:`MenuItem`.
   *
   *    .. js:function:: ()
   *
   *
   */
  var MenuContext = Vue.extend({
    template: Tcontext.trim(),
    props: {
      items: Object,
    },
    created: function(){
      if(this.items===undefined)
        this.$set("items", {});
    },
    data: function() {
      return {
        invokeXY: [0, 0],
      };
    },
    computed: {
      root: {
        cached: false,
        get: function() {
          return this;
        },
      },
      menuStyle: function() {
        var w = window.innerWidth;
        var h = window.innerHeight;
        var x = this.invokeXY[0];
        var y = this.invokeXY[1];
        var style = {};
        if (x > 0.5 * w)
          style.right = w - x + "px";
        else
          style.left = x + "px";
        if (y > 0.5 * h)
          style.bottom = h - y + "px";
        else
          style.top = y + "px";
        return style;
      },
    },
    methods: {
      show: function(ev) {
        if (ev)
          this.invokeXY = [
            ev.clientX,
            ev.clientY,
          ];
        this.$els.btn.click();
        // this.$el.classList.add("open");
      },
    },
    mixins: [
      VueBase.itemsAcc,
    ],
    components: {
      "menu-item": MenuItem,
    }
  });

  /** indent: 1
   * .. js:function:: objClone(obj)
   *
   *    Generates a proper (pseudo-)clone of an object describing a menu.
   *
   *    :param Object obj: The object descibing the menu.
   *    :return: The cloned menu descibing object
   *    :rtype: Object
   */
  var objClone = function(obj) {
    var ret = {};
    if (!obj) return ret;
    for(var i in obj) {
      if (obj[i].unique) {
        ret[i] = Vue.util.extend({}, obj[i]);
      } else {
        ret[i] = obj[i];
      }
      if (obj[i].items) {
        ret[i].items = objClone(obj[i].items);
      }
    }
    return ret;
  };

  return {
    item:     MenuItem,
    button:   MenuButton,
    headless: MenuHeadless,
    context:  MenuContext,
    objClone: objClone,
  };
});
