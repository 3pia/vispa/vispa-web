/**
 * vue/sidebar
 * =================
 */
define([
  "jquery",
  "vue/vue",
  "vispa/vue/base",
  "vispa/vue/menu",
  "text!html/index/vue/sidebar.html",
  "text!html/index/vue/toggle.html"
], function (
  $,
  Vue,
  VueBase,
  Menu,
  Tsidebar,
  Ttoggle
) {

  /**
   * .. js:class:: Toggle()
   *
   *    :extends: MenuItem
   *    :param config object: The config of the Toggle.
   *
   *    A toggle. This class should not be instanciated directly. It extends :js:class:`MenuItem`
   *    to be a toggle with no submenu and eventually an img instead of an icon.
   *
   *    .. js:attribute:: img
   *
   *       :type: Bool
   *
   *       Determines, if the toggle is an image instead of an icon.
   *
   *    .. js:attribute:: imgClass
   *
   *       :type: String
   *
   *       The class the toggle gets in case of an img.
   *
   *    .. js:attribute:: src
   *
   *       :type: String
   *
   *       The html src in case of an img.
   *
   *    .. js:attribute:: contentItem
   *
   *       :type: Bool
   *
   *       Only content items will have an entry in the overflow menu as well as in the body.
   *
   *    .. js:attribute:: id
   *
   *       :type: String
   *
   *       The corresponding element in the body will get this id.
   *
   **/
  var Toggle = Menu.item.extend({
    template: Ttoggle.trim(),
    mixins: [
      VueBase.compProxy("data", [
        "img",
        "contentItem",
        "id"
      ], [
        "imgClass",
        "src"
      ]),
    ],
  });

  /**
   * .. js:class:: Sidebar()
   *
   *    :extends: Vue
   *    :param config object: The config of the MenuButton. See attributes for more details.
   *
   *    A side bar with a header of toggles and an overflow button menu as well as a body with
   *    where the toggles place their content.
   *
   *    .. js:attribute:: itemClass
   *
   *       :type: String
   *
   *       The class each toggle gets.
   *
   *    .. js:attribute:: contentClass
   *
   *       :type: String
   *
   *       The class a content in the body gets.
   *
   *    .. js:attribute:: items
   *
   *       :type: Object (toggle definitions)
   *
   *       The items are used in two (three) ways.
   *
   *    .. js:attribute:: active
   *
   *       :type: Bool
   *
   *       Determines, whether the sidebar gets the CSS class 'active' und thus, is expanded
   *
   *    .. js:attribute:: size
   *
   *       :type: Integer
   *
   *       The width of the sidebar in percent when active.
   *
   *    .. js:attribute:: footer
   *
   *       :type: Object
   *
   *       Contains the data for the footer: footer.items, footer.menu.iconClass, footer.menu.label,
   *       footer.menu.btnClass, footer.menu.items

   **/
  var Sidebar = Vue.extend({
    template: Tsidebar.trim(),
    data: function(){return {
      items: {},
      maxNumItems: 0,
      active: true, // should be false, but we need to toggle it to trigger the watch
      toggleClass: "vispa-toggle",
      contentClass: "",
      headerClass: "vispa-sidebar-header",
      bodyClass: "vispa-sidebar-body",
      footerClass: "vispa-sidebar-footer",
    };},
    created: function () {
      // this needs to be done manually
      this.$set("active", false);
    },
    ready: function () {
      this.computeMaxItems();
      $(window).on("resize.sidebar", function () {
        this.computeMaxItems();
        this.updatePopovers();
      }.bind(this));
    },
    computed: {
      contentItems: function () {
        // sort out the img
        var newItems = {}
        $.each(this.items, function (key, item) {
          if (item.contentItem) {
            newItems[key] = item;
          }
        });
        return newItems;
      },
      numItems: function () {
        return Object.keys(this.items).length;
      }
    },
    watch: {
      active: function () {
        this.computeMaxItems();
        this.updatePopovers();
        if (!this.active) {
          $.each(this.$refs.toggles, function (key, toggle) {
            toggle.selected = false;
          });
        }
      },
      items: {
        handler: function () {
          this.updatePopovers();
        },
        deep: true
      }
    },
    methods: {
      computeMaxItems: function () {
        if (this.active) {
          this.maxNumItems = parseInt(($(this.$els.header).width() - 41) / 40);
        } else {
          this.maxNumItems = parseInt(($(this.$els.header).height() - 41) / 40);
        }
      },
      updatePopovers: function () {
        $.each(this.$refs.toggles, function (key, toggle) {
          if (!toggle.contentItem) return true;
          if (this.active) {
            $("#" + toggle.id, this.$el).css("width", "");
            $("#" + toggle.id, this.$el).css("top", "");
          } else if (toggle.selected) {
            var $node = $(toggle.$el);
            var $content = $("#" + toggle.id, this.$el);
            var $caret = $content.prev();
            var offset = $node.offset();
            Vue.nextTick(function () {
              $caret.css("top", String(offset.top + 20 - $caret.outerHeight() / 2) + "px");
              $content.css("width", String(this.size) + "%");
              var contentHeight = $content.outerHeight();
              var contentTop = offset.top + 20 -  contentHeight / 2;
              var contentBottom = offset.top + 20 + contentHeight / 2;
              var windowHeight = $(window).height()
              if (contentBottom > windowHeight) contentTop = windowHeight - contentHeight;
              if (contentTop < 0) contentTop = 0;
              $content.css("top", String(contentTop) + "px");
            }.bind(this));
          }
        }.bind(this));
      }
    },
    components: {
      "toggle-item": Toggle,
      "menu-button": Menu.button
    }
  });

  return Sidebar

});
