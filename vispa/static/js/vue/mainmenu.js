define([
  "jquery",
  "vue/vue",
  "vispa/vue/base",
  "vispa/vue/menu",
  "vispa/workspace",
  "vispa/common/color",
  "text!html/index/vue/menu-main.html",
  "text!html/index/ws/menu-item.html",
], function(
  $,
  Vue,
  VueBase,
  Menu,
  Workspace,
  Color,
  tMainMenu,
  tWsItem
) {

  var wsSubItemDisconnect = {
    iconClass: "fa-bolt",
    label: "Disconnect",
    callback: function() {
      Workspace._members.byId(this.$parent.data.id).disconnect();
    },
  };

  var wsExtra = {
    divider: {
      divider: true,
      position: 10,
    },
    configure: {
      iconClass: "fa-wrench fa-fw",
      label: "Configure",
      position: 30,
    },
    add: {
      iconClass: "fa-plus fa-fw",
      label: "Add",
      hidden: false,
      position: 0,
    },
    disconnectAll: {
      iconClass: "fa-eject fa-fw",
      label: "Disconnect all",
      position: 20,
      disabled: function() {
        return !this.$root.wsAnyConnected;
      },
      callback: function() {
        Workspace._members.disconnectAll();
      },
    },
  };

  var MainMenuItem = Menu.item.extend({
    computed: {
      disabled: {
        get: function () {
          if (this.data.needWs && this.root.currentState!="connected")
            return true;
          else
            return this.data.disabled;
        },
        set: function (val) {
          this.$set("data.disabled", val);
        },
      },
    },
  });
  MainMenuItem.options.components["menu-item"] = MainMenuItem;

  var WsMenuItem = Menu.item.extend({
    template: tWsItem.trim(),
    props: ["data", "current"],
    computed: {
      items: function() {
        var ret = {};
        if ("connected" === this.data.state) {
          ret.disconnect = wsSubItemDisconnect;
        }
        // state reconstruction question
        return ret;
      },
      color: function () {
        if (!Workspace._members.useColor) return;
        var c = this.workspace.color;
        if (!c) return;
        c = new Color(c);
        if (!c.value.s) return;
        // c.setSaturation(0.5 + 0.5 * c.value.s);
        return c.toString();
      },
      class: function() {
        return this.data.state +
          (this.hasSubmenu ? " dropdown-submenu" : "");
      },
      iconClass: function() {
        switch (this.data.state) {
          case "connected":
            return "fa-fw " + (this.current ? "fa-circle" : "fa-circle-o");
          case "disconnected":
            return "fa-fw";
          default:
            return "fa-fw fa-refresh fa-spin";
        }
      },
      workspace: function () {
        return Workspace._members.byId(this.data.id);
      },
    },
    methods: {
      clicked: function() {
        if (this.data.state == "disconnected")
          this.workspace.connect();

        Workspace._members.current = this.data.id;
      },
    },
  });

  var MainMenu = Menu.headless.extend({
    data: function () {
      return {
        direction: "left",
      };
    },
    template: tMainMenu.trim(),
    created: function() {
      this.wsExtra = $.extend(true, {}, wsExtra);
      if (!vispa.args.user.addWorkspaces) {
        this.wsExtra.add.hidden = true;
      }
    },
    computed: {
      wsAnyConnected: function() {
        for(var id in this.wsData.workspaces) {
          if (this.wsData.workspaces[id].state=="connected") {
            return true;
          }
        }
        return false;
      },
      currentState: function() {
        return this.wsData.current ? this.wsData.workspaces[this.wsData.current].state : "";
      },
      wsStateIcon: function() {
        switch (this.currentState) {
          case "connected":
            return "fa-fw fa-check";
          case "disconnected":
            return "fa-fw fa-bolt";
          default:
            return "fa-fw fa-server";
        }
      },
      wsClass: function() {
        return "dropdown-submenu ws-menu state-" + (this.currentState || "none");
      },
    },
    methods: {
      spawn: function(viewClass, args) {
        viewClass._members.spawn(
          args || {},
          {tabber: this.$options.tabber},
          {workspace: Workspace._members.current}
        );
      },
      /** indent: 1
       * .. js:function:: spawnFromFile(viewClass, selectorArgs, callback)
       *
       *    :param BaseView viewClass: The view class to be spawned.
       *    :param Object selectorArgs: Additional arguments to be passed to the file selector.
       *
       *    :callback String path: The path argument of the fileSelector callback. If the callback
       *        does not return *undefined*, this value will be used as *args* while spawning the
       *        *viewClass*.
       */
      spawnFromFile: function(viewClass, selectorArgs, callback) {
        var fs = vispa.extManager.byName("file2").getView("FileSelector");
        var viewArgs = {tabber: this.$options.tabber};
        var baseArgs = {workspace: Workspace._members.current};
        this.spawn(fs, $.extend({}, selectorArgs, {
          callback: function() {
            var args = callback.apply(this, arguments);
            if (args !== undefined) {
              viewClass._members.spawn(
                args || {},
                viewArgs,
                baseArgs
              );
            }
          },
        }));
      }
    },
    components: {
      "main-menu-item": MainMenuItem,
      "ws-menu-item": WsMenuItem,
    },
  });

  return MainMenu;
});
