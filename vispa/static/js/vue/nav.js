define([
  "jquery",
  "vue/vue",
  "text!html/index/vue/nav-item.html",
  "text!html/index/vue/nav-root.html",
], function(
  $,
  Vue,
  Titem,
  Troot
){
  var NavRoot = Vue.extend({
    template: Troot.trim(),
  });
  Vue.component("nav-root", NavRoot);

  var NavItem = Vue.extend({
    template: Titem.trim(),
    props: ["key", "title", "active"],
  });
  Vue.component("nav-item", NavItem);

  return {
    root: NavRoot,
    item: NavItem,
  };
});
