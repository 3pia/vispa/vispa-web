/**
 * vue/base
 * =================
 */
define(["vue/vue"],function(Vue){

  // a default filter
  Vue.filter("iconPrefix", function(iconClass){
    if (!iconClass) return iconClass;
    var hi = iconClass.indexOf("-");
    return iconClass.substr(0,hi) + " " + iconClass;
  });

  var uncleanRE = /\./g;
  Vue.filter("cleanId", function(val){
    return val.replace(uncleanRE, "-");
  });

  // drag-n-drop directives
  Vue.directive("drag", {
    bind: function() {
      var self = this;
      var tars;
      var tarFlag = function (state) {
        for(var i = 0; i<tars.length; i++) {
          var t = tars[i];
          for(var cl in self.modifiers)
            t[cl] = state;
        }
      };
      this.ev = {
        dragstart: function(ev) {
          ev.dataTransfer.effectAllowed = "all";
          ev.dataTransfer.effectAllowed = self.arg;
          // ev.dataTransfer.dropEffect = "copy";
          var data = self.data;
          if (typeof data === "function")
            data = data.call(self.vm, ev);
          for(var k in data)
            if (typeof data[k] === "string")
              ev.dataTransfer.setData(k, data[k]);
          // visuals
          tars = data["vispa/drag-targets"] || [self.vm];
          tarFlag(true);
        },
        dragend: function() {
          tarFlag(false);
        },
      };
      for(var ev in this.ev)
        this.el.addEventListener(ev, this.ev[ev]);
    },
    update: function(data) {
      this.data = data;
    },
    unbind: function() {
      for(var ev in this.ev)
        this.el.removeEventListener(ev, this.ev[ev]);
    },
  });

  Vue.directive("drop", {
    // acceptStatement: true,
    bind: function() {
      var self = this;
      var ref = 0;
      var modRef = function(delta) {
        ref += delta;
        var value = [false, true][ref];
        if (delta && value !== undefined)
          for(var cl in self.modifiers)
            self.vm[cl] = value;
      };
      var modes = ["copy", "move", "link"];
      var dropEffect = function(ev) {
        // get an actual value
        var val = self.val;
        if (typeof val === "function")
          val = val.call(self.vm, ev);
        if (typeof val === "string")
          val = val.split(" ");
        if (!(val instanceof Array))
          return false;
        var matched = false;
        var operation = self.arg; // this is non-spec
        var types = ev.dataTransfer.types;
        for(var i=0; i < val.length; i++) {
          var v = val[i];
          if (~modes.indexOf(v)) {
            operation = v;
            continue;
          }
          if (matched) continue;
          matched = !!~types.indexOf(v);
        }
        return matched ? operation : undefined;
      };
      var dragCheck = function(ev) {
        var enter = ev.type === "dragenter";
        var effect = dropEffect(ev);
        if (effect) {
          ev.stopPropagation();
          modRef(enter ? 1 : -1);
        } else
          effect = "none";
        if (enter)
          ev.dataTransfer.dropEffect = effect;
      };
      this.ev = {
        dragenter: dragCheck,
        dragleave: dragCheck,
        dragover: function(ev) {
          if (dropEffect(ev))
            ev.preventDefault();
        },
        drop: function(ev) {
          if (dropEffect(ev)) {
            modRef(-ref);
            if (!ev.defaultPrevented) {
              var data = {};
              var types = ev.dataTransfer.types;
              for(var i=0; i < types.length; i++) {
                var t = types[i];
                if (t === "Files") {
                  data[t] = ev.dataTransfer.files;
                } else {
                  data[t] = ev.dataTransfer.getData(t);
                }
              }
              self.vm.$emit("dropped", data, ev);
              ev.preventDefault();
            }
          }
        },
      };
      for(var ev in this.ev)
        this.el.addEventListener(ev, this.ev[ev]);
    },
    update: function(val) {
      this.val = val;
    },
    unbind: function() {
      for(var ev in this.ev)
        this.el.removeEventListener(ev, this.ev[ev]);
    },
  });

  Vue.directive("indeterminate", {
    update: function(val) {
      this.el.indeterminate = !!val;
    },
  });

  /** indent: 1
   * .. js:function:: runSync(func[, scope])
   *
   *    Runs the function in syncronous mode.
   *
   *    .. warning::
   *       Avoid using this at all cost!
   *
   *    .. note::
   *       This will not flush the Vue internal queue, thus some action may actually not be
   *       syncronous because they are already queued (and we have not means to flush it manually).
   *
   *
   *   :param Function func: The function to be run.
   *   :param Object scopy: The scope (*this*) to run the function on.
   */
  var runSync = function(func, scope) {
    var oldSync = Vue.config.async;
    Vue.config.async = false;
    func.call(scope||this);
    Vue.config.async = oldSync;
  };

  /** indent: 1
   * .. js:function:: syncVuewFrags(vueInstances)
   *
   *		Moves a list of (single node) vue instance behind the fragment start node - if any.
   *
   *    :param Array|Object vueInstances: An collection of vue instances
   *    	(usually obtained from $refs).
   */
  var syncVueFrags = function(vueInstances) {
    for(var i in vueInstances) {
      var vi = vueInstances[i];
      if (!vi._frag) continue;
      var n = vi.$el;
      var f = vi._frag.node;
      if (n.previousSibling == f) continue;
      n.parentNode.removeChild(n);
      f.parentNode.insertBefore(n, f.nextSibling);
    }
  };

  // automatically generate computed properties that proxy items in an object accessable by path
  /** indent: 1
   * .. js:function:: compProxy(base, keys)
   *
   *    Generates a mixin of computed properties that delegate their access to a object contained
   *    withing the instance.
   *
   *    .. note::
   *       Setting value that were previously unset is done in a manner that support their
   *       detection within the Vue framework.
   *
   *
   *    :param String base: The name of the object that the access should be delegated to.
   *    :param Array[String] keys: The name of the properties to be delegated to the objecet.
   *    :param Array[String] keysFunc: The name of the properties to be delegated to the objecet, if these are functions they will be called for generation of dynamic properties.
   *
   *    :return: The object in Vue mixin style.
   *    :rtype: Object (Vue mixin)
   */
  var compProxyHelper = function(base, func, key) {
    // var base = base;
    // var key  = key;
    this[key] = {
      cache: false,
      get: (func ? function() {
        var v = this[base][key];
        return (typeof v === "function") ? v.call(this) : v;
      } : function() {
        return this[base][key];
      }),
      set: function(v) {
        this.$set(base+"."+key, v);
      },
    };
  };
  var compProxy = function(base, keys, keysFunc) {
    var comp = {};
    keys.forEach(compProxyHelper.bind(comp, base, false));
    if (Vue.util.isArray(keysFunc) && keysFunc.length) {
      keysFunc.forEach(compProxyHelper.bind(comp, base, true));
    }
    return {computed: comp};
  };

  /** indent: 1
   * .. js:function:: update(target, source[, clean])
   *
   *    Recursively updates a *target* with data from *source*, with Vue data interactivity.
   *
   *    :param target Object: The object to be updated.
   *    :param source Object: The object with the update template.
   *    :param clean Bool: Clean all key from the *target* that are not in source.
   *    	Essentially a Vue aware deep copy.
   */
  var update = function(target, source, clean) {
    var key, targetValue, sourceValue;
    for (key in source) {
      targetValue = target[key];
      sourceValue = source[key];
      if (!Vue.util.hasOwn(target, key)) {
        Vue.set(target, key, sourceValue);
      } else if (Vue.util.isObject(targetValue) && Vue.util.isObject(sourceValue)) {
        update(targetValue, sourceValue);
      } else {
        target[key] = sourceValue;
      }
    }
    if (clean) {
      for (key in target) {
        if (!Vue.util.hasOwn(source, key)) {
          Vue.delete(target, key);
        }
      }
    }
  };


  /**
   * .. js:mixin:: itemsAcc
   *
   *    Provides access to the instances that correspond with the configs with the same key.
   *
   *    .. js:function:: get(key)
   *
   *       Retrieves a :js:class:`MenuItem` for the corresponding entry in :js:attr:`items`.
   *
   *       :param String key: The key of entry.
   *       :return: The entry for the key.
   *       :rtype: MenuItem
   *
   *    .. js:function:: set(key, config)
   *
   *       Sets the config for for a given key. This is the only valid way to create a new
   *       :js:class:`MenuEntry`.
   *
   *       :param String key: The key of the entry.
   *
   *    .. js:function:: del(key)
   *
   *       Removes an item for :js:attr:`items`. This is the only valid way to delete a
   *       :js:class:`MenuEntry`.
   *
   *       :param String key: The key of the entry.
   *
   *    .. js:attribute:: length read-only
   *
   *       :type: Number
   *
   *       The number of entries in :js:attr:`items`.
   */
  var itemsAcc = {
    methods: {
      get: function(key) {
        return this.$refs.items[key];
      },
      set: function(key, value) {
        Vue.set(this.items, key, value);
      },
      del: function(key) {
        Vue.delete(this.items, key);
      },
      upd: function(source, clean) {
        update(this.items, source, clean);
      },
    },
    computed: {
      length: function() {
        return this.items ? Object.keys(this.items).length : 0;
      },
    },
  };

  return {
    syncVueFrags: syncVueFrags,
    compProxy   : compProxy,
    itemsAcc    : itemsAcc,
    update      : update,
    runSync     : runSync,
  };
});
