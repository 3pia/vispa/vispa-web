define([
  "jquery",
  "vue/vue",
  "vispa/vue/base",
  "vispa/vue/menu",
  "text!html/index/vue/fastmenu.html",
  "text!html/index/vue/fastmenu-entry.html",
], function(
  $,
  Vue,
  VueBase,
  Menu,
  tFastMenu,
  tFastMenuEntry
) {

  var fastMenuId = function(instance)  {
    return "view-"+instance.id+"-fastMenu";
  };

  var MenuItemMixin = {
    ready: function() {
      if (!this.data.header && !this.data.divider && this.data.fastmenu!==false && this.$root.fastmenu) {
        var inst = this.$root.instance;
        var fastmenu = inst.vue.node.$refs.fastmenu;
        var self = this;
        this.draggable = $(this.$el).draggable({
          disabled: this.inFastMenuBar,
          distance: 20,
          containment: inst.$node,
          appendTo: inst.$node,
          helper: "clone",
          cursor: "move",
          cursorAt: {
            left: 0,
            top: 0
          },
          connectToSortable: "#"+fastMenuId(inst),
          start: function(event, ui) {
            fastmenu.force = true;
            ui.helper.empty().append(
              $("<span class='label label-default'>")
                .text(self.label)
                .css("font-size", 14)

            ).data("key", self.key);
            $(self.root.$els.btn).dropdown("toggle");
          },
          stop: function() {
            Vue.nextTick(function() {
              fastmenu.force = false;
            });
          }
        });
        this.$watch("inFastMenuBar", function(){
          this.draggable.draggable(this.inFastMenuBar ? "disable" : "enable");
        });
      }
    },
    beforeDetroy: function() {
      if (this.draggable) {
        this.draggable.draggable("destroy");
      }
    },
    computed: {
      inFastMenuBar: function() {
        if (this.$root.fastmenu) {
          return !!~this.$root.instance.prefs.get("fastmenu").indexOf(this.key);
        }
      }
    }
  };
  var MenuItem = Menu.item.extend(MenuItemMixin);

  var MenuButtonMixin = {
    components: {
      "menu-item": MenuItem,
    }
  };
  var MenuButton = Menu.button.extend(MenuButtonMixin);

  var deleting = function(event) {
    // height(header + tabs) = 90
    var offset = 90;
    return event.clientY > 2.5 * offset;
  };

  var FastMenuEntry = Vue.extend({
    template: tFastMenuEntry,
    props: {
      item: Object,
      deleting: Boolean,
    },
    ready: function() {
      $(this.$el).data("key", this.item.key);
      this.$parent.refresh();
    },
    computed: {
      subType: function() {
        return this.item.length ? (this.item.data.fastmenu || "group") : "";
      },
      removeMode: function() {
        return this.$parent.removeMode;
      },
      buttonClass: function() {
        return "btn btn-sm "+(this.item.auxClass||"btn-default");
      },
    },
    components: {
      "menu-button": Menu.button,
    },
    mixins: [
      VueBase.compProxy("item", [
        "label",
        "items",
        "iconClass",
        "multiple",
        "value"
      ])
    ]
  });
  var FastMenuBar = Vue.extend({
    template: tFastMenu,
    data: function () {return {
      force: false,
    };},
    ready: function() {
      var self = this;
      $(this.$el).sortable({
        axis: "x",
        distance: 20,
        cursor: "move",
        cancel: "",
        forcePlaceholderSize: true,
        sort: function(event, ui) {
          var key = ui.item.data("key");
          if (!key) return;
          var i = self.config.indexOf(key);
          if (!~i) return;
          self.$refs.items[i].deleting = deleting(event);
        },
        stop: function(e, ui) {
          var key = ui.item.data("key");
          if (!key) return;
          var i = self.config.indexOf(key);
          if (!~i) return;
          if (self.$refs.items[i].deleting) {
            VueBase.syncVueFrags(self.$refs.items);
            self.config.splice(i, 1);
          } else {
            var arr = [];
            var jqo = $(self.$el).children();
            for(var j=0; j<jqo.length; j++)
              arr.push(jqo.eq(j).data("key"));
            VueBase.syncVueFrags(self.$refs.items);
            self.config = arr;
          }
        },
        receive: function(e, ui) {
          var key = ui.helper.data("key");
          ui.helper.remove();
          self.config.push(key);
        },
      });
    },
    methods: {
      refresh: function() {
        if(this._isReady)
          $(this.$el).sortable("refresh");
      },
    },
    computed: {
      instance: {
        cached: false,
        get: function() {
          return this.$root.instance;
        }
      },
      config: {
        get: function () {
          return this.instance.prefs.get("fastmenu") || [];
        },
        set: function (val) {
          this.instance.prefs.set("fastmenu", val);
        }
      },
      items: function() {
        var conf = this.config;
        var ret = [];
        for(var i=0; i<conf.length; i++) {
          var item = this.instance.menu.get(conf[i]);
          if (item)
            ret.push(item);
        }
        return ret;
      },
      enabled: function() {
        return !!this.item.length || this.force;
      },
      id: function() {
        return fastMenuId(this.instance);
      }
    },
    components: {
      "fastmenu-entry": FastMenuEntry,
    }
  });

  return {
    itemMixin:  MenuItemMixin,
    item:       MenuItem,
    buttonMixin:MenuButtonMixin,
    button:     MenuButton,
    bar:        FastMenuBar,
  };
});
