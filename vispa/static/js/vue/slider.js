define([
  "jquery",
  "vue/vue",
  "text!html/index/vue/slider.html"
], function(
  $,
  Vue,
  template
){
  var Slider = Vue.extend({
    template: template.trim(),
    props: ["min","max","step","value"],
    ready: function() {
      var self = this;
      this.$slider = $(this.$els.slider)
      this.$slider.slider()
        .slider("on", "change", function(vals){
          self.value = vals.newValue;
        });
    },
    beforeDestroy: function() {
      if (this.$slider) { // no idea how this can run be fore ready
        this.$slider.slider("destroy");
        this.$slider = undefined;
      }
    },
    watch: {
      value: function(newValue) {
        if(this.$slider) {
          var curValue = this.$slider.slider("getValue");
          if (curValue != newValue)
            this.$slider.slider("setValue", Number(newValue));
        }
      },
    }
  });

  return Slider;
});
