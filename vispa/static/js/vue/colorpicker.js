define([
  "jquery",
  "vue/vue",
  "text!html/index/vue/colorpicker.html",
  "vendor/bootstrap/plugins/colorpicker/bootstrap-colorpicker",
], function(
  $,
  Vue,
  template
){
  var VueColorPicker = Vue.extend({
    template: template.trim(),
    props: {
      value: String,
      format: {
        type: String,
        default: "alias",
      },
      fallbackFormat: {
        type: String,
        default: "hex",
      },
      selfcontain: Boolean,
    },
    ready: function() {
      this.$els.input.value = this.value;
      this.$colorpicker = $(this.$els.colorpicker);
      this.$colorpicker.colorpicker({
        format: this.format || false,
        fallbackFormat: this.fallbackFormat || false,
        container: this.selfcontain ? this.$el : false,
        component: ".input-group-addon:last",
      }).on("changeColor", function(ev){
        if (ev.color.origFormat !== null)
          this.value = ev.color.toString("alias");
          // this.value = this.$colorpicker.colorpicker("getValue");
      }.bind(this));
    },
    beforeDestroy: function() {
      if (this.$colorpicker) { // no idea how this can run before ready
        this.$colorpicker.colorpicker("destroy");
        this.$colorpicker = undefined;
      }
    },
    watch: {
      value: function(newValue, oldValue) {
        if (this.$colorpicker) {
          var curValue = this.$colorpicker.colorpicker("getValue");
          if (curValue != newValue)
            this.$colorpicker.colorpicker("setValue", newValue);
        }
      },
    }
  });

  return VueColorPicker;
});
