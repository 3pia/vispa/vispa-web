define([
  "vue/vue",
  "text!html/index/vue/renamer.html",
], function(
  Vue,
  template
){
  var vco = function(v, l) {
    if (v < 0) v += l + 1;
    if (v < 0) v = 0;
    if (v > l) v = l;
    return v;
  };

  var VueRenamer = Vue.extend({
    template: template.trim(),
    props: {
      value: String,
      active: Boolean,
      sync: Boolean,
      selectionRangeBegin: {
        type: Number,
        default: 0,
      },
      selectionRangeEnd: {
        type: Number,
        default: -1,
      },
    },
    data: function() {
      return {
        newName: this.value,
      };
    },
    watch: {
      value: function(name) {
        this.newName = name;
      },
      active: function(val) {
        if (val) {
          this.$els.input.focus();
          this.$els.input.setSelectionRange(
            vco(this.selectionRangeBegin, this.value.length),
            vco(this.selectionRangeEnd, this.value.length)
          );
        } else
          this.newName = this.value;
      },
    },
    methods: {
      start: function() {
        this.active = true;
      },
      finish: function() {
        var name = this.newName;
        this.active = false;
        if (name == this.value) return;
        if (this.sync)
          this.value = name;
        this.$emit("renamed", name);
      },
      abort: function() {
        this.active = false;
      },
    },
  });

  return VueRenamer;
});
