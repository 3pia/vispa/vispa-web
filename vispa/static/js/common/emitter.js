define(["jclass", "vendor/eventemitter2/eventemitter2"], function(JClass, EventEmitter2) {

  // simply return a JClass compatible class
  return JClass._convert(EventEmitter2);

});
