/**
 * common/dialog
 * =================
 */
define([
  "jquery",
  "vue/vue",
  "vispa/vue/base",
  "text!html/index/dialog/base.html",
  "text!html/index/dialog/header.html",
  // "text!html/index/dialog/body.html",
  "text!html/index/dialog/footer.html",
  "text!html/index/dialog/prompt.html",
], function(
  $,
  Vue,
  VueBase,
  tBase,
  tHeader,
  // tBody,
  tFooter,
  tPrompt
) {

  /**
   * .. js:class:: Dialog(vueOpts)
   *
   *    :extends: Vue
   *
   *		Creates an simple extensible (modal) dialog.
   *
   *    :param Object vueOpts: The options given to the vue instance.
   *    :param HTMLElement vueOpts.appendTo: The element to append the dialog to. (optional)
   *    :param Object vueOpts.data: The configuration of the dialog.
   *      The keys in this object are the same as the properties on the instance.
   *
   *
   *    .. js:attribute:: rootClass
   *
   *       :type: String
   *       :default: ""
   *
   *			 An optional, additional CSS class on the very root node of the dialog.
   *
   *    .. js:attribute:: containerClass
   *
   *       :type: String
   *       :default: ""
   *
   *       An optional, additional CSS class on the container node of the dialog.
   *
   *    .. js:attribute:: iconClass
   *
   *       :type: String (CSS icon class)
   *       :default: ""
   *
   *       If given, this icon will be displayed in front of the title.
   *
   *    .. js:attribute:: title
   *
   *       :type: String
   *       :default: ""
   *
   *       The title of the dialog.
   *
   *    .. js:attribute:: body
   *
   *       :type: String (HTML)
   *       :default: ""
   *
   *       The body of the dialog. Is interpreted as HTML.
   *
   *    .. js:attribute:: closable
   *
   *       :type: Boolean
   *       :default: true
   *
   *		   Whether the dialog can be closed by clicking a cross in its opper right corner.
   *
   *    .. js:attribute:: backdrop
   *
   *       :type: Boolean
   *       :default: true
   *
   *		   Whether the background should be grayed out or not.
   *
   *    .. js:attribute:: isShown
   *
   *       :type: Boolean
   *       :default: false
   *
   *	     Wherther the dialog is currently shown.
   *	     If set from the beginning, the inital animation is skipped.
   *
   *    .. js:attribute:: onOpen
   *
   *       :type: Function
   *       :default: null
   *
   *       If providied, this function will be called when opening the dialog.
   *
   *    .. js:attribute:: onClose
   *
   *       :type: Function
   *       :default: null
   *
   *       If providied, this function will be called when closing the dialog.
   *
   *    .. js:attribute:: buttons
   *
   *       :type: Object
   *       :default: {}
   *
   *       An object containing further object describing the button to be present in the footer.
   *
   *    .. js:attribute:: button.*.label
   *
   *       :type: String
   *
   *       The label of the button.
   *
   *    .. js:attribute:: button.*.iconClass
   *
   *       :type: String (CSS icon class)
   *
   *       The icon of the button.
   *
   *    .. js:attribute:: button.*.class
   *
   *       :type: String (CSS class)
   *
   *       The CSS class of the button.
   *
   *    .. js:attribute:: button.*.pos
   *
   *       :type: Number|String
   *
   *       The buttons are sorted by this value. (ascending).
   *
   *    .. js:attribute:: button.*.callback
   *
   *       :type: Function
   *
   *       The function to be called when this button is clicked. (*this* is the dialog instance).
   *       If it returns true, the dialog will stay open. If not provided it will close too.
   *
   *    .. js:attribute:: focusButton
   *
   *       :type: String
   *
   *       The button with this key will be initally focused.
   */

  var Dialog = Vue.extend({
    template: tBase.trim(),
    data: function(){return {
      rootClass: "",
      containerClass: "",
      iconClass: "",
      title: "",
      body: "",
      buttons: {},
      closable: true,
      backdrop: true,
      isShown: true,
      onOpen: null,
      onClose: null,
    };},
    created: function() { // needs to be pre, porp compilation so we can make sure it runs
      var p = this.$options.appendTo || document.body;
      var n = document.createElement("div");
      p.appendChild(n);
      this.$options.el = n;
    },
    ready: function() {
      var btnComp = this.$refs.buttons;
      if (btnComp) {
        btnComp = btnComp[this.focusButton];
        if (btnComp) {
          btnComp.$el.focus();
        }
      }
      if (this.onOpen) {
        this.onOpen.call(this);
      }
    },
    beforeDestroy: function() {
      if (this.onClose) {
        this.onClose.call(this);
      }
    },
    computed: {
      rootClassFinal: function() {
        var r = ["modal"];
        if (this.backdrop)  r.push("backdrop");
        if (this.rootClass) r.push(this.rootClass);
        return r.join(" ");
      },
      hasBody: function() {
        return !!this.body;
      },
      hasFooter: function() {
        return !!this.buttons;
      }
    },
    methods: {
      close: function() {
        this.$destroy(true);
      },
      btnClick: function(btn) {
        if (!btn) return;
        if (!(btn.callback && btn.callback.call(this))) {
          this.close();
        }
      },
    },
    partials: {
      header: tHeader.trim(),
      body: "{{{body}}}",
      footer: tFooter.trim(),
      footerLeft: " ",
    },
    components: {
      "c-button": {
        template: "<button><slot></slot></button>",
      },
    },
  });

  /**
   * .. js:class:: PromptDialog(vueOpts)
   *
   *    :extends: Dialog
   *
   *    .. js:attribute:: callback
   *
   *       :type: Function
   *
   *       The callback that will be called with the result of the prompt.
   *
   *    .. js:attribute:: value
   *
   *       :type: String
   *       :default: ""
   *
   *       The current value of the prompt input. If provided from the beginning, this is the
   *       default value.
   *
   *    .. js:attribute:: password
   *
   *       :type: Boolean
   *       :default: False
   *
   *			 Wheter the prompted value should be masked (for passwords prompting).
   *
   *    .. js:attribute:: placeholder
   *
   *       :type: String
   *       :default: ""
   *
   *       The placeholder value of prompt input - only visible with no current input and no focus.
   */
  var PromptDialog = Dialog.extend({
    data: function(){return {
      callback: function(){},
      value: "",
      password: false,
      placeholder: "",
    };},
    computed: {
      hasBody: function() {
        return true;
      },
    },
    ready: function() {
      this.$els.input.focus();
    },
    beforeDestroy: function() {
      if (this.callback) {
        this.callback.call(this, this.aborted ? null : this.value);
      }
    },
    methods: {
      abort: function() {
        this.aborted = true;
        this.close();
      },
    },
    partials: {
      body: tPrompt.trim(),
    },
  });

  /** indent: 1
   * .. js:function:: Dialog.dialog(opts[, appendTo])
   *
   *    Opens a dialog.
   *
   *    :param Object opts: The same object as *vueOpts.data* of :js:class:`Dialog`.
   *    :param HTMLNode appendTo: The same parameter as *vueOpts.appendTo* of :js:class:`Dialog`.
   *      If not providied, it will attempted to fill it with *this.$messageContainer*
   *      (see :js:class:`BaseView`).
   */
  Dialog.dialog = function(opts, appendTo) {
    opts = opts || {};
    return new Dialog({
      data: opts,
      appendTo: appendTo || this.$messageContainer && this.$messageContainer[0],
    });
  };

  /** indent: 1
   * .. js:function:: Dialog.alert(message[, opts[, appendTo]])
   *
   *    Shows an alert message.
   *
   *    :param String message: The message to be displayed
   *    :param Object opts: The same object as *vueOpts.data* of :js:class:`Dialog`.
   *      Will be merged over the opts generated by the other parameters.
   *    :param String opts.okLabel: An easy way to overwrite the default label "Ok"
   *      (other route is *opts.buttons.ok.label*).
   *    :param HTMLNode appendTo: The same parameter as *vueOpts.appendTo* of :js:class:`Dialog`.
   *      If not providied, it will attempted to fill it with *this.$messageContainer*
   *      (see :js:class:`BaseView`).
   */
  Dialog.alert = function(message, opts, appendTo) {
    opts = opts || {};
    return new Dialog({
      data: $.extend(true, {
        iconClass: "fa-info",
        title: "Alert",
        body: message,
        buttons: {
          ok: {
            class: "btn-primary",
            label: opts.okLabel || "Ok",
          },
        },
        focusButton: "ok",
      }, opts),
      appendTo: appendTo || this.$messageContainer && this.$messageContainer[0],
    });
  };

  /** indent: 1
   * .. js:function:: Dialog.confirm(message, callback[, opts[, appendTo]])
   *
   *    Shows an alert message.
   *
   *    :param String message: The message to be displayed
   *    :callback Boolean yesChosen: The yes(-kind) optiong was chosedn.
   *    :param Object opts: The same object as *vueOpts.data* of :js:class:`Dialog`.
   *      Will be merged over the opts generated by the other parameters.
   *    :param String opts.yesLabel: An easy way to overwrite the default label "Yes"
   *      (other route is *opts.buttons.yes.label*).
   *    :param String opts.noLabel: An easy way to overwrite the default label "No"
   *      (other route is *opts.buttons.no.label*).
   *    :param HTMLNode appendTo: The same parameter as *vueOpts.appendTo* of :js:class:`Dialog`.
   *      If not providied, it will attempted to fill it with *this.$messageContainer*
   *      (see :js:class:`BaseView`).
   */
  Dialog.confirm = function(message, callback, opts, appendTo) {
    opts = opts || {};
    return new Dialog({
      data: $.extend(true, {
        iconClass: "fa-question",
        title: "Confirm",
        body: message,
        buttons: {
          no: {
            label: opts.noLabel || "No",
            callback: function() {
              callback.call(this, false);
            },
          },
          yes: {
            class: "btn-primary",
            label: opts.yesLabel || "Yes",
            callback: function() {
              callback.call(this, true);
            },
          },
        },
        closable: false,
        focusButton: "no",
      }, opts),
      appendTo: appendTo || this.$messageContainer && this.$messageContainer[0],
    });
  };

  /** indent: 1
   * .. js:function:: Dialog.prompt(message, callback[, opts[, appendTo]])
   *
   *    Shows an alert message.
   *
   *    :param String message: The message to be displayed
   *    :callback String|null result: The result of the prompt.
   *    :param Object opts: The same object as *vueOpts.data* of :js:class:`Dialog`.
   *      Will be merged over the opts generated by the other parameters.
   *    :param String opts.cancelLabel: An easy way to overwrite the default label "Cancel"
   *      (other route is *opts.buttons.cancel.label*).
   *    :param String opts.okLabel: An easy way to overwrite the default label "Ok"
   *      (other route is *opts.buttons.ok.label*).
   *    :param HTMLNode appendTo: The same parameter as *vueOpts.appendTo* of :js:class:`Dialog`.
   *      If not providied, it will attempted to fill it with *this.$messageContainer*
   *      (see :js:class:`BaseView`).
   */
  Dialog.prompt = function(message, callback, opts, appendTo) {
    opts = opts || {};
    return new PromptDialog({
      data: $.extend(true, {
        iconClass: "fa-pencil",
        title: "Prompt",
        body: message,
        rootClass: "prompt",
        buttons: {
          cancel: {
            label: opts.cancelLabel || "Cancel",
            callback: function() {
              this.aborted = true;
            },
          },
          ok: {
            class: "btn-primary",
            label: opts.okLabel || "Ok",
          },
        },
        closable: false,
        callback: callback,
      }, opts),
      appendTo: appendTo || this.$messageContainer && this.$messageContainer[0],
    });
  };

  return Dialog;
});
