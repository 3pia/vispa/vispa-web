/**
 * common/color
 * ============
 */

define([
  "jclass",
  "jquery",
  "vendor/bootstrap/plugins/colorpicker/bootstrap-colorpicker"
], function(JClass, $) {

  /**
   * .. js:class:: Color(val[, predefinedColors])
   *
   *    A class providing color conversion and parsing.
   *
   *    .. note::
   *       This class is provided and documented by
   *       `Bootstrap Colorpicker <https://mjolnic.com/bootstrap-colorpicker/>`.
   *
   *    :param Sting|Object val: The color value, either as valid CSS color or object describing
   *       in the HSBA color scheme.
   *    :param Number val.h: HSBA hue.
   *    :param Number val.s: HSBA saturation.
   *    :param Number val.b: HSBA brightness.
   *    :param Number val.a: HSBA alpha.
   *    :param Object predefinedColors: Additional named color aliases.
   */
  var Color = JClass._convert($.colorpicker.Color);
  var colorNames = Object.keys(new Color().colors);

  Color.getRandomName = function () {
    return colorNames[Math.floor(Math.random()*colorNames.length)];
  };

  return Color;
});
