/**
 * common/socket
 * =================
 */
define(["jquery", "emitter"], function($, Emitter) {

  var paramsToObject = function(params) {
    var obj = {};

    params.split("&").forEach(function(param) {
      var parts = param.split("=");
      var key   = parts.shift();
      var value = parts.join("=");

      if (key) {
        obj[key] = decodeURI(value);
      }
    });

    return obj;
  };

  var objectToParams = function(obj) {
    var params = [];

    Object.keys(obj).forEach(function(key) {
      params.push(key + "=" + encodeURI(obj[key]));
    });

    return params.join("&");
  };

  /**
   * .. _socket:
   *
   * .. js:class:: Socket(url[, options])
   *
   *    text
   *
   *    :param string url: Url.
   *    :param string options: Options.
   */

  var Socket = Emitter._extend({

    init: function init(url, options) {
      init._super.call(this, {wildcard: true});

      this.url = url;

      var defaultOptions = {
        autoReconnect    : true,
        reconnectAttempts: Infinity,
        reconnectDelay   : 3000,
        params           : {},
        fallback         : true,
        forcePoll        : false,
        sendUrl          : url ? url.replace("wss:", "https:").replace("ws:", "http:") : url,
        sendMethod       : "POST",
        pollUrl          : url ? url.replace("wss:", "https:").replace("ws:", "http:") : url,
        pollMethod       : "GET",
        pollDelay        : 3000,
        pollGracePeriod  : 15000,
        pollParams       : {}
      };
      this.options = $.extend(true, {}, defaultOptions, options);

      this._socket   = null;
      this._attempts = 0;

    },

    connect: function() {
      var self = this;

      // close and delete any existing socket
      this.close();
      delete this._socket;
      this._socket = null;

      // we emit the basic WebSocket events
      var events = ["open", "close", "message"];//"error"];

      // create the socket, based on browser capabilities and options
      if (window.WebSocket && !this.options.forcePoll) {
        // WebSockets are available and allowed

        // parse params
        var params = this.options.params;
        if ($.isFunction(params)) {
          params = params.call(this) || {};
        }
        if ($.isPlainObject(params)) {
          params = objectToParams(params);
        }

        // create WebSocket
        var url = this.url;
        if (params) {
          url += "?" + params;
        }
        this._socket = new window.WebSocket(url);

        // bind to events
        $.each(events, function(i, event) {
          self._socket["on" + event] = self.emit.bind(self, event);
        });

      } else if (this.options.fallback) {
        // WebSockets are unavailable or not allowed

        // create a PollSocket
        this._socket = new PollSocket(this, this.url, this.options);

        // bind to events
        $.each(events, function(i, event) {
          self._socket.on(event, self.emit.bind(self, event));
        });
      }

      // try to start auto reconnect
      if (this._socket && this.options.autoReconnect) {
        setTimeout(this.checkConnection.bind(this), this.options.pollDelay);
      }
    },

    status: function(asString) {
      if (!asString) {
        return this._socket.readyState;
      } else {
        var status = null;
        $.each(Socket.states, function(key, id) {
          if (id == this._socket.readyState) {
            status = key;
            return false;
          }
        }.bind(this));
        return status;
      }
    },

    type: function() {
      if (window.WebSocket && this._socket instanceof window.WebSocket) {
        return "WebSocket";
      } else if (this._socket instanceof PollSocket) {
        return "PollSocket";
      } else {
        return null;
      }
    },

    checkConnection: function() {
      if (this._attempts > this.options.reconnectAttempts) {
        return this;
      }

      if (this.status() == Socket.states["CLOSED"]) {
        this._attempts++;
        setTimeout(this.connect.bind(this), this.options.reconnectDelay);
      } else {
        this._attempts = 0;
        setTimeout(this.checkConnection.bind(this), this.options.reconnectDelay);
      }

      return this;
    },

    send: function(topic, data) {
      if (arguments.length == 1) {
        if ($.isPlainObject(topic)) {
          data = topic;
        } else {
          data = {topic: topic};
        }
      } else if (arguments.length == 2) {
        if ($.isPlainObject(data)) {
          data.topic = topic;
        } else {
          data = {
            topic: topic,
            data: data
          };
        }
      }

      this._socket.send(JSON.stringify(data));

      return this;
    },

    close: function() {
      if (this._socket) {
        this._socket.close();
      }

      return this;
    }
  });


  var PollSocket = Emitter._extend({

    init: function init(parent, url, options) {
      init._super.call(this);

      this.parent     = parent;
      this.url        = url;
      this.options    = options;
      this.readyState = Socket.states["CLOSED"];

      this.xhr = null;

      this.nPolls = 0;
      this.poll();
    },

    poll: function() {
      var self = this;

      if (this.readyState >= Socket.states["CLOSING"]) {
        this.readyState = Socket.states["CONNECTING"];
      }

      // parse params
      var allParams = [];

      var params = this.options.params;
      if ($.isFunction(params)) {
        params = params.call(this.parent) || {};
      }
      if ($.isPlainObject(params)) {
        params = objectToParams(params);
      }
      if (params) {
        allParams.push(params);
      }

      // parse pollParams
      var pollParams = this.options.pollParams;
      if ($.isFunction(pollParams)) {
        pollParams = pollParams.call(this) || {};
      }
      if ($.isPlainObject(pollParams)) {
        pollParams = objectToParams(pollParams);
      }
      if (pollParams) {
        allParams.push(pollParams);
      }

      var timeout = null;
      this.xhr = $.ajax({
        url : this.options.pollUrl,
        type: this.options.pollMethod,
        data: allParams.length ? paramsToObject(allParams.join("&")) : {},
        dataType: "json",
      }).always(function() {
        clearTimeout(timeout);
      }).done(function(response) {
        self.nPolls++;

        if (self.readyState >= Socket.states["CLOSING"]) {
          return;
        }
        if (self.readyState == Socket.states["CONNECTING"]){
          self.emit("open");
        }

        self.readyState = Socket.states["CONNECTED"];

        if (!$.isArray(response.data)) {
          response.data = [response.data];
        }
        $.each(response.data, function(i, data) {
          self.emit("message", {data: data});
        });

        setTimeout(self.poll.bind(self), self.options.pollDelay);
      }).fail(function(response, state, message) {
        self.close();
      });

      timeout = setTimeout(function() {
        if (self.xhr.state() == "pending") {
          self.xhr.abort("connection timeout (" + self.options.pollGracePeriod + ")");
        }
      }, this.options.pollGracePeriod);

      return this;
    },

    close: function() {
      if (this.readyState <= Socket.states.CONNECTED) {
        this.readyState = Socket.states.CLOSING;
        this.readyState = Socket.states.CLOSED;

        // set the poll delay to a high number to prevent a burst of poll requests
        this.options.pollDelay = Number.MAX_VALUE;

        if (this.xhr) {
          this.xhr.abort();
        }

        this.emit("close");
      }

      return this;
    },

    send: function(data) {
      $.ajax({
        url        : this.options.sendUrl,
        type       : this.options.sendMethod,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        data       : data
      });
      return this;
    }
  });


  Socket.PollSocket = PollSocket;

  // define common states
  Socket.states = {
    CONNECTING: 0,
    CONNECTED : 1,
    CLOSING   : 2,
    CLOSED    : 3
  };


  return Socket;

});
