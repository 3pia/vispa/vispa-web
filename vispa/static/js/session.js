define([
  "jquery",
  "vispa/module",
  "vispa/utils",
  "vispa/common/dialog",
  "vispa/workspace",
], function(
  $,
  Module,
  Utils,
  Dialog,
  Workspace
) {

  var SessionManager = Module._extend({
    init: function init() {
      init._super.call(this, "sessionManager", {
        label: "Session Manager",
        iconClass: "fa-desktop",
        items: {
          restoreLast: {
            label: "Restore last session",
            description: "Whether to restore the last session.",
            options: ["always", "ask", "never"],
            flat: true,
            type: "string",
            global: true,
            value: "ask",
          },
          autoConnect: {
            label: "Autoconnect workspaces",
            description: "When restoreing a session, autoconnect to workspaces that are not yet" +
              " connected but in the saved session.",
            options: ["always", "ask", "never"],
            flat: true,
            type: "string",
            global: true,
            value: "always",
          },
          lazyLoad: {
            label: "Lazily load tabs",
            description: "Restored tabs will load lazily, when they are show the first time.",
            options: ["lazy", "immediate"],
            type: "boolean",
            global: true,
            value: false,
          },
          restoreRunning: {
            level: 10,
            hidden: true,
            type: "boolean",
            global: true,
            value: false
          }
        }
      });

      this._jsondata = vispa.jsondata.getInst("session");
    },

    lazy: {
      descriptor: true,
      get: function() {
        return this.prefs.get("lazyLoad") || this._lazyOnce;
      },
    },

    run: function(callback) {
      if (!this.storedSession) return this.plain(callback);
      var self = this;

      if (this.prefs.get("restoreRunning")) {
        new Dialog({
          data: {
            iconClass: "fa-desktop",
            title: "Last restore failed",
            body: "Restoring the last session previously failed.",
            buttons: {
              again: {
                pos: 1,
                label: "Try again",
                callback: function() {
                  self.prefs.set("restoreRunning", false);
                  self.run(callback);
                },
              },
              lazy: {
                pos: 2,
                label: "Try again with lazy tab loading",
                class: "btn-primary",
                callback: function() {
                  self._lazyOnce = true;
                  self.prefs.set("restoreRunning", false);
                  self.run(callback);
                },
              },
              dont: {
                pos: 3,
                label: "Don't try again",
                class: "btn-primary",
                callback: function() {
                  self.prefs.set("restoreRunning", false);
                  self.plain(callback);
                },
              },
            },
          }
        });
        return;
      }

      switch(this.prefs.get("restoreLast")) {
        case "always":
          return this.restore(callback);
        case "never":
          return this.plain(callback);
        case "ask":
          var ds = this.storedSession.time;
          if (ds) {
            ds = " (from " + Utils.formatDate(new Date(ds)) + ") ";
          } else {
            ds = " ";
          }
          new Dialog({
            props: {
              remember: Boolean,
            },
            data: {
              iconClass: "fa-desktop",
              title: "Restore last session?",
              body: "Restore the last saved session" + ds + "?", // TODO: put date here
              buttons: {
                no: {
                  pos: 1,
                  label: "Don't restore",
                  callback: function() {
                    if (this.remember) {
                      self.prefs.set("restoreLast", "never");
                    }
                    self.plain(callback);
                  },
                },
                yes: {
                  pos: 2,
                  class: "btn-primary",
                  label: "Restore",
                  callback: function() {
                    if (this.remember) {
                      self.prefs.set("restoreLast", "always");
                    }
                    self.restore(callback);
                  },
                },
              },
              closable: true,
            },
            partials: {
              footerLeft:
                "<label><input type=\"checkbox\" v-model=\"remember\"> Remember choice</label>",
            },
          });
          break;
      }
    },

    restore: function(callback) {
      if (!this.storedSession) return this.plain(callback);

      this.prefs.set("restoreRunning", true);

      // main view restore
      var wsIds = vispa.mainViewManager.loadState(this.storedSession.mainView);
      // get all valid Workspaces
      var wsToConnect = [];
      for(var i in wsIds) {
        var ws = Workspace._members.byId(wsIds[i]);
        if (ws && !~wsToConnect.indexOf(ws) && ws.state == "disconnected") {
          wsToConnect.push(ws);
        }
      }
      // are there some that need to be connected?
      if (wsToConnect.length) {
        var connectWs = function () {
          for (var i in wsToConnect) {
            wsToConnect[i].connect();
          }
        };
        // check what we should do
        switch(this.prefs.get("autoConnect")) {
          case "always":
            connectWs();
            break;
          case "never":
            break;
          case "ask":
            var self = this;
            new Dialog({
              props: {
                remember: Boolean,
              },
              data: {
                iconClass: "fa-desktop",
                title: "Connect all used workspaces?",
                body: "Connect to the following workspaces?",
                wsNames: wsToConnect.map(function(ws){return ws.name}),
                buttons: {
                  no: {
                    pos: 1,
                    label: "Don't auto-connect",
                    callback: function() {
                      if (this.remember) {
                        self.prefs.set("autoConnect", "never");
                      }
                    },
                  },
                  yes: {
                    pos: 2,
                    class: "btn-primary",
                    label: "Auto-connect",
                    callback: function() {
                      if (this.remember) {
                        self.prefs.set("autoConnect", "always");
                      }
                      connectWs();
                    },
                  },
                },
                closable: true,
              },
              partials: {
                footerLeft:
                  "<label><input type=\"checkbox\" v-model=\"remember\"> Remember choice</label>",
                body:
                  "<p>{{body}}</p>" +
                  "<ul><li v-for=\"wsName in wsNames | orderBy true\">{{wsName}}</li></ul>",
              },
            });
            break;
        }
      }

      Utils.callback(callback)(null);
      this.prefs.set("restoreRunning", false);
      vispa.applyFragment();
    },

    plain: function(callback) {
      // TODO: perform default action
      // var is: vispa.args.global.workspaceAction
      // example value is: openFileBrowser
      Utils.callback(callback)(null);
      vispa.applyFragment();
    },

    save: function(delay) {
      // delay logic
      if (delay !== undefined) {
        delay = parseFloat(delay);
        if (isNaN(delay)) {
          delay = 1.0;
        }
        if (delay < 0 || delay == Infinity) {
          delay = undefined;
        }
      }
      if (delay) {
        var saveBy = Date.now() + delay * 1000;
        if (!this._saveByTime || this._saveByTime > saveBy) {
          if (this._saveTimeout) {
            clearTimeout(this._saveTimeout);
          }
          this._saveTimeout = setTimeout(this.save.bind(this), delay * 1000);
          this._saveByTime  = saveBy;
        }
        return;
      } else {
        if (this._saveTimeout) {
          clearTimeout(this._saveTimeout);
        }
        this._saveTimeout = undefined;
        this._saveByTime  = undefined;
      }

      // actual save process
      this._jsondata.data = this.currentSession;
      this._jsondata.push();
    },

    currentSession: {
      descriptor: true,
      get: function() {
        return {
          mainView: vispa.mainViewManager.getState(),
          time: Date.now(),
        };
      }
    },

    storedSession: {
      descriptor: true,
      get: function () {
        return this._jsondata.data;
      },
    },
  });

  return SessionManager;
});
