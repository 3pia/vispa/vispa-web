/**
 * utils
 * =================
 */
define(["require"], function(require) {

  var emptyCallback = function(){};

  var propertyConfig = {
    descriptor: true,
    value: null,
    enumerable: true,
    configurable: true,
  };

  var validOSs = ["mac", "win", "linux"];
  var noOSPrefixRE = new RegExp("^("+validOSs.join("|")+"):");
  var sizeSuffixes = ["", "k", "M", "G", "T", "P", "E"];

  var Utils = {

    /** indent: 1
     * .. js:function:: format(template, ...)
     *
     *    Substitutes each occurence of the curly brackets enclosed number *n* with the with the
     *    *n+1* additional argument.
     *
     *    :param String template: The template to use.
     *    :param String ...: The substitute.
     *
     *    :return: The processed string.
     *    :rtype: String
     *
     *    :example: format("foo {0}!", "bar") == "foo bar!"
     */
    format: function(template) {
      var args = Array.prototype.slice.call(arguments, 1);
      return template.replace(/\{(\d+)\}/g, function(_, arg) {
        return arg in args ? args[arg] : _;
      });
    },

    /** indent: 1
     * .. js:function:: uuid([length])
     *
     *    Generates a UUID cosisting of upper and lower case characters and numbers at random.
     *
     *    :param Number length: The length of the rquested UUID. Defaults to *10*.
     *
     *    :return: The UUID.
     *    :rtype: String
     */
    uuid: function(length) {
      length = parseInt(length) || 10;
      var key = "";
      var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
      for (var i = 0; i < length; i++)
        key += chars.charAt(Math.floor(Math.random() * chars.length));
      return key;
    },

    /** indent: 1
     * .. js:function:: capitalize(string)
     *
     *    Capitalizes the first character of the given string with toUpperCase, hence is not
     *    resprecting the locale specific case definitions.
     *
     *    :param String string: The input string.
     *
     *    :return: The capitalized string.
     *    :rtype: String
     */
    capitalize: function(string) {
      return string.substr(0, 1).toUpperCase() + string.substr(1);
    },

    /** indent: 1
     * .. js:function:: callback(callback)
     *
     *    Used for callback type checking and defaulting.
     *
     *    :param Function callback: The callback to check. If it is not a function it will return
     *    an empty function.
     *
     *    :return: A proper callback function, the provided one if it is valid.
     *    :rtype: Function
     */
    callback: function(callback) {
      return $.isFunction(callback) ? callback : emptyCallback;
    },

    /** indent: 1
     * .. js:function:: ROP(value)
     *
     *    Return a property descriptor for a read-only property, which is sitll configurable and
     *    enumerable. It also containes the descriptor flag required for JClass property
     *    definitions.
     *
     *    :param Any value: The value of the property.
     *    :return: A property descriptior that is JClass ready.
     *    :rtype: Object
     */
    ROP: function(value) {
      propertyConfig.value = value;
      return propertyConfig;
    },

    /** indent: 1
     * .. js:function:: setROP(object, name, value)
     *
     *    Sets a read only (but still configurable and enumerable) property on the given object.
     *
     *    :param Object object: The object to set the property on.
     *    :param String name: The name of the property.
     *    :param Any value: The value to set the property to.
     *
     *    :return: The *value*.
     *    :rtype: Any
     */
    setROP: function(object, name, value) {
      propertyConfig.value = value;
      Object.defineProperty(object, name, propertyConfig);
      return value;
    },

    /** indent: 1
     * .. js:function:: asyncRequire(path)
     *
     *    Interfaces the a require calls into the async callback system.
     *
     *    :param String path: The path to require.
     *
     *    :return: A function that prefroms the require call and accepts an
     *    	callback(err, returnValue)
     *    :rtype: Function
     */
    asyncRequire: function(path) {
      var path = path;
      return function(callback) {
        require([path], function(module){
          callback(null, module);
        },function(err) {
          callback(Utils.format(
            "'{0}' failed to load due to: {1}",
            err.requireModules[0].split("/")[1],
            err.requireType
          ));
        });
      };
    },

    /** indent: 1
     * .. js:function:: formatDate(data[, options])
     *
     *		Format s date object appropriately.
     *
     *    :param Date date: The date to format.
     *    :param Object options: Further options for the formatting. (none supported yet)
     *
     *    :return: The formatted date.
     *    :rtype: String
     */
    formatDate: function(date, options) {
      if (!(date instanceof Date)) return "";
      var opts = {}; // TODO: fill these appropriately
      try {
        return date.toLocaleString(vispa.locale, opts);
      } catch (e) {
        return date.toLocaleString();
      }
    },

    /** indent: 1
     * .. js:function:: formatSize(bytes[, opts])
     *
     *    :param Number bytes: The number of bytes.
     *    :param Object opts: Further configuration options.
     *    :param Number opts.digits: The number of after comma digits. Default: *0*
     *    :param String opts.suffix: The unit suffix, if *false* no suffix is added at all.
     *      Default: *"B"*
     *    :param Boolean opts.base10: Whether to use base10 or base2. Defalt: *False*
     *    :param Number opts.bias: The bias factor over wich to use the next larger step.
     *    :param String opts.space: The space character to use. Default *"&nbsp;"*.
     *      Default: *2.4*
     */
    formatSize: function(bytes, opts) {
      opts        = opts        || {};
      opts.digits = opts.digits || 0;
      opts.suffix = opts.suffix || "B";
      opts.base10 = opts.base10 || false;
      opts.bias   = opts.bias   || 2.4;
      opts.step   = opts.base10 ? 1000 : 1024;
      opts.space  = opts.space  || "&nbsp;";
      var num = bytes;
      var ret = String(bytes);
      var suf = "";
      for(var i = 0; i < sizeSuffixes.length; i++) {
        if (num < opts.bias * opts.step) {
          ret = num.toFixed(opts.digits);
          suf = sizeSuffixes[i] + (i && !opts.base10 ? "i": "");
          break;
        }
        num /= opts.step;
      }
      if (opts.suffix !== false)
        ret += opts.space + suf + opts.suffix;
      return ret;
    },

    /** indent: 1
     * .. js:function:: shortcutPP(definition[, os])
     *
     *    Shortcut preprocessing
     *
     *    :param String definition: The shortcuts definition.
     *    :param String os: The OS for which the shortcuts should be preprocessed. If not given,
     *      the value from *vispa.device.os* will be used.
     *
     *    :return: The shortcut ready to be handles by the jQuery.Shortcuts plugin.
     *    :rtype: String
     */
    shortcutPP: function(definition, os) {
      if (!~validOSs.indexOf(os)) {
        os = vispa.device.os;
      }
      var m = definition.match(os+":(\\S+)");
      if (m) return m[1];
      m = definition.match(/\S+/g);
      for (var i in m) {
        if (!noOSPrefixRE.test(m[i])) {
          return m[i];
        }
      }
    },

    /** indent: 1
     * .. js:function:: throttle(func[, delay[, update]])
     *
     *    Throttle the call of the given function.
     *
     *    :param Function func: the function to throttle.
     *    :param Number delay: the minimum delay, if *undefined* it will try to use
     *      *requestAnimationFrame* or a delay of *0*.
     *    :param Boolean update: Whether to call the function with the most recent arguments.
     *
     *    :return: The wrapped function.
     *    :rtype: Function
     */
    throttle: function(func, delay, update) {
      var that;
      var args = [];
      var active = false;
      var runner = function() {
        func.apply(that, args);
        active = false;
      };
      return function() {
        if (!active || update) {
          that = this;
          args = [].slice.call(arguments);
        }
        if (!active) {
          active = true;
          if (window.requestAnimationFrame && delay === undefined)
            window.requestAnimationFrame(runner);
          else
            setTimeout(runner, delay || 0);
        }
      };
    },

    /** indent: 1
     * .. js:function:: cleanPath(path)
     *
     *    :param String path: The path to clean up.
     *
     *    :return: The cleaned path.
     *    :rtype: String
     */
    cleanPath: function (path) {
      return path.
        replace(/\/+/g, "/").
        replace(/\/\.(?=\/|$)/g, "").
        replace(/\/[^\/]+\/\.\.(?=\/|$)/g, "");
    },

    /** indent: 1
     * .. js:function:: singleQuote(string[, trigger])
     *
     *    :param String string: The string to quote.
     *    :param String|RexEx trigger: The trigger for quoting to *search* for in *string*. The
     *      default will match anything.
     *
     *    :return: The cleaned path.
     *    :rtype: String
     */
    singleQuote: function (string, trigger) {
      if (trigger === undefined)
        trigger = /^/;
      if (~string.search(trigger))
        return "'" + string.replace(/'/g, "'\"'\"'") + "'";
      else
        return string;
    }
  };

  return Utils;
});
