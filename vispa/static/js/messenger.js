define([
  "jquery",
  "vispa/common/dialog",
  "vispa/module",
  "text!html/index/log.html"
], function(
  $,
  Dialog,
  Module,
  tLog
) {

  var defaultContext = "default";

  var Messenger = Module._extend({

    init: function init() {
      init._super.call(this, "messenger", {
        label: "Messenger",
        iconClass: "fa-bell",
        items: {
          logDuration: {
            level: 2,
            label: "Message duration",
            description: "The duration (in seconds) a message is shown.",
            range: [0, 20, 0.5],
            type: "float",
            global: true,
            value: 5,
          },
          logAlignment: {
            level: 2,
            label: "Message alignment",
            description: "The alignment of messages at the lower screen border.",
            options: ["left", "right"],
            flat: true,
            type: "string",
            global: true,
            value: "left",
          },
          logLevel: {
            level: 2,
            label: "Message level",
            description: "Upto this log level messages will be shown.",
            options: ["all", "trace", "debug", "info", "warn", "error", "fatal", "off"],
            type: "string",
            global: true,
            value: "info",
          },
          showLogLevel: {
            level: 2,
            label: "Show message level",
            description: "Show the log level in log messages.",
            type: "boolean",
            global: true,
            value: false,
          }
        }
      });

      this.varData = {
        contexts: [],
      };
      this.nodes = {
        logContainer: $(".vispa-logs"),
      };

      this.logLevels = {
        all: 0,
        trace: 2,
        debug: 4,
        info: 6,
        warn: 8,
        error: 10,
        fatal: 12,
        off: 20
      };
      this.stacks = {};

      this.prefs.watch("logAlignment", this.setLogAlignment.bind(this));
      this.setLogAlignment();
    },

    addContext: function(context, fetch) {
      if (!~this.varData.contexts.indexOf(context))
        this.varData.contexts.push(context);
      if (fetch || fetch == null)
        this.fetch(context);
      return this;
    },

    removeContext: function(context) {
      var idx = this.varData.contexts.indexOf(context);
      if (~idx)
        this.varData.contexts.splice(idx, 1);
      return this;
    },

    messageCount: function(context) {
      var stack = this.stacks[context];
      if (!stack)
        return 0;
      return stack.length;
    },

    fetch: function(context) {
      var self = this;

      // TODO: fetch with namespaces via "."

      var stack = this.stacks[context];
      if (!stack) {
        return this;
      }
      $.each(stack, function(i, data) {
        var method = "_" + data.method;
        if (self[method]) {
          self[method](data);
        }
      });
      // empty the stack
      stack.length = 0;
      return this;
    },

    _alert: function(data) {
      Dialog.alert(data.message, data);
      return this;
    },

    _prompt: function(data) {
      Dialog.prompt(data.message, data.callback, data);
      return this;
    },

    _confirm: function(data) {
      Dialog.confirm(data.message, data.callback, data);
      return this;
    },

    _dialog: function(data) {
      Dialog.dialog(data);
      return this;
    },

    _pushMessage: function(data) {
      data = $.extend(true, {
        context: defaultContext,
        backdrop: false
      }, data);

      var stack = this.stacks[data.context] = this.stacks[data.context] || [];
      stack.push(data);

      // fetch?
      var fetch = data.context == defaultContext || ~this.varData.contexts.indexOf(data.context);
      if (fetch) {
        this.fetch(data.context);
      }

      return this;
    },

    alert: function(message, opts) {
      var data = {method: "alert"};
      if ($.isPlainObject(message)) {
        $.extend(true, data, message);
      } else {
        data.message = message;
        $.extend(true, data, opts);
      }
      this._pushMessage(data);
      return this;
    },

    prompt: function(message, callback, opts) {
      var data = {method: "prompt"};
      if ($.isPlainObject(message)) {
        $.extend(true, data, message);
      } else {
        data.message = message;
        data.callback = callback;
        $.extend(true, data, opts);
      }
      this._pushMessage(data);
      return this;
    },

    confirm: function(message, callback, opts) {
      var data = {method: "confirm"};
      if ($.isPlainObject(message)) {
        $.extend(true, data, message);
      } else {
        data.message = message;
        data.callback = callback;
        $.extend(true, data, opts);
      }
      this._pushMessage(data);
      return this;
    },

    dialog: function(opts) {
      var data = { method: "dialog" };
      $.extend(true, data, opts);
      this._pushMessage(data);
      return this;
    },

    // logger
    _levelToNumber: function(level) {
      if ($.isNumeric(level))
        return level;
      else {
        var idx = this.prefs.vue.items.logLevel.options.indexOf(level);
        return ~idx ? idx : 3;
      }
    },

    _levelToString: function(level) {
      if (!$.isNumeric(level))
        return level;
      else {
        return this.prefs.vue.items.logLevel.options[level] || "info";
      }
    },

    log: function(level, message, icon) {
      level = this._levelToNumber(level);
      if (level < this._levelToNumber(this.prefs.get("logLevel")))
        return null;

      var data = {message: message};
      if (this.prefs.get("showLogLevel"))
        data.level = " (" + this._levelToString(level) + ")";

      var log = $(tLog).appendTo(this.nodes.logContainer)
        .render(data, {
          icon: {
            class: function() {
              return icon || "glyphicon glyphicon-info-sign";
            }
          }
        }).slideDown(200);

      setTimeout(function() {
        log.slideUp(200, log.remove.bind(log));
      }, this.prefs.get("logDuration") * 1000);

      return log;
    },

    debug: function() {
      var args = Array.prototype.slice.call(arguments);
      args.unshift("debug");
      return this.log.apply(this, args);
    },
    info: function() {
      var args = Array.prototype.slice.call(arguments);
      args.unshift("info");
      return this.log.apply(this, args);
    },
    warn: function() {
      var args = Array.prototype.slice.call(arguments);
      args.unshift("warn");
      return this.log.apply(this, args);
    },
    error: function() {
      var args = Array.prototype.slice.call(arguments);
      args.unshift("error");
      return this.log.apply(this, args);
    },

    setLogAlignment: function() {
      $(".vispa-logs-wrapper")
        .toggleClass("vispa-logs-wrapper-left",  this.prefs.get("logAlignment") == "left")
        .toggleClass("vispa-logs-wrapper-right", this.prefs.get("logAlignment") == "right");

      return this;
    }
  });

  return Messenger;
});
