/**
 * module
 * =================
 */
define([
  "emitter",
  "vispa/mixins/logger",
  "vispa/mixins/preferences",
  "vispa/utils"
], function(
  Emitter,
  LoggerMixin,
  PreferencesMixin,
  Utils
) {

  /**
   * .. js:class:: Module(name, prefs)
   *
   *    :extends: Emitter
   *    :mixins: Logger
   *    :mixins: Preferences
   *
   *    A class for internal modules without any visual representation.
   *
   *    :param String name: The name of the module.
   *    :param Object prefs: The preference to use for this module. See :js:mixin:`PreferencesMixin` for details.
   *
   *    :throws Error: If the name is not a string
   */

  var Module = Emitter._extend($.extend({},
    LoggerMixin,
    PreferencesMixin,
  {

    init: function init(name, prefs) {
      if (typeof name != "string") {
        throw new Error("name must be a string");
      }

      init._super.call(this, { wildcard: true });

      LoggerMixin.init.call(this, "core."+name);
      PreferencesMixin.init.call(this, "core", name, prefs);

      /** indent: 1
       * .. js:attribute:: name read-only
       *
       *    :type: String
       *
       *    The name of the module.
       */
      Utils.setROP(this, "name", name);
    },

  }));

  return Module;
});
