/**
 * filehandler
 * =================
 */
define([
  "jquery",
  "jclass",
  "vispa/utils",
], function(
  $,
  JClass,
  Utils
) {
  // some globals
  // the "manager"
  var Manager = JClass._extend({
    init: function () {
      this.clsBib = {};
      this.handlersBase = {};
      this.handlersOpen = {};
    },

    makeMenu: function(info) {
      var menu = this._filterMenu(this.handlersBase, info);
      if (!info.data && !info.items) { // nothing directly hit (just in a folder)
        info.data = {
          root: ".",
          name: ".",
          ext: "",
          type: "d",
        };
      }
      var openMenu = this._filterMenu(this.handlersOpen, info);
      if (Object.keys(openMenu).length > 1)
        menu.openWith = {
          label: "Open with ...",
          iconClass: "fa-file",
          position: 5,
          items: openMenu,
        };
      // add divider
      menu.divider1 = {divider: true, position: 10};
      menu.divider2 = {divider: true, position: 100};
      if (info.menuPP)
        menu = info.menuPP(menu) || {};
      return menu;
    },
    action: function(what, info) {
      var handler = this.handlersBase[what];
      if (!handler)
        throw Error("so such file handler: "+what);
      else
        return handler.attempt(info);
    },
    addOpen: function() {
      var args = [].slice.call(arguments);
      return this.add.apply(this, ["open"].concat(args));
    },
    addExt: function(cls, extension) {
      cls = (this.clsBib[cls] || cls)._extend(extension);
      var args = [].slice.call(arguments, 2);
      return this.add.apply(this, [cls].concat(args));
    },
    add: function(cls) {
      cls = (this.clsBib[cls] || cls);
      return new (cls.bind.apply(cls, arguments));
    },

    _filterMenu: function(handlers, info) {
      var ret = {};
      for (var i in handlers) {
        var h = handlers[i];
        var m = h.check(info) && h.menu(info);
        if (m)
          ret[i] = m;
      }
      return ret;
    },
    _bestOpen: function(info, _handlers) {
      var b, m = _handlers || this._filterMenu(this.handlersOpen, info);
      for(var h in m)
        if (!b || m[h].position < b.position)
          b = m[h];
      return b;
    },
  });

  var M = new Manager();

  // base classes
  M.clsBib.base = JClass._extend({
    init: function(name) {
      Utils.setROP(this, "name", name);
      if (this._peers[name])
        throw Error("this filehandler is already defined: "+name);
      else
        this._peers[name] = this;
    },
    _peers: M.handlersBase,
    attempt: function(info, ev) {
      if (ev instanceof Event)
        info = $.extend({}, info, {event: ev});
      return this.check(info) ? this.run(info) || true : false;
    },

    // to be overwritten
    menuTmpl: undefined,
    check: function(info) {
      return info.view && info.pathBase;
    },
    run: null,
    menu: function(info) {
      var r = this.attempt.bind(this, info);
      return $.extend({}, this.menuTmpl, {
        callback: function() {
          r();
        },
      });
    },
  });

  M.clsBib.single = M.clsBib.base._extend({
    fullPath: function (info) {
      return Utils.cleanPath(info.pathBase + "/" + info.data.name);
    },
    ext: function (info) {
      if (!info.data) return;
      if (info.data.type == "d")
        return false;
      var f = info.data.name.split(".");
      if (f[0] == "")
        f = f.slice(1);
      var l = f.length;
      if (l < 2)
        return "";
      if (l > 2 && f[l-1] && f[l-2].toLowerCase() == "tar")
        return f.slice(-2).join(".");
      return f.pop();
    },

    check: function check(info) {
      return check._super.call(this, info) && info.data && info.data.name;
    },
  });

  M.clsBib.open = M.clsBib.single._extend({
    init: function init(name, menu, saf, fext, fextNot) {
      init._super.call(this, name);
      this.menuTmplF = menu;
      this.saf = saf;
      this.fext = fext;
      if (fextNot === undefined && !this._testExt(false, this.fext))
        fextNot = false;
      this.fextNot = fextNot;
    },
    _peers: M.handlersOpen,
    _testExt: function (ext, value) {
      return value instanceof Array ? ~value.indexOf(ext) : (value === ext);
    },

    check: function check(info) {
      var ext = this.ext(info);
      return check._super.call(this, info) &&
            (this.fext === undefined || this._testExt(ext, this.fext)) &&
            !this._testExt(ext, this.fextNot);
    },
    menu: function menu(info) {
      var m = {};
      var e = this.ext(info);
      for (var k in this.menuTmplF) {
        var v = this.menuTmplF[k];
        m[k] = $.isFunction(v) ? v.call(this, e, info) : v;
      }
      return $.extend(m, menu._super.call(this, info));
    },
    run: function (info) {
      return info.view.spawnInstance.apply(info.view, this.saf.call(this, this.fullPath(info), info));
    },
  });

  M.clsBib.multi = M.clsBib.base._extend({
    items: function(info) {
      var r = info.items;
      if (!r)
        return [];
      if (this.filter)
        return r.filter(this.filter);
      else
        return r.slice();
    },
    fullPaths: function (info, processing, _items) {
      return (_items || this.items(info)).map(function (it) {
        if (processing)
          it.processing = true;
        return Utils.cleanPath(info.pathBase + "/" + it.name);
      });
    },

    // to be overwritten as needed
    check: function check(info) {
      return check._super.call(this, info) && this.items(info).length;
    },
    filter: null,
  });

  return M;
});
