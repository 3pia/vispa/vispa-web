/**
 * VISPA
 * =====
 *
 * TODO: introduction to VISPA
 */
define([
  "jquery",
  "emitter",
  "jclass",
  "vispa/mixins/ajax",
  "vispa/mixins/logger",
  "vispa/common/dialog",
  "vispa/common/socket",
  "vispa/ui/viewmanager",
  "vispa/ui/uinode",
  "vispa/ui/split",
  "vispa/filehandler",
  "vispa/filehandler2",
  "vispa/prefs/manager",
  "vispa/messenger",
  "vispa/session",
  "vispa/workspace",
  "vispa/extension",
  "vispa/jsondata",
  "vispa/views/main",
  "vispa/vue/mainmenu",
  "vispa/vue/menu",
  "vispa/vue/sidebar",
  "vue/vue",
  "async",
  "vispa/utils",
  "text!html/index/dialog/feedback.html",
  "vendor/uaparser/uaparser",
  // TODO: dafuq is up this these jquery plugins, shouldn't the be in the config or somewhere else?
  "vendor/jquery/plugins/logger/jquery.logger",
  "vendor/jquery/plugins/hotkeys/jquery.hotkeys",
  "vendor/jquery/plugins/shortcuts/jquery.shortcuts",
  "vendor/jquery/plugins/localesorter/jquery.localesorter",
  "vendor/jquery/plugins/polling/jquery.polling",
  "vendor/jquery/plugins/ellipsis2/jquery.ellipsis2",
  "css!styles/index.css",
], function(
  $,
  Emitter,
  JClass,
  AjaxMixin,
  LoggerMixin,
  Dialog,
  Socket,
  ViewManager,
  UINode,
  Split,
  FileHandler,
  FileHandler2,
  PrefsManager,
  Messenger,
  SessionManager,
  Workspace,
  Extension,
  JSONData,
  MainView,
  MainMenu,
  Menu,
  Sidebar,
  Vue,
  async,
  Utils,
  tFeedback,
  UAParser
) {

  var Vispa = Emitter._extend($.extend({},
  AjaxMixin,
  LoggerMixin,
  {

    init: function init(args) {
      var self = this;
      init._super.call(this, { wildcard: true });
      window.vispa = this;

      // store args
      this.args = args;

      // save the startup time
      this.startupTime = new Date();

      // a unique window id
      this.windowId = Utils.uuid(16);

      // store for variable data (is this still needed)
      this.varData = {
        socketDisconnectTimeout: null,
        socketDisconnectDialog: null,
      };

      // fragment data store
      this.fragmentData = {
        loadLock: false,
        baseTitle: window.document.title,
        lastData: {}
      };

      // init mixins used
      LoggerMixin.init.call(this, "vispa.main");
      AjaxMixin.init.call(this,
        this.args.global.url.dynamicBase,
        this.args.global.url.staticBase,
        "_windowId=" + this.windowId
      );

      // set logging level
      $.Logger().level(args.global.logLevel);
      // Vue.config.debug = args.global.devMode;

      // small but helpful device flags
      this.device = {
        os      : (navigator.platform.match(/mac|win|linux/i) || ["other"])[0].toLowerCase(),
        hasTouch: $.support.touch,
      };
      this.device.isWin   = (this.device.os == "win");
      this.device.isMac   = (this.device.os == "mac");
      this.device.isLinux = (this.device.os == "linux");

      this.callbacks = new Emitter();

      // now start everything

      // misc stuff
      this.utils       = Utils;
      this.wsManager   = Workspace._members;
      this.extManager  = Extension._members;
      this.jsondata    = JSONData._members;
      this.fileHandler = new FileHandler();
      this.fileHandling = FileHandler2;

      // modules
      this.preferences    = new PrefsManager(); // actually not a module but a vuejs
      this.sessionManager = new SessionManager();
      this.messenger      = new Messenger();

      // register extra prefs
      this.wsManager.initPrefs();

      // setup other events
      $(window).bind("beforeunload", function() {
        if (!self.args.global.devMode && !self.varData.socketDisconnectDialog) {
          return "";
        }
      });

      // main GUI with split
      this.mainSplit = new Split();
      $("body").prepend(this.mainSplit.$node);
      this.mainViewManager = new ViewManager(".vispa-body");
      this.mainSplit.attach(this.mainViewManager);
      // static user menu items
      var footerItems = {};
      if (this.args.global.useFeedback) {
        footerItems.feedback = {
          iconClass: "fa-comment",
          label: "Feedback",
          position: 1001,
          callback: this.showFeedbackDialog.bind(this),
        };
      }
      var footerMenuItems = {
        userHeader: {
            iconClass: "fa-user",
            label: this.args.user.name,
            header: true,
            position: 0,
        },
        userLogout: {
            label: "Logout",
            iconClass: "fa-sign-out",
            position: 2,
            callback: function () {
              window.location.href = this.applyDefaultQuery(this.dynamicURL("logout"));
            }.bind(this)
        },
        nonWsDivider: {
          divider: true,
          position: 1000,
        }
      };
      if (this.args.user.isGuest) {
        footerMenuItems.register = {
          label: "Register",
          iconClass: "fa-edit",
          position: 1,
          callback: this.registerGuest.bind(this),
        };
      } else {
        footerMenuItems.userResetPassword = {
          label: "Reset Password",
          iconClass: "fa-key",
          position: 1,
          callback: this.resetPassword.bind(this),
        };
      }
      // sidebar
      this.sidebar = new Sidebar({
        el: ".vispa-sidebar",
        data: {
          size: 20,
          items: {
            "ball": {
              imgClass: "vispa-header-logo",
              position: -Infinity,
              img: true,
              src: this.staticURL("img/vispa/header_ball_margin.png"),
              callback: function () {
                this.$root.active = !this.$root.active;
              }
            }
          },
          footer: {
            items: footerItems,
            menu: {
              label: this.args.user.name,
              iconClass: "fa-user",
              btnClass: "btn-default",
              items: footerMenuItems
            }
          },
        },
        watch: {
          active: function () {
            if (this.active) {
              $(this.$el).parent().css({
                "flex-basis": String(this.size) + "%",
                "flex-shrink": "",
              });
              vispa.mainViewManager.$node.parent().css("flex-basis", String(100-this.size) + "%");
              vispa.mainSplit.resizeEnabled = true;
            } else {
              $(this.$el).parent().css({
                "flex-basis": "40px",
                "flex-shrink": "0",
              });
              vispa.mainViewManager.$node.parent().css("flex-basis", "100%");
              vispa.mainSplit.resizeEnabled = false;
            }
            vispa.mainViewManager.emit("resize");
            this.computeMaxItems();
          }
        },
        // trigger correct behavior of main split
      });
      // sidebar UINode for split
      this._sidebarUINode = new UINode();
      this._sidebarUINode.$node = $(".vispa-sidebar");
      this._sidebarUINode.on("resize", function () {
        this.sidebar.size = parseInt($(this.sidebar.$el).parent().css("flex-basis"));
        this.nextTick(this.sidebar.computeMaxItems);
      }.bind(this));
      this.mainSplit.attach(this._sidebarUINode, this.mainViewManager, undefined, this.sidebar.size);

      // main menu
      this.mainMenu = new MainMenu({
        data: {
          wsData: Workspace._members._vueData,
          items: {
            nonWsDivider: {
              divider: true,
              position: 1000,
            }
          }
        },
      });
      this.mainMenu.$mount();

      $(window).bind("resize", Utils.throttle(function(){
        self.mainViewManager.emit("resize");
      }));

      // now start all the loading
      async.parallel([
        Extension._members.load.bind(Extension._members),
        async.series.bind(async, [ // the workspaces need the socket for status communication
          this.setupSocket.bind(this),
          Workspace._members.load.bind(Workspace._members),
        ])
      ], function(err) {
        if (err) {
          this.logger.error("error during startup", err);
          return;
        }
        this.logger.info("everything loaded");

        // needs workspaces
        this.jsondata.loadKey("preferences", vispa.args.preferences);

        // we may need to add workspaces
        if (!Workspace._members.workspaceCount) {
          var core = Extension._members.byName("core");
          if (core) {
            core.openWorkspaceAddDialog();
          }
        } else {
          // auto-select single connected workspace
          var c, ws = Workspace._members.workspaces;
          for(var i in ws) {
            var w = ws[i];
            var s = w.state;
            if (s == "connected") {
              c = c ? null : w;
            } else if (s != "disconnected")
              c = null;
            if (c == null) break;
          }
          if (c)
            Workspace._members.current = c;
        }

        // start session managment
        this.sessionManager._jsondata._pullProcess(null, vispa.args.session);

        // bind mainViewManager events
        this.mainViewManager.on("viewmanager.*"    ,
          this.sessionManager.save.bind(this.sessionManager, 20));
        this.mainViewManager.on("viewmanager.blur" ,
          this.sessionManager.save.bind(this.sessionManager, 1));
        this.mainViewManager.on("viewmanager.focus", function(){
          if (this.focusInstance && this.focusInstance.workspace) {
            vispa.wsManager.current = this.focusInstance.workspace;
          }
          vispa.sessionManager.save(1);
        }.bind(this.mainViewManager));

        // configure syling for other ws
        var applyOtherStyle = function(style) {
          vispa.mainViewManager.$node.attr("data-ws-other-style", style);
        };
        applyOtherStyle(this.wsManager.prefs.get("otherStyle"));
        this.wsManager.prefs.watch("otherStyle", applyOtherStyle);

        // start session manager
        this.sessionManager.run();

        // apply the current fragment
        window.addEventListener("popstate", this.applyFragment.bind(this));
      }.bind(this));

    },

    $messageContainer: {
      descriptor: true,
      get: function() {
        return $(document.body);
      }
    },

    /** indent: 1
     * .. js:function:: spawnInstance(extName, viewName[, args[, viewArgs[, baseArgs]]])
     *
     *    Spawn a new view from the given parameters.
     *
     *    :param String extName: The name of the extension that has the view.
     *    :param String viewName: The anem of the view to open.
     *    :param Object args: The args to be provided to the view.
     *    :param Object viewArgs: The viewArgs to be provided to the view.
     *    :param Object baseArgs: The baseArgs to be provided to the view.
     *
     *    :return: The view that was opened. It is most likely not ready to use yet.
     *    :rtype: BaseView
     *
     *    :throws Error: If either the extension of the view was not found.
     */
    spawnInstance: function (extName, viewName, args, viewArgs, baseArgs) {
      var ext  = Extension._members.byName(extName);
      if (!ext ) throw new Error("extension does not exist: " + extName);
      var view = ext.getView(viewName);
      if (!view) throw new Error("view does not exist: " + viewName);
      return view._members.spawn(
        args || {},
        viewArgs || {},
        $.extend({
          workspace: this.wsManager.current
        }, baseArgs)
      );
    },

    setupSocket: function (callback) {
      if (this.socket) return;
      var self = this;

      callback = Utils.callback(callback);

      // initialize the socket
      var url = this.args.global.socket.url + "?" + this.defaultQuery;
      var params = $.extend({}, this.args.global.socket, {
        pollDelay      : 0,
        pollGracePeriod: 15000,
        pollParams     : function() {
          if (this.nPolls === 0) {
            return { timeoutms: 0 };
          }
        },
      });
      params.pollUrl += "?" + this.defaultQuery;
      params.sendUrl += "?" + this.defaultQuery;
      this.socket = new Socket(url, params);

      this.socket.once("open", function() {
      });
      this.socket.on("open", function() {
        self.logger.info("socket connected (" + self.socket.type() + ")");
        self.messenger.info("Server connected", "glyphicon glyphicon-ok");
        if (self.varData.socketDisconnectTimeout) {
          window.clearTimeout(self.varData.socketDisconnectTimeout);
          self.varData.socketDisconnectTimeout = null;
        }
        if (self.varData.socketDisconnectDialog) {
          self.varData.socketDisconnectDialog.close();
          self.varData.socketDisconnectDialog = null;
        }
      });
      this.socket.once("open", callback.bind(null, null));
      this.socket.on("close", function() {
        self.logger.info("socket closed");
        if (!self.varData.socketDisconnectTimeout) {
          self.varData.socketDisconnectTimeout = window.setTimeout(function() {
            self.messenger.info("Server disconnected", "glyphicon glyphicon-remove");
            self.varData.socketDisconnectDialog = Dialog.dialog({
              closable: false,
              iconClass: "fa-bolt",
              title: "Server disconnected",
              body: "The server may be unreachable right now. Reload the page or wait until the " +
                    "server is available again.",
              buttons: {
                reload: {
                  iconClass: "fa-refresh",
                  label: "Reload",
                  class: "btn btn-primary",
                  callback: window.location.reload.bind(window.location),
                }
              }
            });
          }, 25e3);
        }
      });

      // re-emit topics
      this.socket.on("message", function(message) {
        try {
          var data = message.data;
          data = $.isPlainObject(data) ? data : JSON.parse(data);
          if (data.topic !== null) {
            self.nextTick(function() {
              self.socket.emit(data.topic, data.data);
            });
          }
        } catch (err) {}
      });

      // close the socket when we received a stop or onbeforeunload
      var closeSocket = function() {
        self.socket.close();
        return undefined;
      };
      this.socket.on("stop", closeSocket);

      this.socket.connect();
    },

    format: function() {
      this.logger.warning("using vispa.format is deprecated");
      return Utils.format.apply(Utils, arguments);
    },

    uuid: function(length) {
      this.logger.warning("using vispa.uuid is deprecated");
      return Utils.uuid(length);
    },

    capitalize: function(str) {
      this.logger.warning("using vispa.capitalize is deprecated");
      return Utils.capitalize(str);
    },

    showFeedbackDialog: function() {
      if (!this.args.global.useFeedback) {
        return null;
      }

      // acquire data from user agent
      var uaData = (new UAParser()).getResult();
      var bd = uaData.browser;
      var ed = uaData.engine;
      var od = uaData.os;
      var dd = uaData.device;

      var uaText = "Your feedback\n\n";
      if (bd.name) {
        uaText += "Browser: " + bd.name + " " + bd.major + " (" + bd.version + ")\n";
      }
      if (ed.name) {
        uaText += "Engine: " + ed.name + " (" + ed.version + ")\n";
      }
      if (od.name) {
        uaText += "OS: " + od.name + " (" + od.version + ")\n";
      }
      if (dd.vendor) {
        uaText += "Device: " + dd.vendor + " " + dd.type + " (" + dd.model + ")\n";
      }
      uaText += "URL: " + window.location.href + "\n";

      var self = this;
      return new Dialog({
        data: {
          body: true,
          feedback: uaText,
          anonymous: false,
          rootClass: "feedback",
          iconClass: "fa-comment",
          title: "Feedback",
          buttons: {
            cancel: {
              label: "Cancel",
              pos: 1,
            },
            send: {
              label: "Send",
              class: "btn-primary",
              pos: 2,
              callback: function() {
                self.POST("ajax/feedback", {
                  content:   this.feedback,
                  anonymous: this.anonymous
                });
              },
            }
          },
        },
        partials: {
          body: tFeedback,
          footerLeft:
            "<label><input type=\"checkbox\" v-model=\"anonymous\"> Send anonymous</label>",
        },
        onReady: function() {
          this.$els.textarea.setSelectionRange(0, 13);
          this.$els.textarea.focus();
        }
      });
    },

    registerGuest: function () {
      var self = this;
      var msg = "Please note that your guest account will be deleted.";
      self.messenger.confirm(msg, function (result) {
        if (result) {
          window.location.href = self.dynamicURL("logout?path=/login#register-pane");
        }
      }, {
        iconClass: "fa-question",
        title: "Create account",
      });
    },

    resetPassword: function () {
      this.POST("/ajax/forgotpassword", {username: this.args.user.name}, function(err) {
        if (err) {
          vispa.messenger.alert(err.message, {
            iconClass: "fa-exclamation",
            title: "Error",
          });
        } else {
          vispa.messenger.alert("Further instructions have been sent to your mail address!", {
            iconClass: "fa-envelope",
            title: "Mail sent"
          });
        }
      });
    },

    nextTick: function(callback, delay) {
      window.setTimeout(callback, delay || 0);
      return this;
    },

    saveFragment: function() {
      if (this.fragmentData.loadLock) return;

      var fragment      = "";
      var data          = {};
      var title         = "";
      var focusInstance = this.mainViewManager.focusInstance;

      if (focusInstance) {
        var extName  = focusInstance.extension.name;
        var viewName = focusInstance.name;
        var ws       = focusInstance.workspace;

        data.state       = focusInstance.state.obj;
        data.viewId      = focusInstance.id;
        data.workspaceId = ws ? ws.id : null;

        fragment = focusInstance.getFragment();

        if (fragment === undefined)
          fragment = "";

        fragment = "workspace:" + (ws ? ws.name : "") + "/" + extName + ":" +
          (extName == viewName ?  "" : viewName) + ":" + fragment;

        title = focusInstance.label;
      }

      title = this.fragmentData.baseTitle + (title ? " - " + title : "");

      if ( this.fragmentData.lastFragment         != fragment ||
           this.fragmentData.lastData.workspaceId != data.workspaceId ||
           this.fragmentData.lastData.viewId      != data.viewId ) {

        // check if we can have a drop in replacement (for blured states)
        var replace = this.fragmentData.lastFragment === "" ||
          fragment.indexOf(this.fragmentData.lastFragment) !== -1;

        this.fragmentData.lastFragment = fragment;
        this.fragmentData.lastData     = data;

        // just encode #, the rest should be fine
        var safeFragment = fragment.replace("#",encodeURIComponent("#"));

        History[replace ? "replaceState" : "pushState"](data, title, "?" + safeFragment);
      }
    },

    applyFragment: function() {
      var state = History.getState();
      var i     = state.url.indexOf("?");
      if (!~i) return false;
      var fragment = state.url.substr(i + 1);
      fragment     = decodeURIComponent(fragment);
      var data     = state.data || {};

      // did it change?
      if ( this.fragmentData.lastFragment         != fragment
        || this.fragmentData.lastData.workspaceId != data.workspaceId
        || this.fragmentData.lastData.viewId      != data.viewId ) {


        // get base fragment
        var view;
        var i = fragment.indexOf("/");
        if (!~i) return;
        var wsPart   = fragment.substr(0, i);
        var viewPart = fragment.substr(i + 1).split(":");
        var extName  = viewPart[0];
        var viewName = viewPart[1] || extName;
        var viewFrag = viewPart.slice(2).join(":");

        // 1st attempt to find the view by id
        if (data.viewId) {
          view = this.mainViewManager.byId(data.viewId);
        }

        if (!view) {
          // get viewClass
          var ext = Extension._members.byName(extName);
          if (!ext) return;
          var viewClass = ext.getView(viewName);
          if (!viewClass) return;

          // 2nd attempt to focus view with same fragment (the last one)
          var inst = viewClass.instances;
          for (var i in inst) {
            if (inst[i].getFragment() === viewFrag) {
              view = inst[i];
            }
          }

          // 3rd spawn a new view
          if (!view) {
            // parse ws
            var ws = Workspace._members.byId(data.workspaceId) ||
                     Workspace._members.byName(wsPart.split(":").pop()) ||
                     Workspace._members.current || null;

            if (ws && ws.state !== "connected") {
              ws.once("connected", function(){
                this.fragmentData.loadLock = true;
                var view = viewClass._members.spawn(data.state, {}, {workspace: ws});
                this.fragmentData.loadLock = false;
                this._applyFragment(view, viewFrag);
              }.bind(this));
              ws.connect();
            } else {
              this.fragmentData.loadLock = true;
              view = viewClass._members.spawn(data.state, {}, {
                workspace: ws
              });
              this.fragmentData.loadLock = false;
            }
          }
        }

        if (view) {
          this.fragmentData.lastFragment = fragment;
          this.fragmentData.lastData     = data;

          this._applyFragment(view, viewFrag);

          return true;
        }
      }
      return false;
    },

    _applyFragment: function(view, viewFrag) {
      this.fragmentData.loadLock = true;
      // TODO: consider applying the state object aswell
      if (view.getFragment() !== viewFrag) {
        view.applyFragment(viewFrag);
      } else {
        view.focus();
      }
      this.fragmentData.loadLock = false;
    },

  }));

  return Vispa;
});
