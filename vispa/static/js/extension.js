/**
 * extension
 * =================
 */

define([
  "jquery",
  "emitter",
  "require",
  "vispa/utils",
  "vispa/mixins/ajax",
  "vispa/mixins/logger",
  "vispa/views/base"
], function(
  $,
  Emitter,
  require,
  Utils,
  AjaxMixin,
  LoggerMixin,
  BaseView
) {

  /**
   * .. js:class:: Extension(name)
   *
   *    :extends: Emitter
   *    :mixins: AjaxMixin
   *    :mixins: LoggerMixin
   *
   *    The basis of an extension.
   */
  var Extension = Emitter._extend($.extend({},
  AjaxMixin,
  LoggerMixin,
  {

    init: function init(name, label) {
      init._super.call(this);

      if (Extension._members._extensions[name]) {
        throw new Error("there is already an Extension with this name");
      }

      LoggerMixin.init.call(this, "extension."+name);
      AjaxMixin.init.call(this,
        vispa.dynamicURL("extensions/"+name),
        vispa.dynamicURL("extensions/"+name+"/static") // static resource at dynamic path: strange
      );

      // set properties
      /** indent: 1
       * .. js:attribute:: name read-only
       *
       *    :type: String
       *
       *    The name of the workspace.
       */
      Utils.setROP(this, "name", name);
      this._views = {};

      // register
      Extension._members._extensions[name] = this;
      vispa.preferences.addCategory(name, label);
    },

    /** indent: 1
     * .. js:function:: addView(viewClass)
     *
     *    Adds a view class to the extension. A view can be party of only one extension.
     *
     *    :param JClass(BaseView) viewClass: The view class to add.
     *
     *    :throws Error: If *viewClass* is not a JClass extending BaseView.
     *    :throws Error: If the *viewClass* is already assigned to another extension.
     *    :throws Error: If a *viewClass* with the same name is alrady registered.
     *
     *    :return: The main menu entry of the class (viewClass._members.mainMenuEntry).
     *    :rtype: Object|null
     */
    addView: function(viewClass) {
      if (!viewClass._extends || !viewClass._extends(BaseView)) {
        throw new Error("viewClass must extend BaseView");
      }
      if (viewClass._members.extension) {
        throw new Error("viewClass is already bound to an extension");
      }
      if (this._views[viewClass._members.name]) {
        throw new Error("there is already a viewClass registered with the same name");
      }

      this._views[viewClass._members.name] = viewClass;
      viewClass._members._register(this);

      return viewClass._members.mainMenuEntry;
    },

    /** indent: 1
     * .. js:function:: (menuItems)
     *
     *		Adds the given menu items to the main menu.
     *
     *    :param Array menuItems: The items to add.
     */
    mainMenuAdd: function(menuItems) {
      if (!$.isArray(menuItems)) {
        throw new Error("menuItems must be an Array");
      }

      for(var i=0, j=0; i<menuItems.length; i++) {
        var item = menuItems[i];
        if (!$.isPlainObject(item)) continue;
        vispa.mainMenu.set("extension_"+this.name+"_item"+j, item);
        j++;
      }
    },

    /** indent: 1
     * .. js:function:: addViews(viewClasses)
     *
     *    Adds multiple views to the extension. Calls :js:func:`addView` for each element of the
     *    provided Array.
     *
     *    :param Array(JClass(BaseView)) viewClasses: An Array of view classes.
     *
     *    :throws Error: If *viewClasses* is not an Array.
     */
    addViews: function(viewClasses) {
      if (!$.isArray(viewClasses)) {
        throw new Error("the class list must be an Array");
      }
      for(var i = 0; i < viewClasses.length; i++) {
        this.addView(viewClasses[i]);
      }
    },

    /** indent: 1
     * .. js:function:: getView(viewName)
     *
     *    Retrieves the view class for the view with this name.
     *
     *    :param String viewName: The name of the view to retrieve.
     *
     *    :return: The view class or *null* if there is none for the given name.
     *    :rtype: BaseView
     */
    getView: function(viewName) {
      return this._views[viewName] || null;
    },

    /** indent: 1
     * .. js:attribute:: views read-only
     *
     *    :type: Object(name => JClass(BaseView))
     *
     *    The object that holds all the views by their name. Safe aginst manipulation.
     */
    views: {
      descriptor: true,
      get: function() {
        return $.extend({}, this._views);
      }
    },

    /** indent: 1
     * .. js:function:: closeAllInstances()
     *
     *    Closes all know instances of this extension. Is syncronus.
     */
    closeAllInstances: function () {
      for(var i in this._views) {
        this._views[i].closeAllInstances();
      }
    },
  }),$.extend({},
  LoggerMixin,
  {
    init: function () {
      LoggerMixin.init.call(this, "extension");
    },

    /** indent: 1
     * .. js:function:: load(callback) class-member
     *
     *    Loades all extensions.
     *
     *    :callback String|null err: *null* on success else the error description.
     */
    load: function(callback) {
      if(this._extensions) return;
      this._extensions = {};

      callback = $.isFunction(callback) ? callback : function(){};

      var self = this;
      var extensions = vispa.args.user.extensions;
      var loadCount = extensions.length;
      var loadCallback = function(ext) {
        if (ext && ext._extends && ext._extends(Extension)) {
          new ext();
        }
        if (--loadCount) return;
        self.logger.info("loading of extensions finished");
        callback(null);
      };
      var errorCallback = function(err) {
        var em = err.requireModules[0]+" failed to load: "+err.message;
        if (err.fileName) {
          em += " ("+err.fileName+":"+err.lineNumber+":"+err.columnNumber+")";
        }
        self.logger.error(em);
        loadCallback();
      };
      for(var i = 0; i < extensions.length; i++) {
        require(["extensions/"+extensions[i]+"/static/js/extension"], loadCallback, errorCallback);
      }
    },

    /** indent: 1
     * .. js:function:: byName(name) class-member
     *
     *    Retrieves the view class by its name.
     *
     *    :param String name: The new of the view class.
     *
     *    :return: The view class.
     *    :rtype: JClass(BaseView)
     */
    byName: function(name) {
      return this._extensions[name] || null;
    },

    /** indent: 1
     * .. js:attribute:: extensions read-only class-member
     *
     *    :type: Object(name => Extension)
     *
     *    The object that holds all the extensions by their name. Safe aginst manipulation.
     */
     extensions: {
       descriptor: true,
       get: function() {
         return $.extend({}, this._extensions);
       }
     },
  }));

  return Extension;
});
