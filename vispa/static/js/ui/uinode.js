/**
 * ui/uinode
 * =================
 */

define(["jquery", "emitter"], function($, Emitter) {

  /**
   * .. js:class:: UINode()
   *
   *    :extends: Emitter
   *
   *    A class with an assigned DOM node representing it.
   */
  var UINode = Emitter._extend({
    init: function init() {
      init._super.apply(this, arguments);

      this._master = null;
      this._parent = null;
      this._$node  = null;
    },

    /** indent: 1
     * .. js:attribute:: master
     *
     *    :type: UINode
     *    :default: null
     *    :throws Error: If attempting to set the master to an invalid type/class.
     *
     *    The designated master for this node.
     */
    master: {
      descriptor: true,
      enumerable: true,
      get: function() {
        return this._master;
      },
      set: function(master) {
        if (master instanceof UINode || master === null) {
          var oldMaster = this._master;
          this._master = master;
          this.emit("masterChanged", oldMaster);
        } else {
          throw new Error("master is not of expected type/class")
        }

        return this._master;
      }
    },

    /** indent: 1
     * .. js:attribute:: parent
     *
     *    :type: UINode
     *    :default: null
     *    :throws Error: If attempting to set the parent to an invalid type/class.
     *
     *    The designated parent for this node.
     */
    parent: {
      descriptor: true,
      enumerable: true,
      get: function() {
        return this._parent;
      },
      set: function(parent) {
        if (parent instanceof UINode || parent === undefined) {
          this._parent = parent;
        } else {
          throw new Error("parent is not of expected type/class");
        }

        return this._parent;
      }
    },

    /** indent: 1
     * .. js:attribute:: $node
     *
     *    :type: JQuery
     *    :default: null
     *    :throws Error: If attempting to set the $node to an invalid type/class.
     *
     *    The JQuery object containing the DOM element (exactly 1) representing this node.
     */
    $node: {
      descriptor: true,
      enumerable: true,
      get: function() {
        return this._$node;
      },
      set: function($node) {
        if ($node instanceof $ && $node.length == 1) {
          if (this._$node) {
            this._$node.data("instance", undefined);
            this._$node.replaceWith($node);
          }
          this._$node = $node.data("instance", this);
        } else {
          throw new Error("$node is not of expected type/class");
        }

        return this._$node;
      }
    },

  });

  return UINode;
});
