/**
 * ui/tabber
 * =================
 */

define([
  "jquery",
  "vispa/ui/uinodecollection",
  "vispa/ui/split",
  "vispa/views/main"
], function(
  $,
  UINodeCollection,
  Split,
  MainView
) {

  // just used to simplify DOM node creation
  var $simpleDiv = function(className) {
    return $("<div class=\"vispa-ui-"+className+"\"/>");
  };

  /**
   * .. js:class:: Tabber()
   *
   *    :extends: UINodeCollection
   *
   *    This Class arranges its nodes in an tab bar to display only one at a time.
   */
  var Tabber = UINodeCollection._extend({

    init: function init() {
      init._super.apply(this, arguments);

      var self = this;

      this.$node  = $simpleDiv("tabber");
      this._nodes = {
        $tabbar   : $simpleDiv("tabbar"  ).appendTo(this.$node),
        $wrapper  : $simpleDiv("wrapper" ).appendTo(this.$node),
        $dropzone : $simpleDiv("dropzone").appendTo(this.$node),
      };
      this._nodes.$spawner  = $simpleDiv("spawner dropdown").appendTo(this._nodes.$tabbar)
        .html("<button data-toggle=\"dropdown\"><i class=\"fa fa-plus\"/></button>");
      this._nodes.$highlight= $simpleDiv("highlight").appendTo(this._nodes.$dropzone);

      // these are all managed by the JugglerAdvaned event handlers
      this._activeInstance  = null; // instance currently shown (can have focus too)
      this._activeIndexList = []; // in sync with instances
      this._lastActiveIndex = 2;

      // setup tab sorting
      this._nodes.$tabbar.sortable({
        containment: "window",
        items   : "> .vispa-ui-tab",
        distance: 20,
        cursor  : "move",
        handle  : ".vispa-ui-handle",
        zIndex  : 20,
        receive: function(event, ui) {
          var view = ui.item.data("instance");
          var pos  = ui.item.index();
          view.parent.detach(view);
          self.attach(view, pos);
        },
        update  : this._syncTabOrder.bind(this),
        start : function () {
          self.master.toggleTabMove(true);
        },
        stop : function(event, ui) {
          var parent = ui.item.parent();
          // move the spawner to the end
          var spawner = parent.children(".vispa-ui-spawner");
          if (spawner.next().length) {
            spawner.detach().appendTo(parent);
          }
          self._nodes.$tabbar.toggleClass("vispa-ui-recieving", false);
          ui.item.data("instance").master.toggleTabMove(false);
        },
        sort: function(event, ui) {
          var parent = ui.placeholder.parent();
          if (!parent.hasClass("vispa-ui-dropzone")) return;
          var target = parent.parent().data("instance"); // a tabber
          if (target===self && self.length>1) {
              var sector = self._getSector(parent, ui.item);
              ui.item.data("sector", sector);
              parent.attr("data-sector", sector);
          } else {
            parent.attr("data-sector", "whole");
          }
        },
        over: function(event, ui) {
          if (ui.sender) {
            self._nodes.$tabbar.toggleClass("vispa-ui-recieving", true);
          }
        },
        out: function(event, ui) {
          self._nodes.$tabbar.toggleClass("vispa-ui-recieving", false);
        },
      });

      // setup drag reciever (via srotable)
      this._nodes.$dropzone.sortable({
        items   : "> .vispa-ui-tab",
        distance: 20,
        cursor  : "move",
        handle  : ".vispa-ui-handle",
        zIndex  : 20,
        receive: function(event, ui) {
          var view = ui.item.data("instance");

          if (view.parent !== self) { // actually moved
            view.parent.detach(view);
            self.attach(view);
            return;
          }
          if (self.length==1) { // just reattacht the tab of our only node
            ui.item.prependTo(self._nodes.$tabbar);
            return;
          }

          var sector = ui.item.data("sector");
          var newTabber = new Tabber();
          var split;
          if (self.parent.length==1) {
            switch(sector) {
              case "left":
              case "right":
                if (self.parent.direction == "vertical") {
                  self.parent.direction = "horizontal";
                }
                break;
              case "top":
              case "bottom":
                if (self.parent.direction == "horizontal") {
                  self.parent.direction = "vertical";
                }
                break;
            }
          }
          if (self.parent.direction == "horizontal") {
            switch(sector) {
              case "left":
                self.parent.attach(newTabber, self);
                break;
              case "right":
                self.parent.attach(newTabber, undefined, self);
                break;
              case "top":
                split = new Split();
                split.direction = "vertical";
                self.parent.replace(self, split);
                split.attach(newTabber);
                split.attach(self);
                break;
              case "bottom":
                split = new Split();
                split.direction = "vertical";
                self.parent.replace(self, split);
                split.attach(self);
                split.attach(newTabber);
                break;
            }
          } else {
            switch(sector) {
              case "top":
                self.parent.attach(newTabber, self);
                break;
              case "bottom":
                self.parent.attach(newTabber, undefined, self);
                break;
              case "left":
                split = new Split();
                split.direction = "horizontal";
                self.parent.replace(self, split);
                split.attach(newTabber);
                split.attach(self);
                break;
              case "right":
                split = new Split();
                split.direction = "horizontal";
                self.parent.replace(self, split);
                split.attach(self);
                split.attach(newTabber);
                break;
            }
          }
          view.parent.detach(view);
          newTabber.attach(view);
        },
        start : function () {
          self.master.toggleTabMove(true);
        },
        stop  : function () {
          self.master.toggleTabMove(false);
        },
      });

      // setup master migration
      this.on("masterChanged", function (oldJuggler) {
        if (!this.master && oldJuggler) {
          this._nodes.$tabbar.add(this._nodes.$dropzone)
            .removeClass("vispa-ui-scope-"+oldJuggler.scope)
            .sortable("option","connectWith", "");
        } else {
          this._nodes.$tabbar.add(this._nodes.$dropzone)
            .addClass("vispa-ui-scope-"+this.master.scope)
            .sortable("option","connectWith", ".vispa-ui-scope-"+this.master.scope);
        }
      });

      this.on("resize", function() {
        this.broadcast("resize", {
          width: this._nodes.$wrapper.width(),
          height: this._nodes.$wrapper.height(),
        });
      });

      // initialize spaner
      this._nodes.$spawner.children().click(function() {
        // $(this).toggleClass("open", true);
        var r = this.parentNode.getBoundingClientRect();
        var l = r.left + window.pageXOffset;
        var w = window.innerWidth;
        vispa.mainMenu.direction = 2*l > w ? "right" : "left";
        vispa.mainMenu.$appendTo(this.parentNode);
        vispa.mainMenu.$options.tabber = self;
      });
    },

    /** indent: 1
     * .. js:attribute:: activeInstance
     *
     *    :type: UINode|null
     *    :default: null
     *
     *    Holds the currently active instance of the :js:class:`Tabber`
     */
    activeInstance: {
      descriptor: true,
      enumerable: true,
      get: function() {
        return this._activeInstance;
      },
      set: function(instance) {
        var index = this._instances.indexOf(instance);
        if (~index) {
          this._activeIndexList[index] = ++this._lastActiveIndex;

          this._activeInstance = instance;
        } else if (instance === undefined) {
          this._activeInstance = instance;
        }
      }
    },

    _getActiveIndex: function(instance) {
      var i = this._instances.indexOf(instance);
      return ~i ? this._activeIndexList[i] : 0;
    },

    /** indent: 1
     * .. js:function:: attach(instance[, leftOf[, rightOf[, activeIndex]]])
     *
     *    Attaches an instance.
     *
     *    .. note::
     *       Instances are expected to have a $tab attribute consisting of a
     *       JQuery object containing the DOM node representing its tab.
     *
     *    :param UINode instance: The instance to attach.
     *    :param UINode|Number leftOf: See :js:func:`UINodeCollection.attach`.
     *    :param UINode|Number rightOf: See :js:func:`UINodeCollection.attach`.
     *    :param Number activeIndex: The active index for the instance to attach.
     *      The higher the index the more recent this tab was shown. The currently shown tab has
     *      the highest index. If it is not a valid Number it will be set to the highest index.
     *      Negative numbers will be substracted from the currently highest active index, but are
     *      clipped at *0*.
     */
    attach: function attach(instance, leftOf, rightOf, activeIndex) {
      if(!(instance.$tab instanceof $) || instance.$tab.length!=1) {
        throw new Error("instance.$tab was not set appropriately");
      }
      leftOf = attach._super.call(this, instance, leftOf, rightOf);

      // prepare ativeIndex
      if (isNaN(activeIndex)) {
        activeIndex = this._lastActiveIndex;
      }
      if (activeIndex <= 0) {
        activeIndex = this._lastActiveIndex + activeIndex;
        if (activeIndex < 0) {
          activeIndex = 0;
        }
      }

      // actually attach
      this._nodes.$wrapper.append(instance.$node);
      if (leftOf < this.length) {
        this._instances.splice(leftOf, 0, instance);
        this._activeIndexList.splice(leftOf, 0, activeIndex);
        this._nodes.$tabbar.children().eq(leftOf).before(instance.$tab);
      } else {
        this._instances.push(instance);
        this._activeIndexList.push(activeIndex);
        this._nodes.$tabbar.children().last().before(instance.$tab);
      }
      instance.master = this.master;
      instance.parent = this;

      // show if required
      if (activeIndex >= this._lastActiveIndex) {
        if (activeIndex > this._lastActiveIndex) {
          this._lastActiveIndex = activeIndex-1;
        }
        instance.show();
      } else {
        instance.hide();
      }

      this.emit("resize");
    },

    /** indent: 1
     * .. js:function:: replace(oldInstance, newInstance)
     *
     *    Replaces an instance with a new one.
     *
     *    :param UINode oldInstance: Instance to replace.
     *    :param UINode newInstance: Instance to take the place.
     */
    replace: function replace(oldInstance, newInstance) {
      var index = replace._super.call(this, oldInstance, newInstance);

      oldInstance.$node.replaceWith(newInstance.$node);
      oldInstance.$tab .replaceWith(newInstance.$tab);

      this._instances[index] = newInstance;
      if (this._activeInstance === oldInstance) {
        this._activeInstance = null; // will be properly set later
      }

      oldInstance.master = null;
      oldInstance.parent = undefined;
      newInstance.master = this.master;
      newInstance.parent = this;
    },

    /** indent: 1
     * .. js:function:: detach(instance)
     *
     *    Detaches a currently attached instance.
     *
     *    :param UINode instance: Instance to detach.
     */
    detach: function(instance) {
      var index = this._instances.indexOf(instance);
      if (!~index) return;

      instance.hide();

      instance.$node.detach();
      instance.$tab.detach();
      instance.master = null;
      instance.parent = undefined;

      this._instances.splice(index, 1);
      this._activeIndexList.splice(index, 1);

      var lastTab = this._lastTab();
      if (lastTab) {
        if (!this._activeInstance) {
          lastTab.show();
        }
      } else {
        if (this.master.findFirstTabber(this)) {
          this.parent.detach(this);
        }
      }
    },

    /** indent: 1
     * .. js:function:: getNeighbor(instance, dir)
     *
     *    Get the neighboring instance left/right of the given one.
     *
     *    :param UINode instance: The instance whose neighbor is to be found.
     *    :param String dir: The direction to look for the neighbor.
     *      Valid values are *"+"* for left or *"-"* for right.
     *    :return: The neighboring instance or undefined if there is no neighbor.
     *    :rtype: UINode
     */
    getNeighbor: function(instance, dir) {
      var index = this._instances.indexOf(instance);
      if (!~index) return undefined;

      if (!~["+","-"].indexOf(dir)) return undefined;
      dir = (dir == "+")?1:-1;

      return this._instances[index + dir];
    },

    getState: function() {
      var state = ["tabber"];
      for (var i=0; i<this.length; i++) {
        state.push(this._instances[i].rid);
      }
      return state;
    },

    loadState: function(tree, inst) {
      for (var i=0; i<tree.length; i++) {
        MainView._members._unserialize(this, inst[tree[i]]);
      }
    },

    _syncTabOrder: function() {
      var self = this;

      var newInstanceOrder = this._nodes.$tabbar.children(".vispa-ui-tab").map(function(i, e){
        return $(e).data("instance");
      }).get();

      if (newInstanceOrder.length != this.length) {
        return;
      }

      this._activeIndexList = newInstanceOrder.map(function(instance){
        return self._activeIndexList[self._instances.indexOf(instance)];
      });
      this._instances = newInstanceOrder;
    },

    _lastTab: function() {
      var maxActiveIndex = 0;
      var maxActiveInstance = null;
      for(var i=this.length-1; i>=0; i--) {
        if (this._activeIndexList[i] > maxActiveIndex) {
          maxActiveIndex = this._activeIndexList[i];
          maxActiveInstance = this._instances[i];
        }
      }

      return maxActiveInstance;
    },

    _getSector: function(parent, item) {
      var offParent = parent.offset();
      var offItem   = item.offset();
      var rox = (offItem.left - offParent.left + item.width() /2)/parent.width();
      // prevent zero division
      var roy = (offItem.top  - offParent.top  + item.height()/2)/parent.height() || 1e-20;
      var diag1 = rox/roy;
      var diag2 = (1-rox)/roy;
      if (diag1 < 1) {
        if (diag2 < 1) {
          return "bottom";
        } else {
          return "left";
        }
      } else {
        if (diag2 < 1) {
          return "right";
        } else {
          return "top";
        }
      }
    },

  },{
    /** indent: 1
     * .. js:attribute:: validInstanceClass class-member
     *
     *    :value: MainView
     *
     *    See :js:class:`UINodeCollection` for details.
     */
    validInstanceClass: MainView,
  });

  return Tabber;

});
