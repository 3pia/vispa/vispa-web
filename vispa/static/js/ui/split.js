/**
 * ui/split
 * =================
 */

define([
  "jquery",
  "vispa/ui/uinodecollection",
  "require",
  "vendor/jquery/plugins/ui/jquery.ui"
], function(
  $,
  UINodeCollection,
  require
) {

  // just used to simplify DOM node creation
  var $simpleDiv = function(className) {
    return $("<div class=\"vispa-ui-"+className+"\"/>");
  };

  /**
   * .. js:class:: Split(instances[, sizes])
   *
   *    :extends: UINodeCollection
   *
   *    This class provides the ability to arrange the attached nodes next
   *    to each other in the set direction. The size of each can be changed with
   *    the draggable bars divideing the nodes.
   *
   *    :param Array(UINode) instances: An array of instances to attach.
   *    :param Array(Number) sizes: An array of the sizes for the instances to attach.
   */
  var Split = UINodeCollection._extend({
    init: function init(instances, sizes) {
      init._super.apply(this, arguments);

      this.$node = $simpleDiv("split");
      this.direction = "horizontal";


      var instancesChecked = [];
      var sizeTotal        = 0;
      var sizeDefined      = 0;
      var sizeMissing      = 0;
      if($.isArray(instances)) {
        var sizesChecked     = [];

        for(var i=0; i<instances.length; i++) {
          if(!(instances[i] instanceof this._class._members.validInstanceClass)) continue;
          instancesChecked.push(instances[i]);
          if ($.isArray(sizes) && $.isNumeric(sizes[i]) && sizes[i]>0) {
            sizesChecked.push(sizes[i]);
            sizeTotal += sizes[i];
            sizeDefined++;
          } else {
            sizesChecked.push(0);
            sizeMissing++;
          }
        }
      }

      if (instancesChecked.length) {
        var sizeDefault = Math.max(10, sizeDefined ? sizeTotal/sizeDefined : 1);
        sizeTotal += sizeDefault*sizeMissing;
        var sizeNorm = 100/sizeTotal;
        for (var i=0; i<instancesChecked.length; i++) {
          this.attach(instancesChecked[i], undefined, undefined, sizesChecked[i] || sizeDefault);
        }
        this._fixedSplit = true;
      } else {
        this._fixedSplit = false;
      }

      this.on("resize", function(){
        this.broadcast("resize");
      });
    },

    /** indent: 1
     * .. js:attribute:: direction
     *
     *    :type: String
     *    :default: "horizontal"
     *
     *    This lower-case string sets the direction along which the nodes are arranged.
     *    Valid values are "horizontal" or "vertical".
     *    For setting this value it is enough to provide the first letter: "h" or "v".
     *
     *    :throws Error: If attempting to set an invalid direction or
     *      set the direction while more than 1 :js:class:`UINode` is attached.
     */
    direction: {
      descriptor: true,
      enumerable: true,
      get: function() {
        return this._direction;
      },
      set: function(direction) {
        var dir = {
          h: "horizontal",
          v: "vertical"
        }[String(direction).toLowerCase()[0]];
        if (dir && this.length<=1) {
          this._direction = dir;

          var hor = dir=="horizontal";
          this._$node.toggleClass("vispa-ui-horizontal",  hor);
          this._$node.toggleClass("vispa-ui-vertical"  , !hor);
          this._$node.children(".vispa-ui-dragger").draggable({
            axis  : this._direction=="vertical"?"y"        :"x",
            cursor: this._direction=="vertical"?"ns-resize":"ew-resize",
          });
        } else {
          if (dir) {
            throw new Error("Setting direction with more than one attached node in not allowed.");
          } else {
            throw new Error("The direction given was not valid.");
          }
        }

        return this._direction;
      }
    },

    /** indent: 1
     * .. js:function:: attach(instance[, leftOf[, rightOf]])
     *
     *    This function attaches an UINode instance.
     *
     *    :param UINode instance: The instance to attach.
     *    :param UINode|Number leftOf: See :js:func:`UINodeCollection.attach`.
     *    :param UINode|Number rightOf: See :js:func:`UINodeCollection.attach`.
     */
    attach: function attach(instance, leftOf, rightOf, _forceSize) {
      leftOf = attach._super.call(this, instance, leftOf, rightOf);

      // actual attaching
      var wrapper = $simpleDiv("wrapper");
      var dragger = $simpleDiv("dragger");

      // setup dragger
      dragger.draggable({
        axis          : this.direction=="vertical"?"y"        :"x",
        cursor        : this.direction=="vertical"?"ns-resize":"ew-resize",
        opacity       : 0.75,
        revert        : true,
        revertDuration: 0,
        scroll        : false,
        zIndex        : 20,
        stop          : this._handleResize.bind(this),
      });

      if (leftOf < this.length) {
        this._instances.splice(leftOf, 0, instance);
        this._$node.children(".vispa-ui-wrapper").eq(leftOf).before(wrapper, dragger);
      } else {
        this._instances.push(instance);
        if (this.length > 1) {
          this._$node.append(dragger, wrapper);
        } else {
          this._$node.append(wrapper);
        }
      }
      instance.master = this.master;
      instance.parent = this;

      // calculate new size
      if (_forceSize) {
        wrapper[0].style.flexBasis = _forceSize + "%";
      } else {
        var siblings = this._getSiblings(wrapper);
        var n = siblings.length;
        if (n === 0) {
          wrapper[0].style.flexBasis = "100%";
        } else {
          var totalWidth = 0;
          for(var i=0; i<n; i++) {
            var v = Number(siblings[i].style.flexBasis.replace("%",""));
            totalWidth += v;
            siblings[i].style.flexBasis = String(v * n/(n+1))+"%";
            siblings.eq(i).children().data("instance").emit("resize");
          }
          wrapper[0].style.flexBasis = String(totalWidth / (n+1))+"%";
        }
      }

      // just now add content
      wrapper.append(instance.$node);
      this.emit("resize");
    },

    /** indent: 1
     * .. js:function:: replace(oldInstance, newInstance)
     *
     *    Replaces an instance with a new one.
     *
     *    :param UINode oldInstance: Instance to replace.
     *    :param UINode newInstance: Instance to take the place.
     */
    replace: function replace(oldInstance, newInstance) {
      var index = replace._super.call(this, oldInstance, newInstance);

      oldInstance.$node.detach();
      this._$node.children(".vispa-ui-wrapper").eq(index).empty().append(newInstance.$node);

      this._instances[index] = newInstance;

      oldInstance.master = null;
      oldInstance.parent = undefined;
      newInstance.master = this.master;
      newInstance.parent = this;

      newInstance.emit("resize");
    },

    /** indent: 1
     * .. js:function:: detach(instance)
     *
     *    Detaches a currently attached instance.
     *
     *    :param UINode instance: Instance to detach.
     */
    detach: function(instance) {
      var index = this._instances.indexOf(instance);
      if (!~index) return;

      instance.$node.detach();
      instance.master = null;
      instance.parent = undefined;

      var wrapper  = this._$node.children(".vispa-ui-wrapper").eq(index);
      var siblings = this._getSiblings(wrapper);

      wrapper.next().add(wrapper.prev()).eq(0).add(wrapper).remove();
      this._instances.splice(index, 1);

      // proper resize
      var n = siblings.length;
      if (n === 0) {
        if(this.parent) {
          this.parent.detach(this);
        }
      // can we collapse into the parent Split ?
      } else if (n == 1 && this._$node.children(".vispa-ui-wrapper").length == 1 && this.parent) {
        // get the remaining instance
        var otherInstance = this._instances[0];
        // detach it manually (regular detach will fuck this process up)
        otherInstance.$node.detach();
        otherInstance.parent = undefined;
        this._$node.empty();
        // now actually replace this
        this.parent.replace(this, otherInstance);
      } else {
        var totalWidth = 0;
        for(var i=0; i<n; i++) {
          totalWidth += Number(siblings[i].style.flexBasis.replace("%",""));
        }
        var scale = 1 + Number(wrapper[0].style.flexBasis.replace("%",""))/totalWidth;
        for(var i=0; i<n; i++) {
          var v = Number(siblings[i].style.flexBasis.replace("%",""));
          siblings[i].style.flexBasis = String(v * scale)+"%";
          siblings.eq(i).toggleClass("vispa-ui-resized", false);
        }

        this.emit("resize");
      }
    },

    /** indent: 1
     * .. js:attribute:: resizeEnabled
     *
     *    :type: Boolean
     *    :default: true
     *
     *    Whether resizing is enabled.
     */
    resizeEnabled: {
      descriptor: true,
      get: function() {
        return !this._$node.children(".vispa-ui-dragger").draggable("option", "disabled");
      },
      set: function(enabled) {
        this._$node.children(".vispa-ui-dragger").draggable("option", "disabled", !enabled);
      },
    },

    getState: function() {
      var state = ["split", this.direction[0]];
      var wrapper = this._$node.children(".vispa-ui-wrapper");
      for (var i=0; i<this.length; i++) {
        state.push(
          Number(wrapper[i].style.flexBasis.replace("%","")),
          this._instances[i].getState()
        );
      }

      return state;
    },

    loadState: function(tree, inst) {
      this.direction = tree[0];
      for(var i=1; i<tree.length; i+=2) {
        var instance = new (require("vispa/ui/"+tree[i+1][0]))();
        this.attach(instance, undefined, undefined, tree[i]);
        instance.loadState(tree[i+1].slice(1), inst);
      }
    },

    _getSiblings: function(wrapper) {
      // get not manually resized direct peers
      var siblings = wrapper.prevUntil(".vispa-ui-resized")
                .add(wrapper.nextUntil(".vispa-ui-resized"))
                .filter(".vispa-ui-wrapper");

      // include manually resized peers if no other are present
      if (siblings.length === 0) {
        siblings = wrapper.prev().prev()
              .add(wrapper.next().next());
      }

      return siblings;
    },

    _handleResize: function(event, ui) {
      var ele = ui.helper;
      var dir = this.direction!="vertical"; // hor/ver -> t/f
      var wh = dir?"width":"height";
      var off = ui.position[dir?"left":"top"];

      var pseudoAvail = ele.parent()[wh]();

      if ( Math.abs(off) < ele[off>0?"next":"prev"]()[wh]()) {
        var pfb = 100.0*(ele.prev()[wh]()+off)/pseudoAvail+"%";
        var nfb = 100.0*(ele.next()[wh]()-off)/pseudoAvail+"%";

        ele.prev().css("flex-basis", pfb).toggleClass("vispa-ui-resized", true)
          .children().data("instance").emit("resize");
        ele.next().css("flex-basis", nfb).toggleClass("vispa-ui-resized", true)
          .children().data("instance").emit("resize");
      }
    },
  });

  return Split;
});
