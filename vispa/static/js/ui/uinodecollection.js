/**
 * ui/uinodecollection
 * =================
 */

define(["jquery", "vispa/ui/uinode"], function($, UINode) {

  /**
   * .. js:class:: UINodeCollection()
   *
   *    :extends: UINode
   *
   *    Designed to hold :js:class:`UINode` s.
   *
   *    .. warning::
   *       This is only a skeleton class. It is not to be instanciated!
   */
  var UINodeCollection = UINode._extend({
    init: function init() {
      init._super.apply(this, arguments);

      this._instances = [];
    },

    /** indent: 1
     * .. js:function:: attach(instance)
     *
     *    Attach instance to internal instance array.
     *
     *    .. warning::
     *       This method does only implement some checks and does detach
     *       the instance if it is already attached.
     *
     *    :param UINode instance: The instance to attach.
     *    :param UINode|Number leftOf: The instance to attach will be left of this instance
     *      or the instance at this postion.
     *    :param UINode|Number rightOf: If *leftOf* is *undefined* then this option is considered.
     *      The instance to attach will be right of this instance or the instance at this postion.
     *    :return: The postion the instance will be placed at.
     *    :rtype: Number
     *
     *    .. note::
     *       Both, *leftOf* and *rightOf* are interpreted this way:
     *         * If it isn't a *Number* then interpret the argument as an instance. If it isn't an valid instance either, just default to the postion at the very end.
     *         * If it is a *Number* then interpret it as position in the list of instance, with 0 being the first. If it is negative then use it as an offst from the end. e.g. -2 is the second last position (clipped at the first position)
     */
    attach: function(instance, leftOf, rightOf) {
      if (!(instance instanceof this._class._members.validInstanceClass)) {
        throw new Error("instance is not the correct Class");
      }


      if (!instance.$node) {
        throw new Error("Instance has no content");
      }
      if (instance.parent) {
        instance.parent.detach(instance);
      }

      if (leftOf === undefined) {
        if (isNaN(rightOf)) {
          leftOf  = this._instances.indexOf(rightOf);
        } else {
          leftOf  = rightOf;
        }
        rightOf = true;
      } else {
        if (isNaN(leftOf)) {
          leftOf  = this._instances.indexOf(leftOf);
        }
        rightOf = false;
      }

      if (leftOf < 0) {
        leftOf += 1 + this.length;
      }
      if (leftOf < 0) {
        leftOf = 0;
      }
      if (rightOf) {
        leftOf++;
      }

      return leftOf;
    },

    /** indent: 1
     * .. js:function:: replace(oldInstance, newInstance)
     *
     *    Replaces a currently attached instance with another one.
     *
     *    .. warning::
     *       This method does only implement some checks und does detach
     *       the newInstance if it is already attached
     *
     *    :param UINode oldInstance: Instance to replace.
     *    :param UINode newInstance: Instance to take the place.
     *    :return: Index of the instance to replace in the internal instance array.
     *    :rtype: Number
     */
    replace: function(oldInstance, newInstance) {
      if (oldInstance === newInstance) {
        throw new Error("Instances must be driffrent");
      }

      var index = this._instances.indexOf(oldInstance);
      if ((!~index) || (oldInstance.parent !== this)) {
        throw new Error("oldInstance is not a member of this UIPart");
      }


      if (!(newInstance instanceof this._class._members.validInstanceClass)) {
        throw new Error("newInstance is not the correct Class");
      }


      if (!newInstance.$node) {
        throw new Error("Instance has no content");
      }

      if (newInstance.parent) {
        newInstance.parent.detach(instance);
      }

      return index;
    },

    /** indent: 1
     * .. js:function:: detach(instance)
     *
     *    Detaches a currently attached instance.
     *
     *    .. warning::
     *       This method does not implement anyting, it is to be extended.
     *
     *    :param UINode instance: Instance to detach.
     */
    detach: function(instance) {
        return undefined;
    },

    /** indent: 1
     * .. js:function:: detachAll()
     *
     *    Detaches all instance (one by one).
     *
     *    :return: Count of instances detached.
     *    :rtype: Number
     */
    detachAll: function() {
      for (var count=0; this.length; count++) {
        this.detach(this._instances[0]);
      }

      return count;
    },

    /** indent: 1
     * .. js:attribute:: length
     *
     *    :type: Integer
     *
     *    The number of current instances this collection has.
     */
    length: {
      descriptor: true,
      get: function() {
        return this._instances.length;
      },
    },

    /** indent: 1
     * .. js:function:: broadcast(...)
     *
     *    Calls emit(...) on every instance.
     */
    broadcast: function() {
      for(var i=0; i<this.length; i++) {
        var inst = this._instances[i];
        inst.emit.apply(inst, arguments);
      }
    }

  }, {
    /** indent: 1
     * .. js:attribute:: validInstanceClass class-member
     *
     *    :value: UINode
     *
     *    Only instances of this Class will be accepted by :js:func:`attach` and :js:func:`replace`.
     *    Classes extending :js:class:`UINodeCollection` are expected to define it appropriately.
     */
    validInstanceClass: UINode,
  });

  return UINodeCollection;
});
