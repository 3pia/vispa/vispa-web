/**
 * ui/viewmangager
 * =================
 */

define([
  "jquery",
  "vispa/ui/uinodecollection",
  "vispa/utils",
  "emitter",
  "vispa/ui/split",
  "vispa/ui/tabber",
  "vispa/views/main"
], function(
  $,
  UINodeCollection,
  Utils,
  Emitter,
  Split,
  Tabber,
  MainView
) {

  /**
   * .. js:class:: ViewManager(container, listenerTarget)
   *
   *    :extends: UINodeCollection
   *
   *    This window manager allows for multiple abitrary arranged
   *    :js:class:`MainView` by using :js:class:`Split` and :js:class:`Tabber`.
   *
   *    :param JQuery container: The JQuery container DOM node in which
   *      all content will be rendered.
   *    :param Emitter listenerTarget: The Emitter whichi is watched for
   *      focus/blur and close events to take action upon.
   */
  var ViewManager = UINodeCollection._extend({

    init: function init(container, listenerTarget) {
      var superArgs = Array.prototype.slice.call(arguments, 3);
      superArgs[0] = $.extend({wildcard: true}, superArgs[0]);
      init._super.apply(this, superArgs);

      var self = this;

      this._scope = Utils.uuid(); // used for containing the tabs within a the viewmanager instance
      this._focusInstance = null; // instance having focus (can only be one)
      this._visibleInstances = []; // list of visible instance (can be multiple, atm just 1)
      this._focusIndex = []; // focusIndex list needs to be in sync with this._instances
      this._lastFocusIndex = 2;

      if (!listenerTarget || !(listenerTarget instanceof Emitter)) {
        listenerTarget = this;
      }
      this._listenerTarget = listenerTarget;

      // set up the first container
      this._split = new Split();
      this._split.master = this;
      this._split.direction = "horizontal";
      this._split.attach(new Tabber());

      this.$node = $(container).append(this._split.$node);

      // prepare common event handlers to be attached later
      this._instanceEventHandler = {
        show: function(opts) {
          if (!~self._instances.indexOf(this)) return;
          if (!this.parent) return;
          if (this.parent.activeInstance===this) return;

          if (this.parent.activeInstance) {
            this.parent.activeInstance.hide();
          }

          if (!~self._visibleInstances.indexOf(this)) {
            self._visibleInstances.push(this);
          }

          this.$node.show();
          this.$tab.toggleClass("shown", true);
          this.parent.activeInstance = this;

          self.emit("viewmanager.show", this);
        },
        hide: function() {
          if (!~self._instances.indexOf(this)) return;
          if (!this.parent) return;

          this.blur();

          var visIndex = self._visibleInstances.indexOf(this);
          if (~visIndex) {
            self._visibleInstances.splice(visIndex, 1);
          }

          this.$node.hide();
          this.$tab.toggleClass("shown", false);
          if (this.parent.activeInstance === this) {
            this.parent.activeInstance = undefined;
          }

          self.emit("viewmanager.hide", this);
        },
        focus: function(opts) {
          var index = self._instances.indexOf(this);
          if (!~index) return;
          if (!this.parent) return;

          if (!~self._visibleInstances.indexOf(this)) {
            this.show(opts);
          }

          if (self._focusInstance === this) return;
          if (~self._instances.indexOf(self._focusInstance)) {
            self._focusInstance.blur();
          }

          this.$tab.toggleClass("focused", true);

          self._lastFocusIndex = Math.max(self._lastFocusIndex+1, self._focusIndex[index]);
          self._focusIndex[index] = self._lastFocusIndex;
          self._focusInstance = this;
          self.emit("viewmanager.focus", this);

          vispa.saveFragment();
        },
        blur: function() {
          if (!~self._instances.indexOf(this)) return;
          if (!this.parent) return;

          this.$tab.toggleClass("focused", false);

          if (self._focusInstance === this) {
            self._focusInstance = null;
          }

          self.emit("viewmanager.blur", this);

          vispa.saveFragment();
        },
        close: function() {
          self.detach(this);
        },
      };

      // own event Handler
      this._ownEventHandler = {
        focus: function() {
          if (self._focusInstance && ~self._instances.indexOf(self._focusInstance)) return;

          var lastFocus = self._lastFocus();
          if (lastFocus) {
            lastFocus.focus();
          }
        },
        blur: function() {
          if (!~self._instances.indexOf(self._focusInstance)) return;

          self._focusInstance.blur();
        },
        close: function() {
          self.destroy();
        },
        resize: function() {
          this._split.emit("resize");
        },
      };

      // attach own event handler to desired target
      for (var i in this._ownEventHandler) {
        this._listenerTarget.on(i, this._ownEventHandler[i]);
      }

    },

    /** indent: 1
     * .. js:attribute:: scope read-only
     *
     *    :type: String
     *
     *    An UUID. This is internally needed to prevent the attached UINodes
     *    from escapeing when they are dragged around.
     */
    scope: {
      descriptor: true,
      enumerable: true,
      get: function() {
        return this._scope;
      },
    },

    /** indent: 1
     * .. js:attribute:: instances read-only
     *
     *    :type: Array(MainView)
     *
     *    An array of all attached instances.
     */
    instances: {
      descriptor: true,
      enumerable: true,
      get: function() {
        return this._instances.slice();
      },
    },

    /** indent: 1
     * .. js:attribute:: visiableInstances read-only
     *
     *    :type: Array(MainView)
     *
     *    An array of all currently visivle instances (shown).
     */
    visibleInstances: {
      descriptor: true,
      enumerable: true,
      get: function() {
        return this._visibleInstances.slice();
      },
    },

    /** indent: 1
     * .. js:attribute:: focusInstance read-only
     *
     *    :type: MainView
     *    :default: undefined
     *
     *    The currently focused instance or undefined if none is focused.
     */
    focusInstance: {
      descriptor: true,
      enumerable: true,
      get: function() {
        return this._focusInstance;
      },
    },

    _getFocusIndex: function (instance) {
      var i = this._instances.indexOf(instance);
      return ~i ? this._focusIndex[i] : 0;
    },

    /** indent: 1
     * .. js:function:: isManaged(instance)
     *
     *    Test wether the instance is attached.
     *
     *    :param MainView instance: The instance to test.
     *    :return: Is the instance attached?
     *    :rtype: Bool
     */
    isManaged: function(instance) {
      return !!~this._instances.indexOf(instance);
    },

    /** indent: 1
     * .. js:function:: isVisible(instance)
     *
     *    Test wether the instance is visible.
     *
     *    :param MainView instance: The instance to test.
     *    :return: Is the instance visible?
     *    :rtype: Bool
     */
    isVisible: function(instance) {
      return this.isManaged(instance) && !!~this._visibleInstances.indexOf(instance);
    },

    /** indent: 1
     * .. js:function:: isFocused(instance)
     *
     *    Test wether the instance is focused.
     *
     *    :param MainView instance: The instnace to test.
     *    :return: Has the instance focus?
     *    :rtype: Bool
     */
    isFocused: function(instance) {
      return this.isManaged(instance) && this._focusInstance === instance;
    },

    /** indent: 1
     * .. js:function:: attach(instance[, viewArgs])
     *
     *    Attaches an instance.
     *
     *    :param MainView instance: The instance to attach.
     *    :param Object viewArgs: An object holding further options for the placeing of the
     *    	instance.
     *    :param MainView viewArgs.replace: Replaces this currently attached instance.
     *      If it is not valid instance to replace, this option will be ignored entirely.
     *    :param Tabber viewArgs.tabber: Attach the instance inside this tabber.
     *      Will attempt to resprect the following 2 parameters (for numeric placement).
     *      If it is not valid tabber, this option will be ignored entirely.
     *    :param MainView|Number viewArgs.leftOf: See :js:func:`attach` of
     *      :js:class:`UINodeCollection`.
     *    :param MainView|Number viewArgs.rightOf: See :js:func:`attach` of
     *      :js:class:`UINodeCollection`.
     *    :param Number viewArgs.focusIndex: The focus index for the instance to attach.
     *      The higher the index the more recent this instance had focus. The currently focused
     *      instance has the highest index. If this value is not present or *0* it will default to
     *      the currently highest index. Negative values will be used as and offset from the
     *      currently highest value.
     *    :param Number viewArgs.activeIndex: The active index. See :js:func:`attach` of
     *    	:js:class:`Tabber`.
     *
     *    .. note::
     *       Both *viewArgs.leftOf* and *viewArgs.rightOf* are interpreted this way:
     *         * If it is not a valid Number it will be set to the highest index.
     *         * Negative numbers will be substracted from the currently highest active index,
     *           but are clipped at *0*.
     */
    attach: function(instance, viewArgs) {
      // check if everything is properly set up
      if (!(instance instanceof MainView)) {
        throw new Error("Instance needs to be an MainView");
      }

      if (!(instance.$node instanceof $)) {
        throw new Error("Instance has no valid $node.");
      }
      if (instance.$node.length != 1) {
        throw new Error("Instances $node has not exactly one DOM-Node.");
      }

      if (!(instance.$tab instanceof $)) {
        throw new Error("Instance has no valid $tab.");
      }
      if (instance.$tab.length != 1) {
        throw new Error("Instances $tab has not exactly one DOM-Node.");
      }

      // get args
      viewArgs = viewArgs || {};
      var focusIndex  = viewArgs.focusIndex;
      var activeIndex = viewArgs.activeIndex;
      var leftOf      = viewArgs.leftOf;
      var rightOf     = viewArgs.rightOf;
      var replace     = viewArgs.replace;
      var tabber      = viewArgs.tabber;

      // detach if it already attached somewhere
      if (instance.master && instance.master.detach) {
        instance.master.detach(instance);
      }

      // check if replace is asked for
      var replaceIndex = this._instances.indexOf(replace);
      if (~replaceIndex && replace.master == this) {
        tabber  = replace.parent;
        leftOf  = undefined;
        rightOf = undefined;
      } else {
        replace = undefined;
      }

      // fill in the missing tabber
      if (!(tabber instanceof Tabber) || tabber.master !== this) {
        tabber = this.findFirstTabber();
        if (leftOf instanceof MainView && leftOf.master === this) {
          tabber = leftOf.parent;
        } else if (rightOf instanceof MainView && rightOf.master === this) {
          tabber = rightOf.parent;
        }
      }

      // correct focus index
      if (focusIndex < 0) {
        focusIndex += this._lastFocusIndex;
        if (focusIndex <= 0) {
          focusIndex = 1;
        }
      }
      if (!focusIndex) {
        focusIndex = this._lastFocusIndex;
      }

      // add reference from $tab to instance
      instance.$tab.data("instance", instance);

      // register common event halder
      for (var i in this._instanceEventHandler) {
        instance.on(i, this._instanceEventHandler[i]);
      }

      if (replace) {
        // alter data tables
        this._instances[replaceIndex] = instance;
        focusIndex = this._focusIndex[replaceIndex];

        // perform replacement
        tabber.replace(replace, instance);

        // remove our handlers from old one
        for (var i in this._instanceEventHandler) {
          replace.off(i, this._instanceEventHandler[i]);
        }

        // pseudo hide (activeInstance was changed by replace)
        var visIndex = this._visibleInstances.indexOf(replace);
        if (~visIndex) {
          // pseudo blur
          if (this._focusInstance === replace) {
            replace.$tab.toggleClass("focused", false);
            this._focusInstance = null;
            this.emit("viewmanager.blur", replace);
            replace.blur();
          }

          replace.$tab.toggleClass("shown", false);
          this._visibleInstances.splice(visIndex, 1);
          this.emit("viewmanager.hide", replace);
          replace.hide();
        } else {
          instance.hide();
        }

        // pseudo detach
        replace.$tab.data("instance", undefined);
        replace.emit("detachedFrom", this);
        this.emit("viewmanager.detached", replace);
      } else {
        // add to data tables
        this._instances.push(instance);
        this._focusIndex.push(focusIndex);

        // actually attach them to a tabber
        tabber.attach(instance, leftOf, rightOf, activeIndex);
      }

      // emit events
      instance.emit("attachedTo", this);
      this.emit("viewmanager.attached", instance);

      // focus if required
      if (focusIndex >= this._lastFocusIndex) {
        instance.focus();
      } else {
        instance.blur();
      }
    },

    /** indent: 1
     * .. js:function:: detach(instance)
     *
     *    Detaches a currently attached instance.
     *
     *    :param MainView instance: Instance to detach.
     */
    detach: function(instance) {
      var index = this._instances.indexOf(instance);
      if (!~index) return;

      instance.hide();

      instance.parent.detach(instance);
      instance.$tab.data("instance", undefined);

      this._instances.splice(index, 1);
      this._focusIndex.splice(index, 1);

      for (var i in this._instanceEventHandler) {
        instance.off(i, this._instanceEventHandler[i]);
      }

      if (!this._focusInstance) {
        var lastFocus = this._lastFocus();
        if (lastFocus) {
          lastFocus.focus();
        }
      }

      instance.emit("detachedFrom", this);
      this.emit("viewmanager.detached", instance);
    },

    /** indent: 1
     * .. js:function:: focusNeighbor(dir)
     *
     *    Focuses the neighboring instance left/right of the currently focused one.
     *
     *    :param String dir: The direction of the neighbor.
     *      Valid values are *"+"* for left or *"-"* for right.
     */
    focusNeighbor: function(dir) {
      if (!this._focusInstance) return;

      var neighbor = this._focusInstance.parent.getNeighbor(this._focusInstance, dir);
      if (neighbor) {
        neighbor.focus();
      }
    },

    getState: function() {
      var instances = {};
      for(var i in this._instances) {
        instances[this._instances[i].rid] = this._instances[i]._serialize();
      }

      return {
        inst: instances,
        tree: this._split.getState(),
      };
    },

    loadState: function(state) {
      if (!state || !state.tree || !state.tree.slice || !state.inst) return;
      this._split.detachAll();
      this._split.loadState(state.tree.slice(1), state.inst);
      var wsIds = [];
      for(var i in state.inst) {
        var wsId = state.inst[i][2];
        if (!~wsIds.indexOf(wsId)) {
          wsIds.push(wsId);
        }
      }
      return wsIds;
    },

    /** indent: 1
     * .. js:function:: destroy()
     *
     *    Destroys itself: Detaches all instances, stops listening to events and renders itself
     *    unusable.
     */
    destroy: function() {
      this.detachAll();

      for (var i in this._ownEventHandler) {
        this._listenerTarget.off(i, this._ownEventHandler[i]);
      }

      this._instances = null; //force all subsequent calls to fail

      return null;
    },

    _lastFocus: function() {
      var maxFocusIndex = 0;
      var maxFocusInstance = null;
      for(var i=this.length-1; i>=0; i--) {
        if (this._focusIndex[i] > maxFocusIndex) {
          maxFocusIndex = this._focusIndex[i];
          maxFocusInstance = this._instances[i];
        }
      }

      return maxFocusInstance;
    },

    // these are only used internally by the Tabber instances
    toggleTabMove: function(enabled) {
      this.$node.toggleClass("vispa-ui-tabmove", !!enabled);
      if (!enabled) {
        this.$node.find("[data-sector]").removeAttr("data-sector");
      }
    },

    findFirstTabber: function(notThis, currentPart) {
      if (currentPart instanceof Tabber) {
        if (currentPart === notThis) {
          return undefined;
        } else {
          return currentPart;
        }
      }
      if (!currentPart) {
        currentPart = this._split;
      }
      var ret;
      for(var i=0; i<currentPart.length && !ret; i++) {
        ret = this.findFirstTabber(notThis, currentPart._instances[i]);
      }
      return ret;
    },

    /** indent: 1
     * .. js:function:: byId(id)
     *
     *    Retrieves an attached view by id.
     *
     *    :param String id: The :js:attr:`id` of the view to retrieve.
     *
     *    :return: The view, if available, else *null*.
     *    :rtype: MainView|null
     */
    byId: function(id) {
      for (var i in this._instances) {
        if (this._instances[i].id === id) {
          return this._instances[i];
        }
      }
      return null;
    },

  });

  return ViewManager;
});
