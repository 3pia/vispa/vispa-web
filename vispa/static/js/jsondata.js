/**
 * jsondata
 * ========
 */

define([
  "jquery",
  "emitter",
  "vue/vue",
  "vispa/utils",
  "vispa/workspace",
], function(
  $,
  Emitter,
  Vue,
  Utils,
  Workspace
) {

  /**
   * .. js:class:: JSONData(key[, workspace])
   *
   *    :extends: Emitter
   *
   *    The mannager of a JSONData object saved to the server.
   *
   *    :param String key: The key of the data-object
   *    :param Workspace|Int workspace: If given, the workspace or its id for which the object
   *    	is stored.
   */
  var JSONData = Emitter._extend({
    init: function(key, workspace) {
      key = String(key);
      var wid = Workspace._members.getId(workspace, null);
      var root = this._class._members;

      if (root._instances[key] && root._instances[key][wid]) {
        console.trace();
        throw new Error("attempted to create instance twice, use getInst instead!");
      }

      /** indent: 1
       * .. js:attribute:: key read-only
       *
       *    :type: String
       *
       *    The key of the JSONData object.
       */
      Utils.setROP(this, "key", key);
      /** indent: 1
       * .. js:attribute:: wid read-only
       *
       *    :type: Int
       *
       *    The workspace id of the JSONData object.
       */
      Utils.setROP(this, "wid", wid);

      // register
      if (!root._keys[this.key]) {
        root._keys[this.key] = {};
      }
      if (!root._instances[this.key]) {
        root._instances[this.key] = {};
      }
      root._instances[this.key][this.wid] = this;

      // push and pull flags
      this._pushRequested = undefined;
      this._pullRequested = undefined;
    },

    /** indent: 1
     * .. js:attribute:: data
     *
     *    :type: Any
     *
     *		The data content of the JSONData object.
     */
    data: {
      descriptor: true,
      get: function() {
        return this._class._members._keys[this.key][this.wid];
      },
      set: function(value) {
        Vue.set(this._class._members._keys[this.key], this.wid, value);
      },
    },

    /** indent: 1
     * .. js:function:: pull([callback])
     *
     *    Instructs the JSONData to be fetched from the server. The callback is triggered when the
     *    pull has finished.
     *
     *    :emits pulled: When the pull has finished.
     */
    pull: function(callback) {
      if (callback) {
        this.once("pulled", callback);
      }
      if (this._pullRequested !== undefined) {
        this._pullRequested = true;
        return;
      }
      this._pullRequested = false;
      vispa.POST("ajax/getjson", {
        key: this.key,
        wid: this.wid,
      }, this._pullProcess.bind(this));
    },
    _pullProcess: function(err, info) {
      if (err) throw err;

      if (!info) return;

      // apply changes
      this.data  = JSON.parse(info.data);
      this._time = info.time;

      // finalize
      if (this._pullRequested) {
        this._pullRequested = undefined;
        this.pull();
      } else {
        this._pullRequested = undefined;
        this.emit("pulled");
      }
    },

    /** indent: 1
     * .. js:function:: push([callback])
     *
     *    Instructs the JSONData to be sent to the server. The callback is triggered when the
     *    push has finished. Will not do anything (except calling the callback) when there is
     *    nothing to do.
     *
     *    :emits pulled: When the push has finished.
     */
    push: function(callback) {
      var encData = JSON.stringify(this.data);
      if (this._lastEncData === encData) {
        Utils.callback(callback)(null);
        return;
      }
      if (callback) {
        this.once("pushed", callback);
      }
      if (this._pushRequested !== undefined) {
        this._pushRequested = true;
        return;
      }
      this._pushRequested = false;
      vispa.POST("ajax/setjson", {
        key: this.key,
        wid: this.wid,
        value: encData,
      }, this._pushProcess.bind(this, encData));
    },
    _pushProcess: function(encData, err) {
      if (err) throw err;

      // remember last pushed change
      this._lastEncData = encData;

      // finalize
      if (this._pushRequested) {
        this._pushRequested = undefined;
        this.push();
      } else {
        this._pushRequested = undefined;
        this.emit("pushed");
      }
    },


  },{
    init: function init() {
      init._super.call(this);

      this._keys = {};
      this._instances = {};
    },

    /** indent: 1
     * .. js:function:: load(infos)
     *
     *    Sets up the JSONData objects for the given keys in infos. This will internally call
     *    ;js:function:`loadKey`.
     *
     *    :param Object(key=>JSONDataInfos) infos: Has a *infos* objects with the same syntax as
     *      in :js:func:`loadKey` for every *key*.
     */
    load: function(infos) {
      for (var key in infos) {
        this.loadKey(key, infos[key]);
      }
    },

    /** indent: 1
     * .. js:function:: loadKey(key, JSONDataInfos)
     *
     *    Sets up the JSONData obejct for the given workspaces in JSONDataInfos.
     *
     *    :param Object(workspace.id => JSONDataInfo) JSONDataInfos: Every *key* holds a JSONData info
     *    	object (and internal data collection) from which it is initialized.
     */
    loadKey: function(key, JSONDataInfos) {
      for (var wid in JSONDataInfos) {
        this.getInst(key, wid)._pullProcess(null, JSONDataInfos[wid]);
      }
    },

    /** indent: 1
     * .. js:function:: getInst(key[, workspace])
     *
     *    Retrives the JSONData instance for the given key and workspace. It will be created if it
     *    does not already exist. This should be preferred over directly instanciating
     *    :js:class:`JSONData` itself.
     *
     *    :param String key: The key of the JSONData.
     *    :param Workspace|Int(workspace.id) workspace: The workspace the JSONData is bound to. If
     *    	omitted, it will be global.
     *
     *    :return: The requested JSONData.
     *    :rtype: JSONData
     */
    getInst: function(key, workspace) {
      var inst = this._instances[key];
      if (inst) {
        inst = inst[Workspace._members.getId(workspace, null)];
        if (inst) {
          return inst;
        }
      }
      return new this._class._instanceClass(key, workspace);
    },

    /** indent: 1
     * .. js:function:: syncObj(key[, push])
     *
     *    Access to synchronous data objects for a given *key*. To be used with VueJS.
     *
     *    :param String key: The key of the JSONData group to update.
     *    :param Boolean push: If true, all the JSONData objects will be instructed to push their
     *    	data.
     *
     *    :return: The synchronous data object for the given *key*.
     *    :rtype: Object(workspace.id => Any)
     */
    syncObj: function(key, push) {
      var obj = this._keys[key];
      if (obj) {
        var insts = this._instances[key] || {};
        for (var wsId in obj) {
          var inst = insts[wsId] || this.getInst(key, wsId);
          if (push) {
            inst.push();
          }
        }
      } else {
        this._keys[key] = obj = {};
      }
      return obj;
    },
  });

  return JSONData;
});
