(function($) {

  // default options
  var defaults = {
    originalAttribute: "data-original",
    position: "end",
    chars : "..."
  };

  $.fn.ellipsis2 = function(options) {

    options = $.extend({}, defaults, options);

    this.each(function() {
      var $this = $(this);

      var original, attr = options.originalAttribute;
      if ($this.is("[" + attr + "]")) {
        original = $this.attr(attr);
      } else {
        original = $this.text();
        $this.attr(attr, original);
      }

      var position = options.position;
      if (typeof(position) == "string") {
        if (position == "front") position = 0.0;
        else if (position == "middle") position = 0.5;
        else position = 1.0;
      }

      var targetWidth  = $this.width();
      var textWidth    = stringWidth(original, $this);

      if (textWidth <= targetWidth) {
        $this.text(original);
        return;
      }

      var charWidth   = stringWidth(options.chars, $this);
      var wantedWidth = targetWidth - charWidth;

      var cutPos = parseInt(Math.round(original.length * position));
      var left   = original.substr(0, cutPos);
      var right  = original.substr(cutPos);

      while (true) {
        if (!left && !right) {
          break;
        }

        // cut from left or right?
        var cutFromLeft = (left.length/(left.length+right.length) > position && left) || !right;

        if (cutFromLeft) {
          // left
          left = left.substr(0, left.length - 1);
        } else {
          // right
          right = right.substr(1);
        }

        if (stringWidth(left + right, $this) <= wantedWidth) {
          break;
        }
      }

      $this.text(left + options.chars + right);

    });

    return this;
  };

  // string width helper
  var stringWidthStyles = ["font-family", "font-size", "font-style", "font-weight",
                           "text-decoration", "letter-spacing", "line-height", "padding-top",
                           "padding-right", "padding-bottom", "padding-left"];
  var stringWidth = function(s, target) {
    var span = $("<span>").html(s).css("display", "none");

    // style adjustments
    if (target) {
      target = $(target);
      stringWidthStyles.forEach(function(style) {
        span.css(style, target.css(style));
      });
    }

    var width = span.appendTo("body").width();
    span.remove();
    return width;
  };

})(jQuery);
