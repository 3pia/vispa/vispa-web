(function($) {

  var umlauts = [], targets = [], regExps = [];

  var setMap = function(map) {
    umlauts.length = targets.length = regExps.length = 0;
    $.each(map, function(target, umlautList) {
      $.each(umlautList.split(""), function(i, umlaut) {
        targets.push(target);
        umlauts.push(umlaut);
        regExps.push(new RegExp(umlaut, "g"));
      });
    });
  };

  var replaceUmlauts = function(s) {
    $.each(umlauts, function(i) {
      s = s.replace(regExps[i], targets[i]);
    });
    return s;
  };

  var sorter = function(a, b) {
    if (a == b)
      return 0;

    var aConv = replaceUmlauts(a);
    var bConv = replaceUmlauts(b);

    if (aConv != bConv) {
      if (aConv.toLowerCase() == bConv.toLowerCase())
        return aConv > bConv ? 1 : -1;
      else
        return aConv.toLowerCase() > bConv.toLowerCase() ? 1 : -1;
    }

    var aIdx, bIdx;
    for (var i = 0; i < a.length; ++i) {
      aIdx = umlauts.indexOf(a[i]);
      bIdx = umlauts.indexOf(b[i]);
      if (aIdx == bIdx)
        continue;
      else if (~aIdx && ~bIdx)
        return umlauts[aIdx] > umlauts[bIdx] ? 1 : -1;
      else if (~aIdx || ~bIdx)
        return !!~aIdx ? 1 : -1;
    }

    return 0;
  };

  // "array" can contain arbitrary elements, e.g. numbers, string, objects
  // to specify the target used for sorting, you can pass a "selectFn" that
  // receives an element of the array and has to return the target
  // this is, of course, not neccessary for arrays of numbers or strings
  $.LocaleSorter = function(array, selectFn) {
    var sortFn = !$.isFunction(selectFn) ? sorter : function(a, b) {
      return sorter(selectFn(a), selectFn(b));
    };
    return array.sort(sortFn);
  };

  $.LocaleSorter.setMap = setMap;
  $.LocaleSorter.sorter = sorter;

  // default map for de and fr
  var map = {
    a: "\u00e1\u00e0\u00e2\u00e3\u00e4\u0105\u00e5",
    A: "\u00c1\u00c0\u00c2\u00c3\u00c4\u0104\u00c5",
    c: "\u00e7\u0107\u010d",
    C: "\u00c7\u0106\u010c",
    e: "\u00e9\u00e8\u00ea\u00eb\u011b\u0119",
    E: "\u00c9\u00c8\u00ca\u00cb\u011a\u0118",
    i: "\u00ed\u00ec\u0130\u00ee\u00ef\u0131",
    I: "\u00cd\u00cc\u0130\u00ce\u00cf",
    o: "\u00f3\u00f2\u00f4\u00f5\u00f6",
    O: "\u00d3\u00d2\u00d4\u00d5\u00d6",
    s: "\u00df",
    S: "\u1e9e",
    u: "\u00fa\u00f9\u00fb\u00fc\u016f",
    U: "\u00da\u00d9\u00db\u00dc\u016e"
  };
  // set the default map
  setMap(map);


})(jQuery);
