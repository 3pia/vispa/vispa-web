define([
  "jquery",
  "vispa/extension",
  "vispa/views/dialog",
  "vispa/common/dialog",
  "vispa/filehandler2",
  "text!../html/galleryBody.html",
  "css!../css/gallery"
], function (
  $,
  Extension,
  DialogView,
  Dialog,
  FileHandler2,
  galleryBodyTmpl
) {

  var GalleryExtension = Extension._extend({
    init: function init() {
      init._super.call(this, "gallery", "Gallery");

      this.addView(GalleryView);
    }
  });

  var shortcuts = {
    arrowLeft: {
      description: "Navigation in gallery (previous 1)",
      type: "shortcut",
      level: 2,
      value: "left",
      callback: function () {
        this.previous();
      }
    },
    arrowUp: {
      description: "Navigation in gallery (previous 2)",
      type: "shortcut",
      level: 2,
      value: "up",
      callback: function () {
        this.previous();
      }
    },
    arrowRight: {
      description: "Navigation in gallery (next 1)",
      type: "shortcut",
      level: 2,
      value: "right",
      callback: function () {
        this.next();
      }
    },
    arrowDown: {
      description: "Navigation in gallery (next 2)",
      type: "shortcut",
      level: 2,
      value: "down",
      callback: function () {
        this.next();
      }
    },
    escape: {
      description: "Close gallery",
      type: "shortcut",
      level: 2,
      value: "esc",
      callback: function (event) {
        this.close()
      }
    },
  };

  var GalleryView = DialogView._extend({

    init: function init(args) {
      var self = this;
      init._super.apply(self, arguments);

      // parse args
      // items
      self.items = [];
      if (args.items) {
        self.items = args.items;
      } else if (args.path) {
        // only one path given -> create one item
        self.items.push({
          title: args.path.split("/").pop(),
          src: self.applyDefaultQuery(self.dynamicURL("/fs/getfile?path="+args.path)),
          type: args.path.split(".").pop().toLowerCase() == "pdf" ? "iframe" : "img"
        });
      } else if (args.paths) {
        // several paths given -> create items
        $.each(args.paths, function (index, path) {
          self.items.push({
            title: path.split("/").pop(),
            src: self.applyDefaultQuery(self.dynamicURL("/fs/getfile?path="+path)),
            type: path.split(".").pop().toLowerCase() == "pdf" ? "iframe" : "img"
          });
        });
      } else {
        // no information provided
        vispa.messenger.alert("No valid file")
      }
      // index
      self.index = args.index || 0;
      if (self.index < 0) {
        self.index = 0;
      } else if (self.index >= self.items.length) {
        self.index = self.items.length - 1;
      }
      // type
      self.type = args.type || "img";

      this.socket.on("watch", function(data) {
        var watch = data.watch_id.split("_", 2);
        if (watch[0] == "content") {
          if (data.event == "modify") {
            self.refresh(parseInt(watch[1]));
          }
        }
      });
      this.on("close", function() {
        self.POST("/ajax/fs/unwatch", {});
      });
    },

    render: function ($content) {
      // header
      this.icon = "fa-image";
      this.label = "Gallery";

      // setup HTML
      $content.append($(galleryBodyTmpl))
      var newWindowButton = $("<button type='button' id='new-window' class='btn btn-default btn-xs'>\
                                 <i class='glyphicon glyphicon-new-window'></i>\
                               </button>");
      $("span", $(".modal-title", this._dialog.$el)).last().after(newWindowButton);

      // set options and show dialog
      this.focus();
      this.setupCSS();
      this.setupEvents();
      this.select(this.index);

      return this;
    },

    select: function (index) {
      $("img, iframe", this._dialog.$el).css("display", "none");
      var type = this.items[index].type || this.type;
      var $node = $(type + "[index=" + index + "]", this._dialog.$el)
      if ($node.length == 0) {
        var $node = $("<" + type + ">")
        $node.attr({
          src: this.items[index].src + "&watch_id=content_" + index,
          index: index
        });
        $(".vispa-content", this._dialog.$el).append($node);
      } else {
        $node.css("display", "inline");
      }
      this.label = String(index + 1) + "/" + String(this.items.length) + " " + this.items[index].title;
      this.index = index;
      // emit select event
      this.emit("select");
    },

    refresh: function (index) {
      var type = this.items[index].type || this.type;
      $(type + "[index=" + index + "]", this._dialog.$el).remove();
      if (this.index == index);
        this.select(this.index);
    },

    previous: function () {
      if (this.index == 0) {
        this.select(this.items.length - 1);
      } else {
        this.select(this.index - 1);
      }
      // emit select event
      this.emit("previous");
    },

    next: function () {
      if (this.index == this.items.length - 1) {
        this.select(0);
      } else {
        this.select(this.index + 1);
      }
      // emit select event
      this.emit("next");
    },

    new_window: function () {
      window.open(this.items[this.index].src, "_blank");
    },

    setupEvents: function () {
      var self = this;
      // header
      $("#new-window", this._dialog.$el).click(function () {
        self.new_window();
      });
      // body
      $("#previous", this._dialog.$el).click(function () {
        self.previous();
      });
      $("#next", this._dialog.$el).click(function () {
        self.next();
      });
    },

    setupCSS: function () {
      $(".modal-dialog", this._dialog.$el).toggleClass("gallery", true);
      if (this.items.length <= 1) {
        $("#previous", this._dialog.$el).css("display", "none");
        $("#next", this._dialog.$el).css("display", "none");
      }
    }

  }, {
    name: "Gallery",
    iconClass: "fa-image",
    label: "Gallery",
    preferences: {
      items: shortcuts,
    },
    fileHandlers: [{
      label: "Gallery",
      iconClass: "fa-image",
      position: function () {
        var ext = this.value.split(".").pop().toLowerCase();
        return (~["png", "jpg", "jpeg", "bmp", "pdf"].indexOf(ext)) ? 1 : 1001;
      },
      callback: function () {
        vispa.mainMenu.spawn(GalleryView, {
          path: this.value
        });
      }
    }]
  });

  var fext = ["png", "jpg", "jpeg", "bmp", "pdf"];
  FileHandler2.addOpen("gallery", {
    label: "Preview in gallery",
    iconClass: "fa-image",
    position: 10,
  }, function (ext, info) {
    var args = {};
    var base = info.pathBase + "/";
    if (info._f2br) {
      var self = this;
      // TODO: have this in sort order
      args.paths = info._f2br.sortedItems.filter(function (it) {
        return self._testExt(self.ext({data: it}), fext);
      }).map(function (it) {
        return base + it.name;
      });
      args.index = args.paths.indexOf(base + info.data.name);
    } else
      args.path = base + info.data.name;
    return ["gallery", "Gallery", args];
  }, fext);

  return GalleryExtension;
});
