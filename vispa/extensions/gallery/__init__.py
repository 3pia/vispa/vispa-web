# -*- coding: utf-8 -*-

import cherrypy

from vispa.server import AbstractExtension
from vispa.controller import AbstractController


class GalleryController(AbstractController):
    pass

class GalleryExtension(AbstractExtension):

    def name(self):
        return 'gallery'

    def dependencies(self):
        return []

    def setup(self):
        self.add_controller(GalleryController())
