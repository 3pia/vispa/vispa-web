// The dummy extension is a minimal example. For a more verbose example,
// please see the demo extension.
define(["vispa/extensions", "vispa/views/center", "require", "jquery"], function(Extensions, CenterView, require, $) {

  var DummyExtension = Extensions.Extension._extend({
    init: function init() {
      init._super.call(this, "dummy");

      var self = this;

      this.addViews({
        center: DummyView,
        dialog: DummyDialogView
      });

      this.getDefaultPreferences(DummyView).fastMenuEntries.value = [
        "myId", "somekey", "somekey-1", "somekey-2"
      ];

      this.addMenuEntry("New Dummy", function(workspaceId) {
        self.createInstance(workspaceId, DummyView);
      }).addMenuEntry("Open Dummy Dialog", function(workspaceId) {
        self.createInstance(workspaceId, DummyDialogView);
      });
    }
  });


  var DummyView = CenterView._extend({
    init: function init() {
      init._super.apply(this, arguments);

      var self = this;
      this.addMenuEntry("myId", {
        label: "My ID",
        iconClass: "glyphicon glyphicon-info-sign",
        buttonClass: "btn-primary",
        callback: function() {
          this.alert("My ID is: " + this.getId());
        }
      });

      this.addMenuEntry("somekey", {
        label: "My submenu",
        iconClass: "glyphicon glyphicon-ok",
        buttonClass: "btn-primary",
        childrenStyle: "group",
        children: [{
          id: "somekey-1",
          childrenStyle: "split",
          buttonClass: "btn-info",
          callback: function() {
            alert("somekey-1");
          },
          children: [{
            id: "somekey-1-1",
            iconClass: "glyphicon glyphicon-info-sign",
            callback: function() {
              alert("somekey-1-1");
            }
          }]
        }, {
          id: "somekey-2",
          callback: function() {
            alert("somekey-2");
          },
          childrenStyle: "basic",
          children: [{
            id: "somekey-2-1",
            callback: function() {
              alert("somekey-2-1");
            }
          }]
        }]
      });

      this.label = "id: " + this.getId();

      this.socket.on("test-topic", function(data) {
        console.log(self.getId(), "received data on topic 'test-topic':", data.data);
      });
    },

    render: function(node) {
      var self = this;

      this.setLoading(true);
      this.GET("data", function(err, response) {
        if (err) {
          self.setLoading(false);
          return;
        }

        node.html(response);

        // example showing how to open a dialog with a button that opens a file selector
        // var $btn1  = $("<button class='btn btn-success'>").html("Open Dialog");
        var $btn2 = $("<button class='btn btn-danger'>").html("Open File Selector");
        var openFS = function() {
          var args = {
            callback: function(paths) {
              console.log("paths:", paths);
            }
          };
          self.spawnInstance("file", "FileSelector", args);
          //vispa.callbacks.emit("openFileSelector", self.workspaceId, args);
        };
        // $btn1.click(function(event) {
        //   self.dialog({body: $("<div>").append($btn2)});
        // });
        $btn2.click(function(event) {
          openFS();
        });
        node.append($btn2);

        self.setLoading(false);
      });
    }
  });


  var DummyDialogView = CenterView._extend({
    init: function init() {
      init._super.apply(this, arguments);

      this.addMenuEntry("test", {
        iconClass: "glyphicon glyphicon-info-sign",
        callback: function() {
          alert("Test!");
        }
      }).addMenuEntry("test2", {
        callback: function() {
          alert("Foo bar!");
        }
      });
      this.label = "id: " + this.getId();
    },

    render: function($body, $footer) {
      var self = this;

      this.icon = "glyphicon glyphicon-ok"

      $body.html("I'm a dialog!");

      $("<button>")
        .addClass("btn btn-default")
        .html("Close")
        .click(this.close.bind(this))
        .prependTo($footer);

      $("<button>")
        .addClass("btn btn-info")
        .html("Toggle loading")
        .click(function() {
          self.setLoading(!self.isLoading());
        }).prependTo($footer);

      return this;
    }
  });

  return DummyExtension;
});
