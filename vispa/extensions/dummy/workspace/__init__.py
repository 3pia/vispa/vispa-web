# -*- coding: utf-8 -*-

import os
import sys
import logging
from time import sleep
from threading import Thread

logger = logging.getLogger(__name__)


class DummyRpc:
    def __init__(self):
        logger.debug("DummyRpc created")
        self.scheduler = Thread(target=Scheduler, name="Scheduler")
        self.scheduler.daemon = True
        self.scheduler.start()

    def dummy(self):
        logger.debug("DummyRpc::dummy called")
        return ",".join(sys.path)

    def wait(self, cb):
        logger.info("wait started")
        cbs.append(cb)
        sleep(3)
        logger.info("wait done")


cbs = []

class Scheduler:
    def __init__(self):
        while True:
            while len(cbs):
                cbs.pop(0)()
            sleep(1)
