# -*- coding: utf-8 -*-

# imports
import cherrypy
import vispa.workspace
from vispa.controller import AbstractController
from vispa import AjaxException

class DummyController(AbstractController):

    def __init__(self, extension):
        AbstractController.__init__(self)
        self.extension = extension

    @cherrypy.expose
    def data(self):
        content = ""

        rsys = vispa.workspace.module("sys")
        content += "<p>sys.path: %s</p>" % rsys.path

        dummyrpc = self.extension.get_workspace_instance("DummyRpc")
        content += "<p>dummy: %s</p>" % dummyrpc.dummy()

        session_id = self.get("session_id")
        view_id = self.get("view_id")
        def send():
            self.extension.send_socket(view_id, "test-topic",
                session_id=session_id, data="foobar")
        dummyrpc.wait(send)

        return content

    @cherrypy.expose
    def failure(self, msg=None):
        if msg == None:
            msg = "This is a failure message. Something went wrong!"
        raise AjaxException(msg)

    @cherrypy.expose
    @cherrypy.tools.workspace(on=False)
    @cherrypy.tools.json_parameters()
    def sigtest(self, o, l, i, s):
        for elem in (o, l, i, s):
            print "%s (%s)" % (elem, type(elem))
        return "works"