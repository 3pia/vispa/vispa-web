# -*- coding: utf-8 -*-

from vispa.server import AbstractExtension
from controller import DummyController


class DummyExtension(AbstractExtension):

    def name(self):
        return "dummy"

    def dependencies(self):
        return []

    def setup(self):
        self.add_controller(DummyController(self))
        self.add_workspace_directoy()
