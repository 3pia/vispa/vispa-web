# -*- coding: utf-8 -*-

from vispa.server import AbstractExtension
from controller import EditorController


class CodeEditorExtension(AbstractExtension):

    def name(self):
        return "codeeditor"

    def dependencies(self):
        return []

    def setup(self):
        self.add_controller(EditorController())
        self.add_workspace_directoy()
