# -*- coding: utf-8 -*-

import os
import logging
import vispa
import time
import threading
import select
import signal
from subprocess import Popen, PIPE, STDOUT
import fcntl

logger = logging.getLogger(__name__)

def expand(path):
    return os.path.expanduser(os.path.expandvars(path))


class CodeEditorRpc:
    MAX_BURST = 1024*1024  # max bytes transmitted at once
    MAX_RATE = 1024*1024  # desired baud rate for transmission
    BURST_DELAY = 0  # delay between transmission when burst buffer is exausted
    BURST_BUFFER = 1024*1024  # burst buffer size
    SIGTEM_SIGKILL_DELAY = 0.1

    def __init__(self, window_id, view_id):

        self._view_id = view_id
        self._window_id = window_id
        self._topic = "extension.%s.socket" % self._view_id

        self._thread = None
        self._kill = False
        self._abort = False

        self._popen = None

        logger.debug("CodeEditorRpc created")

    # so far equivalent to abort (might be extended in the future)
    def close(self):
        self._abort = True

    def _send(self, topic, data=None):
        vispa.remote.send_topic(
            self._topic + "." + topic, window_id=self._window_id, data=data)

    def runningjob(self):
        if self._popen is None:
            return False

        returncode = self._popen.poll()
        if returncode is None:
            return True
        else:
            return False

        return bool(self._thread and self._thread.is_alive())

    def start(self, cmd, base):
        if self.runningjob():
            return "There is already a job running"

        self._cmd = expand(cmd)
        self._base = expand(base)
        self._starttime = time.time()
        self._read_amount = 0
        self._kill = False
        self._abort = False

        try:
            self._popen = Popen(
                self._cmd,
                cwd=self._base,
                shell=True,
                stdin=PIPE,
                stdout=PIPE,
                stderr=STDOUT,
                bufsize=1,
                preexec_fn=os.setsid # so kill group does not kill this process
            )
        except Exception as e:
            return str(e)

        # send result via socket to ensure in order processing
        self._send('start', {
            "command": self._cmd,
            "base": self._base,
        })

        logger.debug("CodeEditorRpc starting _stream")

        self._thread = threading.Thread(target=self._stream)
        self._thread.daemon = True
        self._thread.start()

        return ""

    def _stream(self):
        returncode = None
        self._abort = False

        fno = self._popen.stdout.fileno()

        fcntl.fcntl(fno, fcntl.F_SETFL, fcntl.fcntl(fno, fcntl.F_GETFL) | os.O_NONBLOCK)
        self._read_time = time.time()

        while not self._abort:
            r, _, _ = select.select([fno], [], [], 0.1)
            if fno in r:
                self._read(self.MAX_BURST)

            returncode = self._popen.poll()
            if returncode is not None:
                break

        # soft kill
        if self._abort:
            pgid = os.getpgid(self._popen.pid)
            os.killpg(pgid, signal.SIGTERM)
            returncode = -signal.SIGTERM
            time.sleep(self.SIGTEM_SIGKILL_DELAY)
            try:
                os.killpg(pgid, signal.SIGKILL)
            except OSError:
                pass
            else:
                returncode = -signal.SIGKILL

        # send remaing data
        self._send("data", self._popen.communicate()[0])

        returncode = self._popen.poll()
        runtime = round(time.time() - self._starttime, 2)

        self._send('done', {
            "success": True,
            "runtime": runtime,
            "returncode": returncode,
            "aborted": self._abort
        })

        logger.debug("CodeEditorExecuteRpc _stream finished")

    def _read(self, burst):
        if self._read_amount >= self.BURST_BUFFER:
            time.sleep(self.BURST_DELAY)
        data = self._popen.stdout.read(burst)
        self._read_amount += len(data)
        self._send('data', data)
        if self._read_amount > 0:
            self._read_amount = int(max(0, self._read_amount - self.MAX_RATE * (time.time() - self._read_time)))
            self._read_time = time.time()
        return bool(data)

    def abort(self):
        if self.runningjob():
            pgid = os.getpgid(self._popen.pid)
            if self._kill:
                os.killpg(pgid, signal.SIGKILL)
            else:
                self._kill = True
                os.killpg(pgid, signal.SIGTERM)
            return self.runningjob()
        else:
            return False
