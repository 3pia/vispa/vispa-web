define(["jquery", "emitter", "text!../html/preview.html"], function($, Emitter, PreviewTemplate) {

  var CodeEditorPreview = Emitter._extend({

    init: function init(view) {
      init._super.call(this);
      var self = this;

      this.view = view;
      this.editor = view.editor;

      this.actionBtn = null;
      this.loaderInterval = null;
      this.previewArea = null;
      this.pathInput = null;
      this.previewChanged = true;
      this.previewUpdating = false;
      this.inputFocused = false;
      this.alphabeticSort = false;

      this.shadowTmpl = "inset {0}px 0px #428cba";

      this.allowedExtensions = ["png", "jpg", "jpeg", "bmp"];
      this.pictureExtensions = ["png", "jpg", "jpeg", "bmp"];

      this.previewData = [];

      this.thumbnailSize = null;


      this.view.on("changedState", function(key) {
        switch (key) {
          case "path":
            var path = self.view.state.get("path");
            if (path && !self.view.state.get("previewPath")) {
              var cookie = $.cookie("VISPA-preview-path");
              if (cookie && self.view.prefs.get("cookies")) {
                cookie = JSON.parse(cookie);
                var key = self.view.state.get("path");
                key = key ? key.split("/").pop() : "UNTITLED";
                if (cookie[key]) {
                  self.setPath(cookie[key], false);
                  return;
                }
              }
              self.setPath(path, true);
            }
            break;
          case "previewPath":
            var previewPath = this.state.get("previewPath");
            if (previewPath) {
              if (self.pathInput) {
                self.pathInput.val(previewPath);
                self.pathToCookie(previewPath);
              }
              self.previewChanged = true;
              self.refresh();
            }
            break;
        }
      });
    },

    setup: function(node) {
      var self = this;
      var rightBottomNode = node.find(".codeeditor-right-bottom");

      this.actionBtn = $("#zoom", rightBottomNode);
      this.previewArea = $(".well", rightBottomNode);
      this.pathInput = $("#path-input", rightBottomNode);

      // events
      //set the previewSize
      this.actionBtn.find("li").click(function() {
        var action = $(this).data("action");
        self.handleAction(action);
      });
      //open fileselector
      $(".select-button", rightBottomNode).click(this.selectPath.bind(this));
      this.pathInput.on("keypress", function(event) {
        if (event.keyCode == 13)
          this.blur();
      }).focus(function() {
        self.inputFocused = true;
      }).blur(function() {
        self.previewChanged = true;
        self.view.state.set("previewPath", this.value);
        self.inputFocused = false;
      });

      this.view.socket.on("watch", function(data) {
        if (data.watch_id != "preview") {
          return;
        }
        if (data.event == "vanish") { // TODO: actually do something useful
          return;
        }
        self.previewChanged = true;
      });

      this.thumbnailSize = this.view.prefs.get("previewSize");
      this.alphabeticSort = this.view.prefs.get("previewOrder");

      // start refrehsing
      self.refresh();
      self.loaderInterval = setInterval(function() {
        self.refresh();
      }, 1000);
      self.view.emit("changedState", "previewPath");

      return this;
    },

    clearInterval: function() {
      if (this.loaderInterval !== null) {
        clearInterval(this.loaderInterval);
      }
      return this;
    },

    //refresh initialized with size as indicated in the prefs
    externalRefresh: function() {
      this.thumbnailSize = this.view.prefs.get("previewSize");
      this.previewChanged = true;
    },

    refresh: function() {
      var self = this;
      if (this.inputFocused || !this.pathInput) {
        return this;
      }

      var path = this.pathInput.val();

      if (!path) {
        return this;
      }

      if (this.previewUpdating) {
        return this;
      }

      if (!this.previewChanged) {
        return this;
      }
      this.previewUpdating = true;
      this.previewChanged = false;
      //clear the entire preview
      this.clearPreviews();

      if (path.substr(-1) == "/")
        path = path.substr(0, path.length - 1);

      self.view.GET("/ajax/fs/filelist", {
        path: path,
        filefilter: "\\.(" + this.allowedExtensions.join("|") + ")$",
        reverse: true,
        watch_id: "preview"
      }, function(err, res) {
        if (err) {
          self.previewUpdating = false;
          return;
        }

        var files = $.grep(res.filelist, function(file) {
          return file.type == "f" && ~self.allowedExtensions.indexOf(file.name.split(".").pop().toLowerCase());
        });
        var previewData = [];
        files.forEach(function(file) {
          var filePath = path + "/" + file.name;
          if (~self.pictureExtensions.indexOf(file.name.split(".").pop().toLowerCase())) {
            var filePath = path + "/" + file.name;
            var src = vispa.dynamicURL(vispa.format(
              "/fs/thumbnail?path={0}&height={1}&width={1}&_workspaceId={2}&_mtime={3}",
              filePath,
              self.thumbnailSize,
              self.view.workspaceId,
              file.mtime
            ));
            var srcFull = vispa.dynamicURL(vispa.format(
              "/fs/getfile?path={0}&_workspaceId={1}&_mtime={2}",
              filePath,
              self.view.workspaceId,
              file.mtime
            ));

            previewData.push({
              type: "picture",
              filePath: filePath,
              title: filePath.split("/").pop(),
              src: src,
              srcFull: srcFull,
              mtime: file.mtime
            });
          } else {
            previewData.push({
              type: "other",
              extension: file.name.split(".").pop().toLowerCase(),
              filePath: filePath,
              mtime: file.mtime
            });
          }
        });

        // sort by mtime
        previewData = previewData.sort(function(a, b) {
          if (self.alphabeticSort) return a.title < b.title;
          else return a.mtime - b.mtime;
        });

        previewData.forEach(function(data) {
          self.addPreview(data);
        });

        this.previewData = previewData;

        self.previewUpdating = false;
      });

      return this;
    },

    clearPreviews: function() {
      this.previewData = [];
      this.previewArea.children().each(function(i, child) {
        $(child).remove();
      });
    },

    getPreviewData: function(filePath) {
      var data = null;
      this.previewData.forEach(function(_data) {
        if (!data && _data.filePath == filePath)
          data = _data;
      });
      return data;
    },

    addPreview: function(data) {
      this.previewData.push(data);
      this._addPreviewNode(data);
      return this;
    },

    _addPreviewNode: function(data) {
      var self = this;
      var node = $(PreviewTemplate).prependTo(this.previewArea);
      node.find("img").css("height", String(this.thumbnailSize) + "px");
      var title = data.filePath.split("/").pop();
      node.find("span").html(title);
      if (data.type == "picture") {
        node.find("img").attr("src", data.src);
        node.click(function() {
          var previewArray = [];
          var startIndex = 0;
          var count = 0;
          for (var i = self.previewData.length - 1; i >= 0; i--) {
            var data = self.previewData[i];
            if (data.type != "picture")
              continue;
            if (title == data.title)
              startIndex = count;
            previewArray.push({ title: data.title, src: data.srcFull });
            count = count + 1;
          }
          self.view.spawnInstance("gallery", "Gallery", {
            items: previewArray,
            index: startIndex
          });
        });

      } else {
        node.css({
          "height": String(this.thumbnailSize) + "px",
          "width": String(this.thumbnailSize) + "px",
          "cursor": "pointer"
        });
        node.click(function() {
          self.clickHandler(data.extension, data);
        });
      }

      return this;
    },

    selectPath: function() {
      var self = this;
      var path = this.pathInput.val();
      self.view.GET("/ajax/fs/exists", {
        path: path
      }, function(err, selectedType) {
        if (err || selectedType != "d") {
          path = "$HOME";
        }

        var args = {
          "label": "Select preview folder",
          "path": path,
          "foldermode": true,
          callback: function(path) {
            self.setPath(path, false);
          }
        };
        self.view.spawnInstance("file2", "FileSelector", args);
      });

      return this;
    },

    setPath: function(path, isFile) {
      if (isFile) {
        var parts = path.split("/");
        parts.pop();
        path = parts.join("/");
      }

      this.view.state.set("previewPath", path);
      return this;
    },

    handleAction: function(action) {
      switch (action) {
        case "default":
          size = this.view.prefs.get("previewSize");
          break;
        case "in":
          this.thumbnailSize = Math.round(this.thumbnailSize * 1.5);
          break;
        case "out":
          this.thumbnailSize = Math.round(this.thumbnailSize * 2.0 / 3.0);
          break;
        case "alphabet-sort":
          this.alphabeticSort = true;
          this.view.prefs.set("previewOrder", true);
          break;
        case "mtime-sort":
          this.alphabeticSort = false;
          this.view.prefs.set("previewOrder", false);
          break;
          size = this.view.prefs.get("previewSize");
      }
      this.previewChanged = true;
    },

    pathToCookie: function(previewPath) {
      if (!this.view.prefs.get("cookies"))
        return;
      var cookie = $.cookie("VISPA-preview-path");
      if (cookie)
        cookie = JSON.parse(cookie);
      else
        cookie = {};
      var key = this.view.state.get("path");
      key = key ? key : "UNTITLED";
      cookie[key] = previewPath;
      $.cookie("VISPA-preview-path", JSON.stringify(cookie), {
        expires: 7
      });
    },

    clickHandler: function(extension, data) {
      switch (extension) {
        case "root":
          this.view.spawnInstance("jsroot", "jsroot", {
            path: data.filePath
          });
          break;
        default:
          return;
      }
      return;
    }
  });

  return CodeEditorPreview;
});
