define(["jquery", "emitter", "vispa/utils"], function($, Emitter, Utils) {

  var CodeEditorActions = Emitter._extend({

    init: function init(editor, view) {
      init._super.call(this);
      var self = this;

      this.editor = editor;
      this.view = view;

      this.mtime = null;
      this.savingProcessing = false;
      this.lastContent = "";
      this.modifiedProcessing = false;
      this.autosaveTimeOut = null;
    },

    watchSetup: function() {
      var self = this;

      this.view.socket.on("watch", function(data) {
        if (data.watch_id != "code") {
          return;
        }
        if (self.modifiedProcessing)
          return;
        if (data.mtime == -1) {
          self.modifiedProcessing = true;
          self.view.confirm(
            "The file " + self.editor.path + " has been deleted or renamed." +
            "<br/>Do you want to recreate it?",
            function(confirmed) {
              if (confirmed) {
                self.save(null, "new");
              } else {
                self.view.state.set("path", undefined);
                self.editor.path = null;
                self.view.setLabel(null, false);
              }
              self.modifiedProcessing = false;
            });
        } else if (data.mtime > self.mtime && !self.savingProcessing) {
          self.modifiedProcessing = true;
          self.view.confirm(
            "The file " + self.editor.path + " has changed on disk." +
            "<br />Do you want to reload it?",
            function(confirmed) {
              if (confirmed)
                self.loadContent(self.editor.path);
              else {
                self.view.state.set("path", undefined);
                self.editor.path = null;
                self.view.setLabel(null, false);
              }
              self.modifiedProcessing = false;
            });
        }
      });
    },


    loadContent: function(path) {
      var self = this;

      path = path || this.editor.path;
      if (!path) {
        this.checkModifications();
        return this;
      }

      this.view.setLoading(true);

      var setContent = function(res, reset) {
        reset = reset === undefined ? false : true;

        if (!res.success) {
          self.view.alert("<html>An internal server error has occured: <br />" +
            "" + res.error + " <br />" +
            "<br /> The editor has been closed. </html>");
          self.view.close();
        }
        if (res.size_limit) {
          self.view.alert("<html> The file size exceeds " + String(res.max_size) + " MB. <br />" +
            "Only the first lines are displayed and the mode is 'read-only'. </html>");
        }
        self.editor.path = path;
        self.view.state.set("writable", self.view.state.get("writable") && res.writable, 1);
        self.view.state.set("path", path);
        self.parseShebang(res.content);
        self.parsePreview(res.content);
        self.mtime = res.mtime;
        self.editor.setContent(res.content);
        if (!reset)
          self.lastContent = self.editor.getContent();
        self.checkModifications();
        self.editor.setMode();
        self.editor.goToTop();
        self.removeAutoSaveFile();
        self.setupAutoSave();
        self.editor.goToLastLine();
        self.view.setLoading(false);
      };
      //new: check for auto save tmp file
      this.view.GET("/ajax/fs/getfile", {
        path: self.giveAutoSavePath(),
        utf8: true
      }, function(err, res) {
        if (err) {
          self.view.setLoading(false);
          return;
        }
        if (res.success) { //an autosave file exists
          self.view.confirm("An autosave file exists. Do you want to load its content?",
            function(b) {
              if (b)
                setContent(res, true);
              else
                self.view.GET("/ajax/fs/getfile", {
                  path: path,
                  utf8: true,
                  watch_id: "code"
                }, function(err, res) {
                  if (err) {
                    self.view.setLoading(false);
                    return;
                  }
                  setContent(res);
                });
            });
        } else {
          self.view.GET("/ajax/fs/getfile", {
            path: path,
            utf8: true,
            watch_id: "code"
          }, function(err, res) {
            if (err) {
              self.view.setLoading(false);
              return;
            }
            setContent(res);
          });
        }
      });
      return this;
    },

    parseShebang: function(content) {
      var shebang = content.match(/^#! ?(\S+) *(.*)/);
      if (!shebang) return;
      var command = Utils.singleQuote(shebang[2], /\s/);
      if (shebang[1] == "/usr/bin/env") {
        if (shebang[2].startsWith("-S"))
          command = shebang[2].replace(/^-S\s+/, "");
      } else
        command = Utils.singleQuote(shebang[1], /\s/) + " " + command;
      command += " '%file'"
      this.view.state.set("command", command);
    },

    parsePreview: function(content) {
      var preview = content.match(/(?:preview|plot)s?_?(?:path|dir|prefix)\s*[:=]*\s*(["']+)(.+?)\1[^\n"']*\bVISPA\b[^\n"']*$/im);
      if (!preview) return;
      preview = preview[2];
      if (!preview.startsWith("/")) {
        var parts = this.view.state.get("path").split("/");
        parts[parts.length - 1] = preview;
        preview = parts.join("/");
      }
      this.view.state.set("previewPath", preview);
    },

    setupAutoSave: function() {
      var self = this;

      clearTimeout(this.autosaveTimeOut);
      this.autosaveTimeOut = null;

      var t = self.view.prefs.get("autosave");
      t = t * 1000.0;
      if (t == 0)
        return this;

      this.autosaveTimeOut = setTimeout(function() {
        if (self.editor.path && self.view.state.get("writable") && self.view.modified) {
          self.savingProcessing = true;
          self.view.POST("/ajax/fs/savefile", {
            path: self.giveAutoSavePath(),
            content: self.editor.getContent(),
            utf8: true
          }, function(err, res) {
            if (err) return;
            self.savingProcessing = false;
            self.mtime = res.mtime;
            self.setupAutoSave();
          });
        } else
          self.setupAutoSave();
      }, t);
    },

    checkModifications: function() {
      clearTimeout(this.editor.keyupMinTimeout);
      clearTimeout(this.editor.keyupMaxTimeout);
      this.editor.keyupMinTimeout = null;
      this.editor.keyupMaxTimeout = null;

      var isModified = this.editor.getContent() != this.lastContent;
      if (isModified != this.view.modified) {
        this.view.modified = isModified;
      }
      this.toggleSaveButton(isModified, this.editor.path, this.view.state.get("writable"));

      return this;
    },

    toggleSaveButton: function(isModified, hasPath, writable) {
      this.view.menu.get("save").disabled = !(isModified && hasPath && writable);

      // var btn = this.view._getFastMenuEntryNode("save");
      // if (btn) {
      //   var msg = "";
      //   if (!(isModified && hasPath && writable)) {
      //     msg = "Saving disabled:";
      //     if (!isModified)
      //       msg = msg + "\n - file unmodified";
      //     if (!hasPath)
      //       msg = msg + "\n - path to file does not exist";
      //     if (writable == false)
      //       msg = msg + "\n - no writing permission";
      //   }
      //   btn.attr("title", msg);
      // }
      return this;
    },

    saveAs: function(callback) {
      var self = this;

      // get the default path for the file selector
      var path = null;
      if (this.editor.path) {
        var parts = this.editor.path.split("/");
        parts.pop();
        path = parts.join("/");
      }

      //file selection with selector
      var args = {
        path: path,
        savemode: true,
        callback: function(path) {
          if (path === null)
            return;
          self.view.GET("/ajax/fs/exists", {
            path: path
          }, function(err, selectedType) {
            //check if saving is possible
            var existingFile = selectedType == "f";

            //case: no problems
            if (!existingFile) {
              self.save(callback, "new", path);
              return;
            }

            //case: problems-->addtional confirm
            var msg = "";
            if (existingFile)
              msg = "<html> File already exists. <br />" +
              "Do you want to overwrite it ? </html>";
            self.view.confirm(msg, function(confirmed) {
              if (confirmed)
                self.save(callback, "new", path);
              else
                return;
            });
          });
        },
        sort: "name",
        reverse: false
      };
      this.view.spawnInstance("file2", "FileSelector", args);

      return this;
    },

    save: function(callback, option, path) {
      var self = this;
      var isNew = option == "new";

      this.setupAutoSave();
      this.removeAutoSaveFile();
      this.checkModifications();

      if (this.editor.path && !this.view.modified && !isNew) {
        if ($.isFunction(callback))
          callback();
        return this;
      }

      if (!this.view.state.get("writable") && !isNew) {
        console.log("No writing access!");
        if ($.isFunction(callback))
          callback();
        return this;
      }

      if (!this.editor.path && !path) {
        //a new file must be given a default command
        var extendedCB = function() {
          if ($.isFunction(callback))
            callback();
          self.editor.view.commandLine.setDefault();
        };
        this.saveAs(extendedCB);
        return this;
      }

      this.view.setLoading(true);
      this.savingProcessing = true;

      this.view.POST("/ajax/fs/savefile", {
        path: path !== undefined ? path : this.editor.path,
        content: this.editor.getContent(),
        utf8: true,
        watch_id: "code"
      }, function(err, res) {
        if (err) {} else if (!res.success) {
          self.view.alert("Saving not possible. Please check file permissions.");
        } else {
          self.mtime = res.mtime;
          self.editor.path = res.path;
          self.view.state.set("writable", true, 1);
          self.view.state.set("path", res.path);
          self.lastContent = self.editor.getContent();
          self.checkModifications();
          if (isNew) {
            self.editor.manualSyntax = false;
            self.view.preview.setPath(self.editor.path, true);
          }
          self.editor.setMode();
          if ($.isFunction(callback))
            callback();
        }
        self.view.setLoading(false);
        self.savingProcessing = false;
      });

      return this;
    },

    giveAutoSavePath: function() {
      if (!this.editor.path)
        return "";
      var path = this.editor.path;
      var parts = path.split("/");
      var file = parts.pop();
      path = parts.join("/") + "/." + file + "~";

      return path;
    },

    removeAutoSaveFile: function() {
      this.view.POST("/ajax/fs/remove", {
        "path": JSON.stringify(this.giveAutoSavePath())
          //syntax of path required for the fs remove function
      });
    }

  });

  return CodeEditorActions;
});
