define(function() {

  var metaCtrl = function (key) {
    return "mac:meta+"+key+" ctrl+"+key;
  };

  var defaultPreferences = {
    // ui
    inputOutputRatio: {
      level: 4,
      label: "inputOutputRatio",
      descr: "The width ratio between text area and output area. 100 = text area only.",
      type: "integer",
      range: [0, 100, 1],
      value: 50
    },
    outputPreviewRatio: {
      level: 4,
      label: "outputPreviewRatio",
      descr: "The height ratio between output and preview window. 100 = output only.",
      range: [0, 100, 1],
      type: "integer",
      value: 60
    },
    //input editor
    theme: {
      level: 0,
      label: "theme",
      description: "The editor's theme.",
      type: "string",
      value: "textmate",
      options: ["chrome", "eclipse", "monokai", "textmate", "xcode"]
    },
    fontSize: {
      level: 0,
      label: "fontSize",
      description: "The editor\'s font size.",
      type: "integer",
      value: 14,
      options: [8, 10, 12, 14, 16, 18, 20, 24, 30]
    },
    showInvisibles: {
      level: 2,
      label: "showInvisibles",
      type: "boolean",
      value: false,
      description: "Toggles invisible characters."
    },
    indentationGuides: {
      level: 1,
      label: "indentationGuides",
      description: "Toggles intendation guides.",
      type: "boolean",
      value: true,
    },
    tabIndent: {
      level: 0,
      label: "tabIndent",
      description: "Indent using tabs.",
      type: "boolean",
      value: false
    },
    ruler: {
      level: 1,
      label: "ruler",
      description: "The ruler position. 0 means no ruler.",
      type: "integer",
      value: 80,
      options: [0, 60, 80, 100, 120, 140, 180, 200]
    },
    //autosave
    autosave: {
      level: 1,
      label: "autosave",
      description: "Automatic save time interval (seconds). '0' turns it off.",
      type: "integer",
      value: 60,
      range: [0, 300, 10]
    },
    //autosave
    autoCompletion: {
      level: 0,
      label: "auto complete",
      description: "Enable ace's autocompletion?",
      type: "boolean",
      value: true,
    },
    // output
    autoClear: {
      level: 1,
      label: "autoClear",
      description: "Clear the output between jobs?",
      type: "boolean",
      value: false
    },
    maxLines: {
      level: 2,
      label: "maxLines",
      description: "Set the maximum number of lines of the output.",
      type: "integer",
      value: 2000,
      range: [100, 5000, 100]
    },
    // preview
    previewSize: {
      level: 4,
      label: "previewSize",
      description: "The size of the preview pictures.",
      type: "integer",
      value: 100,
      range: [50, 500, 10]
    },
    previewOrder: {
      description: "The order by which the preview images are arranged. True: alphabetically; False: make time",
      type: "boolean",
      value: false
    },

    //history commands
    replacements: {
      level: 2,
      label: "replacements",
      type: "string",
      value: "%croot==-I $(root-config --incdir) $(root-config --libs);",
      description: "Paste your command replacement logic in this field." +
        " Rules: The assignment is performed via '==', the end of a statement is indicated by ';'." +
        " Do not use empty spaces after the logical operators! Also, the following characters are not" +
        " allowed: ` "
    },
    commandHistory: {
      level: 2,
      label: "commandHistory",
      description: "The total number of stored commands in a session.",
      type: "integer",
      value: 3,
      range: [1, 10, 1]
    },
    cookies: {
      level: 3,
      label: "cookies",
      description: "Allow the web-browser to store the last executed commands and preview paths of each file.",
      type: "boolean",
      value: true
    },
    saveShortcut: {
      level: 2,
      label: "save",
      description: "Save the input editor's content.",
      type: "shortcut",
      value: metaCtrl("s"),
      callback: function() {
        this.editor.actions.save();
      }
    },
    executeShortcut: {
      level: 2,
      label: "execute",
      description: "Execute the current scipt.",
      type: "shortcut",
      value: metaCtrl("e"),
      callback: function() {
        this.output.execute();
      }
    },
    commentShortcut: {
      level: 2,
      label: "comment",
      description: "Add a file specific comment lign.",
      type: "shortcut",
      value: metaCtrl("d"),
      callback: function() {
        this.editor.addComment();
      }
    },
    helpShortcut: {
      level: 2,
      label: "help",
      description: "Lists all defined shortcuts for the current 'Ace' session.",
      type: "shortcut",
      value: metaCtrl("shift+h"),
      callback: function() {
        this.editor.listAceShortcuts();
      }
    }
  };

  var menu = {
    save: {
      label: "Save",
      iconClass: "glyphicon glyphicon-floppy-disk",
      callback: function() {
        this.$root.instance.editor.actions.save();
      },
      unique: true,
      position: 1
    },
    saveAs: {
      label: "Save as ...",
      iconClass: "glyphicon glyphicon-floppy-save",
      callback: function() {
        this.$root.instance.editor.actions.saveAs();
      },
      position: 2
    },
    openFile: {
      label: "Open file...",
      iconClass: "glyphicon glyphicon-file",
      callback: function() {
        this.$root.instance.openNew();
      },
      position: 3
    },
    saveOutput: {
      label: "Save output as...",
      iconClass: "glyphicon glyphicon-floppy-save",
      callback: function() {
        this.$root.instance.output.saveAs();
      },
      position: 4
    },
    highlightingMode: {
      // label: "Highlighting mode",
      label: function() {
        return this.value ? "Current mode: "+this.value : "Highlighting mode";
      },
      iconClass: "glyphicon glyphicon-asterisk",
      items: {},
      unique: true,
      multiple: false,
      fastmenu: "dropdown",
      position: 5
    },
    updateInfo: {
      label: "1.1 Update",
      iconClass: "glyphicon glyphicon-info-sign",
      callback: function() {
        this.$root.instance.updateInfo.start();
      },
      position: 6
    },
  };

  var highlightModes = {
    "C/C++":      "c",
    "XML":        "xml",
    "JavaScript": "js",
    "CSS":        "css",
    "SH":         "sh",
    "Python":     "py",
    "R":          "",
    "Matlab":     "matlab",
    "JSON":       "json",
    "Text":       "",
  };

  var highlightModeSelect = function() {
    this.$root.instance.editor.setSyntaxHighlighting(this.value);
  };

  for(var i in highlightModes) {
    menu.highlightingMode.items[i] = {
      label: i,
      position: i, // dirty sorting
      callback: highlightModeSelect,
      value: i,
    };
  }

  var fileExtensions = ["bsc", "c", "cc", "conf", "cpp", "csh", "css", "cxx", "diff", "f", "f70",
    "f90", "f95", "f03", "h", "hh", "hxx", "hpp", "html", "ini", "java", "js", "less", "log", "json",
    "m", "md", "orig", "php", "py", "r", "rb", "sh", "tex", "txt", "xml", "yml", "yaml", "zsh"
  ];

  var fileHandlers = [{
    label: "Code Editor",
    iconClass: function () {
      var ext = this.value.split(".").pop().toLowerCase();
      return (~fileExtensions.indexOf(ext)) ? ("fi fi-" + ext) : "fa fa-file-text";
    },
    position: function () {
      var ext = this.value.split(".").pop().toLowerCase();
      return (~fileExtensions.indexOf(ext)) ? 1 : 1001;
    },
    callback: function () {
      (this.$root && this.$root.view || vispa).spawnInstance("codeeditor", "CodeEditor",
        {path: this.value});
    }
  }];

  return {
    preferences: defaultPreferences,
    menu: menu,
    extensions: fileExtensions,
    handlers: fileHandlers,
  };

});
