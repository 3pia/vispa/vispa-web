define([
  "jquery",
  "vispa/views/main",
  "./prefs",
  "./editor",
  "./output",
  "./preview",
  "./ui",
  "./commandline",
  "./updateInfo",
  "text!../html/main.html",
], function(
  $,
  MainView,
  Prefs,
  Editor,
  Output,
  Preview,
  UI,
  CommandLine,
  UpdateInfo,
  MainTemplate
) {

  var CodeEditorView = MainView._extend({
    init: function init(args) {
      init._super.apply(this, arguments);
      var self = this;

      this.editor = new Editor(this);
      this.output = new Output(this);
      this.preview = new Preview(this);
      this.ui = new UI(this);
      this.commandLine = new CommandLine(this);
      //this.updateInfo = new UpdateInfo(this);

      // trigger inital shortcut setup manually since we needed editor ready
      this.editor.applyShortcuts();

      // set default state
      this.state.setup({
        path: undefined,
        previewPath: undefined,
        command: undefined,
        writable: true,
        showOutput: true,
        line: 0,
      }, args);

      this.forceClose = false;
    },

    getFragment: function() {
      return this.state.get("path") || "";
    },

    applyFragment: function(fragment) {
      this.state.set("path", fragment);
      return this;
    },

    applyPreferences: function applyPreferences() {
      applyPreferences._super.call(this);

      // inputOutputRatio
      this.ui.setInputOutputRatio();
      // textPictureRatio
      this.ui.setTextPictureRatio();

      // theme
      this.editor.setTheme();
      // fontSize
      this.editor.setFontSize();
      // showInvisibles
      this.editor.setShowInvisibles();
      // indentationGuides
      this.editor.setShowIndentationGuides();
      // tabIndent
      this.editor.setTabIndentation();
      // ruler
      this.editor.setRuler();
      // autosave
      this.editor.actions.setupAutoSave();
      //autoCompletion
      this.editor.setAutoCompletion();
      //previewSize
      this.preview.externalRefresh();

      this.editor.applyShortcuts();

      return this;
    },

    render: function(node) {
      this.setLoading(true);

      $(MainTemplate).appendTo(node);

      // setup the ace editor
      this.editor.setup(node);

      // setup the output
      this.output.setup(node);

      // setup the preview
      this.preview.setup(node);

      //setup the UI and the auto resizes
      this.ui.setup();

      //setup the commandline
      this.commandLine.setup(node);

      this.applyPreferences();
      // this._applyShortcuts();
      //initially set the icon
      this.icon = "fa-align-left";
      this.setLoading(false);

      //setup/show the help dialog
      // this.updateInfo.setup(node);
    },

    onResize: function() {
      if (this.editor) {
        this.editor.resize();
      }
    },

    onFocus: function() {
      if (this.editor && this.editor.ace) {
        this.editor.ace.focus();
      }
    },

    onBlur: function() {
      if (this.editor && this.editor.ace) {
        this.editor.ace.blur();
      }
    },

    onBeforeClose: function() {
      var self = this;

      // autosave disabled immediately to avoid unwanted saving
      clearTimeout(self.editor.actions.autosaveTimeOut);

      // cleanup
      var cleanUp = function() {
        self.POST("/ajax/fs/unwatch", {});
        self.editor.actions.removeAutoSaveFile();
        self.POST("close");
        //to be replaced if #2164 is solved (place this call in the appropriate lines in ui.js)
        self.output.running = false;
        self.preview.clearInterval();
        clearTimeout(self.editor.keyupMinTimeout);
        clearTimeout(self.editor.keyupMaxTimeout);
      };
      var checkSave = function() {
        if (doAskSave)
          self.confirm("Save changes before closing?", function(b) {
            var afterSave = function() {
              self.forceClose = true;
              cleanUp();
              self.close(); //close the editor after saving
            };
            if (b) {
              if (self.state.get("writable"))
                self.editor.actions.save(afterSave);
              else
                self.editor.actions.saveAs(afterSave);
            } else
              afterSave();
          });
        else {
          cleanUp();
          self.forceClose = true;
          self.close();
        }
      };
      // ask for saving
      var doAskSave = !this.forceClose && this.modified;
      //ask if job is still running
      var doAskRunning = !this.forceClose && this.output.running;

      if (doAskRunning) //first check if job is still running
        self.confirm("<html>A job is still running. <br />" +
        "Abort and close anyway?</html>",
        function(b) {
          if (b)
            checkSave();
          else {
            self.editor.actions.setupAutoSave();
          }
        });
      //if that is not the case, ask for modifications
      else if (doAskSave)
        checkSave();
      else
        cleanUp();

      return !doAskSave && !doAskRunning;
    },

    openNew: function() {
      var self = this;

      //get the current workpath
      var path;
      if (this.editor.path) {
        path = this.editor.path;
        var parts = path.split("/");
        parts.pop();
        path = parts.join("/");
      }

      var args = {
        path: path,
        multimode: false,
        callback: function(path) {
          if (path === null)
            return;
          var fileExtension = path.split(".").pop().toLowerCase();
          if (Prefs.extensions.indexOf(fileExtension) == -1) {
            var msg = "<html>File extension '" + fileExtension +
              "' is not supported! <br /> Open anyway?</html>";
            self.confirm(msg, function(confirmed) {
              if (confirmed) {
                self.spawnInstance("codeeditor", "CodeEditor", {
                  path: path
                });
              }
            });
          } else {
            self.spawnInstance("codeeditor", "CodeEditor", {
              path: path
            });
          }


        },
        sort: "name",
        reverse: false
      };

      this.spawnInstance("file2", "FileSelector", args);
    }
  },{
    iconClass: "fa-file-text",
    label: "CodeEditor",
    name: "CodeEditor",
    menuPosition: 30,
    preferences: {
      items: Prefs.preferences,
    },
    menu: Prefs.menu,
    fastmenu: ["save","close","saveAs"],
    fileHandlers: Prefs.handlers,
  });

  return CodeEditorView;
});
