define(["jquery", "emitter", "text!../html/updateInfo.html"], function($, Emitter, UpdateInfoTemplate) {

  var UpdateInfo = Emitter._extend({

    init: function(view) {
      this.view = view;
      this.centerviewNode = null;
      this.headerNode = null;
      this.textNode = null;
      this.pageCounterNode = null;
      this.checkBoxNode = null;
      this.totalPages = content.length;
      this.pageCounter = null;
      this.highlightedElement = null;
    },

    setup: function(node) {
      this.centerviewNode = node;
      if (this.checkPreference())
        this.start();
    },

    start: function() {
      var self = this;

      var dialog = null;
      var $all = $(UpdateInfoTemplate);
      var $body = $all.children().first();
      var $footer = $all.children().last();

      self.headerNode = $body.find("#header");
      self.textNode = $body.find("#textfield");
      self.pageCounterNode = $body.find("#pagecounter");
      self.checkBoxNode = $body.find("input");

      //make the page
      self.checkBoxNode.prop("checked", !self.checkPreference());
      self.makePage(1);
      //setup buttons
      $footer.find("#start").click(function() {
        self.makePage(1);
      });
      $footer.find("#previous").click(function() {
        self.makePage(self.pageCounter - 1);
      });
      $footer.find("#next").click(function() {
        self.makePage(self.pageCounter + 1);
      });
      $footer.find("#end").click(function() {
        self.makePage(self.totalPages);
      });
      $footer.find("#quit").click(function() {
        dialog.close();
      });

      self.view.dialog({
        header: "<i class='glyphicon glyphicon-info-sign'></i>" +
          " 1.1 Update",
        body: $body,
        footer: $footer,
        beforeClose: function(cb) {
          self.prefs.set(!self.checkBoxNode.prop("checked"));
          self.highlightElement();
          //code editor specific stuff
          //*****
          //must be triggered
          //so the save button is correctly toggled
          self.view.editor.actions.checkModifications();
          //*****
          cb(); //callback == close call
        },
        onRender: function() {
          dialog = this;
        }
      });
    },

    makePage: function(page) {
      if (page === undefined)
        return this;

      if (page < 1)
        this.pageCounter = 1;
      else if (page > this.totalPages)
        this.pageCounter = this.totalPages;
      else
        this.pageCounter = page;

      this.pageCounterNode.html(String(this.pageCounter + "/" + String(this.totalPages)));

      var header = content[this.pageCounter - 1].header;
      var text = content[this.pageCounter - 1].text;
      var selector = content[this.pageCounter - 1].selector;

      this.headerNode.html(header);
      this.textNode.html(text);
      this.highlightElement(selector);

      return this;
    },

    highlightElement: function(selector) {
      if (this.highlightedElement)
        this.highlightedElement.css("background-color", "");
      if (selector === undefined)
        return this;
      this.highlightedElement = this.centerviewNode.find(selector);
      this.highlightedElement.css("background-color", "red");

      return this;
    },

    checkPreference: function() {
      return this.view.prefs.get("updateTour");
    },

    setPreference: function(value) {
      if (value === undefined)
        value = false;
      this.view.prefs.set("updateTour", value);

      return this;
    }

  });


  var content = [
    {
      header: "Welcome to the VISPA Code Editor 1.1",
      text: "VISPA 1.1 introduces many new features for the code editor as well as" +
        " some crucial bug fixes. </br> This tour will give you a short impression of" +
        " the most relevant changes. </br></br>" +
        " Press 'Next' to proceed or 'Quit' at any point to close the dialog.",
      selector: null
    }, {
      header: "Section Resizing",
      text: "Resizing of the individual sections is finally possible by simple mouse drag.",
      selector: ".codeeditor-x-dragbar, .codeeditor-y-dragbar"
    }, {
      header: "Commands",
      text: "Place your command with which the opened file should be executed.</br></br>Placeholders" +
        " can be defined within the 'replacements' preference (a clearer interface for it is planned" +
        " for next updates).  </br></br> <font color='green'>" +
        "Example:</font></br> With the default assignment, g++ %file %croot && ./a.out as a command" +
        " would include the ROOT libraries for compilation (and subsequent run) of an executed C++" +
        " program.",
      selector: "#command-line"
    }, {
      header: "Memorizing Commands",
      text: "A user defined number of commands is temporarily stored. </br> The given command" +
        " at the last file execution may also be memorized long term if cookies are enabled" +
        " (see preferences).",
      selector: "#history-btn"
    }, {
      header: "Filewatch System",
      text: "Removal and renaming of open files is recognized and an appropriate message is shown." +
        "</br></br> <font color='red'>Warning:</font> </br> Due to problems with the" +
        " workspace-server communication, this may not always work (yet). Thus, be advised to " +
        " take necessary caution when renaming or removing open files.",
      selector: null
    }, {
      header: "Autosave",
      text: "Files are automatically saved at the time interval as indicated in the" +
        "  preferences.</br>It prevents you from loosing valuable modifications to your file" +
        " if the page is reloaded or the platform crashes.",
      selector: null
    }, {
      header: "Preview Thumbnail Resizing",
      text: "The preview thumbnails are now resizable either by use of the highlighted button" +
        " or through the preference system ('previewSize').",
      selector: "#preview-resize-btn"
    }, {
      header: "Syntax Highlighting",
      text: "Instead of being bound to the default schemes, the highlighting mode" +
        " may be set manually for each session.",
      selector: null
    }, {
      header: "Quick Job Submission",
      text: "If the jobmanagement extension is installed on your VISPA setup, an interface," +
        " for e.g. a quick job submission, is provided.",
      selector: "#submit"
    }, {
      header: "Many Bugfixes",
      text: "- Overwriting existing files has to be confirmed now.</br>" +
        "- Job initialization failure is caught and the execution is reset.</br>" +
        "- Random 'rename/removal' alerts do not show up anymore.</br>" +
        "- Saving of generated outputs to file is fixed.</br>" +
        "- Appropriate user confirmations when closing the editor are introduced.</br>" +
        "- Preview thumbnails are really kept up to date.",
      selector: null
    }, {
      header: "Two Words of Caution",
      text: "1) <font color='red'>Do not</font> produce many output lines at high frequencies!" +
        " </br>When producing more than a total of ~1000 lines, the interval between two" +
        " consecutive ones should not be smaller than 10 ms. </br>" +
        " Otherwise, your web browser might crash and the page must be reloaded.</br></br>" +
        " 2) In order to have all your temporary editor settings saved, always close it properly.",
      selector: "#execute"
    }, {
      header: "Preference System",
      text: "Although this is nothing new, we would like to remind you of VISPA's preference system." +
        "</br>It enables you to personalize and enhance your experience with this platform. " +
        "</br></br>" +
        "Open it by clicking on the arrow next to your user name on the top right-hand corner.",
      selector: null
    }, {
      header: "Please Report Issues",
      text: "In case you encounter any difficulties, discover a bug or have suggestions for" +
        " further improval, do not hesitate to contact us! </br></br> For these purposes," +
        " use the feedback button or send an e-mail to vispa@lists.rwth-aachen.de.",
      selector: null
    }, {
      header: "Thank You",
      text: "Thank you for your time and enjoy the improved code editor.</br></br>" +
        "Do not forget to check the box below if this window shall not appear again. </br>" +
        "You may restart this tour anytime by clicking on the appropriate menu entry.</br></br>" +
        "'Quit' will close this dialog.",
      selector: null
    }
  ];

  return UpdateInfo;

});
