define([
  "jquery",
  "emitter",
  "vispa/utils",
  "./action",
  "./prefs",
  "require",
  "ace/ace",
  "ace/ext-language_tools"
], function(
  $,
  Emitter,
  Utils,
  CodeEditorActions,
  Prefs,
  require,
  ace
) {

  var CodeEditor = Emitter._extend({

    init: function init(view) {
      init._super.call(this);
      var self = this;

      this.view = view;

      this.path = undefined;

      this.ace = null;
      this.originalAceCommands = null;

      this.keyupMinTimeout = null;
      this.keyupMaxTimeout = null;

      this.node = null;
      this.manualSyntax = false;

      this.actions = new CodeEditorActions(this, view);

      this.view.on("changedState", function(key) {
        if (key == "path") {
          if (!self.ace) return;
          var path = self.view.state.get("path");
          if (!path) return;
          if (self.path == path) return;

          self.actions.checkModifications();
          if (self.view.modified) {
            // set it back to the current one, a successfull load will set it later
            this.state.set("path", self.path);
            self.actions.save(function() {
              self.actions.loadContent(path);
            });
          } else
            self.actions.loadContent(path);
        }
      });
    },

    setup: function(node) {
      var self = this;

      this.view.setLoading(true);

      var leftNode = node.find(".codeeditor-left");
      this.node = node.find(".codeeditor-ace");
      leftNode.keyup(function() {
        clearTimeout(self.keyupMinTimeout);
        var check = self.actions.checkModifications.bind(self.actions);
        self.keyupMinTimeout = setTimeout(check, vispa.device.hasTouch ? 1000 : 500);
        self.keyupMaxTimeout = self.keyupMaxTimeout || setTimeout(check, 1000);
      });

      self.ace = ace.edit($(self.node).get(0));
      self.setAutoCompletion();

      self.ace.selection.on('changeCursor', function() {
        var line = self.ace.selection.getCursor().row + 1;
          if (!isNaN(line) && !self.view.loading) {
            self.view.state.set("line", line, 1000);
        }
      });
      if (vispa.device.hasTouch)
        self.addOwnScrollBar();

      self.actions.watchSetup();

      // store all original ace commands
      self.originalAceCommands = $.extend(true, {}, self.ace.commands.commands);

      // now apply custom shortuts
      self.applyShortcuts();

      // trigger loading of state again since we can handle it now
      self.view.emit("changedState", "path");
      self.ace.focus();
      self.ace.resize();
      self.view.setLoading(false);
    },

    setContent: function(content) {
      this.ace.setValue(content);
      var UndoManager = require("ace/undomanager").UndoManager;
      this.ace.getSession().setUndoManager(new UndoManager());
      return this;
    },

    getContent: function() {
      return this.ace ? this.ace.getValue() : "";
    },

    append: function(content) {
      this.goToBottom();
      this.ace.insert(content + "\n");
      return this;
    },

    reset: function() {
      this.ace.setValue("");
      return this;
    },

    getLength: function() {
      return this.ace.session.getLength();
    },

    addOwnScrollBar: function() {
      $("head link[rel='stylesheet']").last().after("<link rel='stylesheet'" +
        "href='/extensions/codeeditor/static/css/ownScrollBar.css' " +
        "type='text/css'>");
    },

    goToBottom: function() {
      this.ace.gotoLine(this.getLength());
      return this;
    },

    goToLastLine: function() {
      var line = this.view.state.get("line");
      if (!line) return;
      this.ace.gotoLine(line);
    },
    goToTop: function() {
      this.ace.gotoPageUp();
      this.ace.gotoLine(1);
      return this;
    },

    find: function(key) {
      return this.ace.find(key).start;
    },

    setMode: function(path) {
      path = path || this.path;
      if (path == null)
        return this;

      var label = path;
      if (!this.view.state.get("writable"))
        label = label + " Read-Only";

      this.view.setLabel(label, true);

      var fileExtension = path.split(".").pop().toLowerCase();

      //set the icon (as defined for each extension)
      if (!~Prefs.extensions.indexOf(fileExtension)) {
        console.log("File extension '" + fileExtension + "' is not supported by the ACE framework!");
        this.view.icon = "fi-default";
      } else {
        this.view.icon = "fi-" + fileExtension;
      }

      this.setSyntaxHighlighting();
      return this;
    },

    setSyntaxHighlighting: function(fileExtension) {
      var self = this;

      if (fileExtension === undefined) {
        if (!this.manualSyntax) {
          fileExtension = this.path.split(".").pop().toLowerCase();
          aceMode = this.getAceMode(fileExtension);
        } else
          return this;
      } else
        this.manualSyntax = true;

      var aceMode = this.getAceMode(fileExtension);
      this.ace.getSession().setMode(aceMode);

      return this;
    },

    applyShortcuts: function() {
      if (!this.ace)
        return this;
      
      this.ace.commands.removeCommands(Object.keys(this.ace.commands.commands));
      // add the default commands
      $.each(this.originalAceCommands, function(key, data) {
        if (!(key == "transposeletters")) {
          this.ace.commands.addCommand(data);
        }
      }.bind(this));

      // overwrite with custom commands
      var shortcuts = $.extend(true, {}, this.view.shortcuts.children());
      for (var name in Prefs.preferences) {
        var data = Prefs.preferences[name];
        if (data.type !== "shortcut") continue;
        // silently fail wrong layouts, report when setting this preference
        if (/^key(down|press|up)\:/.test(data.value)) continue;
        this.ace.commands.addCommand({
          name: "ce"+name,
          exec: data.callback.bind(this.view),
          bindKey: {
            mac: Utils.shortcutPP(data.value, "mac").replace(/^(\w+\:)?meta\+/, "command+"),
            win: Utils.shortcutPP(data.value, "win").replace(/^(\w+\:)?meta\+/, "LWin"),
          }
        });
      };
    },

    listAceShortcuts: function() {
      var $node = $("<table/>");
      var $header = $("<tr/>")
        .append($("<th/>").html(" Action "))
        .append($("<th/>").html(" Windows "))
        .append($("<th/>").html(" Mac "));
      $node.append($("<thead/>").append($header));
      $node.append("<tbody/>");

      $.each(this.ace.commands.commands, function(key, data) {
        if (data.bindKey === undefined)
          return;
        var l = $("<tr/>");
        var winKey = data.bindKey.win === null ? " - " : data.bindKey.win;
        var macKey = data.bindKey.mac === null ? " - " : data.bindKey.mac;
        l.append($("<td/>").html(key + " "))
          .append($("<td/>").html(" " + winKey + " "))
          .append($("<td/>").html(" " + macKey));
        $node.find("tbody").append(l);
      });
      //sort for actions
      $node.find("tbody").find("tr").sort(function(a, b) {
        return $("td:first", a).text().localeCompare($("td:first", b).text());
      }).appendTo($node);

      this.view.dialog({
        header: "<i class='glyphicon glyphicon-flag'></i>" +
          " Defined Ace Shortcuts ",
        body: $node,
        footer: $("<a/>").attr({
          href: "http://ace.c9.io/#nav=about",
          target: "_blank"
        }).html("Ace website")
      });
    },

    resize: function() {
      if (this.ace)
        setTimeout(this.ace.resize.bind(this.ace, true), 1000);
      return this;
    },

    setAutoCompletion: function() {
      if (!this.ace)
        return;
      var enable = this.view.prefs.get("autoCompletion");
      this.ace.setOptions({
        enableBasicAutocompletion: enable,
        enableLiveAutocompletion: enable,
        enableSnippets: enable,
        liveAutocompletionDelay: 500,
        liveAutocompletionThreshold: 2
      });
    },

    setTheme: function(theme) {
      if (this.ace) {
        theme = theme == null ? this.view.prefs.get("theme") : theme;
        this.ace.setTheme("ace/theme/" + theme);
      }
      return this;
    },

    setFontSize: function(size) {
      if (this.ace) {
        size = size == null ? this.view.prefs.get("fontSize") : size;
        this.ace.setFontSize(size);
      }
      return this;
    },

    setShowInvisibles: function(show) {
      if (this.ace) {
        show = show == null ? this.view.prefs.get("showInvisibles") : show;
        this.ace.setShowInvisibles(show);
      }
      return this;
    },

    setShowIndentationGuides: function(show) {
      if (this.ace) {
        show = show == null ? this.view.prefs.get("indentationGuides") : show;
        this.ace.setDisplayIndentGuides(show);
      }
      return this;
    },

    setTabIndentation: function(useTabs) {
      if (this.ace) {
        useTabs = useTabs == null ? this.view.prefs.get("tabIndent") : useTabs;
        this.ace.session.setUseSoftTabs(!useTabs);
      }
      return this;
    },

    setRuler: function(position) {
      if (this.ace) {
        position = position == null ? this.view.prefs.get("ruler") : position;
        this.ace.setPrintMarginColumn(position);
      }
      return this;
    },

    getAceMode: function(fileExtension) {
      var mode = "ace/mode/";
      switch (fileExtension) {
        case "c":
        case "cc":
        case "cpp":
        case "cxx":
        case "h":
        case "hxx":
        case "cu":
          mode += "c_cpp";
          break;
        case "xml":
          mode += "xml";
          break;
        case "java":
          mode += "java";
          break;
        case "js":
          mode += "javascript";
          break;
        case "css":
          mode += "css";
          break;
        case "sh":
          mode += "sh";
          break;
        case "py":
          mode += "python";
          break;
        case "r":
          mode += "r";
          break;
        case "m":
          mode += "matlab";
          break;
        default:
          mode += "text";
          break;
        case "json":
          mode += "json";
      }
      return mode;
    }

  });

  return CodeEditor;
});
