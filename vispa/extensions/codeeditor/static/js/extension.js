define([
  "vispa/extension",
  "vispa/filehandler2",
  "./view",
  "css!../css/styles"
], function(
  Extension,
  FileHandler2,
  CodeEditorView
) {

  var CodeEditorExtension = Extension._extend({
    init: function init() {
      init._super.call(this, "codeeditor", "Codeeditor");
      this.mainMenuAdd([
        this.addView(CodeEditorView),
      ]);
    },
  });

  var fileExtensions = [
    "bsc", "c", "cc", "conf", "cpp", "csh", "css", "cxx", "diff", "f", "f70", "f90", "f95", "f03",
    "h", "hh", "hxx", "hpp", "html", "ini", "java", "js", "less", "log", "json", "m", "md", "orig",
    "php", "py", "r", "rb", "sh", "tex", "txt", "xml", "yml", "yaml", "zsh"
  ];

  FileHandler2.addOpen("codeeditor", {
    label: "Code Editor",
    iconClass: function (ext) {
      return (~fileExtensions.indexOf(ext)) ? ("fi fi-" + ext) : "fa fa-file-text";
    },
    position: function (ext) {
      return (~fileExtensions.indexOf(ext)) ? 5 : 1001;
    },
  }, function(path) {
      return ["codeeditor", "CodeEditor", {path: path}];
  });

  return CodeEditorExtension;
});
