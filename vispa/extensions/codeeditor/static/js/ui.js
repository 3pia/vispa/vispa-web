define(["jquery", "emitter"], function($, Emitter) {

  var CodeEditorUI = Emitter._extend({

    init: function init(view) {
      init._super.call(this);

      this.view = view;
      this.editor = view.editor;

      this.xDragbar = null;
      this.yDragbar = null;
      this.oldXDragPosition = null;
      this.oldYDragPosition = null;
    },

    setup: function() { //resize of divs per mouse drag
      //here the functionality of the dragbars is assigned
      var self = this;
      this.xDragbar = $(".codeeditor-x-dragbar", this.view.$content);
      this.yDragbar = $(".codeeditor-y-dragbar", this.view.$content);

      if (vispa.device.hasTouch) {
        var xMarker = $("<div />").css({
          "height": "15px",
          "width": "15px",
          "top": "30%",
          "position": "absolute"
        }).append($("<i />").addClass("glyphicon glyphicon-resize-horizontal"));
        var yMarker = $("<div />").css({
          "height": "15px",
          "width": "15px",
          "left": "30%",
          "position": "absolute",
          "bottom": "0%"
        }).append($("<i />").addClass("glyphicon glyphicon-resize-vertical"));
        this.xDragbar.append(xMarker).css("width","0px");
        this.yDragbar.append(yMarker).css("height","0px");
      }

      this.xDragbar.draggable({
        axis: "x",
        start: function(event, ui) {
          self.oldXDragPosition = self.xDragbar.css("left");
        },
        stop: function(event, ui) {
          var width = $(self.view.$content).width();
          var r = 100.0 * (ui.position.left) / width;
          self.view.state.set("showOutput", true); //reset it, whenever the user wants so
          r = Math.round(r);
          if (r < 5) r = 5;
          if (r > 95) r = 95;
          self.setInputOutputRatio(r);
          self.view.prefs.set("inputOutputRatio", r);
        }
      });

      //the dragging here seems a little more complicated because we need to consider offsets
      this.yDragbar.draggable({
        axis: "y",
        start: function(event, ui) {
          self.oldYDragPosition = self.yDragbar.css("top");
        },
        //revert: true does not do the desired job (position is fixed
        //when resizing the window)-->own hack
        stop: function(event, ui) {
          var height = $(self.view.$content).height();
          var r = 100.0 * (ui.position.top) / height;
          self.view.state.set("showOutput", true);
          r = Math.round(r);
          if (r < 5) r = 5;
          if (r > 95) r = 95;
          self.setTextPictureRatio(r);
          self.view.prefs.set("outputPreviewRatio", r);
        }
      });

      this.view.on("changedState", function(key) {
        if (key == "showOutput") {
          self.setInputOutputRatio();
        }
      });

    },

    setInputOutputRatio: function(r) { //r: ratio between left side width and total width
      r = r == null ? this.view.prefs.get("inputOutputRatio") : r;
      if (!this.view.state.get("showOutput"))
        r = 99.9; //not 100% because we want to allow for the dragbar
      $(".codeeditor-left", this.view.$content).css("width", String(r) + "%");
      $(".codeeditor-x-dragbar", this.view.$content).css("left", String(r) + "%");
      $(".codeeditor-right", this.view.$content).css("width", String((100 - r)) + "%");
      this.editor.resize();
      return this;
    },

    setTextPictureRatio: function(r) {
      r = r == null ? this.view.prefs.get("outputPreviewRatio") : r;
      $(".codeeditor-right-top", this.view.$content).css("height", String(r) + "%");
      $(".codeeditor-y-dragbar", this.view.$content).css("top", String(r) + "%");
      $(".codeeditor-right-bottom", this.view.$content).css("height", String(100 - r) + "%");
      return this;
    }
  });

  return CodeEditorUI;
});
