define(["jquery", "emitter", "vispa/extension"], function($, Emitter, Extension) {

  var CommandLine = Emitter._extend({

    init: function init(view) {
      init._super.call(this);
      var self = this;

      this.view = view;
      // commandline template -> list of extensions
      this.commandlineTemplates = {
        "python '%file'": ["py"],
        "sh '%file'": ["sh"],
        "php '%file'": ["php"],
        "pdflatex '%file'": ["tex"],
        "gcc '%file' -o '%file.out' && './%file.out'": ["c"],
        "g++ '%file' -o '%file.out' && './%file.out'": ["cc", "cp", "cxx", "cpp",
                                                        "CPP", "c++", "C"],
        "R -f '%file'": ["R"],
        "octave -q -f '%file'": ["m"]
      };

      this.commandline = null;
      this.histNode = null;
      this.menuNode = null;

      this.commands = [];
      this.defaultCommand = null;
      this.lastCommand = null;

      this.cookieName = null;
      this.commandCookie = null;

      this.view.on("changedState", function(key) {
        if (!self.commandline)
          return;
        switch (key) {
          case "command":
            var command = this.state.get("command");
            if (self.commandline.val() != command) {
              self.commandline.val(command);
            }
            break;
          case "path":
            self.initEntries();
            break;
        }
      });
    },

    setup: function(node) {
      // command line setup
      var rightTopNode = node.find(".codeeditor-right-top");

      this.commandline = rightTopNode.find("#command-line").tooltip({
        trigger: "focus"
      });
      this.histNode = rightTopNode.find("#history");
      this.menuNode = rightTopNode.find("#menu");

      //main setup
      this.initEntries();
      this.setMenuEntries(); //sets up the buttons in the submit menu
    },

    initEntries: function() {
      var self = this;
      this.getCookie(function() {
        //get the last command and afterwards set the default & render the history
        self.setLastCommandFromCookie();
        self.setDefault();
        self.renderHistory();
      });
    },

    setMenuEntries: function() {
      var node;
      var self = this;
      if (Extension._members.byName("jobmanagement")) {
        //the menu entries should only be added
        node = $("<li/>").append(
          $("<a />").append(
            $("<i/>").addClass("glyphicon glyphicon-time"))
          .click(function() {
            self.view.editor.actions.save(function() {
              var cmd = self.getVal();
              cmd = cmd.replace(/\%file/g, self.view.editor.path);
              self.view.spawnInstance("jobmanagement", "submissionCenter", {
                commands: cmd
              });
            });

          }).append(" Submit"));
        this.menuNode.append(node);
      }
    },

    getCookie: function(callback) {
      this.commandCookie = $.cookie("VISPA-commands");
      if (!this.commandCookie) //if no cookie has been set yet --> make a new one
        this.commandCookie = {};
      else
        this.commandCookie = JSON.parse(this.commandCookie);
      if ($.isFunction(callback))
        callback();
    },

    setLastCommandFromCookie: function() {
      var key = this.getCookieKey();
      if (!key)
        return;
      var last = this.commandCookie[key];
      if (!last)
        return;

      this.pushLast(last);
    },

    pushLast: function(last) {
      if (!last)
        return;
      this.lastCommand = {
        cmd: last
      };
    },

    saveToCookie: function(cmd) {
      //make sure the user allows for cookies
      if (!this.view.prefs.get("cookies"))
        return;
      var key = this.getCookieKey();
      this.commandCookie[key] = cmd;
      $.cookie("VISPA-commands", JSON.stringify(this.commandCookie), {
        expires: 7
      }); //expiry after seven days
    },

    //key == file name
    getCookieKey: function() {
      var key = this.view.state.get("path");
      return key ? key.split("/").pop() : null;
    },

    setDefault: function() {
      if (this.view.editor && this.view.state.get("path")) {
        var fileExtension = this.view.state.get("path").split(".").pop().toLowerCase();
        if (fileExtension) {
          for (var i in this.commandlineTemplates) {
            if (~this.commandlineTemplates[i].indexOf(fileExtension)) {
              if (this.commands.length === 0)
                this.view.state.set("command", i);
              this.pushDefault(i);
              return true;
            }
          }
        }
      }
    },

    pushDefault: function(cmd) {
      if (!cmd)
        return;
      this.defaultCommand = {
        cmd: cmd
      };
    },

    pushEntry: function(cmd) {
      if (this.checkExistence(cmd)) {
        return;
      }
      if (this.commands.length == this.view.prefs.get("commandHistory")) {
        this.commands.pop();
      } //do not exceed maximum length

      this.commands.unshift({
        name: cmd,
        cmd: cmd
      });
    },

    checkExistence: function(cmd) {
      for (var i = 0; i < this.commands.length; i++) {
        if (cmd == this.commands[i].cmd) {
          return true;
        }
      } //no double saving!!
      return false;
    },

    renderHistory: function() {
      var self = this;
      this.histNode.html("");
      $.each(this.commands, function(i, entry) {
        self.histNode.append(self.createNode(entry.name, entry.cmd));
      });

      this.histNode.append($("<li/>").addClass("divider"));
      //append the default and last commands
      if (this.defaultCommand !== null)
        this.histNode.append(self.createNode("default", this.defaultCommand.cmd));
      if (this.lastCommand !== null)
        this.histNode.append(self.createNode("last executed", this.lastCommand.cmd));
    },

    createNode: function(name, cmd) {
      var self = this;

      var node = $("<a/>");
      node.html(name);
      node.on({
        mouseover: function() {
          $("body").css("cursor", "hand");
        },
        mouseleave: function() {
          $("body").css("cursor", "default");
        },
        click: function() {
          self.view.state.set("command", cmd);
        }
      });
      return $("<li/>").append(node);
    },

    getVal: function() {
      return this.commandline.val();
    },

    handleExecute: function() {
      var cmd = this.getVal();
      if (cmd) {
        this.pushEntry(cmd);
        this.saveToCookie(cmd); //save the last command in a cookie
        this.pushLast(cmd); //ID for matching is the set path of the file
      }

      this.renderHistory();
    },

    performReplacements: function(cmd) {
      if (cmd === undefined)
        return cmd;

      var replacements = this.view.prefs.get("replacements").split(";");
      for (var i = 0; i < replacements.length; i++) {
        var replacement = replacements[i].split("==");
        if (replacement.length < 2)
          continue;
        cmd = cmd.replace(replacement[0], replacement[1]);
      }
      return cmd;
    }

  });

  return CommandLine;
});
