define(["jquery", "emitter"], function($, Emitter) {

  var CodeEditorOutput = Emitter._extend({

    init: function init(view) {
      init._super.call(this);

      this.view = view;

      this.textSet = false;
      this.temporaryText = "";
      this.appendInterval = null;

      this.failedExecuteTimeOut = null;

      this.preNode = null;
      this.running = false; //execute command has been fired?
      this.hasStarted = false; //stream in WS has started?
    },

    setup: function(node) {
      var self = this;

      var rightTopNode = node.find(".codeeditor-right-top");

      this.preNode = $("pre", rightTopNode);

      // pre scroll/clear
      $("button.clear", rightTopNode).click(function(event) {
        self.clear();
        this.blur();
        event.preventDefault();
      });
      $("button.gototop", rightTopNode).click(function(event) {
        self.goToTop();
        this.blur();
        event.preventDefault();
      });
      $("button.gotobottom", rightTopNode).click(function(event) {
        self.goToBottom();
        this.blur();
        event.preventDefault();
      });

      // bind execute/abort events
      this.executeBtn = node.find("#execute").click(function(event) {
        self.execute();
        event.preventDefault();
      });
      this.abortBtn = node.find("#abort").click(function(event) {
        self.abort();
        event.preventDefault();
      });

      // check if there is a running job
      this.view.POST("runningjob", function(err, res) {
        if (err) return;
        self.running = !!res.runningjob;
        self.updateExeButtons();
      });

      // register events
      this.view.socket.on("start", function(data) {
        clearTimeout(self.failedExecuteTimeOut);
        self.failedExecuteTimeOut = null;
        self.hasStarted = true;
        var autoClear = self.view.prefs.get("autoClear");
        self.textSet = self.textSet && !autoClear;
          self.append(
            self.getDate() + ": \n" +
            "executing \"" + data.command + "\"\n" +
            "--------------------\n" +
            "OUTPUT:\n\n");
        self.appendInterval = setInterval(function() {
          self.append();
        }, 500);
      });
      this.view.socket.on("data", function(data) {
        self.attachToTemporary(data);
      });
      this.view.socket.on("done", function(data) {
        clearInterval(self.appendInterval);
        self.appendInterval = null;
        if (self.hasStarted) {
          self.append(); //append the remaining temporaries
          self.append(
            "\n--------------------\n" +
            (data.aborted ? "process was aborted!\n" : "") +
            "runtime: " + self.formatRuntime(data.runtime) + "\n" +
            "====================\n\n\n");
        }
        self.running = false;
        self.hasStarted = false;
        self.updateExeButtons();
      });

      return this;
    },

    formatRuntime: function(t) {
      t = parseFloat(t);
      var days = Math.floor(t / 86400);
      t -= days * 86400;
      var hours = Math.floor(t / 3600);
      t -= hours * 3600;
      var minutes = Math.floor(t / 60);
      t -= minutes * 60;
      t = t.toFixed(2);

      var s;
      if (days)
        s = days + "d, " + hours + " h, " + minutes + " m, " + t + " s";
      else if (hours)
        s = hours + " h, " + minutes + " m, " + t + " s";
      else if (minutes)
        s = minutes + " m, " + t + " s";
      else
        s = t + " s";

      return s;
    },

    getDate: function() {
      var d = new Date();

      return d.toLocaleString();
    },

    // execution
    execute: function() {
      var self = this;

      if (this.running) {
        return this;
      }

        //check the command and process the history
      if (!this.view.commandLine.getVal()) {
        this.view.alert("Please indicate a valid command for script execution!");
        return this;
      }
      var cmd = this.view.commandLine.getVal();
      self.view.commandLine.handleExecute();

      this.view.editor.actions.save(function() {
        if (self.running) {
          self.view.alert("There is already a running job!");
          return this;
        }

        self.running = true;
        self.updateExeButtons();

        //parse the command
        var parts = self.view.editor.path.split("/");
        var fileName = parts.pop();
        var filePath = parts.join("/");

        cmd = cmd.replace(/\%file/g, fileName);
        //additional, user defined replacements:
        cmd = self.view.commandLine.performReplacements(cmd);
        var handleFail = function() {
          self.abort();
          self.append(
            "\n--------------------\n" +
            "Initialization has failed, please try again.\n" +
            "====================\n\n\n");
          self.running = false;
          self.updateExeButtons();
        };
        //extra timeout (10s) if the "start" is not sent back/ websockets fail
        self.failedExecuteTimeOut = setTimeout(handleFail, 10000);

        self.view.POST("execute", {
          cmd: cmd,
          base: filePath
        }, function(err, res) { //double check for running jobs
          if (!res.success) {
            self.view.alert("There is already a running job!");
          }
        });

      });

      return this;
    },

    abort: function() {
      if (!this.running)
        return this;

      this.view.POST("abort");

      return this;
    },

    updateExeButtons: function() {
      if (this.executeBtn) {
        this.executeBtn.parent().toggle(!this.running);
        this.abortBtn.parent().toggle(this.running);
      }

      return this;
    },

    // output
    setText: function(text) {
      if (!text) {
        this.preNode.text(text);
        return this;
      }
      var textarray = text.split("\n");
      var maxlines = this.view.prefs.get("maxLines") < textarray.length ?
        this.view.prefs.get("maxLines") : textarray.length;
      var cleantext = textarray.slice(-maxlines).join("\n");
      this.preNode.text(cleantext);
      return this;
    },

    getText: function() {
      return this.preNode.text();
    },

    attachToTemporary: function(text) {
      this.temporaryText = this.temporaryText + String(text).replace(/\r/g, "\n");
    },

    append: function(text) {
      this.initialClear();

      var wasAtBottom = this.isAtBottom();
      this.setText(this.getText() + String(text === undefined ? this.temporaryText : text));
      this.temporaryText = "";
      if (wasAtBottom)
        this.goToBottom();

      return this;
    },

    clear: function() {
      this.setText("");
      return this;
    },

    initialClear: function() {
      if (!this.textSet) {
        this.textSet = true;
        this.clear();
      }
      return this;
    },

    goTo: function(offset) {
      if (this.preNode)
        this.preNode.scrollTop(offset);
      return this;
    },

    goToTop: function() {
      this.goTo(0);
      return this;
    },

    goToBottom: function() {
      var offset = this.preNode.get(0).scrollHeight - this.preNode.innerHeight();
      this.goTo(offset);
      return this;
    },

    isAtBottom: function() {
      var offset = this.preNode.get(0).scrollHeight - this.preNode.innerHeight() - 5;
      return offset <= this.preNode.scrollTop();
    },

    saveAs: function() {
      var self = this;

      //get the current workpath
      var path = this.view.editor.path;
      var parts = path.split("/");
      parts.pop();
      path = parts.join("/");

      var args = {
        path: path,
        savemode: true,
        callback: function(path) {
          var fileExtension = path.split(".").pop().toLowerCase();
          if (fileExtension != "txt")
            self.view.alert("File extension '" + fileExtension + "'' is not supported!");
          else {
            self.view.setLoading(true);
            self.view.POST("/ajax/fs/savefile", {
              path: path,
              content: self.getText(),
              utf8: true,
            }, function(err) {
              if (err) {
                self.view.setLoading(false);
                return;
              }
              self.view.alert("Output saved in file " + path);
              self.setLoading(false);
            });
          }
        },
        sort: "name",
        reverse: false
      };
      this.view.spawnInstance("file2", "FileSelector", args);
    }
  });

  return CodeEditorOutput;
});
