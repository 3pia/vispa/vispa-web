# -*- coding: utf-8 -*-

# imports
import cherrypy
from vispa.controller import AbstractController
from vispa import AjaxException


class EditorController(AbstractController):

    def getrpc(self):
        windowId = cherrypy.request.private_params.get("_windowId", None)
        viewId = cherrypy.request.private_params.get("_viewId", None)
        return self.get("proxy", "CodeEditorRpc", self.get("combined_id"),
                        window_id=windowId, view_id=viewId)

    @cherrypy.expose
    def execute(self, cmd, base):
        self.release_session()
        rpc = self.getrpc()
        err = rpc.start(cmd, base)
        if err:
            raise AjaxException(err)
        return {"success": not err}

    @cherrypy.expose
    @cherrypy.tools.ajax(encoded=True)
    def runningjob(self):
        self.release_session()
        rpc = self.getrpc()
        self.release_database()
        return {"runningjob": rpc.runningjob()}

    @cherrypy.expose
    def abort(self):
        self.release_session()
        rpc = self.getrpc()
        self.release_database()
        return {"success": rpc.abort()}

    @cherrypy.expose
    def close(self):
        self.release_session()
        rpc = self.getrpc()
        rpc.close()
        self.extension.clear_workspace_instance(
            "CodeEditorRpc",
            self.get("combined_id"))
        return {"success": True}
