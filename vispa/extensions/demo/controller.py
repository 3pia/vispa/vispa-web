# -*- coding: utf-8 -*-

import cherrypy

import logging

# import vispa.workspace, providing e.g. "module()" and "get_instance()"
import vispa.workspace

# we import vispa's AbstractController which provides a collection of useful
# methods
from vispa.controller import AbstractController

# we register a logger named after this file
logger = logging.getLogger(__name__)

# define a new class that inherits from AbstractController
class DemoController(AbstractController):

    # instance methods decorated with "@cherrypy.expose" are exposed to clients
    # and accessible via the url (here: <controllerName>/ls)
    # important: the currently selected workspace is made accessible when this
    # controller method is invoked, i.e., in most cases you don't need to
    # distinguish between multiple user-owned workspaces
    @cherrypy.expose
    def ls(self, path=None):
        logger.info(cherrypy.request.workspace)
        # this method will return the names of files contained in "path"
        # if "path" is None, the pwd of the (remote) workspace is used
        if path is None:
            # obtain the remote os module
            ros = vispa.workspace.module("os")
            path = ros.getcwd()

        # in this example, we use the DemoRpc class to obtain the file list
        # (of course, a call to "ros.listdir" would have the same effect)
        demorpc = self.extension.get_workspace_instance("DemoRpc")
        files = demorpc.ls(path)

        # we can use the "success" method implemented in the
        # AbstractController to return a json encoded response
        # this response will look like '{"success":True,"files":"..."}'
        # and contains all key-word arguments that you pass
        return {"files": files}
