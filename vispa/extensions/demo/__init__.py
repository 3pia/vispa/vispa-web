# -*- coding: utf-8 -*-

# we import vispa's AbstractExtension which implements the connection
# and setup to the vispa platform
from vispa.server import AbstractExtension

# the controller (the class that listens to extension specific url requests)
# is imported from a seperate file within the same folder
from controller import DemoController


# define a new class that inherits from AbstractExtension
class DemoExtension(AbstractExtension):

    # required
    def name(self):
        # the returned value must match this extenion's foldername
        return "demo"

    # optional
    def dependencies(self):
        # return a list of potential dependencies
        return []

    # required
    def setup(self):
        # add an instance of the controller imported above
        self.add_controller(DemoController())

        # tell vispa to transfer files located in the workspace folder
        # (vispa/extensions/<name>/workspace) to selected workspaces
        self.add_workspace_directoy()
