//TODO: add some text
//***
define(["vispa/extensions", "vispa/views/center", "jquery", "css!../css/style"],
       function(Extensions, CenterView, $, x) {
  //TODO: Modify text below
  // we define an extension by inheriting/extending the extension base class
  // definition in vispa.Extension
  // there is exactly one extension per extension folder in vispa/extensions
  //***
  var DemoExtension = Extensions.Extension._extend({
    // the constructor
    init: function init() {
      // call the super constructor
      init._super.call(this, "demo");

      this.addView("somename", DemoView);

      var self = this;

      // add default preferences for the DemoView
      // the keys (e.g. backgroundColor) are used as the preference keys
      // within the instance
      // there are more types than just strings:
      // string, integer, float, boolean, list, object
      // all of them can have a predefined selection
      // intergers and floats also can have a range, consisting of a list of 2
      // parameters (upper and lower bound)
      this.setDefaultPreferences(DemoView, {
        backgroundColor: {
          // required
          type: "string",
          value: "blue",
          // optional
          description: "The background color",
          selection: ["#428bca", "#f0ad4e", "#d9534f", "#5cb85c"]
        }
      }, {
        title: "Demo"
      });

      // now we add default shortcuts
      // the structure of the second argument is the same as above in
      // "setDefaultPreferences" except for the missing range/selection entries
      // and the additional callback entry
      // the callback will have the scope of the executing view instance!
      this.setDefaultShortcuts(DemoView, {
        test: {
          description: "some test shortcut",
          value: vispa.device.isMac ? "meta+s" : "ctrl+s",
          callback: function() {
            this.modified = false;
            this.alert("Instance " + this.getId() + " saved!");
          }
        }
      }, {
        title: "Demo"
      });

      // set options for the DemoView
      this.setOptions(DemoView, {
        maxInstances: 5
      });

      // add a menu entry to the main menu
      // there is one main menu per workspace
      // "addMenuEntry" takes two parameters, a title and a callback
      // the second parameter can also be an object for more configuration
      // submenus are not supported (yet)
      this.addMenuEntry("New Demo", {
        // a bootstrap icon class
        iconClass: "glyphicon glyphicon-plus",
        // the callback that is fired when the menu entry is clicked
        // the id of the workspace this menu entry is subject to is passed
        callback: function(workspaceId) {
          // we want to create a new DemoView instance using the "createInstance"
          // method implemented in the extension class
          // this method requires the workspace id as the first and a reference to
          // the target view class as the second argument
          // any additional arguments (starting from index 2) are passed to the
          // constructor of the references view class
          // see its description for further information
          self.createInstance(workspaceId, DemoView);
        }
      });
    }
  });


  // an extension can have multiple views, i.e., multiple (different)
  // representations of content delivered by the extension
  // example: a file browser might have a large (center) view and a smaller one
  // to show a file tree
  // in this case, we define a view that inherits from the center view base class
  // definition in vispa.extensionView.Center
  // other views are not supported yet
  var DemoView = CenterView._extend({
    //TODO: modify text below
    // the constructor
    // the first argument is an object containing synchronized preferences
    // for all instances of this extension view which can also be synchronized
    // with the preference database (see below)
    // the exact content of the preferences depends on the defaults set for this
    // view in the extension above (in this case: {backgroundColor: "blue"})
    // depending on the way this instance is created, there might be additional
    // arguments (starting from index 2)
    //***
    init: function init() {

      //TODO: modify text below
      // call the super constructor with 2 parameters
      // 1. the name of this view (characters, numbers, and underscores only)
      // 2. the preference object
      // after this call, the preferences are automatically available via
      // this.prefs.get(key) and this.prefs.set(key, value)
      //***
      init._super.apply(this, arguments);

      // attributes like e.g. the id are unknown at this point as they are set
      // right after the init function
      // make sure to move all initial operations that require attributes are
      // placed in the setup function or wrapped in callbacks (e.g. actions of
      // menu entries)

      // the content node
      this.node = null;

      // add a menu entry to the instance's menu
      // there is one menu per instance
      // "ExtensionView.addMenuEntry" implements the same syntax as
      // "Extension.addMenuEntry" (see above)
      // this time, we simply pass a callback as the second argument
      var self = this;
      this.addMenuEntry("My ID", function() {
        // alert the id of this instance
        // we don't use the browser's default alert function since each instance
        // has its own context (messages are shown when the instance is shown)
        // all basic message functions are available: alert, prompt, and confirm
        self.alert("My ID is: " + self.getId());
      });

      // set the label
      this.label = "id: " + this.getId();
    },

    //TODO: remove the setup function
    // implement the setup function
    // it is called when all attributes are set and nodes are created
    /*setup: function() {
      this._super();

      // set the label
      this.label = "id: " + this.getId();

      // set the icon
      // this.static returns a path that is relative to the static
      // vispa/extensions/<extensionName>/static
      // there is also a method, this.dynamic, that returns paths relative to the
      // extension folder itself, i.e., vispa/extensions/<extensionName>
      this.icon = this.staticURL("img/myicon.png");

      // there are also some atrribute getters that can be called in the setup
      // method: extension, name, getId(), workspaceId,
      console.log(this.extension);
      console.log(this.name);
      console.log(this.getId());
      console.log(this.workspaceId);

      // additionally, instance visibility can be controller via 3 methods:
      // show(), hide(), and close()
    },*/
    //***

    // applyPreferences is called when there was a change to the preferences
    applyPreferences: function applyPreferences() {
      applyPreferences._super.call(this);

      // backgroundColor
      this.node.css("background-color", this.prefs.get("backgroundColor"));
    },

    // implement the render function
    // the passed node can be filled with content
    // you can store the node to use it for later actions outside of the render function
    render: function(node) {
      var self = this;

      // we store the node
      this.node = node;

      // set the icon
      this.icon = "fa fa-play-circle-o";


      // show the loader overlay
      this.setLoading(true);

      // each extension instance has two ajax methods: this.GET and this.POST
      // their method signature equals jQuery's $.get and $.post signatures
      // they return jQuery promise objects (a subset of deferred objects, see
      // http://api.jquery.com/promise/)
      // this.GET and this.POST automatically transform the path of the request
      // via this.dynamic add meta information such as workspace data
      var promise = this.GET("ls");

      // at this point, it is a common task to load templates
      // this is simplified by using the instance method
      // "getTemplate(path, callback)" where "path" is relative to the static
      // extension path; "callback" has 3 parameters:
      // 1. an error (if any), 2. the template string, and 3. a deferred object
      // (which is also the return value of "getTemplate") which can be resolved/
      // rejected (if no action is performed on this object, it is resolved)
      this.getTemplate("html/democontainer.html", function(err, tmpl) {

        // simply add the template to the node and fill the result
        // res.files is set since the success method in the controller got
        // the keyword argument "files"
        var content = $(tmpl).appendTo(node);

        //Add a label to the view's tab
        self.label = "Demo";

        // set the background color which is already implemented in
        // "applyPreferences"
        self.applyPreferences();

        // connect the button action
        // you might want to check out the background color of other instance
        // of this view ;)
        $("button", content).click(function() {
          // we can manually update the preferences and sync them with the server
          // (and other instances of this view via "applyPreferences")
          var rnd = parseInt(Math.random() * 3);
          self.prefs.set("backgroundColor", ["#428bca", "#f0ad4e", "#d9534f", "#5cb85c"][rnd]);
          self.pushPreferences();
          if (!self.isModified())
            self.modified = true;
        });

        //TODO: check if .getLogger is still an existing function
        // each instance has a logger that can be accessed via this.getLogger()
        // there are multiple available methods: all(), debug(), info(), warn(),
        // error() and fatal(), which are shorthands for e.g. log("debug", ...)
        //self.getLogger().info("initially filled the content node");
        //***

        // we can even create sub loggers via this.createSubLogger(name) with
        // the sub-namepsace "name"
        var contentLogger = self.createSubLogger("content");
        contentLogger.debug("created");
        // loggers can be steered via "disable()" and "enable()" where a
        // namespace also affects its sub-namespace as well
        // in this case, this.getLogger().disable() would also disable
        // contentLogger as it is a sublogger of its namespace

        // hide the loader overlay
        self.setLoading(false);

        // in case there was a change to the content, we can mark the instance as
        // modified via this.modified = true|false
        self.modified = false;
      });
    },

    // called when the instance is resized
    onResize: function(dimensions) {
      // dimensions contains the new height and width
    },

    // the following methods are fired before and after certain events
    // return false in the "before" methods to prevent the event
    onFocus: function() {
      return this;
    },
    onBlur: function() {
      return this;
    },
    onBeforeClose: function() {
      return this;
    }
  });

  return DemoExtension;
});
