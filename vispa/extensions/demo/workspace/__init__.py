# -*- coding: utf-8 -*-

# this file will be subject to the selected workspace's machine

# import basic modules
import os, sys

# import logging and start a logger whose messages are transferred back to to
# the server
import logging
logger = logging.getLogger(__name__)


# define a class that implements arbitrary code
class DemoRpc:
    def __init__(self):
        logger.debug("DemoRpc instance created")

    def ls(self, path):
        logger.debug("DemoRpc.ls called")
        return ", ".join(os.listdir(path))
