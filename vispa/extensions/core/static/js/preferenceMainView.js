define([
  "jquery",
  "vispa/views/main",
], function($, MainView) {

  var PreferenceView = MainView._extend({

    render: function(node) {
      if (node.children().length) return; // already mounted?
      vispa.preferences.$appendTo(this.$content[0]);
    },

    destroy: function destroy() {
      vispa.preferences.$remove();
      destroy._super.call(this);
    },

  },{
    iconClass: "fa-cogs",
    label: "Preferences",
    name: "PreferenceView",
    menuPosition: 1050,
    workspace: null,
    singleton: true,
  });

  return PreferenceView;
});
