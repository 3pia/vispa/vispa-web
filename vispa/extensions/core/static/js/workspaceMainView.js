define([
  "jquery",
  "vue/vue",
  "vispa/vue/base",
  "vispa/vue/nav",
  "vispa/workspace",
  "vispa/views/main",
  "vispa/vue/colorpicker",
  "./openWorkspaceAddDialog",
  "text!../html/workspaceMainView.html",
  "text!../html/workspaceMainViewItem.html",
], function(
  $,
  Vue,
  VueBase,
  Nav,
  Workspace,
  MainView,
  VueColorPicker,
  openWorkspaceAddDialog,
  tMain,
  tItem
) {

  var WorkspaceView = MainView._extend({

    render: function(node) {
      if (node.children().length) return; // already mounted?

      var view = this;
      this.vue.content = new Vue({
        template: tMain.trim(),
        el      : node[0],
        data    : Workspace._members._vueData,
        methods : {
          showAdd: openWorkspaceAddDialog.bind(this, undefined, undefined),
        },
        computed: {
          canAdd: function() {
            return vispa.args.user.addWorkspaces;
          },
        },
        components: {
          workspace: {
            template: tItem.trim(),
            props: ["config", "serverConfig", "state"],
            computed: {
              changed: function() {
                return (this.config.name    != this.serverConfig.name)    ||
                       (this.config.host    != this.serverConfig.host)    ||
                       (this.config.login   != this.serverConfig.login)   ||
                       (this.config.command != this.serverConfig.command) ||
                       (this.config.key     != this.serverConfig.key)     ;
              },
              canAdd: function() {
                return vispa.args.user.addWorkspaces;
              },
              ws: {
                cached: false,
                get: function() {
                  return Workspace._members.byId(this.serverConfig.id);
                },
              },
              color: {
                get: function() {
                  return this.ws.color;
                },
                set: function(newColor) {
                  this.ws.color = newColor;
                },
              },
            },
            methods: {
              template: function() {
                // call on view so it gets properly
                openWorkspaceAddDialog.call(view, this.config);
              },
              remove: function() {
                vispa.messenger.confirm("", function(result){
                  if (result) {
                    this.ws.remove();
                  }
                }.bind(this), {
                  iconClass: "fa-trash",
                  title    : "Remove workspace: " + this.config.name + "?",
                  yesLabel : "Remove",
                  noLabel  : "Cancel",
                });
              },
              reset: function() {
                Vue.util.extend(this.config, this.serverConfig);
              },
              save: function() {
                var newConfig = {};
                for(var i in this.config) {
                  if (this.config[i] != this.serverConfig[i]) {
                    newConfig[i] = this.config[i];
                  }
                }
                this.ws.edit(newConfig, function(err){
                  if (err) {
                    vispa.alert("Saving edited workspace failed:"+err);
                  }
                });
              },
              changeState: function() {
                switch(this.state) {
                  case "disconnected":
                    this.ws.connect();
                    break;
                  case "connected":
                    this.ws.disconnect();
                    break;
                  default:
                    break;
                }
              },
            },
            components: {
              "vs-colorpicker": VueColorPicker,
            },
          },
        },
      });
    },

    destroy: function destroy() {
      this.vue.content.$destroy(true);
      destroy._super.call(this);
    },

  },{
    iconClass: "fa-server",
    label    : "Workspaces",
    name     : "WorkspaceView",
    menuPosition: 1005,
    workspace: null,
    singleton: true,
  });

  // register within the special mountpoint of the mainmenu
  vispa.mainMenu.$refs.wsExtra.configure.callback = function(){
    this.root.spawn(WorkspaceView);
  };

  return WorkspaceView;
});
