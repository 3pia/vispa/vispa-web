define([
  "jquery",
  "vispa/common/dialog",
  "vispa/common/color",
  "vispa/workspace",
  "vispa/vue/colorpicker",
  "text!../html/workspaceAddBody.html",
  "text!../html/workspaceAddFooter.html",
], function(
  $,
  Dialog,
  Color,
  Workspace,
  VueColorPicker,
  tBody,
  tFooter
) {

  var openWorkspaceAddDialog = function(defaults, appendTo) {
    defaults = defaults || {};
    var userConf = vispa.args.user;
    if (!userConf.addWorkspaces) return;

    var color = new Color();
    while(color.value.s < 0.5 || color.value.v > 1.0)
      color = new Color(Color.getRandomName());
    color = color.toString("alias");

    return new Dialog({
      data: {
        moreOpts      : !!(defaults.command || defaults.keyUsed),
        status        : "",
        statusClass   : "",
        wsName        : defaults.name || "",
        wsHost        : defaults.host || "",
        wsUser        : defaults.login || userConf.name || "",
        wsCommand     : defaults.command || "",
        wsColor       : defaults.color || color,
        wsKey         : "", // dont use the truncated key
        containerClass: "workspace-add-dialog",
        title         : "Add a workspace",
        iconClass     : "fa-server",
        backdrop      : true,
        buttons       : {
          action: {
            label    : "Add",
            class    : "btn-primary",
            iconClass: "fa-plus",
            callback : function() {
              return this.action();
            },
          },
        },
        onOpen: function() {
          this.$els.wsName.focus();
        },
      },
      methods: {
        action: function() {
          var chk = ["Name", "Host", "User"];
          for (var i in chk) {
            var k = "ws"+chk[i];
            if (this[k]) continue;
            this.$els[k].focus();
            this.statusClass = "label label-warning";
            this.status = chk[i]+" needs to be provided";
            return true;
          }
          var config = {
            name : this.wsName,
            host : this.wsHost,
            login: this.wsUser,
            cmd  : this.wsCommand,
            key  : this.wsKey,
          };
          var self = this;
          Workspace._members.createNew(config, function(err, ws){
            if (err) {
              self.statusClass = "label label-danger";
              self.status = err.message;
              return;
            }
            self.close();
            ws.color = self.wsColor;
            ws.connect();
          });
          return true;
        },
      },
      computed: {
        hasBody: function() {
          return true;
        },
      },
      partials: {
        body      : tBody.trim(),
        footerLeft: tFooter.trim(),
      },
      components: {
        "vs-colorpicker": VueColorPicker,
      },
      appendTo: appendTo || this.$messageContainer && this.$messageContainer[0],
    });
  };

  // register within the special mountpoint of the mainmenu
  vispa.mainMenu.$refs.wsExtra.add.callback = function(){
    openWorkspaceAddDialog(undefined, vispa.$messageContainer[0]);
  };

  return openWorkspaceAddDialog;
});
