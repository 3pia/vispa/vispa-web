define([
  "jquery",
  "vispa/views/main",
  "./umVue/main"
], function($, MainView, MainVue) {

  var UsermanagementView = MainView._extend({

    init: function init(args) {
      init._super.apply(this, arguments);

      this.state.setup({
        "section": "",
        "target": "",
      }, args);

      this.on("changedState", function(key, val){
        if (this.vue.content && key == "section") {
          this.vue.content.setSection(val);
        }
        if (this.vue.content && key == "target") {
          this.vue.content.target = val;
        }
      });
    },

    getFragment: function() {
      return this.state.get("section") + ":" + this.state.get("target");
    },

    applyFragment: function(fragment) {
      var parts = fragment.split(":");
      this.state.set("section", parts[0], 0.1);
      if (parts.length>1)
        this.state.set("target", parts[1]);
    },

    render: function(node) {
      if (node.children().length) return; // already mounted?
      this.vue.content = new MainVue({
        el: this.$content[0],
        data: {
          view: this,
          section: this.state.get("section"),
          target: this.state.get("target"),
        },
      });
    },

    destroy: function destroy() {
      this.vue.content.$destroy();
      destroy._super.call(this);
    },

  },{
    iconClass: "fa-users",
    label: "Usermanagement",
    name: "UsermanagementView",
    menuPosition: 1010,
    workspace: null,
    singleton: true,
  });

  return UsermanagementView;
});
