define([
  "vispa/views/main",
  "async",
], function(
  MainView,
  async
) {

  var PlaceholderView = MainView._extend({
    init: function init(args) {
      init._super.apply(this, arguments);

      this.args = args;

      this.label = args.label;
      this.icon  = args.icon;
    },

    render: function(node) {
      var spawn = function() {
        this.args.view._members.spawn(this.args.state, {
          tabber : this.tabber,
          replace: this,
        }, {
          workspace: this.args.ws,
        });
      }.bind(this);

      if (vispa.sessionManager.lazy) {
        if (this.isVisible()) {
          spawn();
        } else {
          this.once("show", async.nextTick.bind(this, spawn));
        }
      } else {
        this.args.ws.once("connected", spawn);
        node.html("<h5>Workspace <i>"+this.args.ws.name+"</i> is not connected</h5>");
      }
    },

    // fake being the view we represent
    _serialize: function () {
      return [
        this.master._getFocusIndex(this),       // 0
        this.parent._getActiveIndex(this),      // 1
        this.args.ws && this.args.ws.id,        // 2
        this.args.view._members.extension.name, // 3
        this.args.view._members.name,           // 4
        this.args.state,                        // 5
        this.icon,                              // 6
        this.label,                             // 7
      ];
    },

  },{
    iconClass: "fa-blank",
    label: "",
    name: "PlaceholderView",
    workspace: null,
  });

  return PlaceholderView;
});
