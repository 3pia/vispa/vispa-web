define([
  "jquery",
  "vispa/extension",
  "vispa/utils",
  "./placeholderMainView",
  "./workspaceMainView",
  "./openWorkspaceAddDialog",
  "./preferenceMainView",
  "./usermanagementMainView",
  "css!../css/styles"
], function(
  $,
  Extension,
  Utils,
  PlaceholderMainView,
  WorkspaceMainView,
  openWorkspaceAddDialog,
  PreferenceMainView,
  UsermanagementMainView
) {

  var CoreExtension = Extension._extend({
    init: function init() {
      init._super.call(this, "core", "VISPA core");

      // override dynamic base back to vispa dynamic base
      Utils.setROP(this, "dynamicBase", vispa.dynamicBase);

      // not added to main menu
      this.addView(PlaceholderMainView);
      var wsMenu = this.addView(WorkspaceMainView);
      var prMenu = this.addView(PreferenceMainView);
      var umMenu = this.addView(UsermanagementMainView);

      // add menus items
      this.mainMenuAdd([
        prMenu,
        umMenu,
      ]);

      var fm = vispa.sidebar.$refs.footerMenu;
      fm.set("core_workspace", wsMenu);
      fm.set("core_preference", prMenu);
      fm.set("core_usermanagement", umMenu);

      this.openWorkspaceAddDialog = openWorkspaceAddDialog;

    }
  });

  return CoreExtension;
});
