define([
  "vispa/utils",
  "vue/vue",
  "./base",
  "text!../../html/umVue/workgroup-display.html",
], function (
  Utils,
  Vue,
  Base,
  Template
) {
  var Workgroup = Base.BaseDisplay.extend({
    template: Template,
  });

  return Workgroup;
});
