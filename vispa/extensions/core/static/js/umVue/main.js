/**
 * vue/sidebar
 * =================
 */
define([
  "vispa/utils",
  "vue/vue",
  "./base",
  "./workgroup",
  "./group",
  "./project",
  "./role",
  "text!../../html/umVue/main.html",
  "text!../../html/umVue/main-item.html",
  "text!../../html/umVue/main-nav-list.html",
], function (
  Utils,
  Vue,
  Base,
  Workgroup,
  Group,
  Project,
  Role,
  TemplateMain,
  TemplateItem,
  TemplateNavList
) {
  var Main = Vue.extend({
    template: TemplateMain,
    data: function(){
      return {
        isAdmin: vispa.args.user.isAdmin,
        sections: [
          "project",
          "group",
          "workgroup",
          "role",
          "permission",
        ],
        list: {
          project: [],
          group: [],
          workgroup: [],
          role: [],
          permission: [],
        },
        anyAdd: {
          workgroup: true,
        },
        adminOnly: {
          role: true,
          permission: true,
        },
        noNav: {
          permission: true,
        },
        refreshing: false,
        maxNav: 20,
      };
    },
    ready: function() {
      this.$emit("refresh");
    },
    watch: {
      section: function(sec) {
        this.view.state.set("section", sec, 0.1);
        this.$emit("refresh");
      },
      target: function(tar) {
        this.view.state.set("target", tar, 0.1);
      },
    },
    events: {
      refresh: function() {
        this.$emit("load", this.section);
      },
      go: function(section, target) {
        this.target = "";
        this.section = section;
        this.target = target || "";
      },
      attempt: function(what, callback, ev) {
        if (ev && ev.shiftKey) {
          callback();
        } else {
          this.view.confirm(what, function(ok){
            if (ok) callback();
          }, {
            focusButton: "yes",
          });
        }
      },
      load: function(sec, justInital) {
        if (!sec) return;
        if (this.list[sec].length && justInital) return;
        this.refreshing = true;
        this.$dispatch("GET", sec + "/list", {}, function(err, res) {
          if (err) return;
          this.list[sec] = res;
          this.refreshing = false;
        }.bind(this));
      },
      GET: function(url, params, callback) {
        return this.view.GET("ajax/um/" + url, params, callback);
      },
      "group-join": function() {
        this.$emit("load", "group");
      },
    },
    methods: {
      setSection: function(sec) {
        if (this.list[sec] === undefined) return;
        this.section = sec;
      },
    },
    components: {
      "nav-list": Vue.extend({
        template: TemplateNavList,
        props: {
          list: Array,
          target: String,
          sec: String,
        },
        computed: {
          listFiltered: function() {
            return this.list.filter(function(item){
              switch (item.status) {
                case 0:
                case 2:
                  return false;
              }
              return true;
            });
          },
        },
      }),
      "item-list": Base.NameList.extend({
        props: {
          list: Array,
        },
        paramName: "name",
        components: {
          "item-filter": Base.MultiFilter,
          "item": Base.BaseItem.extend({
            template: TemplateItem,
            data: function(){return {
              nameNew: false,
            };},
            watch: {
              nameNew: function() {
                this.$els.input.focus();
              },
            },
            methods: {
              rename: function() {
                var name = this.nameNew;
                this.nameNew = false;
                if (this.info.name == name || !name) return;
                this.$dispatch("GET", this.$parent.url + "/rename", {
                  name: this.info.name,
                  "new_name": name,
                }, function(err) {
                  if (err) return;
                  this.$dispatch("refresh");
                }.bind(this));
              },
            },
            components: {
              "group-control": Base.GroupControl,
            },
          }),
        },
      }),
      "group-join": Base.GroupJoin,
      workgroup: Workgroup,
      group: Group,
      project: Project,
      role: Role,
    },
  });

  return Main;
});
