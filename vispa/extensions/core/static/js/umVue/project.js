define([
  "vispa/utils",
  "vue/vue",
  "vue/vue-strap",
  "./base",
  "text!../../html/umVue/project-display.html",
  "text!../../html/umVue/project-filter.html",
  "text!../../html/umVue/project-item.html",
], function (
  Utils,
  Vue,
  VueStrap,
  Base,
  TemplateMain,
  TemplateFilter,
  TemplateItem
) {
  var Project = Base.BaseDisplay.extend({
    template: TemplateMain,
    computed: {
      status: {
        get: function() {
          return this.info.status;
        },
        set: function(v) {
          if (v == 2)
            this.remove();
          else
            this.update("status", v);
        },
      },
    },
    methods: {
      update: function(key, value, force) {
        if (!force && this.info[key] === value) return;
        var args = {};
        args[key] = value;
        this.$dispatch("GET", "project/update", args, function(err) {
          if (err) return;
          this.$emit("refresh");
        }.bind(this));
      },
    },
    watch: {
      managing: function(val) {
        if (val) this.$dispatch("load", "role", true);
      },
    },
    ready: function() {
      this.$dispatch("load", "group", true);
      if (this.managing) this.$dispatch("load", "role", true);
    },
    components: {
      "vs-select": VueStrap.select,
      "vs-group": VueStrap.buttonGroup,
      "vs-radio": VueStrap.radio,
      "member-list": Base.NameList.extend({
        computed: {
          itemUrl: function() {
            return this.url + "/" + this.url.split("/").reverse()[0] + "_role";
          },
          mergedRoles: function() {
            var roles = [];
            this.list.map(function(item){
              item.roles.map(function(r){
                if (!~roles.indexOf(r)) roles.push(r);
              });
            });
            roles.sort();
            return roles;
          },
          allRoles: function() {
            if (!this.canModItem) return [];
            var roles = this.$root.list.role;
            if (!roles) return [];
            return roles.map(function(r){return r.name;});
          },
        },
        components: {
          "item-filter": Base.BaseFilter.extend({
            template: TemplateFilter,
            data: function(){return {
              text: "",
              role: " ",
            };},
            computed: {
              func: function() {
                var text = this.text.trim();
                var role = this.role.trim();
                return function(item) {
                  if (!~item.name.indexOf(text)) return false;
                  if (role && !~item.roles.indexOf(role)) return false;
                  return true;
                };
              },
              roleOptions: function() {
                return this.$parent.mergedRoles.map(function(v){
                  return {
                    value: v,
                    label: v,
                  };
                }).concat([
                  {
                    value: " ",
                    label: "<i>Roles</i>",
                  }
                ]);
              },
            },
            components: {
              "vs-select": VueStrap.select,
            },
          }),
          "item": Base.BaseItem.extend({
            template: TemplateItem,
            props: {
              canModItem: Boolean,
            },
            data: function() {return {
              roles: [],
            };},
            ready: function() {
              this.roles = this.info.roles.slice();
              this.$refs.select.$el.getElementsByTagName("button")[0].classList.add("btn-xs");
            },
            events: {
              GET: function(url, params) {
                params["mid_name"] = params["mid_name"] || this.info.name;
                return true;
              },
              "refresh": function() {
                this.$dispatch("GET", this.url + "/list", {}, function(err, res) {
                  if (err) return;
                  this.info.roles = res;
                }.bind(this));
                return true;
              },
            },
            watch: {
              "info.roles": function(val) {
                this.roles = val.slice();
              },
              roles: function(newRoles) {
                var oldRoles = this.info.roles;
                var sem = 0;
                var cb = function() {
                  sem--;
                  if (sem) return;
                  this.$emit("refresh");
                }.bind(this);
                // remove old
                oldRoles.map(function(role){
                  if (~newRoles.indexOf(role)) return;
                  sem++;
                  this.$dispatch("GET", this.url + "/remove", {
                    "assoc_name": role,
                  }, cb);
                }.bind(this));
                // add new
                newRoles.map(function(role){
                  if (~oldRoles.indexOf(role)) return;
                  sem++;
                  this.$dispatch("GET", this.url + "/add", {
                    "assoc_name": role,
                  }, cb);
                }.bind(this));
              },
            },
            computed: {
              roleOptions: function() {
                var roles = this.$parent.allRoles.slice();
                roles = roles.concat(
                  this.info.roles.filter(function(r){return !~roles.indexOf(r)})
                );
                roles.sort();
                return roles.map(function(v){
                  return {
                    value: v,
                    label: v,
                  };
                });
              },
            },
            components: {
              "vs-select": VueStrap.select,
            },
          }),
        }
      }),
    },
  });

  return Project;
});
