define([
  "vispa/utils",
  "vue/vue",
  "vue/vue-strap",
  "./base",
  "text!../../html/umVue/group-display.html",
  "text!../../html/umVue/group-filter.html",
  "text!../../html/umVue/group-item.html",
  "text!../../html/umVue/group-group-item.html",
], function (
  Utils,
  Vue,
  VueStrap,
  Base,
  TemplateMain,
  TemplateFilter,
  TemplateItem,
  TemplateGroupItem
) {
  var Group = Base.BaseDisplay.extend({
    template: TemplateMain,
    data: function(){return {
      passwordNew: "",
      privacyOptions: ["public", "protected", "private"],
      memberOptions: ["active", "unconfirmed", "inactive"],
    };},
    events: {
      "group-join": function() {
        this.$broadcast("refresh");
      },
    },
    computed: {
      privacy: {
        get: function() {
          return this.info.privacy;
        },
        set: function(v) {
          this.update("privacy", v);
        },
      },
      status: {
        get: function() {
          return this.info.status;
        },
        set: function(v) {
          if (v == 2)
            this.remove();
          else
            this.update("status", v);
        },
      },
      canViewMembers: function() {
        return this.managing || (
          this.info && (
            this.info.member === 0 || this.info.privacy === 0
          )
        );
      },
      passwordStatus: function() {
        return this.info.password ? "active" : "not set";
      },
      memberStatus: function() {
        return this.info.member > 1 ? "inactive" : "active";
      }
    },
    methods: {
      update: function(key, value, force) {
        if (!force && this.info[key] === value) return;
        var args = {};
        args[key] = value;
        this.$dispatch("GET", "group/update", args, function(err) {
          if (err) return;
          this.$emit("refresh");
        }.bind(this));
      },
      setPassword: function(value) {
        this.update("password", value, true);
        this.passwordNew = value;
      },
    },
    components: {
      "vs-select": VueStrap.select,
      "vs-group": VueStrap.buttonGroup,
      "vs-radio": VueStrap.radio,
      "group-control": Base.GroupControl,
      "group-join": Base.GroupJoin,
      "member-list": Base.NameList.extend({
        events: {
          confirm: function(target) {
            this.$dispatch("GET", this.url + "/confirm", {
              "assoc_name": target,
            }, function(err){
              if (err) return;
              this.$emit("refresh");
            }.bind(this));
          },
        },
        components: {
          "item": Base.BaseItem.extend({
            template: TemplateItem,
          }),
          "item-filter": Base.BaseFilter.extend({
            template: TemplateFilter,
            data: function(){return {
              text: "",
              mode: 2,
            };},
            computed: {
              func: function() {
                var text = this.text;
                var mode = [false, true, null][this.mode];
                return function(item) {
                  if (!~item.name.indexOf(text)) return false;
                  if (item.confirmed === mode) return false;
                  return true;
                };
              },
              modeName: function() {
                return ["confirmed", "unconfirmed", "all"][this.mode];
              },
            },
            methods: {
              step: function() {
                this.mode = (this.mode + 1) % 3;
              },
            },
            components: {
              "vs-check-btn": VueStrap.checkboxBtn,
            },
          }),
        }
      }),
      "parent-list": Base.BaseList.extend({
        paramName: "name",
        computed: {
          groupList: function() {
            return this.$root.list.group || [];
          },
        },
        methods: {
          fillList: function(err, res) {
            if (!err) {
              var confirmed = {};
              var name = this.$parent.name;
              var lut = [true, false, undefined];
              for(var i = 0; i < res.length; ++i)
                confirmed[res[i].name] = res[i].confirmed;
              this.list = this.groupList.map(function(item){
                var ret = {};
                Vue.util.extend(ret, item);
                ret.member = lut.indexOf(confirmed[ret.name]);
                delete confirmed[ret.name];
                return ret;
              }).filter(function(grp){
                if (grp.member == 2 && grp.privacy == 2 && !grp.password) return false;
                if (grp.name == name) return false;
                return true;
              }).concat(Object.keys(confirmed).map(function(name){
                return {
                  name: name,
                  member: lut.indexOf(confirmed[name]),
                  privacy: 2,
                };
              }));
            }
            this.refreshing = false;
          },
        },
        components: {
          "item-filter": Base.MultiFilter,
          "item": Base.BaseItem.extend({
            template: TemplateGroupItem,
            components: {
              "group-control": Base.GroupControl.extend({
                subjectKind: "child_group",
                computed: {
                  subject: function() {
                    return this.$parent.$parent.$parent.name;
                  },
                },
              }),
            },
          }),
        },
      }),
    },
  });

  return Group;
});
