define([
  "vispa/utils",
  "vue/vue",
  "vue/vue-strap",
  "./base",
  "text!../../html/umVue/role-display.html",
], function (
  Utils,
  Vue,
  VueStrap,
  Base,
  TemplateMain
) {
  var Role = Base.BaseDisplay.extend({
    template: TemplateMain,
    data: function(){return {
      permissions: [],
    };},
    ready: function() {
      this.$dispatch("load", "permission", true);
    },
    events: {
      refresh: function() {
        if (this.isAdmin) {
          this.$parent.$dispatch("GET", "permission/list", {}, function(err, res) {
            if (err) return;
            this.permissions = res.map(function(v){return v.name});
          }.bind(this));
        }
      },
    },
    computed: {
      sugId: function() {
        return "suggestion_permissons_" + this._uid;
      },
    },
    components: {
      "permission-list": Base.NameList,
    },
  });

  return Role;
});
