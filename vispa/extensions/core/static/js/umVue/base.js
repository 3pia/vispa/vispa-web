/**
 * vue/sidebar
 * =================
 */
define([
  "vispa/utils",
  "vue/vue",
  "vue/vue-strap",
  "text!../../html/umVue/list.html",
  "text!../../html/umVue/name-item.html",
  "text!../../html/umVue/name-filter.html",
  "text!../../html/umVue/display.html",
  "text!../../html/umVue/group-control.html",
  "text!../../html/umVue/group-join.html",
  "text!../../html/umVue/multi-filter.html",
], function (
  Utils,
  Vue,
  VueStrap,
  TemplateList,
  TemplateNameItem,
  TemplateNameFilter,
  TemplateDisplay,
  TemplateGroupControl,
  TemplateGroupJoin,
  TemplateMultiFilter
) {
  var BaseList = Vue.extend({
    template: TemplateList,
    props: {
      name: String,
      title: String,
      url: String,
      canAdd: Boolean,
      suggestionId: String,
      canModItem: Boolean,
      canBrowse: Boolean,
    },
    paramName: "assoc_name",
    data: function(){return {
      list: [],
      target: "",
      refreshing: false,
    };},
    events: {
      refresh: function() {
        this.refreshing = true;
        this.$dispatch("GET", this.url + "/list", {}, this.fillList.bind(this));
      },
      remove: function(target, callback, ev) {
        var args = {};
        args[this.$options.paramName] = target;
        var f = this.$dispatch.bind(this, "GET", this.url + "/remove", args, function(err){
          if (err) return;
          if (callback) callback();
          this.$emit("refresh");
        }.bind(this));
        this.$dispatch("attempt", "Remove "+this.name+" \""+target+"\"?", f, ev);
      },
    },
    computed: {
      targetExists: function() {
        var target = this.target;
        return this.list.filter(function(item){
          return item.name == target;
        }).length > 0;
      },
      filtered: function() {
        var filter = this.$refs.filter;
        if (filter) {
          return this.list.filter(filter.func);
        } else {
          return this.list;
        }
      },
      numFiltered: function() {
        return this.list.length - this.filtered.length;
      },
      empty: function() {
        return this.list ? this.list.length === 0 : true;
      },
      numFilteredInfo: function() {
        var num = this.numFiltered;
        if (num === this.list.length) return "all items";
        if (num === 1) return "1 item";
        return num + " items";
      },
    },
    methods: {
      fillList: function(err, res) {
        this.list = err ? [] : res;
        this.refreshing = false;
      },
      toggle: function(ev) {
        if (!this.target) return;
        if (this.targetExists) {
          this.$emit("remove", this.target, function(){
            this.target = "";
          }.bind(this), ev);
        } else {
          var args = {};
          args[this.$options.paramName] = this.target;
          this.$dispatch("GET", this.url + "/add", args, function(err){
            if (err) return;
            this.target = "";
            this.$emit("refresh");
          }.bind(this));
        }
      },
    },
  });
  var BaseItem = Vue.extend({
    props: {
      info: Object,
      name: String,
      canModItem: Boolean,
      url: String,
    },
    methods: {
      remove: function(ev) {
        this.$dispatch("remove", this.info.name, null, ev);
      },
    },
  });
  var BaseFilter = Vue.extend({
    props: {
      name: String,
    },
  });


  var NameList = BaseList.extend({
    components: {
      "item": BaseItem.extend({
        template: TemplateNameItem,
      }),
      "item-filter": BaseFilter.extend({
        template: TemplateNameFilter,
        data: function(){return {
          text: "",
        };},
        computed: {
          func: function() {
            var text = this.text;
            return function(item) {
              if (!~item.name.indexOf(text)) return false;
              return true;
            };
          },
        },
      }),
    },
  });

  var BaseDisplay = Vue.extend({
    template: "",
    props: {
      kind: String,
      name: String,
      isAdmin: Boolean,
    },
    data: function(){return {
      refreshing: false,
      info: null,
    };},
    ready: function() {
      this.init();
    },
    watch: {
      name: function(val) {
        if (val) this.init();
      },
      infoName: function(val) {
        if (val) this.$broadcast("refresh");
      },
    },
    events: {
      refresh: function() {
        this.refreshing = true;
        this.$dispatch("GET", this.kind + "/info", {}, function(err, res) {
          if (err) return;
          this.info = res;
          this.refreshing = false;
        }.bind(this));
      },
      GET: function(url, params) {
        params.name = params.name || this.name;
        return true;
      }
    },
    methods: {
      init: function() {
        this.info = null;
        this.$emit("refresh");
      },
      rename: function() {
        this.$root.view.prompt("Please enter the new name:", function(name) {
          if (!name) return;
          this.$dispatch("GET", this.kind + "/rename", {
            "new_name": name,
          }, function(err) {
            if (err) return;
            this.$dispatch("go", this.kind, name);
            this.$dispatch("load", this.kind);
          }.bind(this));
        }.bind(this), {
          title: "Rename " + this.kind + "group \"" + this.info.name + "\"",
        });
      },
      remove: function(ev) {
        this.$dispatch(
          "attempt",
          "Remove \"" + this.info.name + "\"?",
          this.$dispatch.bind(this,
            "GET", this.kind + "/remove", {},
            function (err) {
              if (err) return;
              this.$dispatch("go", this.kind, "");
              this.$dispatch("load", this.kind);
            }.bind(this)
          ),
          ev
        );
      },
    },
    computed: {
      created: function() {
        return new Date(this.info.created).toLocaleString();
      },
      managing: function() {
        return this.info && this.info.manager || this.isAdmin;
      },
      infoName: function() {
        return this.info ? this.info.name : null;
      },
    },
    components: {
      "name-list": NameList,
    },
    partials: {
      "header": TemplateDisplay,
    }
  });

  var GroupControl = Vue.extend({
    template: TemplateGroupControl,
    props: {
      info: Object,
    },
    subjectKind: "user",
    computed:  {
      subject: function() {
        return vispa.args.user.name;
      },
      urlBase: function() {
        return "group/" + this.$options.subjectKind;
      },
      isAdmin: function() {
        return this.$root.isAdmin;
      },
    },
    methods: {
      join: function() {
        this.$dispatch("GET", this.urlBase + "/add", {
          name: this.info.name,
          "assoc_name": this.subject,
        }, this.$dispatch.bind(this, "refresh"));
      },
      password: function() {
        this.$root.view.prompt("Please enter the password:", function(password) {
          if (!password) return;
          this.$dispatch("GET", this.urlBase + "/add", {
            name: this.info.name,
            "assoc_name": this.subject,
            password: password,
          }, this.$dispatch.bind(this, "refresh"));
        }.bind(this), {
          title: "Join group \"" + this.info.name + "\"",
          password: true,
        });
      },
      leave: function(ev) {
        this.$dispatch(
          "attempt",
          "Leave \"" + this.info.name + "\"?",
          this.$dispatch.bind(this,
            "GET",
            this.urlBase + "/remove",
            {
              name: this.info.name,
              "assoc_name": this.subject,
            },
            this.$dispatch.bind(this, "refresh")
          ),
          ev
        );
      },
    }
  });
  var GroupJoin = Vue.extend({
    template: TemplateGroupJoin,
    props: {
      subject: String,
      subjectKind: String,
    },
    data: function(){return {
      group: "",
      password: "",
    };},
    computed: {
      subjectActual: function() {
        return this.subject || vispa.args.user.name;
      },
      ok: function() {
        return this.group && this.password;
      },
      foo: function() {
        return "group/" + this.subjectKind;
      },
    },
    methods: {
      join: function() {
        if (!this.ok) return;
        this.$dispatch("GET", "group/" + this.subjectKind + "/add", {
          name: this.group,
          "assoc_name": this.subjectActual,
          password: this.password,
        }, function(err){
          if (err) return;
          this.group = "";
          this.password = "";
          this.$dispatch("group-join");
        }.bind(this));
      },
    },
  });

  var MultiFilter = BaseFilter.extend({
    template: TemplateMultiFilter,
    data: function(){return {
      text: "",
      filter: "all",
    };},
    computed: {
      func: function() {
        var text = this.text.trim();
        var filter = this.filter;
        var privacy = ["public", "protected", "private"].indexOf(filter);
        return function(item) {
          if (!~item.name.indexOf(text)) return false;
          if ((item.status==2) != (filter=="deleted")) return false;
          switch(filter) {
            case "manager":
              return !!item.manager;
            case "member":
              return !!~[true, 0].indexOf(item.member);
            case "password":
              return !!item.password;
            case "public":
            case "protected":
            case "private":
              return item.privacy == privacy;
          }
          return true;
        };
      },
      filterOptions: function() {
        var opts = ["manager", "member"];
        if (this.name == "group")
          opts = opts.concat(["public", "protected", "private", "password"]);
        if (this.$root.isAdmin && ~["group", "project"].indexOf(this.name))
          opts.push("deleted");
        return [{
          value: "all", label: "<i>All</i>",
        }].concat(opts.map(function(v){return {
          value: v, label: Utils.capitalize(v)
        };}));
      },
    },
    components: {
      "vs-select": VueStrap.select,
      "vs-option": VueStrap.option,
    },
  });

  return {
    BaseList: BaseList,
    BaseItem: BaseItem,
    BaseFilter: BaseFilter,
    BaseDisplay: BaseDisplay,
    NameList: NameList,
    GroupControl: GroupControl,
    GroupJoin: GroupJoin,
    MultiFilter: MultiFilter,
  };
});
