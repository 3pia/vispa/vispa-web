# -*- coding: utf-8 -*-

from vispa.controller import AbstractController
from vispa.server import AbstractExtension

import cherrypy

class CoreController(AbstractController):
    pass

class CoreExtension(AbstractExtension):

    def name(self):
        return 'core'

    def dependencies(self):
        return []

    def setup(self):
        self.add_controller(CoreController())
        self.add_workspace_directoy()
