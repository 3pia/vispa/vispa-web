# -*- coding: utf-8 -*-

import logging

import vispa
from vispa.models.user import User
from vispa.models.group import Group
from vispa.server import AbstractExtension

from sqlalchemy.orm import scoped_session, sessionmaker

import ldap3
from ldap3.utils.log import set_library_log_detail_level, OFF, BASIC, NETWORK, EXTENDED
from ldap3.core.exceptions import LDAPNoSuchObjectResult, LDAPAttributeOrValueExistsResult, LDAPEntryAlreadyExistsResult, LDAPNoSuchAttributeResult

logger = logging.getLogger(__name__)


def _r(val):
    return [(ldap3.MODIFY_REPLACE, [val])]


class LDAPExport(object):

    def __init__(self):
        self.user_base = vispa.config("ldap-export", "user_base")
        self.group_base = vispa.config("ldap-export", "group_base")
        self.uid_offset = vispa.config("ldap-export", "uid_offset", 10000)
        self.private_group = vispa.config("ldap-export", "private_group", True)
        self.gid_offset = vispa.config("ldap-export", "gid_offset", 100000)
        set_library_log_detail_level(BASIC)

    def connect(self):
        url = vispa.config("ldap-export", "url")
        user = vispa.config("ldap-export", "user")
        password = vispa.config("ldap-export", "password")

        if not url:
            return

        self.connection = ldap3.Connection(url, user, password, client_strategy=ldap3.RESTARTABLE, auto_bind=True, raise_exceptions=True)

        return self.connection != None

    def delete_unknown(self, db):
        try:
            active_users = [unicode(u.name) for u in db.query(User, User.name).filter_by(status=User.ACTIVE)]
            groups = [unicode(g.name) for g in db.query(Group, Group.name)]

            self.connection.search(self.user_base, "(objectClass=posixAccount)", attributes=['uid'])
            for ldap_user in self.connection.entries:
                if ldap_user.uid not in active_users:
                    logger.info("Deleting unknown ldap user: %s", ldap_user.entry_dn)
                    self.connection.delete(ldap_user.entry_dn)

            self.connection.search(self.group_base, "(objectClass=posixGroup)", attributes=['cn'])
            for ldap_group in self.connection.entries:
                private_group = self.private_group and ldap_group.cn in active_users
                public_group = ldap_group.cn in groups
                if not private_group and not public_group:
                    logger.info("Deleting unknown ldap group: %s", ldap_group.entry_dn)
                    self.connection.delete(ldap_group.entry_dn)

        except:
            logger.exception("remove_invalid_ldap_entries")
        finally:
            db.remove()

    def sync_all_users(self, db):
        try:
            users = db.query(User).filter_by(status=User.ACTIVE)
            for user in users:
                try:
                    self.user_add(unicode(user.name), user.id, user.password)
                except:
                    logger.exception("sync_all_users")
        except:
            logger.exception("sync_all_users")
        finally:
            db.remove()

    def sync_all_groups(self, db):
        try:
            groups = db.query(Group)
            for group in groups:
                try:
                    self.group_add(unicode(group.name), group.id + self.gid_offset)
                    for assoc in group.get_users():
                        self.group_add_member(unicode(group.name), unicode(assoc.user.name))
                except:
                    logger.exception("sync_all_groups")
        except:
            logger.exception("sync_all_groups")
        finally:
            db.remove()

    def user_delete(self, name):
        dn = 'cn=%s,%s' % (name, self.user_base)
        logger.info("Delete user: %s", dn)
        self.connection.delete(dn)

        dn = 'cn=%s,%s' % (name, self.group_base)
        logger.info("Delete group: %s", dn)
        self.connection.delete(dn)

    def user_add(self, name, uid, password):
        username = name
        dn = 'cn=%s,%s' % (name, self.user_base)
        classes = ['top', 'person', 'organizationalPerson', 'inetOrgPerson', 'posixAccount', 'shadowAccount']
        attributes = {
            'uid': username,
            'cn': username,
            'sn': username,
            'userPassword': '{CRYPT}' + password,
            'loginShell': '/bin/bash',
            'uidNumber': uid + self.uid_offset,
            'gidNumber': uid + self.uid_offset,
            'homeDirectory': '/home/%s' % username
        }
        logger.info("Add user: %s, %s, %s", dn, classes, attributes)
        try:
            self.connection.add(dn, classes, attributes)
        except LDAPEntryAlreadyExistsResult:
            logger.info(" -> updated")
            changes = {
                'uid': _r(username),
                'userPassword': _r('{CRYPT}' + password),
                'uidNumber': _r(uid + self.uid_offset),
                'gidNumber': _r(uid + self.uid_offset),
                'homeDirectory': _r('/home/%s' % username)
            }
            self.connection.modify(dn, changes)

        if self.private_group:
            self.group_add(username, uid + self.uid_offset)
            self.group_add_member(username, username)


    def group_add(self, name, gid):
        dn = 'cn=%s,%s' % (name, self.group_base)
        classes = ['top', 'posixGroup']
        attributes = {
            'cn': name,
            'gidNumber': gid,
        }
        logger.info("Add group: %s, %s, %s", dn, classes, attributes)
        try:
            self.connection.add(dn, classes, attributes)
        except LDAPEntryAlreadyExistsResult:
            logger.info(" -> updated")
            changes = {
                'gidNumber': _r(gid),
            }
            self.connection.modify(dn, changes)

    def group_add_member(self, groupname, username):
        self.user_set_membership(username, groupname, True)

    def user_set_membership(self, username, groupname, active):
        msg = ("Set membership: of %s, in %s, to %s", username, groupname, active)
        try:
            self.connection.modify('cn=%s,%s' % (groupname, self.group_base), {
                'memberUid': [
                    (ldap3.MODIFY_ADD if active else ldap3.MODIFY_DELETE, [username])
                ],
            })
        except (LDAPAttributeOrValueExistsResult if active else LDAPNoSuchAttributeResult):
            pass
        except Exception:
            logger.exception(*msg)
            return
        logger.info(*msg)

    def user_set_password(self, name, password):
        dn = 'cn=%s,%s' % (name, self.user_base)
        changes = {
            'userPassword': (2, '{CRYPT}' + password)
        }
        logger.info("Change password: %s, %s", dn, changes)
        self.connection.modify(dn, changes)



class LDAPExportExtension(AbstractExtension):
    server_only = True

    def name(self):
        return "ldap_export"

    def dependencies(self):
        return []

    def setup(self):
        self.ldapexport = LDAPExport()
        if self.ldapexport.connect():

            if vispa.config("ldap-export", "sync_on_startup", False):
                session = scoped_session(sessionmaker(autoflush=True, autocommit=False))
                session.configure(bind=self.server._engine)
                if vispa.config("ldap-export", "delete_unknown", False):
                    self.ldapexport.delete_unknown(session)
                self.ldapexport.sync_all_users(session)
                self.ldapexport.sync_all_groups(session)

            vispa.register_callback("user.activate", self.on_activate)
            vispa.register_callback("user.set_password", self.on_set_password)
            vispa.register_callback("user.group.membership", self.on_set_membership)

    def on_activate(self, user):
        self.ldapexport.user_add(unicode(user.name), user.id, user.password)

    def on_set_password(self, user):
        self.ldapexport.user_set_password(unicode(user.name), user.password)

    def on_set_membership(self, user, group, active):
        self.ldapexport.user_set_membership(unicode(user.name), unicode(group.name), bool(active))
