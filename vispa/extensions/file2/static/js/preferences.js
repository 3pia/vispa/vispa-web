define([
], function(
) {

  var metaCtrl = function (key) {
    return "mac:meta+"+key+" ctrl+"+key;
  };

  var preferences = {
    label: "File Manager",
    iconClass: "fa-folder-o",
    items: {
      homePath: {
        label: "home path",
        type: "string",
        value: "$HOME",
        description: "The default open path.",
        level: 1,
      },
      dirFirst: {
        label: "Show directories first",
        type: "boolean",
        value: true,
        level: 2,
        global: true,
      },
      displayType: {
        level: 0,
        label: "Display type",
        description: "The display type for the files.",
        type: "string",
        value: "detail",
        options: ["detail", "icon"],
        flat: true,
        global: true,
      },
      sortOrder: {
        label: "Column sort order",
        type: "object",
        value: [],
        level: 4,
        global: true,
      },
      showHidden: {
        label: "Show hidden files",
        type: "boolean",
        value: false,
        level: 0,
        global: true,
      },
      bookmarks: {
        label: "Bookmarks",
        type: "object",
        value: [],
        level: 4,
      },
      fastmenu: {
        type: "object",
        value: ["home"],
        level: 5,
        global: true,
      },
      // keys
      keyCopy: {
        label: "Copy",
        type: "shortcut",
        global: true,
        level: 2,
        value: metaCtrl("c"),
        callback: function() {
          this.action("copy");
        }
      },
      keyCut: {
        label: "Cut",
        type: "shortcut",
        global: true,
        level: 2,
        value: metaCtrl("x"),
        callback: function() {
          this.action("cut");
        }
      },
      keyPaste: {
        label: "Paste",
        type: "shortcut",
        global: true,
        level: 2,
        value: metaCtrl("v"),
        callback: function() {
          this.action("paste");
        }
      },
      keyDelete: {
        label: "Delete",
        type: "shortcut",
        global: true,
        level: 2,
        value: "mac:meta+backspace del",
        callback: function() {
          this.action("remove");
        }
      },
      // these need "focus": rename, open
      keyParent: {
        label: "Navigate into parent folder",
        type: "shortcut",
        global: true,
        level: 2,
        value: "mac:meta+up backspace",
        callback: function() {
          this.goUp();
        }
      },
      keyReload: {
        label: "Reload the content of the current folder",
        type: "shortcut",
        global: true,
        level: 2,
        value: "mac:meta+r F5",
        callback: function() {
          this.reload();
        }
      },
      keyInfo: {
        label: "Open info dialog of context menu",
        type: "shortcut",
        global: true,
        level: 2,
        value: metaCtrl("i"),
        callback: function() {
          this.action("info");
        }
      },
      // selection: all & none
      keyFilter: {
        label: "Filter files in current view",
        type: "shortcut",
        global: true,
        level: 2,
        value: metaCtrl("f"),
        callback: function() {
          this.vue.content.filterStart();
          event.stopPropagation();
        },
      },
    },
  };

  return preferences;
});
