define([
  "vispa/utils",
], function(
  Utils
) {
  var menu = {
    home: {
      label: "Home",
      iconClass: "fa fa-home",
      callback: function() {
        this.$root.instance.goHome();
      },
      position: 0,
    },
    up: {
      label: "Go up",
      iconClass: "fa fa-arrow-up",
      buttonClass: "btn-default",
      callback: function() {
        this.$root.instance.goUp();
      },
      position: 1,
    },
    upload: {
      label: "Upload",
      iconClass: "fa fa-upload",
      buttonClass: "btn-default",
      callback: function() {
        this.$root.instance.upload();
      },
      position: 2,
    },
    refresh: {
      label: "Refresh",
      iconClass: "fa fa-refresh",
      buttonClass: "btn-default",
      callback: function() {
        this.$root.instance.reload();
      },
      position: 3,
    },
    filter: {
      label: "Filter",
      iconClass: "fa fa-filter",
      callback: function() {
        this.$root.instance.vue.content.$emit("filterStart");
      },
    },
    // FIXME: implement proper auto-menu-entries for level-0-preferences
    displayType: {
      label: function () {
        return (this.value ? Utils.capitalize(this.value) : "Display") + " Mode";
      },
      iconClass: "fa fa-image",
      buttonClass: "btn-default",
      position: 4,
      unique: true,
      multiple: false,
      fastmenu: "dropdown",
      items: {
        symbol: {
          label: "Icon",
          iconClass: "fa fa-image",
          buttonClass: "btn-default",
          value: "icon",
          callback: function() {
            this.$root.instance.prefs.set("displayType", this.value);
          }
        },
        table: {
          label: "Detail",
          iconClass: "fa fa-list",
          buttonClass: "btn-default",
          value: "detail",
          callback: function() {
            this.$root.instance.prefs.set("displayType", this.value);
          }
        }
      }
    },
  };

  // sorting menu (may become deprecated with auto pref menus)
  var sortIcon = {
    name: "fa-sort-alpha-asc",
    size: "fa-sort-amount-asc",
    mtime: "fa-sort-numeric-asc",
  };
  var sortLabel = {
    name: "Name",
    size: "Size",
    mtime: "Date",
  };
  var sortMenu = {
    label: function () {
      var x = this.$root.instance.prefs.get("sortOrder");
      x = sortLabel[x && x[0]];
      return x ? ("Sort by " + x) : "Sort order";
    },
    iconClass: function () {
      var x = this.$root.instance.prefs.get("sortOrder");
      return sortIcon[x && x[0]] || "fa-sort";
    },
    buttonClass: "btn-default",
    position: 5,
    // unique: true,
    fastmenu: "dropdown",
    items: {},
  };
  for (var i in sortIcon) {
    sortMenu.items[i] = {
      label: sortLabel[i],
      buttonClass: "btn-default",
      value: i,
      callback: function () {
        this.$root.instance.vue.content.sortMod(this.value);
      },
    };
    sortIcon["-" + i] = sortIcon[i].replace("-asc", "-desc");
    sortLabel["-" + i] = sortLabel[i].replace("-asc", "-desc");
  }
  menu.sortOrder = sortMenu;

  return menu;
});
