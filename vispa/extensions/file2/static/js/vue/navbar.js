define([
  "vue/vue",
  "vispa/vue/menu",
  "vispa/vue/renamer",
  "./dropfilter",
  "text!../../html/vue/navbar.html",
  "text!../../html/vue/navbar-segment.html",
  "text!../../html/vue/bookmark-root.html",
  "text!../../html/vue/bookmark-item.html",
], function(
  Vue,
  Menu,
  Renamer,
  dropFilter,
  tNavbar,
  tNavbarSegment,
  tBookmarkRoot,
  tBookmarkItem
) {
  var NavbarVue = Vue.extend({
    template: tNavbar,
    props: {
      path: String,
      loading: Boolean,
      workspaceId: Number,
      viewId: String,
      dirNames: Array,
      pathEdit: {
        type: Boolean,
        default: false,
      }
    },
    data: function() {return {
      pathText: this.path,
    }},
    computed: {
      parts: function() {
        return this.path.split("/").filter(function(p, i){return !i || p;});
      },
      paths: function() {
        var parts = this.parts;
        return parts.map(function(p, i){return parts.slice(0, i + 1).join("/") || "/"});
      },
      basename: function() {
        return this.parts.slice(-1)[0] || this.path;
      },
      bookmarks: function() { // only a getter, modify via splice
        return this.$root.pref("bookmarks");
      },
      bookmarkItems: function() {
        return {
          div: {
            position: 0,
            divider: true,
            hidden: !this.bookmarks.length,
          },
          add: {
            position: 1,
            label: "Add",
            iconClass: "fa-plus",
            callback: function() {
              this.bookmarks.push({
                name: this.basename,
                path: this.path,
              });
              return true;
            }.bind(this),
          },
        };
      },
      suggestions: function() {
        var prefix = this.path + "/";
        if (!this.pathText.startsWith(prefix)) return [];
        var base = this.pathText.replace(prefix, "");
        return this.dirNames
          .filter(function(name) {return name.startsWith(base);})
          .map(function(name) {return prefix + name;})
          .sort()
          .slice(0, 10);
      },
      suggestionsId: function() {
        return "file2_navbar_suggestions_" + this.viewId;
      },
    },
    methods: {
      pathNav: function() {
        this.pathEdit = false;
        this.$dispatch("nav", this.pathText);
      },
      refresh: function() {
        this.$dispatch("nav", this.path);
      },
    },
    watch: {
      path: function(path) {
        this.pathText = path;
      },
      pathEdit: function(val) {
        if (val)
          this.$els.pathInput.focus();
      }
    },
    components: {
      "navbar-segment": {
        template: tNavbarSegment,
        props: {
          fullPath: String,
          workspaceId: Number,
        },
        data: function() {
          return {
            dropok: false,
          };
        },
        computed: {
          basename: function() {
            return this.fullPath.split("/").slice(-1)[0];
          },
          root: function() {
            return this.fullPath === "/";
          },
        },
        methods: {
          navi: function() {
            this.$dispatch("nav", this.fullPath);
          },
          dropFilter: function(ev) {
            return dropFilter(ev, this.workspaceId, this.fullPath);
          },
          openMenu: function(ev) {
            this.$dispatch("openMenu", {
              pathBase: this.fullPath,
              menuPP: function(menu) {
                return (menu.openWith || {}).items;
              },
            }, ev);
          },
        },
        events: {
          dropped: function(data, ev) {
            this.$dispatch("handleDrop", this.fullPath, data, ev);
          },
        },
      },
      "menu-button": Menu.button.extend({
        props: {
          bookmarks: Array,
        },
        partials: {
          "menu-root": tBookmarkRoot,
        },
        methods: {
          remove: function(data) {
            var idx = this.bookmarks.indexOf(data);
            if (!~idx) return;
            this.bookmarks.splice(idx, 1);
          },
        },
        components: {
          "bookmark-item": {
            template: tBookmarkItem,
            props: {
              data: Object,
            },
            methods: {
              rename: function(name) {
                this.data.name = name;
              },
              remove: function() {
                this.$parent.remove(this.data);
              },
            },
            components: {
              renamer: Renamer,
            },
          },
        },
      }),
    },
  });

  return NavbarVue;
});
