define([
  "vue/vue",
  "./baseDisplay",
  "./baseItem",
  "vispa/utils",
  "text!../../html/vue/detail.html",
  "text!../../html/vue/detailItem.html",
], function(
  Vue,
  BaseDisplay,
  BaseItem,
  Utils,
  tDetail,
  tDetailItem
) {
  var DetailVue = BaseDisplay.extend({
    template: tDetail,
    props: {
      wsId: Number,
      path: String,
      items: Array,
    },
    breakpoints: {
      "mini-actions": 900,
      "hide-modified": 600,
      "hide-size": 450,
      "hide-actions": 300,
    },
    computed: {
      breakpoints: function() {
        var bp = this.$options.breakpoints;
        var w = this.dims.width;
        return Object.keys(bp).filter(function(p){
          return w < bp[p];
        });
      },
    },
    components: {
      item: BaseItem.extend({
        template: tDetailItem,
      }),
    },
  });

  return DetailVue;
});
