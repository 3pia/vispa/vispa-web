define([
  "vue/vue",
  "./detail",
  "./icons",
  "./navbar",
  "vispa/utils",
  "vispa/vue/menu",
  "vispa/filehandler2",
  "text!../../html/vue/browser.html"
], function(
  Vue,
  DetailVue,
  IconsVue,
  NavbarVue,
  Utils,
  Menu,
  FileHandler2,
  tBrowser
) {
  var BrowserVue = Vue.extend({
    template: tBrowser,
    data: function(){return {
      path: "",
      loading: false,
      loadsem: 0,
      items: [],
      filterActive: false,
      filterText: "",
      filterError: "",
      limit: 100,
      limitBase: 100,
      limitStep: 100,
      pathEdit: false,
      menuItems: {},
      menuStyle: {},
    }},
    ready: function() {
      if (this.path)
        this.$emit("nav", this.path);
    },
    computed: {
      filteredItems: function() {
        var items = this.items;
        if (this.filterActive && this.filterText) {
          this.filterError = "";
          var re = this.filterText.match(/^\/(.+)\/(i?)$/);
          if (!re) {
            re = this.filterText.replace(/[.+^$[\]\\(){}|-]/g, "\\$&");
            if (~re.search(/([^\\]|^)[*?]/))
              re = "^" + re
                .replace(/([^\\]|^)\?/g, "$1.")
                .replace(/([^\\]|^)\*/g, "$1.+") + "$";
            re = [0, re, "i"];
          }
          try {
            re = new RegExp(re[1], re[2]);
          } catch(e) {
            re = null;
            this.filterError = e.message;
          }
          if (re)
            items = items.filter(function(it){
              return re.test(it.name);
            });
        }
        return items;
      },
      dirNames: function() {
        return this.filteredItems.filter(function(item) {
          return item.type === "d";
        }).map(function(item) {
          return item.name;
        });
      },
      sortedItems: function() {
        var order = (this.pref("dirFirst") ? ["type"] : []).concat(this.pref("sortOrder"));
        return this.filteredItems.slice().sort(function(a, b) {
          var i=0, key, rev, va, vb;
          while(key = order[i]) {
            if (rev = (key[0] === "-"))
              key = key.substring(1);
            if ((va = a[key]) === (vb = b[key]))
              i++;
            else
              return ((
                (typeof va === "string") ?
                (va.localeCompare(vb) == -1) :
                (va < vb)
              ) === rev) ? 1 : -1;
          }
          return 0;
        });
      },
      overflow: function() {
        return this.sortedItems.length > this.limit;
      },
      limitedItems: function() {
        return this.overflow ? this.sortedItems.slice(0, this.limit) : this.sortedItems;
      },
      filterKey: function() {
        return Utils.shortcutPP(this.view.prefs.get("keyFilter"));
      },
      filterNum: function() {
        return this.items.length - this.filteredItems.length;
      },
      showHidden: function() {
        return this.pref("showHidden");
      },
      displayType: function() {
        return this.pref("displayType");
      },
      renameingSupport: function() {
        return this.displayType == "detail";
      },
      workspaceId: function() {
        return this.view.workspace.id;
      },
      view: function() {
        return this.$options.view;
      },
      selectedItems: function() {
        return this.items.filter(function (it) {
          return it.selected;
        });
      }
    },
    methods: {
      sortMod: function(col) {
        var so = this.pref("sortOrder").slice();
        var plop = function(n, m) {
          var i = so.indexOf(n);
          switch(i) {
            case -1:
              return true;
            case 0:
              so[0] = m;
              return false;
            default:
              so.splice(i, 1);
              so.splice(0, 0, n);
              return false;
          }
        };
        if (plop(col, "-"+col) && plop("-"+col, col))
          so.splice(0, 0, col);
        this.pref("sortOrder", so);
      },
      filterStart: function() {
        this.filterActive = true;
        this.filterFocus();
      },
      filterFocus: function() {
        this.$els.filter.focus();
        this.$els.filter.setSelectionRange(0, this.filterText.length);
      },
      extendInfo: function(info) {
        var ext = {};
        // add selected
        var sel = this.selectedItems;
        if (sel.length && (!info.data || sel.filter(function (it) {
          return it.name == info.data.name;
        }).length) && !info.pathBase) {
          ext.items = sel;
        } else if(info.data) {
          ext.items = [info.data];
        }
        // add other info
        ext.view = this.view;
        ext.wsId = ext.view.workspaceId;
        ext.pathBase = info.pathBase || this.path;
        // add hook entry point fo us
        ext._f2br = this;
        return $.extend({}, info, ext);
      },
      pref: function(key, value) {
        if (value === undefined)
          return this.view.prefs.get(key);
        else
          this.view.prefs.set(key, value);
      },
      open: function (info) {
        this.$emit("action", "open", info);
      },
    },
    watch: {
      path: function(path) {
        this.view.state.set("path", path);
      },
      filterActive: function(active) {
        if (active)
          this.filterFocus();
      },
      showHidden: function() {
        this.$emit("nav", this.path);
      },
    },
    events: {
      nav: function(path) {
        path = path || "/";
        path = path.replace(/\/+/g, "/");
        path = path.replace(/\/$/, "") || "/";
        this.loading = true;
        var sem = ++this.loadsem;
        this.$dispatch("ajax", "GET", "/ajax/fs/filelist", {
          path: path,
          watch_id: "0",
          hide_hidden: !this.showHidden,
        }, function(err, data) {
          this.loading = false;
          if (err) return;
          if (this.loadsem !== sem) return;
          if (this.path !== data.path)
            this.filterActive = false;
          this.limit = this.limitBase;
          this.path = data.path;
          var items = data.filelist.map(function(d){
            d.dragged = false;
            d.dropok = false;
            d.processing = false;
            d.renameing = false;
            return d;
          });
          if (this.foldersOnly)
            items = items.filter(function (it) {
              return it.type == "d";
            });
          this.items = items;
        }.bind(this));
      },
      patch: function(infos) {
        var infoByName = {};
        infos.forEach(function(info){
          infoByName[info.name]=info
        });
        this.items.forEach(function(item){
          var update = infoByName[item.name];
          if (update)
            Vue.util.extend(item, update);
        });
      },
      filterStart: function() {
        this.filterStart();
      },
      action: function(what, info) {
        FileHandler2.action(what, this.extendInfo(info));
      },
      open: function(info) {
        this.open(info);
      },
      openMenu: function(info, ev) {
        this.menuItems = FileHandler2.makeMenu(this.extendInfo(info));
        this.$refs.menu.show(ev);
        ev.preventDefault();
      },
      handleDrop: function(path, data, ev) {
        if (data.Files) {
          this.view.upload(data.Files, path);
        } else {
          if (!data["vispa/workspace:" + this.workspaceId]) return;
          var src = JSON.parse(data["vispa/files"] || "[]");
          if (data["vispa/file"])
            src.push(data["vispa/file"]);
          if (src.length && src[0]) {
            this.$dispatch("ajax", "POST", "/ajax/fs/paste", {
              path: path,
              paths: JSON.stringify(src),
              cut: !ev.ctrlKey,
            });
          }
        }
      },
      ajax: function() {
        this.view.ajax.apply(this.view, arguments);
      },
    },
    components: {
      detail: DetailVue,
      icon: IconsVue,
      navbar: NavbarVue,
      "menu-context": Menu.context,
    },
  });

  return BrowserVue;
});
