define([
  "vue/vue",
  "./dropfilter",
], function(
  Vue,
  dropFilter
) {
  var BaseDisplay = Vue.extend({
    props: {
      wsId: Number,
      path: String,
      items: Array,
      dims: Object,
    },
    data: function() {return {
      sortCols: ["name", "size", "modified"],
      dropok: false,
    };},
    methods: {
      openMenu: function(ev) {
        if (ev.defaultPrevented) return;
        this.$dispatch("openMenu", {}, ev);
      },
      sortMod: function(col) {
        this.$parent.sortMod(col);
      },
      sortCurr: function(col) {
        var so = this.$root.pref("sortOrder")[0];
        if (so ===     col) return "fa fa-sort-asc";
        if (so === "-"+col) return "fa fa-sort-desc";
        return "fa fa-sort";
      },
      getDragInfo: function(base) {
        var tars = this.$refs.items || [];
        tars = tars.filter(function(t){return t.selected;});
        var ret;
        var prefix = this.path + "/";
        if (tars.legnth < 2 || !~tars.indexOf(base)) {
          ret = {
            "text/plain": base.data.name,
            "text/uri-list": base.downloadLink,
            "vispa/file": prefix + base.data.name,
            "vispa/drag-targets": [base],
          };
        } else {
          var names = tars.map(function(t){
            return prefix + t.data.name;
          });
          ret = {
            "text/plain": names.join("\n"),
            "vispa/files": JSON.stringify(names),
            "vispa/drag-targets": tars,
          };
        }
        ret["vispa/workspace:" + this.workspaceId] = String(this.workspaceId);
        ret["vispa/pathbase:" + this.path.toLowerCase()] = this.path;
        return ret;
      },
      dropFilter: function(ev) {
        if (this.childDragged) return false;
        return dropFilter(ev, this.workspaceId, this.path);
      },
    },
    computed: {
      view: function() {
        return this.$root.view;
      },
      workspaceId: function() {
        return this.view.workspace.id;
      },
      downloadBase: function() {
        return this.view.fileURL("");
      },
      allSelected: {
        get: function() {
          var selAll;
          for(var i = this.items.length - 1; i >= 0; i--) {
            var sel = !!this.items[i].selected;
            if (selAll === undefined)
              selAll = sel;
            else if (selAll !== sel)
              return undefined;
          }
          return !!selAll;
        },
        set: function(val) {
          this.items.map(function(it) {
            Vue.set(it, "selected", val);
          });
        },
      },
      childDragged: function() {
        return this.items.some(function(it){return it.dragged});
      },
    },
    events: {
      dropped: function(data, ev) {
        this.$dispatch("handleDrop", this.path, data, ev);
      },
    },
  });

  return BaseDisplay;
});
