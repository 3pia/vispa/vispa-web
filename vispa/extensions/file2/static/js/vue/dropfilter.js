define([
], function(
) {
  var dropFilter = function(ev, workspaceId, tarPath) {
    var types = ev.dataTransfer.types;
    if (~types.indexOf("Files"))
      return ["Files", "copy"];
    else {
      if (tarPath && ~types.indexOf("vispa/pathbase:" + tarPath.toLowerCase())) return;
      if (!~types.indexOf("vispa/workspace:" + workspaceId)) return;
      return ["vispa/file", "vispa/files", ev.ctrlKey ? "copy" : "move"];
    }
  };

  return dropFilter;
});
