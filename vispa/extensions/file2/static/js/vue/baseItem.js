define([
  "vue/vue",
  "vispa/vue/base",
  "vispa/utils",
  "vispa/vue/renamer",
  "./dropfilter"
], function(
  Vue,
  BaseVue,
  Utils,
  Renamer,
  dropFilter
) {
  var BaseItem = Vue.extend({
    iconSuffixes: [
      "bmp", "c", "cc", "conf", "cpp", "csh", "css", "eps", "f", "f03", "f90", "f95", "f", "f03",
      "f90", "f95", "gif", "h", "html", "ico", "ini", "jade", "jpeg", "jpg", "js", "less", "lnk",
      "log", "m", "mako", "pdf", "png", "ps", "pxlio", "py", "pyc", "r", "rar", "raw", "root",
      "sh", "svg", "tar", "tex", "tif", "tiff", "txt", "xml", "yaml", "yml", "zip"
    ],
    iconTranslation: {
      "targz": ["tar.gz", "tgz"],
    },
    props: {
      data: Object,
      index: Number,
      workspaceId: Number,
      downloadBase: String,
      pathBase: String,
    },
    computed: {
      mtimeFmt: function() {
        return Utils.formatDate(new Date(this.data.mtime * 1000));
      },
      sizeFmt: function() {
        return Utils.formatSize(this.data.size);
      },
      cssIcon: function() {
        if (this.processing)
          return "fa fa-refresh fa-spin";
        switch (this.data.type) {
          case "d":
            return "fi fi-folder" + (this.data.symlink ? "-symlink" : "");
          case "f":
            var name = this.data.name;
            var type = this.getFileType(name) || this.getFileType(name.toLowerCase()) || "";
            if (type) return "fi fi-" + type;
          default:
            return "fi fi-default-" + (this.data.symlink ? "symlink" : "alt");
        }
      },
      fullPath: function() {
        return this.pathBase + "/" + this.data.name;
      },
      downloadLink: function() {
        return this.downloadBase + this.fullPath + "&download=1";
      },
      draggable: function() {
        if (this.data.name == "..") return false;
        if (this.renameing) return false;
        if (this.processing) return false;
        return true;
      },
    },
    methods: {
      dropFilter: function(ev) {
        if (this.dragged) return false;
        if (this.processing) return false;
        if (this.data.type !== "d") return false;
        return dropFilter(ev, this.workspaceId, this.fullPath);
      },
      action: function(what, extra) {
        this.$dispatch("action", what, $.extend({
          data: this.data,
        }, extra));
      },
      open: function(ev) {
        if (ev.defaultPrevented) return;
        var t = ev.target;
        if (t && t.nodeName == "INPUT") return;
        if (vispa.device.isMac ? ev.metaKey : ev.ctrlKey)
          this.selected = !this.selected;
        else
          this.$dispatch("open", {data: this.data});
      },
      openMenu: function(ev) {
        if (ev.defaultPrevented) return;
        var t = ev.target;
        if (t && t.nodeName == "INPUT") return;
        this.$dispatch("openMenu", {
          data: this.data,
        }, ev);
      },
      getDragInfo: function() {
        return this.$parent.getDragInfo(this);
      },
      getFileType: function(fileName) {
        var ext = fileName.split(".");
        if (ext.length == 1) return;
        ext = ext[ext.length - 1];
        if (~this.$options.iconSuffixes.indexOf(ext)) return ext;
        // mapping check
        var map = this.$options.iconTranslation;
        for (ext in map) {
          var ma = map[ext];
          for (var i=0; i<ma.length; i++) {
            var m = ma[i].toLowerCase();
            if (fileName.lastIndexOf(m) + m.length == fileName.length) return ext;
          }
        }
      },
      renameFinish: function(newName) {
        this.action("rename", {
          newName: newName,
        });
      },
    },
    events: {
      dropped: function(data, ev) {
        this.$dispatch("handleDrop", this.fullPath, data, ev);
      },
    },
    components: {
      renamer: Renamer,
    },
    mixins: [
      BaseVue.compProxy("data", [
        "dragged",
        "dropok",
        "processing",
        "renameing",
        "selected",
      ]),
    ],
  });

  return BaseItem;
});
