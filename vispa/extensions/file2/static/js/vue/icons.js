define([
  "vue/vue",
  "./baseDisplay",
  "./baseItem",
  "vispa/utils",
  "text!../../html/vue/icons.html",
  "text!../../html/vue/iconsItem.html",
], function(
  Vue,
  BaseDisplay,
  BaseItem,
  Utils,
  tIcons,
  tIconsItem
) {
  var IconsVue = BaseDisplay.extend({
    template: tIcons,
    components: {
      item: BaseItem.extend({
        template: tIconsItem,
        methods: {
          renameStart: function() {
            this.$root.view.prompt("", function (newName) {
              if (newName !== null && newName !== this.data.name)
                this.rename(newName);
            }.bind(this), {
              value: this.data.name,
              placeholder: "new file name",
              title: "Rename file",
              okLabel: "Rename",
            });

          },
        }
      }),
    },
  });

  return IconsVue;
});
