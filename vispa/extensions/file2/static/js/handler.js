define([
  "vispa/common/dialog",
  "vispa/utils",
  "vispa/filehandler2",
  "text!../html/vue/info-dialog-body.html",
], function(
  Dialog,
  Utils,
  FileHandler2,
  tInfoDialogBody
) {

  var pos = 11; // there is a divider before this position

  // now create the default handler
  FileHandler2.addExt("single", {
    menu: function menu(info) {
      var bo = FileHandler2._bestOpen(info);
      return $.extend({
        label: "Open",
        iconClass: bo && bo.iconClass || "fa-file",
        disabled: !bo,
        position: 1,
      }, menu._super.call(this, info));
    },
    run: function (info) {
      var bo = FileHandler2._bestOpen(info);
      info.viaBestOpen = true;
      return bo ? bo.callback() : false;
    },
  }, "open");

  // clipboard stuff
  var ClipboardHandler = FileHandler2.clsBib.multi._extend({
    init: function init(name) {
      init._super.call(this, name);
      this.menuTmpl = {
        label: Utils.capitalize(name),
        iconClass: "fa-" + name,
        position: pos++,
      };
    },
    run: function (info) {
      vispa.clipboard = {
        wsId: info.view.workspace.id,
        pathBase: info.pathBase,
        names: this.fullPaths(info),
        action: this.name,
      };
    }
  });
  new ClipboardHandler("copy");
  new ClipboardHandler("cut");
  FileHandler2.addExt("base", {
    menuTmpl: {
      label: "Paste",
      iconClass: "fa-paste",
      position: pos++,
    },
    check: function check(info) {
      var cb = vispa.clipboard;
      return check._super.call(this, info) && cb && cb.wsId == info.wsId &&
             ~["copy", "cut"].indexOf(cb.action);
    },
    run: function (info) {
      var cut = (vispa.clipboard.action == "cut");
      info.view.POST("/ajax/fs/paste", {
        path: info.pathBase,
        paths: JSON.stringify(vispa.clipboard.names),
        cut: cut,
      }, function () {
        if (cut)
          vispa.clipboard = null;
      });
    },
  }, "paste");

  // other file ops
  FileHandler2.addExt("single", {
    menuTmpl: {
      label: "Rename",
      iconClass: "fa-pencil",
      position: pos++,
    },
    run: function run(info) {
      if (info.newName) {
        info.data.processing = true;
        info.view.POST("/ajax/fs/rename", {
          "path": info.pathBase,
          "name": info.data.name,
          "new_name": info.newName,
        });
      } else if (info._f2br && info._f2br.renameingSupport) {
        info.data.renameing = true;
      } else {
        info.view.prompt("", function (newName) {
          if (newName === null || newName === info.data.name) return;
          run($.extend({}, info, {newName: newName}));
        }, {
          value: info.data.name,
          placeholder: "new file name",
          title: "Rename file",
          okLabel: "Rename",
        });
      }
    },
  }, "rename");
  FileHandler2.addExt("multi", {
    menuTmpl: {
      label: "Remove",
      iconClass: "fa-times",
      position: pos++,
    },
    run: function run(info) {
      if (info.confimed || (info.event && info.event.shiftKey)) {
        info.view.POST("/ajax/fs/remove", {
          "path": JSON.stringify(this.fullPaths(info, true)),
        });
      } else {
        var it = this.items(info);
        var cb = run.bind(this, $.extend(true, {}, info, {confimed: true}));
        info.view.confirm(
          "Do you really want to delete " + (
            it.length > 1 ?
            it.length + " items" :
            "\"" + it[0].name +  "\""
          ) + "?",
          function (ok) {
            if (ok) cb();
          }, {
            focusButton: "yes",
          }
        );
      }
    },
  }, "remove");
  FileHandler2.addExt("multi", {
    menuTmpl: {
      label: "Download",
      iconClass: "fa-download",
      position: pos++,
    },
    run: function (info) {
      var it = this.items(info);
      var dl = function (path, del) {
        $("<iframe />")
          .attr({
            src: [
              info.view.fileURL(path),
              "&download=1",
              "&deleteoncomplete=" + String(del)
            ].join("&"),
            style: "display:none;"
          }).appendTo("body");
      };

      if (it.length > 1 || it[0].type=="d") {
        info.view.POST("/ajax/fs/compress", {
          paths: JSON.stringify(this.fullPaths(info, false, it)),
          path: "",
          name: [
            "vispa",
            it.length > 1 ?
            info.pathBase.split("/").pop() :
            it[0].name,
            (new Date()).toISOString().slice(0, 19)
          ].join("_"),
          isTmp: true,
        }, function (err, archive) {
          if (err) return;
          dl(archive, true);
        });
      } else {
        dl(info.pathBase + "/" + it[0].name, false);
      }
    },
  }, "download");
  FileHandler2.addExt("multi", {
    menuTmpl: {
      label: "Compress",
      iconClass: "fa-compress",
      position: pos++,
    },
    run: function run(info) {
      if (info.zipName) {
        info.view.POST("/ajax/fs/compress", {
          paths: JSON.stringify(this.fullPaths(info)),
          path: info.pathBase,
          name: info.zipName,
          isTmp: false,
        });
      } else {
        var self = this;
        info.view.prompt(
          "Enter the name for the zip-file.",
          function (zipName) {
            if (zipName)
              run.call(self, $.extend({}, info, {zipName: zipName}));
          }
        );
      }
    }
  }, "compress");
  FileHandler2.addExt("single", {
    fext: [
      "zip",
      // "tar",
      // "tgz", "tar.gz",
      // "tbz2", "tar.bz2",
    ],
    check: function check(info) {
      return false; // TODO: implement this!
      return check._super.call(this, info) && ~this.fext.indexOf(this.ext(info));
    },
    run: function (info) {
      throw Error("NOT IMPLEMENTED");
    },
  }, "extract");

  // create stuff
  var CreateHandler = FileHandler2.clsBib.base._extend({
    init: function init(type) {
      var cType = Utils.capitalize(type);
      init._super.call(this, "create" + cType);
      this._type = type;
      this.menuTmpl = {
        label: "Create " + cType,
        iconClass: "fa-" + type + "-o",
        position: pos++,
      };
    },
    run: function run(info) {
      if (info.name) {
        info.view.POST("/ajax/fs/create" + this._type, {
          path: info.pathBase,
          name: info.name,
        });
      } else {
        info.view.prompt("Enter the new " + this._type + " name:", function (name) {
          if (!name) return;
          run.call(this, $.extend({}, info, {name: name}));
        }.bind(this));
      }
    }
  });
  new CreateHandler("file");
  new CreateHandler("folder");

  pos = 101; // there is a divider before this position

  // info dialog
  var InfoDialog = Dialog.extend({
    computed: {
      typeDesc: function () {
        return (this.summary.type == "d" ? "Directory" : (
          (this.summary.ext ? this.summary.ext + "-" : "") +
          "File"
        )) + (this.summary.symlink ? " (symlink)" : "");
      },
    },
    filters: {
      formatSize: Utils.formatSize,
      formatDate: function (date) {
        return Utils.formatDate(new Date(date * 1000));
      },
    },
    partials: {
      body: tInfoDialogBody.trim(),
    },
  });

  FileHandler2.addExt("multi", {
    menuTmpl: {
      label: "Info",
      iconClass: "fa-info",
      position: pos++,
    },
    run: function (info) {
      var summary;
      var it = this.items(info);
      if (it.length > 1) {
        var nf = 0, nd = 0, ns = 0, ts = 0;
        it.forEach(function (i) {
          if (i.type == "f") nf++;
          if (i.type == "d") nd++;
          if (i.symlink) ns++;
          if (i.type == "f" && !i.symlink) ts += i.size;
        });
        summary = {
          size: ts,
          files: nf,
          directories: nd,
          symlinks: ns,
        };
      } else
        summary = $.extend({}, it[0]);
      summary.pathBase = info.pathBase;

      new InfoDialog({
        data: {
          summary: summary,
          title: (it.length == 1) ? it[0].name : (it.length + " items"),
          body: true,
          buttons: {
            close: {
              label: "Close",
              class: "btn-primary",
              callback: function() {
                this.close();
              },
            },
          },
        },
        appendTo: info.view.$messageContainer[0],
      });
    },
  }, "info");

});
