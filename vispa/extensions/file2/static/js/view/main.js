define([
  "jquery",
  "vue/vue",
  "vispa/views/main",
  "./base",
  "../menu",
  "../vue/browser",
], function (
  $,
  Vue,
  MainView,
  BaseMixin,
  menu,
  BrowserVue
) {

  var MainVue = BrowserVue.extend({
  });

  var FileMainView = MainView._extend($.extend({}, BaseMixin, {
    init: function init(args) {
      init._super.apply(this, arguments);
      this.autoPathLabel = true;
      BaseMixin.init.call(this, args);
    },

    getFragment: function() {
      return this.state.get("path");
    },

    applyFragment: function(fragment) {
      this.state.set("path", fragment);
    },

    render: function() {
      if (this.vue.content) return;
      this.vue.content = new MainVue({
        el: this.$content[0],
        view: this,
        data: {
          dims: {
            width: this.$content.width(),
            height: this.$content.height(),
          },
          path: this.state.get("path"),
        },
      });
    },

    onResize: function(dims) {
      if (this.vue.content)
        Vue.util.extend(this.vue.content.dims, dims);
    },

    destroy: function destroy() {
      this.vue.content.$destroy();
      destroy._super.call(this);
    },
  }),{
    iconClass: "fa-folder-o",
    label    : "File Manager",
    name     : "FileMain",
    menuPosition: 2,
    menu: menu,

    prefrencesGroup: {
      descriptor: true,
      get: function() {
        return this.extension.name;
      }
    },
  });

  return FileMainView;
});
