define([
  "jquery",
  "vue/vue",
  "vispa/views/dialog",
  "./base",
  "../menu",
  "../vue/browser",
  "text!../../html/view/selectorBottom.html"
], function (
  $,
  Vue,
  DialogView,
  BaseMixin,
  menu,
  BrowserVue,
  tSelectorBottom
) {

  var ContentVue = BrowserVue.extend({
    methods: {
      finish: function(info) {
        var path = this.path;
        var value;
        if (this.fsel.multimode) {
          var sel = this.selectedItems;
          if (sel.length)
            value = sel.map(function (it) {
              return path + "/" + it.name;
            });
        } else {
          if (this.fsel.foldermode)
            value = path;
          else if (this.fsel.savemode) {
            if (info && info.newName)
              value = path + "/" + info.newName;
          } else {
            if (info && info.data && info.data.name)
              value = path + "/" + info.data.name;
          }
        }
        if (!value) return; // TODO: add "pls select stuff" warning
        if (this.fsel.callback)
          this.fsel.callback(value, this.fsel.cb_args);
        this.view.close();
      },
      open: function (info) {
        if (this.fsel) {
          if (this.fsel.multimode) {
            Vue.set(info.data, "selected", !info.data.selected);
          } else {
            if (info && info.data && info.data.type == "d")
              this.$dispatch("action", "open", info);
            else
              this.finish(info);
          }
        } else
          this.$emit("action", "open", info);
      }
    },
    computed: {
      foldersOnly: function () {
        return !!this.fsel.foldermode;
      },
    },
  });

  var FileSelectorView = DialogView._extend($.extend({}, BaseMixin, {
    init: function init(args) {
      this._args = args;
      init._super.apply(this, arguments);
      if (args.label === undefined)
        this.label = args.label;
      else
        this.autoPathLabel = true;
      BaseMixin.init.call(this, args);
    },

    render: function() {
      if (this.vue.content) return;
      this.vue.content = new ContentVue({
        el: this.$content[0],
        view: this,
        data: {
          fsel: $.extend({}, this._args),
          dims: {
            width: this.$content.width(),
            height: this.$content.height(),
          },
        },
      });
      this.state.emit("path");
      if (!this._args.foldermode && !this._args.multimode)
        Vue.delete(this.vue.node.buttons, "select");
    },

    dialogExtraOpts: {
      descriptor: true,
      get: function() {
        var buttons = {
          close: {
            pos: 1,
            label: "Close",
            callback: function() {
              this.view.close();
            },
          },
        };
        var sm = this._args.savemode;
        if (this._args.foldermode || this._args.multimode)
          buttons.select = {
            pos: 2,
            label: "Select",
            class: "btn-primary",
            callback: function() {
              this.browser.finish();
            },
          };
        else if (sm)
          buttons.save = {
            pos: 2,
            label: (typeof sm === "string") ? sm : "Save",
            class: "btn-primary",
            callback: function() {
              this.save();
              return true;
            },
          };
        return {
          data: {
            filename: "",
            iconClass: this._class._members.iconClass,
            containerClass: "fileselector2",
            buttons: buttons,
          },
          computed: {
            view: function () {
              return this.$root.instance;
            },
            browser: function () {
              return this.view.vue.content;
            },
          },
          methods: {
            open: function() {
              if (this.browser.fsel.savemode) {
                this.save();
              } else {
                var name = this.filename;
                var br = this.browser;
                var info = br.items.filter(function (it) {
                  return it.name == name;
                })[0];
                if (!info) return;
                br.open({data: info});
                // TODO: select everything in input
              }
            },
            save: function() {
              if (this.filename)
                this.browser.finish({
                  newName: this.filename,
                });
              // TODO: error message when no filename given
            },
          },
          partials: {
            footerLeft: tSelectorBottom.trim(),
          },
        };
      },
    },

    onResize: function(dims) {
      if (this.vue.content)
        Vue.util.extend(this.vue.content.dims, dims);
    },

    destroy: function destroy() {
      this.vue.content.$destroy();
      destroy._super.call(this);
    },
  }),{
    iconClass: "fa-folder-o",
    label    : "File Selector",
    name     : "FileSelector",
    menu     : menu,
    prefrencesGroup: {
      descriptor: true,
      get: function() {
        return this.extension.name;
      }
    },
  });

  return FileSelectorView;
});
