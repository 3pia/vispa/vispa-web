define([
  "vue/vue",
], function(
  Vue
) {
  var BaseMixin = {
    init: function(args) {
      this.state.setup({
        path: this.prefs.get("homePath"),
      }, args);

      this.state.on("path", function(path) {
        if (!this.vue) return;
        if (!this.vue.content) return;
        if (this.autoPathLabel)
          this.setLabel(path, true);
        this.reload();
      }.bind(this));

      this.socket.on("watch", function(data) {
        if (data.event == "modify" && data.subject_infos) {
          this.vue.content.$emit("patch", data.subject_infos);
        } else
          this.reload();
      }.bind(this));

      if (this.autoPathLabel)
        this.setLabel(this.state.get("path"), true);

      Vue.set(this.menu.items.displayType, "value", this.prefs.get("displayType"));
      var so = this.prefs.get("sortOrder");
      if (so && so.length) {
        so = so[0]
        if (so.slice(0, 1) == "-")
          so = so.slice(1);
        Vue.set(this.menu.items.sortOrder, "value", so);
      }

      this.prefs.forceWorkspace("bookmarks");
    },

    navigate: function(path) {
      this.vue.content.$emit("nav", path);
    },

    reload: function() {
      this.navigate(this.state.get("path"));
    },

    goHome: function() {
      this.navigate(this.prefs.get("homePath"));
    },

    goUp: function() {
      var path = this.state.get("path");
      var parent = path.split("/").slice(0, -1).join("/") || "/";
      this.navigate(parent);
    },

    upload: function(files, path) {
      this.spawnInstance("file2", "File2Upload", {
        path: path || this.state.get("path"),
        workspaceId: this.workspace.id,
        files: files,
      });
    },

    onClose: function() {
      this.POST("/ajax/fs/unwatch", {
        watch_id: "0",
      });
    },

    action: function (what) {
      this.vue.content.$emit("action", what, {});
    }
  };

  return BaseMixin;
});
