define([
  "jquery",
  "vue/vue",
  "vispa/utils",
  "vispa/views/side",
  "vispa/workspace",
  "text!../../html/view/upload.html",
  "text!../../html/vue/uploaditem.html",
], function (
  $,
  Vue,
  Utils,
  SidebarView,
  Workspace,
  tUpload,
  tUploadItem
) {

  var UploadVue = Vue.extend({
    template: tUpload,
    events: {
      remove: function(job) {
        var idx = this.jobs.indexOf(job);
        if (~idx)
          this.jobs.splice(idx, 1);
      },
    },
    components: {
      item: {
        template: tUploadItem,
        props: {
          job: Object,
        },
        data: function() {
          return {
            loaded: 0,
            total: 0,
            status: "idle",
            error: null,
          };
        },
        computed: {
          title: function() {
            return Utils.format(
              "Workspace: {0}\nDestination: {1}\nFiles:\n  {2}",
              Workspace._members.byId(this.job.workspaceId).name,
              this.job.path,
              this.names.join("\n  ")
            );
          },
          names: function() {
            return [].map.call(this.job.files || [], function(file){
              return file.name;
            });
          },
          count: function() {
            return this.names.length;
          },
          progress: function() {
            return 100 * this.loaded / (this.total || 1);
          },
          label: function() {
            return Utils.format(
              "{0}%&nbsp;({1}/{2})",
              this.progress.toFixed(0),
              Utils.formatSize(this.loaded),
              Utils.formatSize(this.total)
            );
          },
          progressClass: function() {
            return ["progress-bar", "progress-bar-" + ({
              done: "success",
              error: "danger",
              abort: "warning",
            }[this.status] || "info")];
          },
          finished: function() {
            return !!~["done", "abort", "error"].indexOf(this.status);
          },
        },
        ready: function() {
          var self = this;
          var view = this.$root.$options.view;
          var $fu = $(this.$els.container);
          window.QQ = this;
          $fu.fileupload({
            dropZone: null,
            singleFileUploads: false,
            maxChunkSize: vispa.args.global.max_request_body_size - 500,
            url: view.dynamicURL(
              "/ajax/fs/upload?_workspaceId=" + this.job.workspaceId + "&path=" + this.job.path
            ),
            add: function (event, data) {
              self.$set("job.files", data.files);
              self._jqXHR = data.submit();
            },
            submit: function () {
              self.status = "submit";
            },
            start: function () {
              self.status = "start";
            },
            progress: function (event, data) {
              self.total = data.total;
              self.loaded = data.loaded;
              self.status = "progress";
            },
            done: function () {
              self.status = "done";
            },
            fail: function (event, data) {
              if (data.errorThrown === "abort")
                self.status = "abort";
              else {
                self.status = "error";
                var x = data.jqXHR;
                self.error = x.responseJSON ? x.responseJSON.message : x.responseText;
              }
            },
          });
          if (this.job.files)
            $fu.fileupload("add", {files: this.job.files});
          else
            this.$els.container.firstElementChild.click();
        },
        methods: {
          abort: function() {
            if (this._jqXHR) this._jqXHR.abort();
          },
          remove: function() {
            this.$dispatch("remove", this.job);
          },
        },
        watch: {
          finished: function(val) {
            if (!val) return;
            setTimeout(this.remove.bind(this), 10 * 1000);
          },
        },
      },
    },
  });

  var FileUploadView = SidebarView._extend({
    init: function init(args) {
      init._super.apply(this, arguments);
      this.jobs = [];
      this.processArgs(args);
      this.render();
    },

    processArgs: function(args) {
      if (!args) return;
      if (!Workspace._members.byId(args.workspaceId)) return;
      if (!args.path) return;
      this.jobs.unshift(args);
    },

    render: function() {
      if (!this.vue)
        this.vue = {};
      if (this.vue.content) return;
      this.$content.html("<div/>");
      this.vue.content = new UploadVue({
        el: this.$content.children()[0],
        view: this,
        data: {
          jobs: this.jobs,
        },
      });
    },

  }, {
    name: "File2Upload",
    iconClass: "fa-upload",
    position: 15,
    singleton: true,
  });

  return FileUploadView;
});
