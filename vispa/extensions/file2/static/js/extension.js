define([
  "vispa/extension",
  "vispa/filehandler2",
  "./preferences",
  "./handler",
  "./view/main",
  "./view/selector",
  "./view/upload",
  "css!../css/style",
], function(
  Extension,
  FileHandler2,
  preferences,
  handler,
  MainView,
  SelectorView,
  UploadView
) {

  var File2Extension = Extension._extend({
    init: function init() {
      init._super.call(this, "file2", "File Manager");

      vispa.preferences.addGroup(this.name, this.name, preferences);

      this.mainMenuAdd([
        this.addView(MainView),
      ]);
      this.addView(SelectorView);
      this.addView(UploadView);
    }
  });

  // register new file handler
  FileHandler2.addExt("open", {
    // intercept run if we can handle it "in-house" on a "plain" open
    run: function run(info) {
      if (info._f2br && info.viaBestOpen)
        info._f2br.$emit("nav", this.fullPath(info));
      else
        return run._super.call(this, info);
    },
  }, "fileManager", {
    label: "File Manager",
    iconClass: "fa-folder-open",
    position: 1,
  }, function(path) {
    return ["file2", "FileMain", {path: path}];
  }, false);

  return File2Extension;
});
