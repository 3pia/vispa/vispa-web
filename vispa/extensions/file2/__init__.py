# -*- coding: utf-8 -*-

from vispa.controller import AbstractController
from vispa.server import AbstractExtension

class File2Controller(AbstractController):
    pass

class File2Extension(AbstractExtension):

    def name(self):
        return 'file2'

    def dependencies(self):
        return []

    def setup(self):
        self.add_controller(File2Controller())
        self.add_workspace_directoy()
