define([
  "vispa/extension",
  "vispa/views/main",
  "require",
  "css!../css/styles",
  "css!xterm/src/xterm",
], function(Extension, MainView, require) {

  var TerminalExtension = Extension._extend({
    init: function init() {
      init._super.call(this, "terminal", "Terminal");

      this.mainMenuAdd([
        this.addView(TerminalView),
      ]);
    }
  });

  var xtermmods = [
    "xterm/src/xterm",
    "xterm/addons/fit/fit",
  ];

  var TerminalView = MainView._extend({

    init: function init() {
      init._super.apply(this, arguments);

      this.input_data = "";

      require(xtermmods);
    },

    // called when the instance is resized
    onResize: function(dimensions) {
      if (this.term === undefined) return;
      if (!this.isFocused()) return;

      var geo = this.term.proposeGeometry();

      this.term.resize(geo.cols, geo.rows);
      this.POST('resize', {
        tid: this.terminalid,
        w: geo.cols,
        h: geo.rows
      });
    },

    post_data: function() {
      var view = this;
      if (view.input_data.length > 0 && view.dataTimeout == null) {
        view.dataTimeout = setTimeout(function() {
          var data = view.input_data;
          view.input_data = "";
          view.POST('write', {
            tid: view.terminalid,
            input_data: data
          }, function() {
            view.dataTimeout = null;
            view.post_data();
          });
        }, this.prefs.get("key_timeout"));
      }
    },

    render: function(node) {
      var view = this;

      view.setLoading(true);

      view.terminal_element = node.get(0);
      // create/open a new terminal session, returns id
      view.GET("open", function(err, terminalid) {
        require(xtermmods, function(Terminal) {
          view.term = new Terminal({
            cols: 80,
            rows: 24,
            screenKeys: true,
            parent: view.terminal_element,
            scrollback: 0,
            cancelEvents: true,
            cursorBlink: true,
          });
          view.term.open(view.terminal_element);
          view.terminalid = terminalid;
          // if the terminal is not visible onResize will recures endlessly (idk how)
          if (view.isFocused()) {
            view.onResize();
          } else {
            view.once('focus', view.onResize.bind(view));
          }

          view.dataTimeout = null;
          view.term.on('data', function(data) {
            view.input_data += data;
            view.post_data();
          });

          view.socket.on("data", function(data) {
            if (view.term == null)
              return;
            view.term.write(data);
          });

          view.socket.on("close", function(data) {
            if (view.term == null)
              return;
            view.close();
          });

          view.setLoading(false);
        });
      });
    },

    onClose: function() {
      var view = this;
      view.GET("close", {
        tid: view.terminalid
      });
      if (view.term) {
      	view.term.destroy();
      	view.term = null;
      }
    },

    onFocus: function() {
      var view = this;
      if (view.term && view.term.focus) {
        view.term.focus();
      }
    },

    onBlur: function() {
      var view = this;
      if (view.term && view.term.blur)
        view.term.blur();
    }
  },{
    iconClass: "fa-terminal",
    label: "Terminal",
    name: "TerminalView",
    menuPosition: 50,
    preferences: {
      items: {
        key_timeout: {
          level: 3,
          label: "Key stroke buffer length",
          type: "integer",
          range: [0, 100, 10],
          value: 0,
          description: "The time to wait for further key strokes before sending to server, in" +
            " milliseconds. Lower values improve response times for the single strokes.",
        }
      }
    },
  });

  return TerminalExtension;
});
