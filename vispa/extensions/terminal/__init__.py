# -*- coding: utf-8 -*-

from vispa.controller import AbstractController
from vispa.server import AbstractExtension
import vispa
import types

import cherrypy
import logging
import uuid
import rpyc

logger = logging.getLogger(__name__)

class Terminal(object):

    BUFFER_SIZE = 1024**2

    def __init__(self):
        self.__channel = None
        self.__windowId = None
        self.__viewId = None

    def open(self, channel, windowId, viewId):
        self.__channel = channel
        self.__windowId = windowId
        self.__viewId = viewId
        self.__topic = "extension.%s.socket" % viewId

        # monkey path BufferedPipe to add callback
        def _BufferedPipe_feed(self, data):
            self.original_feed(data)
            if hasattr(self, "on_feed"):
                self.on_feed()

        channel.in_buffer.original_feed = channel.in_buffer.feed
        channel.in_buffer.feed = types.MethodType(
            _BufferedPipe_feed,
            channel.in_buffer)
        channel.in_buffer.on_feed = self._on_feed

        # monkey path BufferedPipe to add callback
        def _BufferedPipe_close(self):
            self.original_close()
            if hasattr(self, "on_close"):
                self.on_close()

        channel.in_buffer.original_close = channel.in_buffer.close
        channel.in_buffer.close = types.MethodType(
            _BufferedPipe_close,
            channel.in_buffer)
        channel.in_buffer.on_close = self._on_close

    def resize(self, w, h):
        self.__channel.resize_pty(w, h)

    def write(self, data):
        self.__channel.sendall(data)

    def _on_feed(self):
        while self.__channel.recv_ready():
            vispa.bus.send_topic(self.__topic+".data", window_id=self.__windowId, data=self.__channel.recv(self.BUFFER_SIZE))

    def _on_close(self):
        vispa.bus.send_topic(self.__topic+".close", window_id=self.__windowId)

    def close(self):
        self.__channel.close()


class TerminalController(AbstractController):

    def __init__(self):
        AbstractController.__init__(self)
        self.__terminals = {}

    def _terminal(self, tid):

        if tid in self.__terminals:
            return self.__terminals[tid]
        else:
            t = Terminal()
            self.__terminals[tid] = t
            return t

    @cherrypy.expose
    def open(self):
        self.release_session()
        windowId = cherrypy.request.private_params.get("_windowId", None)
        viewId = cherrypy.request.private_params.get("_viewId", None)
        # TODO: add userid to tid
        tid = windowId + "-" + viewId
        terminal = self._terminal(tid)

        connection = vispa.workspace.connect()
        channel = connection._connection.client().invoke_shell('xterm')
        terminal.open(channel, windowId, viewId)
        return tid

    @cherrypy.expose
    def close(self, tid):
        self.release_session()
        terminal = self._terminal(tid)
        terminal.close()

    @cherrypy.expose
    def resize(self, tid, w, h):
        self.release_session()
        terminal = self._terminal(tid)
        self.release_database()
        terminal.resize(int(w), int(h))

    @cherrypy.expose
    def write(self, tid, input_data):
        self.release_session()
        terminal = self._terminal(tid)
        self.release_database()
        terminal.write(input_data)


class TerminalExtension(AbstractExtension):

    def name(self):
        return 'terminal'

    def dependencies(self):
        return []

    def setup(self):
        self.add_controller(TerminalController())
        self.add_workspace_directoy()
