# -*- coding: utf-8 -*-

from logging import config as loggingcfg
from pkgutil import iter_modules
import importlib
import inspect
import logging
import os
import sys

from sqlalchemy.orm import scoped_session, sessionmaker
from vispa.models import Base as sql_base
from vispa.models.group import *
from vispa.models.jsondata import *
from vispa.models.project import *
from vispa.models.role import *
from vispa.models.user import *
from vispa.models.workgroup import *
from vispa.models.workspace import *
import cherrypy
import sqlalchemy
import vispa.extensions
import vispa.plugins.template
import vispa.url

import vispa.models.alembic
from urlparse import urlparse
logger = logging.getLogger(__name__)


class AbstractExtension(object):

    """ Base class for Extensions """

    def __init__(self, server):
        self.server = server
        self._config = vispa.VispaConfigParser()
        class_dir = os.path.dirname(inspect.getabsfile(self.__class__))
        if os.path.exists(os.path.join(class_dir, self.name() + ".ini")):
            self._config.read(os.path.join(class_dir, self.name() + ".ini"))
            if vispa.config("usermanagement", "autosetup", False) and \
               vispa.config("usermanagement", "accept_extensions", True):
                # auto setup of user management
                session = scoped_session(sessionmaker(autoflush=True, autocommit=False))
                session.configure(bind=self.server._engine)
                try:
                    # default roles
                    role_1 = vispa.config("usermanagement", "role_1", None)
                    role_1 = Role.get(session, role_1)
                    role_2 = vispa.config("usermanagement", "role_2", None)
                    role_2 = Role.get(session, role_2)
                    role_3 = vispa.config("usermanagement", "role_3", None)
                    role_3 = Role.get(session, role_3)
                    # default permissions
                    permissions = self._config("usermanagement", "permissions", [])
                    permissions = [Permission.get_or_create_by_name(session, x) for x in permissions]
                    # give roles pemissions
                    role_1_permissions = self._config("usermanagement", "role_1_permissions", [])
                    role_1_permissions = [Permission.get(session, x) for x in role_1_permissions]
                    role_1.add_permissions(role_1_permissions)
                    role_2_permissions = self._config("usermanagement", "role_2_permissions", [])
                    role_2_permissions = [Permission.get(session, x) for x in role_2_permissions]
                    role_2.add_permissions(role_2_permissions)
                    role_3_permissions = self._config("usermanagement", "role_3_permissions", [])
                    role_3_permissions = [Permission.get(session, x) for x in role_3_permissions]
                    role_3.add_permissions(role_3_permissions)
                    session.commit()
                except Exception as e:
                    logger.error("Setting default permissions of extension %s failed: %s" %(self.name(), str(e)))
                    session.rollback()
                finally:
                    session.remove()


    def name(self):
        """
        Return the name of the Extension.
        This name is used as part of the URL.
        """
        raise NotImplementedError

    def dependencies(self):
        """
        Return a list of Extension names this Extension depends on.
        """
        raise NotImplementedError

    def setup(self):
        """
        Setup the extension.
        """
        raise NotImplementedError

    def config(self):
        return {}

    def add_controller(self, controller):
        """
        Mount a CherryPy controller using the extension name for path.

        :param controller: filename relative to extension directory
        """
        controller.extension = self
        controller._cp_config['tools.permission.permissions'].append(self.name() + '.allowed')
        controller._cp_config['tools.permission.ignoreInexistent'] = True
        self.server.controller.mount_extension_controller(self.name(),
                                                          controller)

    def add_workspace_directoy(self, directory="workspace"):
        """
        Add files to be transferred to the worker.

        :param directoy: directory relative to extension directory
        """
        class_dir = os.path.dirname(inspect.getabsfile(self.__class__))
        local = os.path.join(class_dir, directory)
        remote = os.path.join('vispa', 'extensions', self.name(), directory)
        vispa.workspace.add_directory_files(local, remote)
        self.__workspace_directory = directory

    def get_workspace_instance(self, name, key=None, user=None,
                               workspace=None, db=None, **kwargs):
        classname = ".".join(["vispa.extensions", self.name(),
                              self.__workspace_directory, name])
        return vispa.workspace.get_instance(classname, key, user,
                                            workspace, db, init_args=kwargs)

    def clear_workspace_instance(self, name, key=None, user=None,
                                 workspace=None, db=None):
        classname = ".".join(["vispa.extensions", self.name(),
                              self.__workspace_directory, name])
        return vispa.workspace.clear_instance(classname, key, user,
                                              workspace, db)

    def create_topic(self, topic="", view_id=None):
        if view_id is None:
            view_id = cherrypy.request.private_params["_viewId"]
        return "extension.%s.socket.%s" % (view_id, topic)

class Server(object):

    __default_server_config = {
        'server.socket_host': '127.0.0.1',
        'server.socket_port': 4282,
        'server.socket_queue_size': 10,
        'server.request_queue_size': 10,
        'server.thread_pool': 10,
        'engine.autoreload.on': False,
    }
    if hasattr(cherrypy, "TimeoutError"):
        __default_server_config['engine.timeout_monitor.on'] = False

    @property
    def __default_mount_config(self):
        base_dynamic = vispa.url.dynamic('/')
        base_static = vispa.url.static('/', timestamp=False)
        return {
            '/': {
                'tools.proxy.on': True,
                'tools.encode.on': False,
                'tools.db.on': True,
                'tools.private_parameters.on': True,
                'tools.user.on': True,
                'tools.user.redirect_url': vispa.url.dynamic('/login'),
                'tools.user.redirect_url_reverse': vispa.url.dynamic('/'),
                'tools.workspace.on': False,
                'tools.sessions.on': True,
                'tools.sessions.path': urlparse(base_dynamic).path,
                'tools.sessions.storage_class': cherrypy.lib.sessions.FileSession,
                'tools.sessions.storage_path': vispa.datapath('sessions'),
                'tools.sessions.timeout': 180,
                'tools.staticdir.on': False,
                'tools.gzip.on': True,
                'tools.gzip.mime_types': ['text/html', 'text/css',
                                          'application/x-javascript',
                                          'application/json'],
                'tools.render.common_data': {
                    'base_dynamic': base_dynamic,
                    'base_static': base_static,
                    'dev_mode': vispa.config("web", "dev_mode", True)
                },
                "tools.status.on": True,
            },
            '/extensions': {
                'tools.workspace.on': True,
                'tools.ajax.on': True
            },
            '/fs': {
                'tools.workspace.on': True
            },
            '/ajax': {
                'tools.workspace.on': True,
                'tools.ajax.on': True
            },
            '/error': {
                'tools.db.on': False,
                'tools.private_parameters.on': False,
                'tools.user.on': False,
                'tools.workspace.on': False
            }
        }

    def __init__(self, **kwargs):
        self.__init_paths(**kwargs)
        self.__init_database(**kwargs)
        self.__init_tools(**kwargs)
        self.__init_platform(**kwargs)
        self.__init_plugins(**kwargs)

    def __init_paths(self, **kwargs):
        # dir for variable files and folders
        self.var_dir = os.path.abspath(kwargs.get('vardir', ''))
        if not os.path.exists(self.var_dir):
            os.makedirs(self.var_dir)

        logger.info('Using %s as data dir.' % self.var_dir)
        vispa.set_datapath(self.var_dir)

        # log dir
        self.log_dir = os.path.join(self.var_dir, 'logs')
        if not os.path.exists(self.log_dir):
            os.mkdir(self.log_dir)

        # session dir
        self.session_dir = os.path.join(self.var_dir, 'sessions')
        if not os.path.exists(self.session_dir):
            os.mkdir(self.session_dir)

        # dir for workspace files
        self.workspace_dir = os.path.join(self.var_dir, 'workspace')
        if not os.path.exists(self.workspace_dir):
            os.mkdir(self.workspace_dir)

        # cache dir
        self.cache_dir = os.path.join(self.var_dir, 'cache')
        if not os.path.exists(self.cache_dir):
            os.mkdir(self.cache_dir)

        # conf dir
        self.conf_dir = os.path.abspath(kwargs.get('configdir', ''))
        vispa.setup_config(self.conf_dir)

    def __init_database(self, **kwargs):

        self._engine = vispa.models.open_database()

        if vispa.config('alembic', 'use_alembic', False):
            logger.info("Use alembic")
            if vispa.config('alembic', 'auto_migrate', True):
                vispa.models.alembic.migrate(self._engine)
        else:
            logger.info("Do not use alembic")
            sql_base.metadata.create_all(self._engine)

        # auto setup of user management
        session = scoped_session(sessionmaker(autoflush=True, autocommit=False))
        session.configure(bind=self._engine)
        try:
            if vispa.config("usermanagement", "autosetup", False):
                logger.info("Do autosetup of user management")
                if vispa.config("usermanagement", "accept_extensions", True):
                    logger.info("Default permissions of extensions are accepted")
                else:
                    logger.info("Default permissions of extensions are not accepted")

                # global project
                global_project = vispa.config("usermanagement", "global_project", None)
                global_project = Project.get_or_create_by_name(session, global_project)
                # user and guest group
                user_group = vispa.config("usermanagement", "user_group", None)
                user_group = Group.get_or_create_by_name(session, user_group, Group.PUBLIC)
                guest_group = vispa.config("usermanagement", "guest_group", None)
                guest_group = Group.get_or_create_by_name(session, guest_group, Group.PUBLIC)
                # put groups in global project
                if not global_project.has_group(user_group):
                    global_project.add_group(session, user_group)
                if not global_project.has_group(guest_group):
                    global_project.add_group(session, guest_group)
                # default roles
                role_1 = vispa.config("usermanagement", "role_1", None)
                role_1 = Role.get_or_create_by_name(session, role_1)
                role_2 = vispa.config("usermanagement", "role_2", None)
                role_2 = Role.get_or_create_by_name(session, role_2)
                role_3 = vispa.config("usermanagement", "role_3", None)
                role_3 = Role.get_or_create_by_name(session, role_3)
                # give groups roles in global project
                user_group_roles = [int(x) for x in vispa.config("usermanagement", "user_group_roles", [])]
                if 1 in user_group_roles: global_project.add_roles_to_group(user_group, [role_1])
                if 2 in user_group_roles: global_project.add_roles_to_group(user_group, [role_2])
                if 3 in user_group_roles: global_project.add_roles_to_group(user_group, [role_3])
                guest_group_roles = [int(x) for x in vispa.config("usermanagement", "guest_group_roles", [])]
                if 1 in guest_group_roles: global_project.add_roles_to_group(guest_group, [role_1])
                if 2 in guest_group_roles: global_project.add_roles_to_group(guest_group, [role_2])
                if 3 in guest_group_roles: global_project.add_roles_to_group(guest_group, [role_3])
                # close session
                session.commit()
            else:
                logger.info("Do not autosetup of user management")
        except Exception as e:
            logger.error("Autosetup of usermanagement failed: " + str(e))
            session.rollback()
        finally:
            session.remove()

    def __init_plugins(self, **kwargs):
        logger.info("init plugins")
        if vispa.config('websockets', 'enabled', False):
            from ws4py.server.cherrypyserver import WebSocketPlugin
            WebSocketPlugin(cherrypy.engine).subscribe()

        logger.info("create bus")
        from vispa.socketbus import Bus
        vispa.bus = Bus()

        logger.info("setup templates")
        mako_lookup_dir = os.path.join(os.path.dirname(__file__), "templates")
        vispa.plugins.template.MakoPlugin(cherrypy.engine,
                                          base_dir=mako_lookup_dir,
                                          module_dir=self.cache_dir
                                          ).subscribe()

    def __init_tools(self, **kwargs):
        from vispa.tools.template import MakoTool
        cherrypy.tools.render = MakoTool()

        from vispa.tools.db import SqlAlchemyTool
        cherrypy.tools.db = SqlAlchemyTool(self._engine)

        from vispa.tools.user import UserTool
        cherrypy.tools.user = UserTool()

        from vispa.tools.workspace import WorkspaceTool
        cherrypy.tools.workspace = WorkspaceTool()

        from vispa.tools.parameters import PrivateParameterFilter
        cherrypy.tools.private_parameters = PrivateParameterFilter()

        from vispa.tools.device import DeviceTool
        cherrypy.tools.device = DeviceTool()

        from vispa.tools.ajax import AjaxTool
        cherrypy.tools.ajax = AjaxTool()

        from vispa.tools.method import MethodTool
        cherrypy.tools.method = MethodTool()

        from vispa.tools.json_parameters import JsonParameters
        cherrypy.tools.json_parameters = JsonParameters()

        from vispa.tools.status import StatusMonitor
        cherrypy.tools.status = StatusMonitor()

        from vispa.tools.permission import PermissionTool
        cherrypy.tools.permission = PermissionTool()

        if vispa.config('websockets', 'enabled', False):
            from ws4py.server.cherrypyserver import WebSocketTool
            cherrypy.tools.websocket = WebSocketTool()

    def __init_platform(self, **kwargs):
        cherrypy.config.update(self.__default_server_config)

        cherrypy_conf = vispa.configpath('cherrypy.ini')
        if os.path.isfile(cherrypy_conf):
            logger.info('Merge cherrpy config file: %s' % cherrypy_conf)
            cherrypy.config.update(cherrypy_conf)

        config_files = vispa.config_files("cherrypy.d")
        logger.info('Merge cherrypy config files: %s' % config_files)
        for f in config_files:
            cherrypy.config.update(f)

        port = kwargs.get('port', None)
        if port:
            cherrypy.config.update({'server.socket_port': int(port)})

        logger.info("create root controller")
        from vispa.controller.root import RootController
        self.controller = RootController(self)
        logger.info("load extensions")
        self._load_extensions()
        script_name = vispa.url.dynamic('/', encoding='utf-8')

        logger.info("Mount app")
        app_config = self.__default_mount_config

        # merge extension config into app config
        for extension in self._extensions.values():
            # config is structured for multiple mountpoints
            for mount, conf in extension.config().items():
                key = "/extensions/%s%s" % (extension.name(), "" if mount == "/" else mount)
                app_config[key] = conf

        self.__application = cherrypy.tree.mount(self.controller, script_name, app_config)

        if os.path.isfile(cherrypy_conf):
            logger.info('Merge app config file: %s' % cherrypy_conf)
            self.__application.merge(cherrypy_conf)
        logger.info('Merge app config files: %s' % config_files)
        for f in config_files:
            self.__application.merge(f)


    def _on_exit(self):
        vispa.bus.send_topic("exit", broadcast=True)
        vispa.publish("exit")

    def _on_stop(self):
        vispa.bus.send_topic("stop", broadcast=True)
        vispa.publish("stop")

    def start(self):
        if hasattr(cherrypy.engine, 'signal_handler'):
            cherrypy.engine.signal_handler.subscribe()
        cherrypy.engine.subscribe("exit", self._on_exit, 0)
        cherrypy.engine.subscribe("stop", self._on_stop, 0)
        cherrypy.engine.start()
        vispa.setup_thread_dump()

    def stop(self):
        cherrypy.engine.exit()

    def run(self):
        self.start()
        cherrypy.engine.block()

    def start_url(self):
        return urlparse("%s:%d%s" % (
            cherrypy.config.get("server.socket_host"),
            cherrypy.config.get("server.socket_port"),
            vispa.url.dynamic('/')
        ), "http", False).geturl()

    def _load_extensions(self):
        self._extensions = {}
        # loop through all extensions and import their files
        # so that 'AbstractExtension' will know about its subclasses
        modulenames = []
        ignored = vispa.config('extensions', 'ignore', [])
        extensionspath = vispa.datapath('extensions')
        vispa.extensions.__path__.append(extensionspath)
        for _, modulename, ispkg in iter_modules(vispa.extensions.__path__):
            if not ispkg:
                continue
            if modulename in ignored:
                logger.info('Ignore extension: %s ' % modulename)
                continue
            try:
                importlib.import_module('vispa.extensions.%s' % modulename)
                modulenames.append(modulename)
            except:
                _, message, _ = sys.exc_info()
                logger.warning('Exception importing extension %s: %s' %
                               (modulename, message))
                vispa.log_exception()

        for _, modulename, ispkg in iter_modules():
            if not ispkg:
                continue
            if not modulename.startswith('vispa_'):
                logger.debug('Not a vispa extension: %s ' % modulename)
                continue
            if modulename in ignored:
                logger.info('Ignore extension: %s ' % modulename)
                continue
            try:
                importlib.import_module(modulename)
                modulenames.append(modulename)
            except:
                _, message, _ = sys.exc_info()
                logger.warning('Exception importing extension %s: %s'
                               % (modulename, message))
                vispa.log_exception()

        load = [x.strip() for x in
                vispa.config('extensions', 'import', '').split(',')]
        load = filter(lambda x: len(x) > 0, load)
        for ext in load:
            logger.info('Import extension: %s ' % ext)
            try:
                importlib.import_module(ext)
                modulenames.append(ext.split('.')[-1])
            except:
                _, message, _ = sys.exc_info()
                logger.warning('Exception importing extension %s: %s'
                               % (modulename, message))
                vispa.log_exception()

        # instantiate all subclasses of the 'AbstractExtension'
        dependencies = {}
        for cls in AbstractExtension.__subclasses__():
            # check: is the modulename accepted?
            modulename = cls.__module__.split('.')[-1]
            if modulename in ignored:
                continue

            logger.debug('Loading Extension "%s"' % cls)
            extension = cls(self)
            name = extension.name()
            if name in self._extensions.keys():
                raise Exception("Fail to load extension: '%s'."
                                "It already exsits!" % name)
            self._extensions[name] = extension
            # update the 'dependencies' dict
            for dep in extension.dependencies():
                if dep not in dependencies.keys():
                    dependencies[dep] = [name]
                else:
                    dependencies[dep].append(name)

        # check dependencies
        for dep in dependencies.keys():
            if dep not in self._extensions:
                data = dep, ", ".join(dependencies[dep])
                msg = """"The following dependency could not be found: '%s'.
                The following extensions depend on it: %s""" % data
                raise Exception(msg)

        # setup all valid extenions
        for extension in self._extensions.values():
            try:
                extension.setup()
            except:
                logger.warn(
                    "Extension '%s' failed to setup!" %
                    extension.name())
                vispa.log_exception()

    def extension(self, name):
        return self._extensions[name]

    def extensions(self):
        return self._extensions.values()

    def application(self):
        return self.__application
