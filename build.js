{
    appDir: 'vispa',
    mainConfigFile: 'vispa/static/js/config.js',
    dir: 'build',
    baseUrl: 'static',
    wrapShim: true,
    preserveLicenseComments: false,
    useStrict: true,
    wrap: false,
    paths: {
        'extensions/pscan': '/home/gmueller/Projects/pscan/pscan'
    },
    modules: [
        {
            name: 'vispa/config',
            include: ['vispa/common/url', 'vispa/common/dialog', 'emitter', 'jclass']
        },
        {
            name: 'vispa/password',
            exclude: ['vispa/config']
        },
        {
            name: 'vispa/login',
            exclude: ['vispa/config']
        },
        {
            name: 'vispa/vispa',
            include: ['vispa/views/dialog'],
            exclude: ['vispa/config']
        },
        {
            name: 'extensions/file/static/js/extension',
            exclude: ['vispa/config', 'vispa/vispa']
        },
        {
            name: 'extensions/codeeditor/static/js/extension',
            exclude: ['vispa/config', 'vispa/vispa', 'ace/ace', 'ace/ext-language_tools']
        },
        {
            name: 'extensions/terminal/static/js/extension',
            exclude: ['vispa/config', 'vispa/vispa']
        }
    ]
}